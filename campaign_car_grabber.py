# -*- coding: utf-8 -*-

import csv
import gzip
import http.cookiejar
import json
import logging
import os
import random
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock, Semaphore
from multiprocessing import Pool
from multiprocessing.pool import ThreadPool

from bs4 import BeautifulSoup


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class CampingCarSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Accept-Language': 'en-US,en;q=0.9',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
    }

    __logger = None
    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 5
    MAX_THREAD = 32
    __lock = Semaphore(1)

    def __init__(self, input_csv, output_csv):
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('image_downloader.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)
        return logger

    def __enter__(self):
        # self.__lock = Semaphore(1)
        self.__logger = self._setup_logger()
        socket.setdefaulttimeout(30)

        hdr = [('url', 'URL'),
               ('name', 'Name'),
               ('address', 'Address'),
               ('post_code', 'Postal Code'),
               ('camp', 'Ouverture et réservation'),
               ('email1', 'Email1'),
               ('email2', 'Email2'),
               ('phone', 'Phone1'),
               ('lat', 'Latitude'),
               ('long', 'Longitude'),
               ('website', 'Website'),
               ('star', 'Star')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.__proxies = json_data

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    if key not in opener.addheaders:
                        opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                cj = http.cookiejar.CookieJar()
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:
            urls = []
            if os.path.exists(self.__input_csv):
                with open(self.__input_csv, 'r+', encoding='utf-8') as f:
                    reader = csv.reader(f)
                    for index, row in enumerate(reader):
                        if 'http' not in row[0]:
                            continue

                        if row[0] in self.__url_cache:
                            continue
                            
                        urls.append(row)

            with Pool(32) as pool:
                pool.map(self.__grab_details, urls)

        except Exception as x:
            self.__logger.error('Error when grab data. Error details: {}'.format(x))

    def __grab_details(self, row, retry=0):
        """
        It'll take title and website as args and search keywords
        :param url:
        :param retry:
        :return:
        """
        try:
            url = row[0]
            self.__logger.info('Details URL: {}'.format(url))
            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()
            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            # After creating soup object simply dispose data to free memory
            del data

            # Init item
            item = {'url': url,
                    'name': row[1].strip(),
                    'address': row[2].strip(),
                    'post_code': row[3].strip(),
                    'camp': row[4].strip(),
                    'email1': row[5].strip(),
                    'email2': row[6].strip(),
                    'phone': row[7].strip(),
                    'lat': row[8].strip(),
                    'long': row[9].strip(),
                    'website': row[10].strip()}

            # star
            star_span = soup.find('span', class_='stars')
            if star_span:
                star_svgs = star_span.find_all('svg', {'height': '12', 'width': '12'})
                item['star'] = len(star_svgs)

            # Dispose soup object
            del soup

            # Finally write item
            self.__write_item(item)
        except Exception as x:
            self.__logger.error('Error when grab details for URL: [{}]. {}'.format(row[0], x))

            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < 5:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(row, retry + 1)

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    input_file = 'car_input.csv'
    output_file = 'car_output_updated.csv'
    with CampingCarSpider(input_file, output_file) as spider:
        spider.grab_data()
