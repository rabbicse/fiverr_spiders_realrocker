import csv
import json
import logging
import os
import re
from collections import OrderedDict
from multiprocessing import Semaphore, Value
from multiprocessing.pool import ThreadPool

from bs4 import BeautifulSoup
from easy_spider import Spider

logger = logging.getLogger(__name__)


class AnnuaireCrawler(Spider):
    __start_url = 'http://annuaire.maisons-de-retraite.fr/resultat_annuaire.php?item=ehpad&loc={}'
    __base_url = 'http://annuaire.maisons-de-retraite.fr'
    __api_url = 'https://t.justdial.com/api/india_api_write/01jan2018/searchziva.php?'
    __url_cache = []
    __total = Value('i', 0)
    __lock = Semaphore()

    def __init__(self, output_csv):
        Spider.__init__(self, log_file='annuarie.log')
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __enter__(self):
        hdr = [('url', 'Url'),
               ('name', 'Name'),
               ('date', 'Dernière mise à jour'),
               ('standard', 'Standard'),
               ('fax', 'Fax'),
               ('email', 'Email'),
               ('address', 'Address'),
               ('post_code_city', 'Postal Code and City'),
               ('capacity', 'Capacité'),
               ('website', 'Website'),
               ('price', 'Prise en charge spécifique Alzheimer')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        # if os.path.exists('proxy.json'):
        #     with open('proxy.json', 'r+', encoding='utf-8') as f:
        #         json_data = json.load(f)
        #         self.proxies = json_data

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Total records: {}'.format(self.__total.value))
        logger.info('Finish!!!')
        del self

    def process_data(self):
        try:
            urls = []
            for i in range(1, 96):
                urls.append(self.__start_url.format('%02d' % i))

            with ThreadPool(4) as pool:
                pool.map(self.do_process, urls)
        except Exception as x:
            logger.error('Error when process data: {}'.format(x))

    def do_process(self, url):
        try:
            next_page = self.__grab_pages(url)
            while next_page:
                next_page = self.__grab_pages(next_page)

        except Exception as x:
            logger.error('Error when get pages: {}'.format(x))

    def __grab_pages(self, url):
        try:
            response = self.fetch_data(url)
            if not response:
                return

            data, redirected_url = response

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            urls = []
            div_list = soup.find_all('div', class_='res_mid_t')
            for div in div_list:
                a_tag = div.find('a')
                if a_tag:
                    details_url = a_tag.get('href')
                    if details_url not in self.__url_cache:
                        urls.append(self.__base_url + '/' + details_url)

            with ThreadPool(32) as pool:
                pool.map(self._grab_details, urls)

            next_p = soup.find('p', class_='pages')
            if next_p:
                next_a_list = next_p.find_all('a')
                if next_a_list and len(next_a_list) > 0 and 'Suiv. >>' in next_a_list[-1].text:
                    return self.__base_url + next_a_list[-1].get('href')
        except Exception as x:
            logger.error('Error when parse next page: {}'.format(x))

    def _grab_details(self, url):
        try:
            logger.info('Details page: {}'.format(url))
            response = self.fetch_data(url)
            if not response:
                return

            data, redirected_url = response

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            item = {'url': url}
            name_tag = soup.find('h1', class_='gray_box_title')
            if name_tag:
                item['name'] = name_tag.text.strip()

            date_tag = soup.find('span', class_='date')
            if date_tag:
                date_text = date_tag.text.strip()
                date_m = re.search(r'(\d+\.\d+\.\d+)', date_text)
                if date_m:
                    item['date'] = date_m.group(1)

            p_list = soup.find_all('p')
            for p in p_list:
                if 'Prise en charge spécifique Alzheimer :' in p.text:
                    price = p.text.replace('Prise en charge spécifique Alzheimer :', '')
                    item['price'] = price.strip()

            info = soup.find('p', class_='infor_method')
            if info:
                a_list = info.find_all('a')
                for a in a_list:
                    if a.has_attr('href') and 'mailto:' in a.get('href'):
                        item['email'] = a.get('href').replace('mailto:', '')
                        break

                i = 0
                info_contents = info.contents
                for content in info_contents:
                    if 'standard :' in content:
                        standard = str(content).replace('standard :', '').strip()
                        item['standard'] = standard

                    if 'fax général :' in content:
                        fax = str(content).replace('fax général :', '').strip()
                        item['fax'] = fax

                    if 'site internet :' in content and len(info_contents) > i + 1:
                        item['website'] = info_contents[i + 1].get('href')

                    i += 1

                item['address'] = str(info_contents[-3]).strip()
                item['post_code_city'] = info_contents[-1].strip()

            legends = soup.find_all('legend')
            for legend in legends:
                if 'Capacité :' in legend.text:
                    item['capacity'] = legend.text.replace('Capacité :', '')

            self.__write_item(item)
        except Exception as x:
            logger.error('Error when parse details page: {}'.format(x))

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                with self.__total.get_lock():
                    self.__total.value += 1
                    logger.info('Total: {}'.format(self.__total.value))
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    with AnnuaireCrawler('annuaire_output.csv') as crawler:
        crawler.process_data()
