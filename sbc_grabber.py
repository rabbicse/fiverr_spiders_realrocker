import csv
import gzip
import os
import platform
import re
import socket
import urllib.request
import urllib.parse

import time
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import logging
import readline

readline.parse_and_bind("control-v: paste")


class SbcGrabber:
    __start_url = 'http://www.sbc.net/'
    __base_url = 'http://www.sbc.net/church/{}/'
    __post_url = 'http://www.sbc.net/churchsearch/churches-near-me.asp'
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}

    def __init__(self, input_file, output_csv):
        self.__input = input_file
        self.__output_csv = output_csv
        self._city_list = []

    def __enter__(self):
        self.__setup_logger('sbc.log')
        socket.setdefaulttimeout(60)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

        if os.path.exists(self.__input):
            with open(self.__input, 'r', encoding='utf-8') as f:
                for line in f.readlines():
                    self._city_list.append(line.strip())

        csv_header = ['Church', 'Address', 'City', 'State', 'Phone', 'Email', 'Website', 'URL']
        self.__write_data(csv_header)

        return self

    def __build_browser(self):
        try:
            cap = webdriver.DesiredCapabilities.PHANTOMJS
            cap["phantomjs.page.settings.javascriptEnabled"] = True
            cap['phantomjs.page.settings.userAgent'] = self.__user_agent
            driver_path = './driver/phantomjs'
            if str(platform.system()).startswith('Windows'):
                driver_path = './driver/phantomjs.exe'

            browser = webdriver.PhantomJS(driver_path, desired_capabilities=cap,
                                          service_args=['--ignore-ssl-errors=true'])
            return browser
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def clean_data(self):
        try:
            with open(self.__output_csv, 'r', newline='', encoding='utf-8') as f:
                reader = csv.reader(f)
                for row in reader:
                    if any(word in str(row[0]).strip().lower() for word in
                           ['school', 'church', 'kindergarten', 'kindy']):
                        print('skipping...')
                        continue
                    self.__write_data(row)
        except Exception as x:
            print(x)

    def grab_data(self):
        try:
            for city in self._city_list:
                for results in self.__grab_crunches(city):
                    for result in results:
                        self.__grab_details(result)
        except Exception as x:
            self.__logger.error('Error grab all city data: {}'.format(x))

    def __grab_crunches(self, search_key):
        browser = None
        try:
            self.__logger.info('Initializing browser with URL: {}'.format(self.__start_url))
            browser = self.__build_browser()
            browser.get(self.__start_url)
            wait = WebDriverWait(browser, 60)
            wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

            self.__logger.info('Searching with city: {}'.format(search_key))
            elem = browser.find_element_by_id('city-input')
            elem.send_keys(search_key)
            elem.send_keys(Keys.RETURN)
            wait = WebDriverWait(browser, 60)
            wait.until(EC.presence_of_element_located((By.XPATH, '//div[@id="churchlist_wrapper"]')))

            el = browser.find_element_by_id('churchlist_length')
            for option in el.find_elements_by_tag_name('option'):
                if option.text == '100':
                    option.click()  # select() in earlier versions of webdriver
                    break

            yield self.__parse_results(browser)

            has_next = browser.find_element_by_id('churchlist_next')
            while has_next:
                class_name = has_next.get_attribute('class')
                if class_name == 'next paginate_button':
                    has_next.click()
                    yield self.__parse_results(browser)
                    has_next = browser.find_element_by_id('churchlist_next')
                else:
                    break
        except Exception as x:
            print(x)
        finally:
            try:
                if browser:
                    browser.quit()
                    browser = None
                    del browser
            except:
                pass

    def __parse_results(self, browser):
        try:
            wait = WebDriverWait(browser, 60)
            wait.until(EC.presence_of_element_located((By.XPATH, '//div[@id="churchlist_wrapper"]')))
            time.sleep(5)

            church_div = browser.find_element_by_xpath('//div[@id="churchlist_wrapper"]')
            if not church_div:
                return

            rows = church_div.find_elements_by_xpath('.//tr')
            if rows and len(rows) > 0:
                self.__logger.info('Total records found on this page: {}'.format(len(rows)))
                row_list = []
                for row in rows:
                    id = row.get_attribute('id')
                    columns = row.find_elements_by_xpath('.//td')
                    if columns and len(columns) > 0:
                        row_data = {
                            'id': id,
                            'church': columns[0].text.strip(),
                            'address': columns[1].text.strip(),
                            'city': columns[2].text.strip(),
                            'state': columns[3].text.strip()}
                        row_list.append(row_data)
                        # self.__logger.info('Row Data: {}'.format(row_data))
                return row_list
        except Exception as x:
            self.__logger.error('Error parsing: {}'.format(x))

    def __grab_details(self, row_data):
        try:
            url = self.__base_url.format(row_data['id'])
            self.__logger.info('=== Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            row_data['phone'] = ''
            row_data['email'] = ''
            row_data['website'] = ''

            email_div = soup.find('div', id='cs-email')
            if email_div:
                row_data['email'] = email_div.text.strip()

            phone_div = soup.find('div', id='cs-phone')
            if phone_div:
                row_data['phone'] = phone_div.text.strip()

            website_div = soup.find('div', id='cs-url')
            if website_div:
                row_data['website'] = website_div.text.strip()

            csv_data = [row_data['church'], row_data['address'], row_data['city'], row_data['state'], row_data['phone'],
                        row_data['email'], row_data['website'], url]
            self.__logger.info('{}'.format(csv_data))
            self.__write_data(csv_data)

        except Exception as x:
            self.__logger.error('Error grabbing details: {}'.format(x))

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)
        except Exception as x:
            print('Error printing csv output: ' + str(x))


if __name__ == '__main__':
    input_file = input('Please specify input txt: ')
    output_file = input('Please specify output csv file name: ')
    # input_file = 'input.txt'
    # output_file = 'sbc_output_final.csv'
    with SbcGrabber(input_file, output_file) as spider:
        spider.grab_data()
