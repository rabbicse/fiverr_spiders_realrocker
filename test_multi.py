from multiprocessing import Pool


class MTest:
    def __init__(self):
        self.a = 10

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def op(self):
        with Pool(5) as p:
            r = p.map(self._f, [1, 2, 3])
            print(r)

    def __test(self):
        pass

    def _f(self, x):
        return x * x


if __name__ == '__main__':
    with MTest() as mt:
        mt.op()
