# -*- coding: utf-8 -*-

import csv
import glob
import os
import time
from multiprocessing import Semaphore, Value

from sqlalchemy import create_engine, Unicode, UnicodeText, or_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float
from sqlalchemy.orm import sessionmaker


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts))
        else:
            print('%r  %2.2f Seconds' % (method.__name__, (te - ts)))
        return result

    return timed


DB_HOST = '127.0.0.1'
DB_PORT = '3306'
DB_USERNAME = 'admin'
DB_PASSWORD = 'password'
DB_NAME = 'clean_data'

engine = create_engine(
    'mysql+mysqldb://{}:{}@{}:{}/{}?charset=utf8&use_unicode=1'.format(DB_USERNAME, DB_PASSWORD, DB_HOST,
                                                                       DB_PORT, DB_NAME), encoding='utf-8', echo=False)
Base = declarative_base()
Session = sessionmaker(bind=engine)


class CsvData(Base):
    __tablename__ = 'csv_data'
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    id = Column(Integer, primary_key=True)
    business_name = Column(UnicodeText)
    category = Column(UnicodeText)
    telephone = Column(Unicode(100))
    email = Column(UnicodeText)
    website = Column(UnicodeText)
    description = Column(UnicodeText)
    full_address = Column(UnicodeText)
    street_address = Column(UnicodeText)
    state = Column(Unicode(512))
    city = Column(Unicode(512))
    zip_code = Column(Integer)
    country = Column(Unicode(255))
    latitude = Column(Float)
    longitude = Column(Float)
    brands = Column(UnicodeText)
    payment = Column(Unicode(255))
    years_in_business = Column(Integer)
    opening_hours = Column(UnicodeText)
    other_inforamtion = Column(UnicodeText)
    facebook_url = Column(UnicodeText)
    twitter_url = Column(UnicodeText)
    instagram_url = Column(UnicodeText)
    linkedin_url = Column(UnicodeText)
    youtube_url = Column(UnicodeText)
    google_plus_url = Column(UnicodeText)

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k) and kwargs[k] and not kwargs[k] == '':
                setattr(self, k, kwargs[k])


class CsvCleaner:
    __ignore_list = [0, 14, 15, 16, 17, 22, 23, 24, 25, 26]
    __total = Value('i', 0)
    __lock = Semaphore()

    def __init__(self, input_csv, output_csv):
        self.__cache = []
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __enter__(self):
        self.__column_headers = [('business_name', 'Business name'),
                                 ('category', 'Category'),
                                 ('telephone', 'Telephone'),
                                 ('email', 'Email'),
                                 ('website', 'Website'),
                                 ('description', 'Description'),
                                 ('full_address', 'Full Address'),
                                 ('street_address', 'Street Address'),
                                 ('state', 'State'),
                                 ('city', 'City'),
                                 ('zip_code', 'Zip code'),
                                 ('country', 'Country'),
                                 ('latitude', 'Latitude'),
                                 ('longitude', 'Longitude'),
                                 ('brands', 'Brands'),
                                 ('payment', 'Payment'),
                                 ('years_in_business', 'Years In Business'),
                                 ('opening_hours', 'Opening Hours'),
                                 ('other_inforamtion', 'Other Inforamtion'),
                                 ('facebook_url', 'Facebook Url'),
                                 ('twitter_url', 'Twitter Url'),
                                 ('instagram_url', 'Instagram Url'),
                                 ('linkedin_url', 'Linkedin Url'),
                                 ('youtube_url', 'Youtube Url'),
                                 ('google_plus_url', 'Google plus Url')]

        # create database table
        Base.metadata.create_all(engine)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    @timeit
    def process_data(self):
        try:
            if os.path.exists(self.__input_csv):
                with open(self.__input_csv, 'r+', encoding='utf-8') as f:
                    reader = csv.reader(f)
                    with open(self.__output_csv, 'w+', encoding='utf-8') as w:
                        # init session
                        session = Session()
                        # models = []

                        try:
                            writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                            i = 0
                            for r in reader:
                                try:
                                    if r[4].strip() == '' and r[5].strip() == '':
                                        continue

                                    # if len(self.__cache) > 0:
                                    #     if any(r[4].strip() in e[0] and r[5].strip() in e[1] for e in self.__cache):
                                    #         continue
                                    #
                                    # self.__cache.append((r[4], r[5]))

                                    r[7], r[9] = r[9], r[7]
                                    row = []
                                    for index, column in enumerate(r):
                                        if index not in self.__ignore_list:
                                            if i == 0:
                                                col = column
                                                if index == 1:
                                                    col = 'Business name'
                                                elif index == 9:
                                                    col = 'City'
                                                elif index == 8:
                                                    col = 'State'
                                                elif index == 10:
                                                    col = 'Zip code'
                                                elif index == 28:
                                                    col = 'Facebook Url'
                                                elif index == 29:
                                                    col = 'Twitter Url'
                                                elif index == 30:
                                                    col = 'Instagram Url'
                                                elif index == 31:
                                                    col = 'Linkedin Url'
                                                elif index == 32:
                                                    col = 'Youtube Url'
                                                elif index == 33:
                                                    col = 'Google plus Url'

                                                row.append(col)
                                            else:
                                                row.append(column)

                                    if i == 0:
                                        row.insert(6, 'Full Address')
                                    else:
                                        address = ', '.join([addr for addr in r[7:11] if not addr.strip() == ''])
                                        row.insert(6, address)

                                        # db operation
                                        try:
                                            q = {}
                                            q_data = None
                                            if row[3] and not row[3] == '':
                                                q['email'] = row[3]
                                            if row[4] and not row[4] == '':
                                                q['website'] = row[4]
                                            # q_data = session.query(CsvData).filter_by(**q).first()

                                            if 'email' in q and 'website' in q:
                                                q_data = session.query(CsvData).filter(
                                                    or_(CsvData.email.like(q['email']),
                                                        CsvData.website.like(q['website']))).first()
                                            elif 'email' in q:
                                                q_data = session.query(CsvData).filter(
                                                    or_(CsvData.email.like(q['email']))).first()
                                            elif 'website' in q:
                                                q_data = session.query(CsvData).filter(
                                                    or_(CsvData.website.like(q['website']))).first()

                                            if q_data:
                                                continue
                                            # if q_data_email or q_data_web:
                                            #     continue

                                            item = {}
                                            for index, col_data in enumerate(row):
                                                item[self.__column_headers[index][0]] = col_data

                                            model = CsvData(**item)
                                            # models.append(model)
                                            writer.writerow(row)
                                            session.add(model)
                                            # session.commit()
                                        except Exception as ex:
                                            session.rollback()
                                            print(ex)
                                finally:
                                    i += 1
                        finally:
                            # session.add_all(models)
                            session.commit()
                            session.close()
        except Exception as x:
            print('Error when process data: {}'.format(x))


if __name__ == '__main__':
    input_dir = './input_data/'
    output_dir = './clean_data/'

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for input_csv in glob.glob('{}*.csv'.format(input_dir)):
        output_csv = os.path.join(output_dir, os.path.basename(input_csv))
        input_csv_file = os.path.abspath(input_csv)

        print('Processing file: {}...'.format(input_csv_file))
        with CsvCleaner(input_csv_file, output_csv) as cleaner:
            cleaner.process_data()
            print('Processing Done!')
