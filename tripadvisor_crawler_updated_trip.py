import csv
import gzip
import json
import os
import platform
import random
import re
import socket
import urllib.parse
import urllib.request

import time
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import logging
import readline
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool

import sys
from OpenGL import GL
from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings, QWebEngineProfile, QWebEnginePage
from PyQt5.QtWidgets import QApplication

readline.parse_and_bind("control-v: paste")


class WebEnginePage(QWebEnginePage):
    def acceptNavigationRequest(self, url, _type, isMainFrame):
        if _type == QWebEnginePage.NavigationTypeLinkClicked:
            return True
        return QWebEnginePage.acceptNavigationRequest(self, url, _type, isMainFrame)


class Browser(QWebEngineView):
    __redirected_url = None
    __html = None
    __clicked = False
    __success_count = 0

    def __init__(self, app, *args, **kwargs):
        QWebEngineView.__init__(self, *args, **kwargs)
        self.__app = app
        self.settings().setAttribute(QWebEngineSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.AllowRunningInsecureContent, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptCanAccessClipboard, True)
        self.urlChanged.connect(self.__url_changed)
        self.loadFinished.connect(self._load_finished)
        self.setPage(WebEnginePage(self))
        self.__loaded = False

    def createWindow(self, wintype):
        return self

    def fetch(self, url, logger):
        try:
            self.__url = url
            self.__logger = logger
            self.timeout_timer = QTimer()
            self.timeout_timer.timeout.connect(self._request_timed_out)
            self.timeout_timer.start(120 * 1000)

            self.__logger.info('Trying to get web address: {}'.format(url))
            self.setUrl(QUrl(url))
            self.resize(1920, 1080)
            self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            self.__logger.error('Error when fetch data: {}'.format(x))
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            self.__logger.warning('Custom request timeout value exceeded.')
            self.timeout_timer.stop()
            self.close()
            self.__app.quit()
        except Exception as x:
            self.__logger.error('Request timeout: {}'.format(x))

    def __url_changed(self, url):
        try:
            current_url = str(url.toString())
            if current_url != self.__url:
                # print(current_url)
                self.__redirected_url = current_url
                self.timeout_timer.stop()
                self.close()
                self.__app.quit()
        except Exception as x:
            self.__logger.error('Error when load response: {}'.format(x))
            self.__app.quit()

    def _load_finished(self):
        try:
            current_url = str(self.page().url().toString())
            self.__logger.info('Redirected URL: {}'.format(current_url))
            if current_url == self.__url:
                submit = """document.getElementsByClassName("blEntry website")[0].click()"""
                self.page().runJavaScript(submit)
                # else:
                #     self.__redirected_url = current_url
                # self.page().toHtml(self.process_html)

        except Exception as x:
            self.__logger.error('Error when load response: {}'.format(x))
            self.__app.quit()

    def process_html(self, html):
        self.__html = html
        self.__app.quit()


class TripAdvisorUpdatedSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url = 'https://www.tripadvisor.com'
    __start_url = 'https://www.tripadvisor.com/Restaurants-g60763-New_York_City_New_York.html'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __proxies = (
        '37.48.118.90:13042',
        '83.149.70.159:13042'
    )
    __url_cache = []
    __total = 0

    def __init__(self, country_link, output_csv):
        self.__country_link = country_link
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler('tripadvisor.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__app = QApplication(sys.argv)
        self.__browser = Browser(self.__app)
        self.__lock = Lock()
        self.__setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(60)

        # csv_header = ['Name', 'Address', 'Country', 'Phone', 'Email', 'Website', 'URL', 'Image URL', 'Image File',
        #               'Average Prices', 'Cuisine', 'Ratings', '# of reviews', 'Menu', 'Business Category']
        self.__field_names = ['name', 'address', 'country', 'phone', 'email', 'website', 'average_prices', 'cuisine',
                              'ratings', 'num_of_reviews', 'business_category', 'url']
        csv_header = {'name': 'Name',
                      'address': 'Address',
                      'country': 'Country',
                      'phone': 'Phone',
                      'email': 'Email',
                      'website': 'Website',
                      'average_prices': 'Average Prices',
                      'cuisine': 'Cuisine',
                      'ratings': 'Ratings',
                      'num_of_reviews': '# of reviews',
                      'business_category': 'Business Category',
                      'url': 'URL'}

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy_url = str(random.choice(self.__proxies))

            if not proxy_url.startswith('http://'):
                proxy_url = 'http://' + proxy_url

            return {
                "http": proxy_url,
                "https": proxy_url
            }
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __build_browser(self):
        try:
            cap = webdriver.DesiredCapabilities.PHANTOMJS
            cap["phantomjs.page.settings.javascriptEnabled"] = True
            cap['phantomjs.page.settings.userAgent'] = self.__user_agent
            driver_path = './driver/phantomjs'
            if str(platform.system()).startswith('Windows'):
                driver_path = './driver/phantomjs.exe'

            browser = webdriver.PhantomJS(driver_path, desired_capabilities=cap,
                                          service_args=['--ignore-ssl-errors=true'])

            # chrome browser
            # chrome_options = Options()
            # chrome_options.add_argument("--headless")
            # chrome_options.add_argument("--window-size=800x600")
            # browser = webdriver.Chrome('/home/racecoder/webapps/htdocs/spiders/fiverr/trip_advisor/driver/chromedriver', desired_capabilities=DesiredCapabilities.CHROME,
            #                            chrome_options=chrome_options)
            # browser = webdriver.Chrome('./driver/chromedriver', desired_capabilities=DesiredCapabilities.CHROME,
            #                            chrome_options=chrome_options)
            return browser
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__browser.close()
        self.__app.exit()
        del self.__browser
        del self.__app
        del self

    def grab_data_by_country(self, country_link=None, retry=0):
        try:
            if not country_link:
                country_link = self.__country_link
            # print('=== Country URL: {} ==='.format(country_link))
            self.__logger.info('=== Country URL: {} ==='.format(country_link))
            # req = urllib.request.Request(country_link, headers=self.__headers)
            # data = urllib.request.urlopen(req).read()
            opener = self.__create_opener()
            data = opener.open(country_link).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data and retry < 10:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_data_by_country(country_link, retry + 1)

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            link_tags = soup.find_all('div', class_='geo_name')
            if link_tags and len(link_tags) > 0:
                for link_tag in link_tags:
                    link = link_tag.find('a')
                    if link:
                        url = self.__base_url + link.get('href')
                        self.grab_all_data(url)

            link_ul = soup.find('ul', class_='geoList')
            if link_ul:
                link_tags = link_ul.find_all('li')
                if link_tags and len(link_tags) > 0:
                    for link_tag in link_tags:
                        link = link_tag.find('a')
                        if link:
                            url = self.__base_url + link.get('href')
                            self.grab_all_data(url)

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

            next_page_tag = soup.find('a', class_='guiArw sprite-pageNext ')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

        except Exception as x:
            self.__logger.error('Error when grabbing data by country: {}'.format(x))
            if retry < 10:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_data_by_country(country_link, retry + 1)

    def grab_all_data(self, url):
        next_page = self.grab_data(url)
        while next_page:
            next_page = self.grab_data(next_page)

    def grab_data(self, url):
        try:
            # print('=== Main URL: {} ==='.format(url))
            self.__logger.info('=== Main URL: {} ==='.format(url))
            # req = urllib.request.Request(url, headers=self.__headers)
            # data = urllib.request.urlopen(req).read()
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            url_list = []

            # get all details link: process 1
            divs = soup.find_all('div', id=re.compile(r'^eatery_\d+$'))
            for div in divs:
                link_tag = div.find('a', class_='property_title')
                if link_tag:
                    link = self.__base_url + link_tag.get('href')
                    if link not in self.__url_cache and link not in url_list:
                        is_done = self.__grab_details(link)
                        # url_list.append(link)
                    else:
                        self.__logger.warning('Link: {} already grabbed.'.format(link))

            # get all details link: process 2
            divs = soup.find_all('div', class_='attraction_element')
            for div in divs:
                link_tag = div.find('div', class_='listing_title ')
                if link_tag:
                    link_tag_a = link_tag.find('a')
                    if link_tag_a:
                        link = self.__base_url + link_tag_a.get('href')
                        if link not in self.__url_cache and link not in url_list:
                            is_done = self.__grab_details(link)
                            # url_list.append(link)
                        else:
                            self.__logger.warning('Link: {} already grabbed.'.format(link))

            # with ThreadPool(16) as p:
            #     p.map(self.__grab_details, url_list)

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                return self.__base_url + next_page_tag.get('href').strip()
        except Exception as x:
            print(x)

    def __grab_details(self, url, retry=0):
        try:
            # print('=== Details URL: {} ==='.format(url))
            self.__logger.info('=== Details URL: {} ==='.format(url))
            # req = urllib.request.Request(url, headers=self.__headers)
            # data = urllib.request.urlopen(req).read()
            # data = gzip.decompress(data).decode('utf-8', errors='ignore')
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            name = ''
            address = ''
            country = ''
            phone = ''
            email = ''
            web = ''
            image_url = ''
            image_file = ''
            average_prices = ''
            cuisine = ''
            ratings = ''
            num_of_reviews = ''
            menu = ''
            business_category = ''

            item = {'url': url}
            name_tag = soup.find('h1', id='HEADING')
            if name_tag:
                item['name'] = name_tag.text.strip()

            address_tag = soup.find('span', class_='format_address')
            if address_tag:
                item['address'] = address_tag.text.strip()

            json_tag = soup.find('script', {'type': 'application/ld+json'})
            if json_tag:
                json_data = json.loads(json_tag.text)
                if 'address' in json_data and 'addressCountry' in json_data['address'] and 'name' in \
                        json_data['address']['addressCountry']:
                    item['country'] = json_data['address']['addressCountry']['name']

                if 'address' in json_data and address == '':
                    address_list = []
                    if 'streetAddress' in json_data['address']:
                        address_list.append(json_data['address']['streetAddress'])
                    if 'streetAddress' in json_data['address']:
                        address_list.append(json_data['address']['streetAddress'])
                    if 'addressLocality' in json_data['address']:
                        address_list.append(json_data['address']['addressLocality'])
                    if 'addressRegion' in json_data['address']:
                        address_list.append(json_data['address']['addressRegion'])
                    if 'postalCode' in json_data['address']:
                        address_list.append(json_data['address']['postalCode'])

                    if 'country' in item:
                        address_list.append(item['country'])
                    item['address'] = ', '.join(address_list)

                    if 'aggregateRating' in json_data:
                        if 'ratingValue' in json_data['aggregateRating']:
                            item['ratings'] = json_data['aggregateRating']['ratingValue']
                        if 'reviewCount' in json_data['aggregateRating']:
                            item['num_of_reviews'] = json_data['aggregateRating']['reviewCount']
                            # if 'image' in json_data:
                            #     image_url = json_data['image']
                            #     image_file = image_url.split('/')[-1]
                            #
                            #     try:
                            #         urllib.request.urlretrieve(image_url, './images/{}'.format(image_file))
                            #     except Exception as ex:
                            #         print('Error when download image from {}.'.format(image_url))
                            #         print(ex)

            phone_tag = soup.find('div', class_='blEntry phone')
            if phone_tag and not 'Add phone number' in phone_tag.text:
                phone = phone_tag.text

            if phone == '':
                phone_tag = soup.find('div', class_='blEntry phone directContactInfo')
                if phone_tag and not 'Add phone number' in phone_tag.text:
                    phone = phone_tag.text
            item['phone'] = phone

            email_tag = soup.find('span', class_='ui_icon email')
            if email_tag:
                email_link_tag = email_tag.find_next_sibling('a')
                if email_link_tag:
                    email = email_link_tag.get('href')
                    email = email.replace('mailto:', '')
                    item['email'] = email.strip()

            details_tags = soup.find_all('div', class_='row')
            if details_tags and len(details_tags) > 0:
                for row in details_tags:
                    title = row.find('div', class_='title')
                    if not title:
                        continue
                    if 'Cuisine' in title.text:
                        contant_tag = row.find('div', class_='content')
                        if contant_tag:
                            item['cuisine'] = contant_tag.text.strip()

                    if 'Average prices' in title.text:
                        contant_tag = row.find('div', class_='content')
                        if contant_tag:
                            item['average_prices'] = contant_tag.text.strip()

            # menu_tab = soup.find('div', id='RESTAURANT_MENU')
            # if menu_tab:
            #     menus = []
            #     menu_tags = menu_tab.find_all('div', class_=re.compile(r'(?:^menuItemTitle$)|(?:^menuItem$)'))
            #     for menu_tag in menu_tags:
            #         menus.append(menu_tag.text.strip())
            #     menu = ', '.join(menus)
            #
            business_category_tag = soup.find('span', class_='header_detail attraction_details')
            if business_category_tag:
                business_category = business_category_tag.text.strip()
                business_category = business_category.replace('More', '')
                business_category = business_category.strip()
                item['business_category'] = business_category.strip(',')

            web_tag = soup.find('div', class_='blEntry website')
            if web_tag and web_tag.has_attr('data-ahref') and len(web_tag.get('data-ahref').strip()) > 0:
                item['website'] = self.grab_web_url(url)
                self.__logger.info('Web site: {}'.format(item['website']))
            else:
                self.__logger.debug('No web address present for {}'.format(url))

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()
            return True
        except Exception as x:
            self.__logger.error('Error when grab details page.{}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def grab_web_url(self, url, retry=0):
        try:
            self.__lock.acquire()
            web_url, data = self.__browser.fetch(url, self.__logger)
            return web_url if web_url and web_url.startswith('http') else ''
        except Exception as x:
            self.__logger.error('Error when getting web site url: {}'.format(x))
            if retry < 5:
                self.__lock.release()
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)
        finally:
            self.__lock.release()
        return ''

    def grab_web_url_bak(self, url, retry=0):
        browser = None
        try:
            self.__lock.acquire()
            self.__logger.info('Trying to get web site address. URL: {}'.format(url))
            browser = self.__build_browser()
            browser.get(url)
            wait = WebDriverWait(browser, 60)
            wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

            # Get web address
            element = browser.find_element_by_xpath('//div[@class="blEntry website"]')
            if element and element.is_enabled():
                print(element.get_attribute('innerHTML'))
                print('ok...')
                webdriver.ActionChains(browser).move_to_element(element).click(element).perform()
                # element.click()

                # Switch tab to the new tab, which we will assume is the next one on the right)
                if len(browser.window_handles) > 1:
                    browser.switch_to_window(browser.window_handles[-1])
                    # wait = WebDriverWait(browser, 60)
                    # wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
                    web = browser.current_url
                    # print(web)
                    return web if web.startswith('http') else ''
        except Exception as x:
            self.__logger.error('Error when getting web site url: {}'.format(x))
            if retry < 5:
                try:
                    if browser:
                        browser.quit()
                        browser = None
                        del browser
                except:
                    pass

                self.__lock.release()

                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)
        finally:
            try:
                if browser:
                    browser.quit()
                    browser = None
                    del browser
            except:
                pass

            self.__lock.release()
        return ''

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    # country_link = input('Please specify Country URL: ')
    # output_file = input('Please specify output file name: ')
    country_link = 'https://www.tripadvisor.com/Restaurants-g187275-Germany.html'
    output_file = 'tripadvisor_output_germany.csv'
    with TripAdvisorUpdatedSpider(country_link, output_file) as spider:
        # web = spider.grab_web_url(
        #     'https://www.tripadvisor.com/Restaurant_Review-g60763-d7616347-Reviews-Lincoln_Square_Steak-New_York_City_New_York.html')
        # print(web)
        spider.grab_data_by_country()
        # with open('urls.txt', 'r+') as rf:
        #     for url_line in rf.readlines():
        #         url = url_line.strip()
        #         print('==== URL from file: {} ==='.format(url))
        #         spider.grab_all_data(url)
