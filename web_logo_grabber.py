import csv
import gzip
import platform
import re
import socket
import urllib.request
import urllib.parse

import time
from multiprocessing.pool import Pool

from bs4 import BeautifulSoup
import logging
from multiprocessing import Process, Lock
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class LogoGrabber:
    __total = 0
    __lock = Lock()
    __cache = {}
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Connection': 'keep-alive'}

    def __init__(self, input_file, output_csv):
        self.__input = input_file
        self.__output_csv = output_csv

    def __enter__(self):
        self.__setup_logger('logo.log')
        socket.setdefaulttimeout(60)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # urllib.request.install_opener(opener)

        csv_header = ['Name', 'Company', 'Address', 'Address2', 'City', 'State', 'Zip', 'Zip4', 'Bar', 'Cart', 'Flag',
                      'Country', 'Postal_cd', 'Listid', 'Scno', 'Keycode', 'Custno', 'Email', 'Cmbemploy', 'Phone',
                      'Sicp', 'Web URL', 'Icon URL', 'Logo URL', 'Comments']
        # ['Host Name', 'Icon URL', 'Logo URL', 'URL']
        self.__write_data(csv_header)

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __build_browser(self):
        try:
            cap = webdriver.DesiredCapabilities.PHANTOMJS
            cap["phantomjs.page.settings.javascriptEnabled"] = True
            cap['phantomjs.page.settings.userAgent'] = self.__user_agent
            driver_path = './driver/phantomjs'
            if str(platform.system()).startswith('Windows'):
                driver_path = './driver/phantomjs.exe'

            browser = webdriver.PhantomJS(driver_path, desired_capabilities=cap,
                                          service_args=['--ignore-ssl-errors=true'])
            return browser
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            # self.__grab_data_by_PhantomJS('http://spencertech.com')
            # return
            with open(self.__input, 'r', encoding='utf-8') as f:
                reader = csv.reader(f)
                for row in reader:
                    # print(row)
                    csv_data = row
                    email = row[17]
                    if '@' not in email:
                        continue
                    domain = email.split('@')[-1]
                    if domain not in self.__cache.keys():
                        results = self.__get_url(domain)
                        if results:
                            csv_data = [*csv_data, *results]
                            self.__cache[domain] = results
                    else:
                        csv_data = [*csv_data, *self.__cache[domain]]
                        self.__logger.warning('Host: {} already processed!'.format(domain))

                    self.__logger.info('Data: {}'.format(csv_data))
                    self.__write_data(csv_data)
        except Exception as x:
            print(x)

    def __process_data(self, *row):
        try:
            csv_data = row
            email = row[17]
            if '@' not in email:
                return
            domain = email.split('@')[-1]
            if domain not in self.__cache.keys():
                results = self.__get_url(domain)
                if results:
                    csv_data = [*csv_data, *results]
                    self.__cache[domain] = results
            else:
                csv_data = [*csv_data, *self.__cache[domain]]
                self.__logger.warning('Host: {} already processed!'.format(domain))

                self.__logger.info('Data: {}'.format(csv_data))
            self.__write_data(csv_data)
        except Exception as x:
            print(x)

    def __get_url(self, domain):
        try:
            self.__logger.info('=== Domain name: {} ==='.format(domain))
            req = urllib.request.Request('http://' + domain, headers=self.__headers)
            res = urllib.request.urlopen(req)

            original_url = res.geturl()
            if domain not in original_url:
                return [original_url, '', '', 'Domain mismatched with email']

            data = res.read().decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            icon = ''
            logo = ''
            icon_tag = soup.find('link', {'rel': 'shortcut icon'})
            if icon_tag:
                icon = icon_tag.get('href')
                if 'http' not in icon:
                    icon = original_url.strip('/') + '/' + icon.strip('/')

            img_tags = soup.find_all('img')
            if img_tags and len(img_tags) > 0:
                for img_tag in img_tags:
                    img_url = img_tag.get('src').strip()
                    if 'logo' in img_url:
                        if 'http' not in img_url:
                            logo = original_url.strip('/') + '/' + img_url.strip('/')
                        break

            # if icon == '' and logo == '':
            #     return self.__grab_data_by_PhantomJS(original_url)

            comments = ''
            if icon == '':
                comments += 'No Icon Found;'
            if logo == '':
                comments += 'No Logo Found;'

            csv_data = [original_url, icon, logo, comments]
            return csv_data
        except Exception as x:
            print(x)
            return ['', '', '', 'Web site is unavailable']

    def __grab_data_by_PhantomJS(self, url):
        browser = None
        try:
            self.__logger.info('Initializing browser with URL: {}'.format(url))
            browser = self.__build_browser()
            browser.get(url)

            wait = WebDriverWait(browser, 60)
            wait.until(EC.presence_of_element_located((By.TAG_NAME, 'html')))

            # Getting data
            print('Going to parse results from page: {}'.format(browser.current_url))
            data = browser.find_element_by_xpath('//html').get_attribute('innerHTML')
            if not data:
                return

            print(data)

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            icon = ''
            logo = ''
            icon_tag = soup.find('link', {'rel': 'shortcut icon'})
            if icon_tag:
                icon = icon_tag.get('href')
                if 'http' not in icon:
                    icon = url.strip('/') + '/' + icon.strip('/')

            img_tags = soup.find_all('img')
            if img_tags and len(img_tags) > 0:
                for img_tag in img_tags:
                    img_url = img_tag.get('src').strip()
                    print(img_url)
                    if 'logo' in img_url:
                        if 'http' not in img_url:
                            logo = url.strip('/') + '/' + img_url.strip('/')
                        break
            comments = ''
            if icon == '':
                comments += 'No Icon Found;'
            if logo == '':
                comments += 'No Logo Found;'

            csv_data = [url, icon, logo, comments]
            print(csv_data)
            return csv_data
        except Exception as x:
            print(x)
        finally:
            try:
                if browser:
                    browser.quit()
                    browser = None
                    del browser
            except:
                pass

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            print('Error printing csv output: ' + str(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    # input_file = input('Please specify input txt: ')
    # output_file = input('Please specify output csv file name: ')
    input_file = 'Data_Output_261017.csv'
    output_file = 'Data_Output_261017_output.csv'
    with LogoGrabber(input_file, output_file) as spider:
        spider.grab_data()
