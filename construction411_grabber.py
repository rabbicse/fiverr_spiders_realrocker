import argparse
import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import sys
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup


class Construction411Spider:
    """
    Company Name - Spécialités - Catégories - Address - Phone number - Email Address - Website - JPEG of the ad - Others social media
    """
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Accept-Language': 'en-US,en;q=0.9',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    __base_url = 'https://www.construction411.com/en'
    __search_url = 'https://www.construction411.com/en/recherche/{}/{}/'

    __proxies = None
    __url_cache = []
    __total = 0

    def __init__(self, input_file, output_csv):
        """
        :param input_file: Script will parse input records from input csv
        :param output_csv: Script will write output records to output csv
        """
        self.__input_file = input_file
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __setup_logger(self):
        """
        Script will create log file and write std output to terminal to track bug or status
        :return:
        """
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('construction411.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        """
        Initialize script and load required modules or initial setup of script.
        :return:
        """
        # lock object will need when we'll apply multi-threaded operation
        self.__lock = Lock()

        # Initialize/setup logger
        self.__setup_logger()

        # Set default socket timeout to 30 seconds
        socket.setdefaulttimeout(30)

        # Create ordered dictionary. It'll use during write record to output csv
        # It's easier to handle dictionary rather than a list to write to as csv file
        # Also initialized output csv file field names
        hdr = [('company', 'Company Name'),
               ('specialities', 'Spécialités'),
               ('categories', 'Catégories'),
               ('address', 'Address'),
               ('phone', 'Phone number'),
               ('email', 'Email Address'),
               ('fax', 'Fax'),
               ('website', 'Website'),
               ('facebook', 'Facebook'),
               ('google', 'Google+'),
               ('twitter', 'Twitter'),
               ('youtube', 'Youtube'),
               ('linkedin', 'LinkedIn'),
               ('cloud', 'Cloud'),
               ('image_url', 'Image URL'),
               ('url', 'URL')
               ]
        csv_header = OrderedDict(hdr)
        self.__field_names = list(csv_header.keys())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.__proxies = json_data

        # If header not yet written then write csv header first
        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)
            self.__url_cache.append(csv_header['url'])

        # Calculate previous records for debugging purpose
        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            if proxy['https'] == 'yes':
                return {'https': '{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': '{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                self.__logger.info('Proxy: {}'.format(proxy))
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        This is the exit point of script. We can dispose any unsafe objects here.
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        del self

    def process_data(self, retry=0):
        """
        Entry point of script. Will read input csv and process one by one with multi-thread
        :param retry:
        :return:
        """
        # self.__grab_details('https://www.construction411.com/en/commercial-contractor/matane/batinord/')
        # return
        try:
            # Read all records from input csv file
            rows = []
            if os.path.exists(self.__input_file):
                with open(self.__input_file, 'r+', encoding='utf-8') as f:
                    for line in f.readlines():
                        rows.append(line.strip())

            # Process all records with multi-thread
            with ThreadPool(4) as p:
                p.map(self.__process_all_kw, rows)
        except Exception as x:
            self.__logger.error('Error when process data.{}'.format(x))

            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < 5:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.process_data(retry + 1)

    def __process_all_kw(self, search_kw, retry=0):
        try:
            page = 1
            url = self.__search_url.format(search_kw, page)
            has_more = self.__process_kw(url)
            while has_more:
                page += 1
                url = self.__search_url.format(search_kw, page)
                has_more = self.__process_kw(url)
        except Exception as x:
            self.__logger.error('Error when process all keyword pages for keyword: {}. Error details: {}'.format(search_kw, x))
            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < 5:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__process_all_kw(search_kw, retry + 1)

    def __process_kw(self, url, retry=0):
        """
        It'll take title and website as args and search keywords
        :param search_kw:
        :param retry:
        :return:
        """
        try:
            self.__logger.info('Search URL: {}'.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            # After creating soup object simply dispose data to free memory
            del data

            rows = []
            results = soup.find_all('div', {'name': re.compile(r'^res\d+$')})
            for result in results:
                if not result.has_attr('onclick'):
                    continue

                details_url = self.__base_url + result.get('onclick').replace('location.href=\'', '').strip('\';')

                if details_url not in self.__url_cache:
                    rows.append(details_url)
                else:
                    self.__logger.warning('URL: {} already processed!'.format(details_url))
                # self.__grab_details(details_url)
                # break

            # Process all records with multi-thread
            with ThreadPool(10) as p:
                p.map(self.__grab_details, rows)

            # Dispose soup object
            del soup

            return len(rows) > 0
        except Exception as x:
            self.__logger.error('Error when process website for search_kw: [{}]. {}'.format(url, x))

            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < 5:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__process_kw(url, retry + 1)

    def __grab_details(self, url, retry=0):
        """
        It'll take title and website as args and search keywords
        :param url:
        :param retry:
        :return:
        """
        try:
            self.__logger.info('Details URL: {}'.format(url))
            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()
            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            # After creating soup object simply dispose data to free memory
            del data

            # Init item
            item = {'url': url}

            # company name
            com_name_div = soup.find('div', class_='cie_title')
            if com_name_div:
                item['company'] = com_name_div.text.strip()

            # specialities
            specialities_div = soup.find('div', class_='row content')
            if specialities_div:
                item['specialities'] = specialities_div.text.strip()

            # categories
            cat_div = soup.find('div', class_='col-sm-6 content')
            if cat_div:
                p_list = cat_div.find_all('p')
                if p_list:
                    categories = []
                    for p in p_list:
                        categories.append(p.text.strip())
                    item['categories'] = ', '.join(categories)

            # Address
            address_i = soup.find('i', class_='fas fa-map-marker co-icon')
            if address_i:
                address_i_div = address_i.find_parent('div')
                if address_i_div:
                    address_divs = address_i_div.find_next_siblings('div')
                    if address_divs:
                        addresses = []
                        for address_div in address_divs:
                            addresses.append(address_div.text.strip())
                        item['address'] = ', '.join(addresses)

            # Phone
            phone_i = soup.find('i', class_='fas fa-phone co-icon')
            if phone_i:
                phone_i_div = phone_i.find_parent('div')
                if phone_i_div:
                    phone_divs = phone_i_div.find_next_siblings('div')
                    if phone_divs:
                        phones = []
                        for phone_div in phone_divs:
                            phone = phone_div.text.strip()
                            phone = re.sub(r'[^0-9\(\)\- ]', '', phone)
                            phone = phone.strip()
                            phones.append(phone)
                        item['phone'] = ', '.join(phones)

            # Fax
            fax_i = soup.find('i', class_='fas fa-fax co-icon')
            if fax_i:
                fax_i_div = fax_i.find_parent('div')
                if fax_i_div:
                    fax_divs = fax_i_div.find_next_siblings('div')
                    if fax_divs:
                        faxes = []
                        for fax_div in fax_divs:
                            faxes.append(fax_div.text.strip())
                        item['fax'] = ', '.join(faxes)

            # Website
            web_tag = soup.find('a', {'title': 'Site web principal de l\'entreprise'})
            if web_tag:
                item['website'] = web_tag.get('href')

            # Facebook
            item['facebook'] = self.__get_social_link(soup, 'fab fa-facebook-square fa-3x active')

            # Youtube
            item['youtube'] = self.__get_social_link(soup, 'fab fa-youtube-square fa-3x active')

            # Google+
            item['google'] = self.__get_social_link(soup, 'fab fa-google-plus-square fa-3x active')

            # Twitter
            item['twitter'] = self.__get_social_link(soup, 'fab fa-twitter-square fa-3x active')

            # LinkedIn
            item['linkedin'] = self.__get_social_link(soup, 'fab fa-linkedin fa-3x active')

            # Cloud
            item['cloud'] = self.__get_social_link(soup, 'fas fa-cloud fa-3x active')

            # Image URL
            image_url_tag = soup.find('img', class_='imgCarte')
            if image_url_tag:
                item['image_url'] = image_url_tag.get('src')

            # Dispose soup object
            del soup

            # Finally write item
            self.__write_item(item)
        except Exception as x:
            self.__logger.error('Error when grab details for URL: [{}]. {}'.format(url, x))

            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < 5:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __get_social_link(self, soup, class_name):
        try:
            social_i = soup.find('i', class_=class_name)
            if social_i:
                social_a = social_i.find_parent('a')
                if social_a and social_a.has_attr('href'):
                    return social_a.get('href')
        except Exception as x:
            self.__logger.error('Error when parse social links. {}'.format(x))

        return ''

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        """
        Write data to csv file as dictionary object,
        quoting=csv.QUOTE_ALL, delimiter=';'
        :param row:
        :param mode:
        :return:
        """
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            pass
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-i', '--input', help='Please specify "Input csv file"!')
    # parser.add_argument('-o', '--output', help='Please specify "Output csv file"!')
    # args = parser.parse_args()
    #
    # # Check input csv file
    # if not args.input:
    #     print('Please specify "Input csv file"!')
    #     sys.exit(0)
    #
    # # Check output csv file
    # if not args.output:
    #     print('Please specify "Output csv file"!')
    #     sys.exit(0)
    #
    # input_file = args.input
    # output_file = args.output
    input_file = 'kw_input.txt'
    output_file = 'construction411_en.csv'
    with Construction411Spider(input_file, output_file) as spider:
        spider.process_data()
