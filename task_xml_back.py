import json
import xml.etree.ElementTree as ET


def process_xml(file_path):
    """
    :param file_path: xml file path
    :return:
    """
    # import data by reading from xml file
    tree = ET.parse(file_path)
    # get root of the xml file
    root = tree.getroot()

    # initialize variables to store favorite moves and not favorite movies and movie array
    favorites_movies = 0
    not_favorites_movies = 0
    movies = []

    # iterate through movie tag inside xml file
    for movie in root.iter('movie'):
        movie_dict = {}

        # if 'favorite' attr found then increment favorite count or not favorite count
        if 'favorite' in movie.attrib:
            if movie.attrib['favorite'] and movie.attrib['favorite'] == 'True':
                favorites_movies += 1
            else:
                not_favorites_movies += 1

        # iterate all child under movie tag
        for child in movie.iter():
            # if child name is movie that means it's element root so skip this element
            if child.tag in 'movie':
                continue

            # set movie dictionary values for each child tag and corresponding value
            movie_dict[child.tag] = child.text

            # print text under description tag
            if child.tag == 'description':
                child_texts = next(child.itertext())
                print('Description: {}'.format(child_texts.strip()))

        # append movie dictionary elements to movies array, will be dumped inside json file
        movies.append(movie_dict)

    with open('data.json', 'w', encoding='utf-8') as outfile:
        outfile = json.dump(movies, outfile, sort_keys=True, indent=4)
    print('Favorite Movies: {}'.format(favorites_movies))
    print('Not Favorite Movies: {}'.format(not_favorites_movies))


if __name__ == '__main__':
    process_xml('movie.xml')
