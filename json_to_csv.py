import json

k = 0
file_name = './siren/siren_{}.txt'.format(k)
of = open(file_name, 'a+')

with open('companies_siren_EI_non_radie.json', 'r+') as f:
    i = 0
    for line in f.readlines():
        if '"siren"' in line:
            txt = line.replace('"siren"', '')
            txt = txt.strip()
            txt = txt.strip(':')
            txt = txt.strip()
            txt = txt.strip('"')

            if i > 0 and i % 200000 == 0:
                of.close()
                k += 1
                file_name = './siren/siren_{}.txt'.format(k)
                of = open(file_name, 'w+')

            of.write(txt + '\n')

            i += 1
if of:
    of.close()
