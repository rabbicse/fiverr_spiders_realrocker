import csv
import glob
import gzip
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from datetime import datetime
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
import eventlet
from bs4 import BeautifulSoup

super_proxy = socket.gethostbyname('zproxy.lum-superproxy.io')


class CorporamaSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
    __base_url = 'https://corporama.com'
    __start_url = 'https://corporama.com/search?company={}'  # 403561137'
    __ajax_url = 'https://corporama.com/ajax/legal/details?{}&_={}'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Cache-Control': 'max-age=0',
        'Accept-Encoding': 'gzip',
        'Host': 'corporama.com',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    __username = 'lum-customer-tycoonex-zone-residential'
    __password = 'b5818f5afa4a'
    __port = 22225
    __proxy_url = "http://%s-session-%s:%s@"+super_proxy+":%d"
    # __proxy_url = "http://%s-country-us-session-%s:%s@" + super_proxy + ":%d"
    #country-us-session
    __proxies = None
    __siren_cache = []
    __total = 0
    MAX_RETRY = 5
    MAX_THREAD = 32

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('corporama.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__lock = Lock()
        self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(30)

        hdr = [('company_name', 'company_name'),
               ('company_address', 'company_address'),
               ('company_result_tranche', 'company_result_tranche'),
               ('company_size_number_tranche', 'company_size_number_tranche'),
               ('additional_details', 'additional_details'),
               ('company_fax', 'company_fax'),
               ('company_website', 'company_website'),
               ('corporama_company_url', 'corporama_company_url'),
               ('naf_code', 'naf_code'),
               ('naf_description', 'naf_description'),
               ('company_type', 'company_type'),
               ('company_founded', 'company_founded '),
               ('company_capital', 'company_capital'),
               ('vat', 'VAT'),
               ('company_linkedin_url', 'company_linkedin_url'),
               ('company_viadeo_url', 'company_viadeo_url'),
               ('siren', 'SIREN'),
               ('postal_code', 'postal_code'),
               ('company_city', 'company_city'),
               ('company_facebooks', 'company_facebooks'),
               ('tags', 'tags'),
               ('top_executives', 'top_executives')]

        self.__csv_header = OrderedDict(hdr)
        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        return self

    def __create_opener(self):
        try:
            session_id = random.random()
            super_proxy_url = self.__proxy_url % (self.__username, session_id, self.__password, self.__port)
            proxy_handler = urllib.request.ProxyHandler({
                'http': super_proxy_url,
                'https': super_proxy_url,
            })
            # opener = urllib.request.build_opener(proxy_handler)

            opener = urllib.request.build_opener(proxy_handler, urllib.request.HTTPCookieProcessor())
            opener.addheaders.clear()
            for key in self.__headers:
                opener.addheaders.append((key, self.__headers[key]))

            return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__logger.info('============ FINISH ============')
        del self

    def grab_data(self, input_dir):
        try:
            if os.path.exists(input_dir):
                for input_file in glob.glob(input_dir + '/*.txt'):
                    self.__logger.info('===== Processing File: {} ====='.format(input_file))
                    self.__output_csv = input_file + '_output.csv'

                    if os.path.exists(self.__output_csv):
                        with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                            reader = csv.DictReader(f, self.__field_names)
                            rows = []
                            for row in reader:
                                rows.append(row)
                                self.__siren_cache.append(row['siren'])

                            with open(self.__output_csv + '.json', 'w+', encoding='utf-8') as f:
                                json.dump(rows, f)

                    if self.__csv_header['siren'] not in self.__siren_cache:
                        self.__write_data(self.__csv_header)

                    self.__total = len(self.__siren_cache)
                    self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

                    siren_list = []
                    try:
                        with open(input_file, 'r+') as f:
                            for line in f.readlines():
                                siren = line.strip()
                                if siren in self.__siren_cache:
                                    self.__logger.warning('Siren: [{}] already processed!'.format(siren))
                                    continue

                                siren_list.append(siren)

                                if len(siren_list) == 4096:
                                    with ThreadPool(self.MAX_THREAD) as p:
                                        p.map(self.__process_siren, siren_list)

                                    siren_list = []

                            with ThreadPool(self.MAX_THREAD) as p:
                                p.map(self.__process_siren, siren_list)
                    except Exception as ex:
                        self.__logger.error('Error when process all sirens. Error Details: {}'.format(ex))
        except Exception as x:
            self.__logger.error('Error when grab data. {}'.format(x))

    def __process_siren(self, siren, retry=0):
        timer = None
        opener = None
        try:
            url = self.__start_url.format(siren)
            self.__logger.info('=== URL: {} ==='.format(url))
            timer = eventlet.Timeout(60)
            opener = self.__create_opener()
            res = opener.open(url)
            redirected_url = res.geturl()
            if not 'siren=' in redirected_url or not 'company=' in redirected_url:
                self.__logger.error('Server returned error 1!')
                self.__logger.error('Redirected URL: {}.'.format(redirected_url))
                if retry < self.MAX_RETRY:
                    sleep_time = random.randint(1, 5)
                    self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                    time.sleep(sleep_time)
                    return self.__process_siren(siren, retry + 1)
                else:
                    return

            data = res.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            ajax_id_m = re.search(r'async_loading\s*\(\'(\d+)\'\,', data, re.MULTILINE)
            if ajax_id_m:
                ajax_id = ajax_id_m.group(1)
                ajax_url = self.__ajax_url.format(redirected_url.split('?')[-1], ajax_id)
                self.__logger.info('Ajax URL: {}'.format(ajax_url))
                ajax_res = opener.open(ajax_url)
                ajax_data = ajax_res.read()
                try:
                    ajax_data = gzip.decompress(ajax_data).decode('utf-8', 'ignore')
                except:
                    ajax_data = ajax_data.decode('utf-8', 'ignore')

                if not ajax_data:
                    return

                if '&#x1f608;' in ajax_data:
                    self.__logger.warning('Server returned error!')
                    if retry < self.MAX_RETRY:
                        sleep_time = random.randint(1, 5)
                        self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                        time.sleep(sleep_time)
                        return self.__process_siren(siren, retry + 1)

                self.__parse_details(ajax_data, redirected_url, siren)

            if timer:
                timer.cancel()

            if opener:
                del opener

        except Exception as x:
            if timer:
                timer.cancel()
            if opener:
                del opener

            self.__logger.error('Error when process Siren. {}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__process_siren(siren, retry + 1)

    def __parse_details(self, data, url, siren):
        try:
            item = {'corporama_company_url': url, 'siren': siren}
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            title_h3 = soup.find('h3', class_='corpo-subtitle fn org')
            if title_h3:
                item['company_name'] = title_h3.text.strip()

            address_div = soup.find('div', class_='adr')
            if address_div:
                item['company_address'] = address_div.text.strip()

            company_size_number_tranche = soup.find('span', id='legal-headcount_value')
            if company_size_number_tranche:
                item['company_size_number_tranche'] = company_size_number_tranche.text.strip()

            company_fax = soup.find('span', class_='fax corpo-fax corpo-number-db')
            if company_fax:
                item['company_fax'] = company_fax.text.strip()

            company_website = soup.find('a', class_='url')
            if company_website:
                item['company_website'] = company_website.get('href')

            company_city = soup.find('span', class_='locality')
            if company_city:
                item['company_city'] = company_city.text.strip()

            corpo_legal_block = soup.find('div', class_='corpo-legal-block')
            if corpo_legal_block:
                post_code_div = corpo_legal_block.find('div', class_='flr text-right')
                if post_code_div:
                    post_code_a = post_code_div.find('a')
                    if post_code_a:
                        post_code_text = post_code_a.get('href')
                        post_code_text = re.sub(r'[^\+0-9]', '', post_code_text)
                        post_code_m = re.search(r'\+(\d{3,})\+', post_code_text)
                        if post_code_m:
                            item['postal_code'] = post_code_m.group(1)

                divs = corpo_legal_block.find_all('div')
                for div in divs:
                    div_strong = div.find('strong')
                    if not div_strong:
                        continue

                    div_strong_text = div_strong.text
                    next_sibling = div_strong.next_sibling
                    if not next_sibling:
                        continue

                    next_sibling_text = str(next_sibling).strip().strip(':').strip()

                    if 'CA' in div_strong_text:
                        item['company_result_tranche'] = next_sibling_text

                    if 'TVA' in div_strong_text:
                        item['vat'] = next_sibling_text

                    # if 'SIREN' in div_strong_text:
                    #     item['siren'] = next_sibling_text

                    if 'NAF' in div_strong_text:
                        naf = next_sibling_text
                        naf_m = re.search(r'^(.*?)\((.*?)\)$', naf)
                        if naf_m:
                            item['naf_description'] = naf_m.group(1).strip()
                            item['naf_code'] = naf_m.group(2).strip()

                    if 'Capital' in div_strong_text:
                        item['company_capital'] = next_sibling_text

                    if 'Forme jurid' in div_strong_text:
                        item['company_type'] = next_sibling_text
                        try:
                            company_founded = div_strong.parent.find_next_sibling('span', class_='display-none')
                            if company_founded and re.match(r'\d{4}\-\d{1,2}-\d{1,2}', company_founded.text.strip()):
                                fd = datetime.strptime(company_founded.text.strip(), '%Y-%m-%d')
                                item['company_founded'] = fd.strftime('%d/%m/%Y')
                        except Exception as dx:
                            print(dx)

                    linkedin = soup.find('a', text=re.compile(r'Page LinkedIn'))
                    if linkedin:
                        item['company_linkedin_url'] = linkedin.get('href')

                    viadeo = soup.find('a', text=re.compile(r'Page Viadeo'))
                    if viadeo:
                        item['company_viadeo_url'] = viadeo.get('href')


                        # item['siren'] =
            tags = []
            li_tags = soup.find_all('li', id=re.compile(r'^legal-flag-\d+$'))
            for li_tag in li_tags:
                tags.append(li_tag.text.strip())

            item['tags'] = '[{}]'.format(', '.join(['"{}"'.format(t) for t in tags]))

            executives = []
            leader_lines = soup.find_all('div', class_='leader-line')
            for leader_line in leader_lines:
                executive = {'job_title': '',
                             'full_name': '',
                             'first_name': '',
                             'last_name': ''}
                job_title = leader_line.find('strong')
                if job_title:
                    executive['job_title'] = job_title.text.strip()

                executive_a = leader_line.find('a', class_='btn-suggested-email')
                if executive_a:
                    executive['job_title'] = job_title.text.strip()
                    executive_text = executive_a.get('href')
                    executive_text = urllib.parse.unquote_plus(executive_text)
                    executive_text_spl = executive_text.split('&')
                    for executive_text_sp in executive_text_spl:
                        if 'ct_first_name=' in executive_text_sp:
                            executive['first_name'] = executive_text_sp.replace('ct_first_name=', '')
                        if 'ct_last_name=' in executive_text_sp:
                            executive['last_name'] = executive_text_sp.replace('ct_last_name=', '')

                    executive['full_name'] = executive['first_name'] + ' ' + executive['last_name']

                executives.append(executive)

            item['top_executives'] = executives  # json.dumps(executives)

            try:
                self.__logger.info('Data: {}'.format(item))
                self.__lock.acquire()
                self.__write_data(item)
                self.__siren_cache.append(siren)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()
        except Exception as x:
            self.__logger.error('Error when parse details! Error details: {}'.format(x))

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # input_file = './siren/siren_0.txt'
    input_dir = './siren'
    output_file = 'out.csv'
    with CorporamaSpider(output_file) as spider:
        spider.grab_data(input_dir)
