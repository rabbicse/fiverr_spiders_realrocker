import csv
import gzip
import json
import logging
import os
import random
import re
import readline
import socket
import string
import urllib.parse
import urllib.request
# import socks

from multiprocessing.pool import ThreadPool

import time
from bs4 import BeautifulSoup
from multiprocessing import Lock

readline.parse_and_bind("control-v: paste")


class YelpCrawler:
    __base_url = 'https://www.yelp.com'
    __search_url = 'https://www.yelp.com/search?find_desc={}&find_loc={}&ns=1'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __proxies = []
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('yelp.log')
        socket.setdefaulttimeout(30)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        # proxy_handler = urllib.request.ProxyHandler(self.__proxies)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # opener = urllib.request.build_opener(proxy_handler)
        # urllib.request.install_opener(opener)
        self.__field_names = ['url', 'name', 'address', 'city', 'zip_code', 'website', 'phone_number', 'working_hours',
                              'latitude', 'longitude', 'category', 'rating', 'reviews_count']
        csv_header = {'url': 'URL',
                      'name': 'Name',
                      'address': 'Address',
                      'city': 'City',
                      'zip_code': 'Zip Code',
                      'website': 'Website',
                      'phone_number': 'Phone Number',
                      'working_hours': 'Working Hours',
                      'latitude': 'Latitude',
                      'longitude': 'Longitude',
                      'category': 'Category',
                      'rating': 'Rating',
                      'reviews_count': 'Reviews Count'}

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)

        self.__load_proxies()

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __load_proxies(self):
        try:
            if os.path.exists('proxies.json'):
                with open('proxies.json') as inFile:
                    self.__proxies = json.load(inFile)
        except Exception as x:
            self.__logger.error('Error when loading proxies.{}'.format(x))

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy_url = str(random.choice(self.__proxies))

            if not proxy_url.startswith('http://'):
                proxy_url = 'http://' + proxy_url

            return {
                "http": proxy_url,
                "https": proxy_url
            }
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def grab_data(self):
        try:
            if not os.path.exists('categories.txt'):
                self.__logger.warning('Please make sure categories.txt exists in path!')
                return

            with open('categories.txt', 'r+') as rcf:
                for category_line in rcf.readlines():
                    category = category_line.strip()

                    with open('locations.txt', 'r+') as rlf:
                        for loc_line in rlf.readlines():
                            location = loc_line.strip()
                            self.__logger.info('==== Category: {}; Location: {} ==='.format(category, location))
                            search_url = self.__search_url.format(urllib.parse.quote_plus(category), urllib.parse.quote_plus(location))
                            next_page = self.__search_by_category_location(search_url)
                            while next_page:
                                next_page = self.__search_by_category_location(next_page)
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __search_by_category_location(self, url):
        try:
            self.__logger.info('=== Search URL: {} ==='.format(url))

            # set proxy
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler)
                urllib.request.install_opener(opener)
                req = urllib.request.Request(url, headers=self.__headers)
            else:
                req = urllib.request.Request(url, headers=self.__headers)

            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            item_tags = soup.find_all('li', class_='regular-search-result')
            if item_tags:
                url_list = []
                for item_tag in item_tags:
                    details_tag = item_tag.find('a', class_='biz-name js-analytics-click')
                    if not details_tag:
                        continue

                    details_url = self.__base_url + details_tag.get('href')
                    if details_url not in self.__url_cache:
                        # if not self.__grab_details(details_url):
                        #     self.__logger.warning('Failed to grab details...')
                        url_list.append(details_url)
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(details_url))

                with ThreadPool(16) as p:
                    p.map(self.__grab_details, url_list)

            next_link_tag = soup.find('a', class_='u-decoration-none next pagination-links_anchor')
            if next_link_tag:
                next_link = next_link_tag.get('href')
                return self.__base_url + next_link
        except Exception as x:
            print(x)

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))

            # set proxy
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler)
                urllib.request.install_opener(opener)
                req = urllib.request.Request(url, headers=self.__headers)
            else:
                req = urllib.request.Request(url, headers=self.__headers)

            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            item = {'url': url}
            name_tag_top_shelf = soup.find('div', {'class': 'top-shelf'})
            if name_tag_top_shelf:
                name_tag = name_tag_top_shelf.find('meta', {'itemprop': 'name'})
                if name_tag and name_tag.has_attr('content'):
                    item['name'] = name_tag.get('content').strip()

            if 'name' not in item:
                name_tag = soup.find('h1', {'class': 'biz-page-title embossed-text-white shortenough'})
                if name_tag:
                    item['name'] = name_tag.text.strip()

            if 'name' not in item:
                name_tag = soup.find('h1', {'class': 'biz-page-title embossed-text-white'})
                if name_tag:
                    item['name'] = name_tag.text.strip(string.whitespace)

            street_address_tag = soup.find('span', {'itemprop': 'streetAddress'})
            if street_address_tag:
                item['address'] = street_address_tag.text.strip()

            city_tag = soup.find('span', {'itemprop': 'addressLocality'})
            if city_tag:
                item['city'] = city_tag.text.strip()

            post_code_tag = soup.find('span', {'itemprop': 'postalCode'})
            if post_code_tag:
                item['post_code'] = post_code_tag.text.strip()

            country_tag = soup.find('address', {'itemprop': 'address'})
            if country_tag:
                country = country_tag.text
                if 'city' in item:
                    country = country.replace(item['city'], '')
                if 'post_code' in item:
                    country = country.replace(item['post_code'], '')
                country = country.strip()
                item['country'] = country

            telephone_tag = soup.find('span', {'class': 'biz-phone'})
            if telephone_tag:
                item['telephone'] = telephone_tag.text.strip()

            try:
                web_url = soup.find('span', {'class': 'biz-website js-add-url-tagging'}).find('a').get('href')
                o = urllib.parse.urlparse(web_url)
                query = urllib.parse.parse_qs(o.query)
                website = query.get('url')[0]
                item['website'] = website
            except Exception as e:
                website = ''

            try:
                working_hours = soup.find('table', {'class': 'table table-simple hours-table'}).text.replace(
                    '\n', '')
                item['working_hours'] = working_hours
            except Exception as e:
                workingHours = ''

            categories = []
            try:
                for category in soup.find('span', {'class': 'category-str-list'}).find_all('a'):
                    categoryObj = {}
                    categoryObj['title'] = category.text
                    categoryObj['url'] = category.get('href')
                    categories.append(categoryObj)
            except Exception as e:
                pass

            try:
                forPrintCategories = []
                for cat in categories:
                    forPrintCategories.append(cat.get('title'))
                category = ', '.join(forPrintCategories)
                item['category'] = category
            except Exception as e:
                category = ''

            try:
                coordinatesDiv = json.loads(
                    str(soup.find('div', {'class': 'lightbox-map hidden'}).get('data-map-state')))
                latitude = coordinatesDiv.get('center').get('latitude')
                item['latitude'] = latitude
                longitude = coordinatesDiv.get('center').get('longitude')
                item['longitude'] = longitude
            except Exception as e:
                latitude = ''
                longitude = ''

            try:
                ratingDiv = soup.find('div', {'class': 'rating-info clearfix'})
                reviewsCount = str(ratingDiv.find('span', {'class': 'review-count'}).text).strip()
                item['review_count'] = reviewsCount
            except Exception as e:
                reviewsCount = ''

            try:
                ratingDiv = soup.find('div', {'class': 'rating-info clearfix'})
                ratings = ratingDiv.find('div', {'class': 'i-stars'})
                ratings = ratings.get('title').replace('star rating', '')
                item['ratings'] = ratings
            except Exception as identifier:
                ratings = ''

            if len(item.keys()) < 3 and retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

            return True

        except Exception as x:
            self.__logger.error('Error grab details page: {}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)
            else:
                return False

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    output_file = 'yelp_output.csv'
    with YelpCrawler(output_file) as spider:
        spider.grab_data()

    input('Press any key to exit...')
