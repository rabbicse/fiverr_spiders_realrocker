import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import sys
import time
import urllib.parse
import urllib.request
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock, Pool
from multiprocessing.pool import ThreadPool

from OpenGL import GL
from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings, QWebEnginePage, QWebEngineProfile
from PyQt5.QtWidgets import QApplication
from bs4 import BeautifulSoup



class WebEnginePage(QWebEnginePage):
    def acceptNavigationRequest(self, url, _type, isMainFrame):
        if _type == QWebEnginePage.NavigationTypeLinkClicked:
            return True
        return QWebEnginePage.acceptNavigationRequest(self, url, _type, isMainFrame)


class Browser(QWebEngineView):
    def __init__(self, app, *args, **kwargs):
        QWebEngineView.__init__(self, *args, **kwargs)
        self.__app = app
        self.settings().setAttribute(QWebEngineSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.AllowRunningInsecureContent, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptCanAccessClipboard, True)
        self.settings().setAttribute(QWebEngineSettings.HyperlinkAuditingEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.LinksIncludedInFocusChain, True)
        self.settings().setAttribute(QWebEngineSettings.ScreenCaptureEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.SpatialNavigationEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.WebGLEnabled, True)
        # self.urlChanged.connect(self.__url_changed)
        self.loadFinished.connect(self._load_finished)

        profile = QWebEngineProfile("storage", self)
        profile.setCachePath('./cookies')
        profile.setPersistentStoragePath('./cookies')
        self.setPage(WebEnginePage(profile, self))

        self.__loaded = False
        self.__redirected_url = None
        self.__html = None
        self.__clicked = False
        self.__success_count = 0

    def createWindow(self, wintype):
        return self

    def fetch(self, url, logger):
        try:
            self.__url = url
            self.__logger = logger
            self.timeout_timer = QTimer()
            self.timeout_timer.timeout.connect(self._request_timed_out)
            self.timeout_timer.start(120 * 1000)

            self.__logger.info('Trying to get web address: {}'.format(url))
            self.setUrl(QUrl(url))
            self.resize(1920, 1080)
            self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            self.__logger.error('Error when fetch data: {}'.format(x))
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            self.__logger.warning('Custom request timeout value exceeded.')
            self.timeout_timer.stop()
            self.close()
            self.__app.quit()
        except Exception as x:
            self.__logger.error('Request timeout: {}'.format(x))

    def __url_changed(self, url):
        try:
            current_url = str(url.toString())
            if current_url != self.__url:
                # print(current_url)
                self.__redirected_url = current_url
                self.timeout_timer.stop()
                # del self.timeout_timer
                # self.close()
                # del self
                # self.__app.quit()
        except Exception as x:
            self.__logger.error('Error when load response: {}'.format(x))
            self.__app.quit()

    def _load_finished(self):
        try:
            current_url = str(self.page().url().toString())
            self.__logger.info('Redirected URL: {}'.format(current_url))
            if current_url == self.__url:
                submit = """document.getElementsByClassName("blEntry website")[0].click()"""
                # self.page().runJavaScript(submit)
                # else:
                #     self.__redirected_url = current_url
                # self.page().toHtml(self.process_html)

        except Exception as x:
            self.__logger.error('Error when load response: {}'.format(x))
            self.__app.quit()

    def process_html(self, html):
        self.__html = html
        self.__app.quit()


class TripAdvisorModifiedSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    # __base_url = 'https://www.tripadvisor.com'
    __base_url = 'https://www.tripadvisor.fr'
    __start_url = 'https://www.tripadvisor.com/Restaurants-g60763-New_York_City_New_York.html'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        # 'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}
    # __proxies = (
    #     '206.189.47.247:3128',
    #     '40.81.10.123:8080',
    #     '34.239.167.200:3128',
    #     '66.82.123.234:8080',
    #     '159.65.110.167:3128'
    # )

    __proxies = None
    __url_cache = []
    __total = 0

    def __init__(self, country_link, output_csv):
        self.__country_link = country_link
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler(log_file_name, maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__app = QApplication(sys.argv)
        self.__browser = Browser(self.__app)
        self.__lock = Lock()
        self.__setup_logger('tripadvisor.log')
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(30)

        # csv_header = ['Name', 'Address', 'Country', 'Phone', 'Email', 'Website', 'URL', 'Image URL', 'Image File',
        #               'Average Prices', 'Cuisine', 'Ratings', '# of reviews', 'Menu', 'Business Category']
        self.__field_names = ['name', 'address', 'country', 'phone', 'email', 'website', 'num_of_reviews',
                              'business_category', 'food_rating', 'service_rating', 'value_rating', 'atmosphere_rating',
                              'average_prices', 'cuisine', 'meals', 'restaurant_features',
                              'good_for', 'hours', 'ratings', 'image_url', 'url']
        csv_header = {'name': 'Restaurant Name',
                      'address': 'Address',
                      'country': 'Country',
                      'phone': 'Phone',
                      'email': 'Email',
                      'website': 'Website',
                      'num_of_reviews': '# of reviews',
                      'business_category': 'Business Category',
                      'food_rating': 'Food Rating',
                      'service_rating': 'Service Rating',
                      'value_rating': 'Value',
                      'atmosphere_rating': 'Atmosphere',
                      'average_prices': 'Average Prices',
                      'cuisine': 'Cuisine',
                      'meals': 'Meals',
                      'restaurant_features': 'Restaurant features',
                      'good_for': 'Good for',
                      'hours': 'Open Hours',
                      'ratings': 'Trip Advisor Rating',
                      'image_url': 'Image URL',
                      'url': 'URL'}

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        # if os.path.exists('proxy.json'):
        #     with open('proxy.json', 'r+', encoding='utf-8') as f:
        #         # print(json.load(f))
        #         self.__proxies = json.load(f)

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__browser.close()
        self.__app.exit()
        del self.__browser
        del self.__app
        del self

    def grab_data_by_country(self, country_link=None, retry=0):
        try:
            if not country_link:
                country_link = self.__country_link
            self.__logger.info('=== Country URL: {} ==='.format(country_link))
            opener = self.__create_opener()
            data = opener.open(country_link).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data and retry < 10:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_data_by_country(country_link, retry + 1)

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            link_tags = soup.find_all('div', class_='geo_name')
            if link_tags and len(link_tags) > 0:
                for link_tag in link_tags:
                    link = link_tag.find('a')
                    if link:
                        url = self.__base_url + link.get('href')
                        self.grab_all_data(url)

            link_ul = soup.find('ul', class_='geoList')
            if link_ul:
                link_tags = link_ul.find_all('li')
                if link_tags and len(link_tags) > 0:
                    for link_tag in link_tags:
                        link = link_tag.find('a')
                        if link:
                            url = self.__base_url + link.get('href')
                            self.grab_all_data(url)

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

            next_page_tag = soup.find('a', class_='guiArw sprite-pageNext ')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

        except Exception as x:
            self.__logger.error('Error when grabbing data by country: {}'.format(x))
            if retry < 10:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_data_by_country(country_link, retry + 1)

    def grab_all_data(self, url):
        # with Pool(5) as p:
        #     r = p.map(self.f, [1, 2, 3])
        #     print(r)
        # return
        next_page = self.grab_data(url)
        while next_page:
            next_page = self.grab_data(next_page)

    def grab_from_file(self, in_file):
        if os.path.exists(in_file):
            with open(in_file, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)

                urls = []
                for row in reader:
                    if row['url'] not in self.__url_cache:
                        urls.append(row['url'])

                    if len(urls) == 1024:
                        with ThreadPool(32) as p:
                            p.map(self.grab_details, urls)
                        urls = []

                with ThreadPool(32) as p:
                    p.map(self.grab_details, urls)

    def grab_data(self, url, retry=0):
        try:
            # self.__grab_details(
            #     'https://www.tripadvisor.com/Restaurant_Review-g155019-d6688677-Reviews-Snakes_Lattes_College-Toronto_Ontario.html')
            # return
            self.__logger.info('=== Main URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            url_list = []

            # get all details link: process 1
            divs = soup.find_all('div', id=re.compile(r'^eatery_\d+$'))
            for div in divs:
                link_tag = div.find('a', class_='property_title')
                if link_tag:
                    link = self.__base_url + link_tag.get('href')
                    if link not in self.__url_cache and link not in url_list:
                        is_done = self.grab_details(link)
                        # url_list.append(link)
                    else:
                        self.__logger.warning('Link: {} already grabbed.'.format(link))

            # get all details link: process 2
            divs = soup.find_all('div', class_='attraction_element')
            for div in divs:
                link_tag = div.find('div', class_='listing_title ')
                if link_tag:
                    link_tag_a = link_tag.find('a')
                    if link_tag_a:
                        link = self.__base_url + link_tag_a.get('href')
                        if link not in self.__url_cache and link not in url_list:
                            is_done = self.grab_details(link)
                            # url_list.append(link)
                        else:
                            self.__logger.warning('Link: {} already grabbed.'.format(link))

            with ThreadPool(32) as p:
                # with Pool(16) as p:
                p.map(self.grab_details, url_list)

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                return self.__base_url + next_page_tag.get('href').strip()
        except Exception as x:
            self.__logger.error('Error when grab details page.{}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_data(url, retry + 1)

    @staticmethod
    def f(x):
        return x * x

    def grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            address = ''
            phone = ''

            item = {'url': url}
            name_tag = soup.find('h1', id='HEADING')
            if name_tag:
                item['name'] = name_tag.text.strip()
                print(item['name'])

            address_tag = soup.find('span', class_='format_address')
            if address_tag:
                item['address'] = address_tag.text.strip()

            json_tag = soup.find('script', {'type': 'application/ld+json'})
            if json_tag:
                json_data = json.loads(json_tag.text)
                if 'address' in json_data and 'addressCountry' in json_data['address'] and 'name' in \
                        json_data['address']['addressCountry']:
                    item['country'] = json_data['address']['addressCountry']['name']

                if 'address' in json_data and ('address' not in item or item['address'] == ''):
                    address_list = []
                    if 'streetAddress' in json_data['address']:
                        address_list.append(json_data['address']['streetAddress'])
                    if 'streetAddress' in json_data['address']:
                        address_list.append(json_data['address']['streetAddress'])
                    if 'addressLocality' in json_data['address']:
                        address_list.append(json_data['address']['addressLocality'])
                    if 'addressRegion' in json_data['address']:
                        address_list.append(json_data['address']['addressRegion'])
                    if 'postalCode' in json_data['address']:
                        address_list.append(json_data['address']['postalCode'])

                    if 'country' in item:
                        address_list.append(item['country'])
                    item['address'] = ', '.join(address_list)

                    if 'aggregateRating' in json_data:
                        if 'ratingValue' in json_data['aggregateRating']:
                            item['ratings'] = json_data['aggregateRating']['ratingValue']
                        if 'reviewCount' in json_data['aggregateRating']:
                            item['num_of_reviews'] = json_data['aggregateRating']['reviewCount']

                if 'image' in json_data:
                    item['image_url'] = json_data['image']
                    #     image_file = image_url.split('/')[-1]
                    #
                    #     try:
                    #         urllib.request.urlretrieve(image_url, './images/{}'.format(image_file))
                    #     except Exception as ex:
                    #         print('Error when download image from {}.'.format(image_url))
                    #         print(ex)

            phone_tag = soup.find('div', class_='blEntry phone')
            if phone_tag and not 'Add phone number' in phone_tag.text:
                phone = phone_tag.text

            if phone == '':
                phone_tag = soup.find('div', class_='blEntry phone directContactInfo')
                if phone_tag and not 'Add phone number' in phone_tag.text:
                    phone = phone_tag.text
            item['phone'] = phone

            email_tag = soup.find('span', class_='ui_icon email')
            if email_tag:
                email_link_tag = email_tag.find_next_sibling('a')
                if email_link_tag:
                    email = email_link_tag.get('href')
                    email = email.replace('mailto:', '')
                    item['email'] = email.strip()

            # menu_tab = soup.find('div', id='RESTAURANT_MENU')
            # if menu_tab:
            #     menus = []
            #     menu_tags = menu_tab.find_all('div', class_=re.compile(r'(?:^menuItemTitle$)|(?:^menuItem$)'))
            #     for menu_tag in menu_tags:
            #         menus.append(menu_tag.text.strip())
            #     menu = ', '.join(menus)
            #
            business_category_tag = soup.find('span', class_='header_detail attraction_details')
            if business_category_tag:
                business_category = business_category_tag.text.strip()
                business_category = business_category.replace('More', '')
                business_category = business_category.strip()
                item['business_category'] = business_category.strip(',')

            rows = soup.find_all('div', class_='row')
            if rows and len(rows) > 0:
                for row in rows:
                    row_title = row.find('div', class_='title')
                    if not row_title:
                        continue

                    row_content = row.find('div', class_='content')
                    if not row_content:
                        continue

                    if 'Cuisine' in row_title.text:
                        item['cuisine'] = row_content.text.strip()

                    if 'Average prices' in row_title.text:
                        item['average_prices'] = row_content.text.strip().replace('\n', '').strip()

                    if 'Good for' in row_title.text:
                        item['good_for'] = row_content.text.strip()

                    if 'Restaurant features' in row_title.text:
                        item['restaurant_features'] = row_content.text.strip()

                    if 'Meals' in row_title.text:
                        item['meals'] = row_content.text.strip()

            hours_tag = soup.find('div', class_='hours content')
            if hours_tag:
                item['hours'] = hours_tag.text.strip()

            ratings_tags = soup.find_all('div', class_='ratingRow wrap')
            if ratings_tags and len(ratings_tags) > 0:
                for ratings_tag in ratings_tags:
                    # print(ratings_tag)
                    label = ratings_tag.find('div', class_='label part ')
                    if not label:
                        continue

                    rating_val_tag = ratings_tag.find('div', class_='wrap row part ')
                    if not rating_val_tag:
                        continue

                    rating_val = rating_val_tag.find('span')
                    if not rating_val:
                        continue

                    if not rating_val.has_attr('alt'):
                        continue

                    rat = rating_val.get('alt')
                    rat = rat.replace('of 5 bubbles', '')
                    rat = rat.strip()

                    if 'Food' in label.text:
                        item['food_rating'] = rat

                    if 'Service' in label.text:
                        item['service_rating'] = rat

                    if 'Value' in label.text:
                        item['value_rating'] = rat

                    if 'Atmosphere' in label.text:
                        item['atmosphere_rating'] = rat

            web_tag = soup.find('div', class_='blEntry website')
            if web_tag and web_tag.has_attr('data-ahref') and len(web_tag.get('data-ahref').strip()) > 0:
                item['website'] = self.grab_web_url(url)
                self.__logger.info('Web site: {}'.format(item['website']))
            else:
                self.__logger.debug('No web address present for {}'.format(url))

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()
            return True
        except Exception as x:
            self.__logger.error('Error when grab details page.{}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_details(url, retry + 1)

    def grab_web_url(self, url, retry=0):
        try:
            self.__lock.acquire()
            web_url, data = self.__browser.fetch(url, self.__logger)
            return web_url if web_url and web_url.startswith('http') else ''
        except Exception as x:
            self.__logger.error('Error when getting web site url: {}'.format(x))
            if retry < 5:
                self.__lock.release()
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_details(url, retry + 1)
        finally:
            self.__lock.release()
        return ''

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            pass
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    # country_link = input('Please specify Country URL: ')
    # output_file = input('Please specify output file name: ')


    # country_link = 'https://www.tripadvisor.com/Restaurants-g155019-Toronto_Ontario.html'
    # output_file = 'tripadvisor_output_modified.csv'
    # with TripAdvisorModifiedSpider(country_link, output_file) as spider:
    #     spider.grab_all_data('https://www.tripadvisor.com/Restaurants-g155019-Toronto_Ontario.html')


    # country_link = 'https://www.tripadvisor.com/Restaurants-g187275-Germany.html'
    # output_file = 'tripadvisor_output_germany_new.csv'
    # with TripAdvisorModifiedSpider(country_link, output_file) as spider:
    #     spider.grab_data_by_country(country_link)
    # spider.grab_all_data('https://www.tripadvisor.com/Restaurants-g155019-Toronto_Ontario.html')


    # country_link = 'https://www.tripadvisor.com/Restaurants-g189806-Sweden.html'
    # output_file = 'tripadvisor_output_sweden.csv'
    # with TripAdvisorModifiedSpider(country_link, output_file) as spider:
    #     # spider.grab_from_file('tripadvisor_output_sweden_bak.csv')
    #     spider.grab_data_by_country(country_link)


    # country_link = 'https://www.tripadvisor.in/Restaurants-g191-United_States.html'
    # output_file = 'tripadvisor_output_USA.csv'
    # with TripAdvisorModifiedSpider(country_link, output_file) as spider:
    #     # spider.grab_from_file('tripadvisor_output_sweden_bak.csv')
    #     spider.grab_data_by_country(country_link)


    # country_link = 'https://www.tripadvisor.in/Restaurants-g147395-Turks_and_Caicos.html'
    # output_file = 'tripadvisor_output_Caicos.csv'
    # with TripAdvisorModifiedSpider(country_link, output_file) as spider:
    #     spider.grab_all_data('https://www.tripadvisor.in/Restaurants-g147395-Turks_and_Caicos.html')
    #     # spider.grab_from_file('tripadvisor_output_sweden_bak.csv')
    #     # spider.grab_data_by_country(country_link)


    country_link = 'https://www.tripadvisor.fr/Restaurants-g187070-France.html'
    output_file = 'tripadvisor_output_France.csv'
    with TripAdvisorModifiedSpider(country_link, output_file) as spider:
        spider.grab_data_by_country(country_link)
        # spider.grab_all_data('https://www.tripadvisor.fr/Restaurants-g187070-France.html')
