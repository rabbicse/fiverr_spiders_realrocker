import random

import requests
from bs4 import BeautifulSoup
import os
import urllib
import openpyxl
from openpyxl import Workbook
import numpy as np
import pandas as pd

# proxies = ['108.59.14.208:13040', '108.59.14.203:13040']
proxies = ['35.189.86.114:3128']  # , '50.207.31.213:80']


def get_random_proxy():
    try:
        if not proxies or len(proxies) == 0:
            return

        proxy_url = str(random.choice(proxies))

        # if not proxy_url.startswith('http://'):
        #     proxy_url = 'http://' + proxy_url

        return {
            "https": proxy_url,
            # "https": proxy_url
        }
    except Exception as x:
        print('Error when get random proxies. {}'.format(x))


# Product name	Category Style	Price Size	Weight	Color	Material	Large Image link	Embed code (large image)	Embed code (small image)	Product Description	Manufacturer

def create_record(sheet, Product, Category, Style, Price, Size, Weight, Color, Material, Prd_url, sml_img_url,
                  Image_link, L_Embed_link, S_Embed_link, Description, Manufacturer):
    sheet[sheet.columns.values[0]] = Product
    sheet[sheet.columns.values[1]] = Category
    sheet[sheet.columns.values[2]] = Style
    sheet[sheet.columns.values[3]] = Price
    sheet[sheet.columns.values[4]] = Size
    sheet[sheet.columns.values[5]] = Weight
    sheet[sheet.columns.values[6]] = Color
    sheet[sheet.columns.values[7]] = Material
    sheet[sheet.columns.values[8]] = Prd_url
    sheet[sheet.columns.values[9]] = sml_img_url
    sheet[sheet.columns.values[10]] = Image_link
    sheet[sheet.columns.values[11]] = L_Embed_link
    sheet[sheet.columns.values[12]] = S_Embed_link
    sheet[sheet.columns.values[13]] = Description
    sheet[sheet.columns.values[14]] = Manufacturer
    return sheet


# ------------------
# ---------------------------------
pwd = os.getcwd()
path = pwd + '/' + 'Houzzdatabase.xlsx'
print(path)
# ----------------------------------
print("Loading------")
sp = pd.ExcelFile(path)
sheet = sp.parse('Sheet1')

for s in sheet:
    print('\'\'' + ': ' + '\'' + s +'\',')
print(sheet)
pd.options.mode.chained_assignment = None
sheet.insert(8, 'Product URL', 'http')
sheet.insert(9, 'small image URL', 'http')
df = pd.DataFrame(sheet.head(0))
from openpyxl import load_workbook

book = load_workbook(path)
writer = pd.ExcelWriter('HouzzDB.xlsx', engine='openpyxl')
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
# print(dict((ws.title, ws) for ws in book.worksheets))
print(book.worksheets[0].columns.values)
# base_url = 'https://www.houzz.in/product/1740968-ss-1102-asian-kitchen-taps'
# max_cnt = 120
# pn = 133
product_types = ['beds-and-headboards', 'rugs', 'decorative-accents']
# product_types = ['home-decor','rugs', 'decorative-accents', 'pillows-and-throws', 'artwork', 'fabric', 'mirrors', 'lamps', 'wall-deco', 'clocks', 'window-treatments', '', '', '', \
# 				'furniture', 'living-room-furniture', 'kitchen-and-dining-furniture', 'bedroom-furniture', 'home-office-furniture', 'entryway-furniture', 'bathroom-storage-and-vanities', 'storage-furniture', 'outdoor-furniture', 'kids-furniture', '', '', '', '', '', '', '', ''
# 				'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', \
# 				'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', \
# 				'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', \
# 				'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', \
# 				'', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
page = 100
for sub_type in product_types:
    print("\n----------------- [ " + sub_type + " ] ------------------\n")
    url = 'https://www.houzz.com/photos/' + sub_type + '/ls=2' + '/p/100'
    print(url)
    base_url = url
    proxy = get_random_proxy()
    r = requests.get(base_url, proxies=proxy)
    data = r.text
    soup1 = BeautifulSoup(data, "lxml")
    items = soup1.select(
        '#hz-page-content-wrapper > div > div.hz-br-container__resultset > div.hz-card.clearfix > div.hz-br-container.hz-spf-animation-container.hz-br-container__products')[
        0]
    z = 0
    for item in items.find_all('a'):
        z += 1
        if (z % 2 == 0) and z == 2:
            prd_url = item['href']
            print(item['href'])
            try:
                # -------- Product details fetching loop ------------ #
                proxy = get_random_proxy()
                r = requests.get(item['href'], proxies=proxy)
                data = r.text
                soup = BeautifulSoup(data, "lxml")
                product = soup.select('#hz-page-content-wrapper > div > div.container.view-product > aside > h1')[
                    0].string
                Specifications = soup.select(
                    '#hz-page-content-wrapper > div > div.container.view-product > aside > div.view-product-specs.hz-track-me > div > div.hz-peekable__mask > div > div > dl')
                print("Product : {}".format(product))
                # print category
                Categ = ''
                Style = ''
                size = ''
                height = ''
                manufacturer = ''

                i = 0
                try:
                    for dd in Specifications[0].find_all('dd'):
                        i += 1
                        Style = ''
                        if i == 1:
                            manufacturer = dd.find('a').string
                            print('Manufacturer : ', manufacturer)
                        elif i == 2:
                            Categ = dd.find('a').string
                            print('category : ', Categ)
                        elif i == 3:
                            Style = dd.find('a').string
                            print('Style : ', Style)

                except:
                    print("No Specifications Available!!")
                    manufacturer = ''
                    Categ = ''
                    Style = ''

                ol_link = soup.select('#hz-page-content-wrapper > div > div.container.view-product > article > ol')[0]
                ul_categ = [ul['href'] for ul in ol_link.find_all('a')]
                try:
                    price = soup.select(
                        '#hz-page-content-wrapper > div > div.container.view-product > aside > div.row.hidden-print.ptxl > div > span')[
                        0]['content']
                    print("Price : {}".format(price))
                except:
                    print("Price Not Available!!")
                    price = ''
                features = soup.select(
                    '#hz-page-content-wrapper > div > div.container.view-product > aside > div.hz-product-description.view-product-description.hidden-print > ul')
                j = 0
                try:
                    for li in features[0].find_all('li'):
                        j += 1
                        if j == 1:
                            size = li.string.split('- ')[1].replace(',', '')
                            print("Size : ", size)
                        if j == 2:
                            height = li.string.split('- ')[1].replace(',', '')
                            print("Height : " + height)
                except:
                    print("No Features Available!!")
                    size = ''
                    height = ''
                description = soup.select(
                    '#hz-page-content-wrapper > div > div.container.view-product > aside > div.hz-product-description.view-product-description.hidden-print')[
                    0].get_text()
                image_link = soup.select(
                    '#hz-page-content-wrapper > div > div.container.view-product > article > div.view-product-image__container.prxl > div > img')[
                    0]['src']
                lr_stg = ''' ' border=0 width='500' height='868' nopin='nopin' ondragstart='return false;' onselectstart='return false;' oncontextmenu='return false;'/></a></div><div style='color:#444;'><small><a style="text-decoration:none;color:#444;" href=" '''
                sm_stg = ''' ' border=0 width='184' height='320' nopin='nopin' ondragstart='return false;' onselectstart='return false;' oncontextmenu='return false;'/></a></div><div style='color:#444;'><small><a style="text-decoration:none;color:#444;" href=" '''
                large_embed_links = "<div><a href='" + base_url + "' target='_blank'><img src='" + image_link + "' alt='" + product + lr_stg + \
                                    ul_categ[-1] + '" target="_blank">kitchen faucets</a></small></div>'
                small_embed_links = "<div><a href='" + base_url + "' target='_blank'><img src='" + image_link + "' alt='" + product + sm_stg + \
                                    ul_categ[-1] + '" target="_blank">kitchen faucets</a></small></div>'
                ''' ' alt='SS-1102' border=0 width='500' height='868' nopin='nopin' ondragstart='return false;' onselectstart='return false;' oncontextmenu='return false;'/></a></div><div style='color:#444;'><small><a style="text-decoration:none;color:#444;" href="https://www.houzz.in/photos/kitchen-faucets" target="_blank">kitchen faucets</a></small></div>'''
                print(image_link)
                print(description)
                Material = ''
                Color = ''
                sheet = create_record(sheet, Product=product, Category=Categ, Style=Style, Price=price, Size=size,
                                      Weight=height, Color=Color, Material=Material, Prd_url=prd_url,
                                      sml_img_url=prd_url, Image_link=image_link, L_Embed_link=large_embed_links,
                                      S_Embed_link=small_embed_links, Description=description,
                                      Manufacturer=manufacturer)
                df = df.append(sheet)
                df.to_excel(writer, sheet_name='Sheet1', index=False)
                writer.save()
                print("Saved!!!")
                print("\n")


            except Exception as e:
                print("Failed to fetch the Item !!")
