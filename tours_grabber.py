import csv
import gzip
import re
import socket
import string
import urllib.request
import urllib.parse
from multiprocessing import Lock, Pool
import time
from bs4 import BeautifulSoup


class ToursGrabber:
    __lock = Lock()
    __sitemap_url = 'http://www.tours.com/sitemapindex.xml'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}

    def __init__(self, output_csv):
        self.__output_csv = output_csv
        socket.setdefaulttimeout(120)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            csv_header = ['Name', 'Country', 'Email', 'Website', 'URL']
            self.__write_data(csv_header)
            # for sitemap_url in self.__grab_sitemap():
            for details_url in self.__grab_sitemap_details('http://www.tours.com/sitemap11.xml.gz'):
                self.__grab_details(details_url)
                # categories = self.__read_category()
                # print(categories)
                # regions = self.__read_region()
                # print(regions)
                #
                # for category in categories:
                #     for region in regions:
                #         url = self.__main_url.format(category, region)
                #         self.__grab_details(url)
                # grab category
                # self.__grab_category_list()

                # grab regions
                # self.__grab_regions()

                # grab details
                # url = 'https://www.localsearch.com.au/find/plumbers/nt/alice-springs-region'
                # self.__grab_details(url)


                # csv_header = ['Name', 'Email', 'URL']
                # self.__write_data(csv_header)
                # for i in range(1, self.MAX_PAGE, 8):
                #     # start 4 worker processes
                #     with Pool(processes=8) as pool:
                #         pool.map(self.pool_data, range(i, i + 8))
        except Exception as x:
            print(x)

    def __grab_sitemap(self):
        try:
            print('=== Sitemap URL: {} ==='.format(self.__sitemap_url))
            req = urllib.request.Request(self.__sitemap_url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            sitemaps = soup.find_all('loc')
            for sitemap in sitemaps:
                yield sitemap.text.strip()

        except Exception as x:
            print(x)

    def __grab_sitemap_details(self, url):
        try:
            print('=== Sitemap Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            sitemaps = soup.find_all('loc')
            for sitemap in sitemaps:
                tmp_url = sitemap.text.strip()
                if 'tour_operators' not in tmp_url:
                    continue
                # print(tmp_url)
                yield tmp_url

        except Exception as x:
            print(x)

    def __grab_details(self, url):
        try:
            print('=== Main URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            main_content = soup.find('div', class_='ltbdr')
            if not main_content:
                return

            info_list = main_content.find_all('div')

            name, country, email, web = '', '', '', ''
            for info in info_list:
                # print(info)
                if 'Tour Operator:' in str(info):
                    name = info.text.replace('Tour Operator:', '')
                    name = name.strip()
                if 'COMPANY HQ:' in str(info):
                    country = info.text.replace('COMPANY HQ:', '')
                    country = country.strip()
                if 'Email:' in str(info):
                    email = info.text.replace('Email:', '')
                    email = email.replace('mailto:', '')
                    email = email.strip()
                if 'Website:' in str(info):
                    web = info.text.replace('Website:', '')
                    web = web.strip()

            csv_data = [name, country, email, web, url]
            print(csv_data)
            self.__write_data(csv_data)

            # for article in article_list:
            #     try:
            #         name, link, email = '', '', ''
            #         h3_tag = article.find('h3', {'itemprop': 'name'})
            #         if h3_tag:
            #             name = h3_tag.text.strip()
            #             link_tag = h3_tag.find_parent('a')
            #             if link_tag:
            #                 link = self.__base_url + str(link_tag.get('href')).strip()
            #         email_link = article.find('link', {'itemprop': 'email'})
            #         if email_link:
            #             email = email_link.get('href').strip()
            #             email = re.sub(r'mailto\:', '', email)
            #
            #         csv_data = [name, email, link]
            #         print(csv_data)
            #         self.__write_data(csv_data)
            #     except Exception as ex:
            #         print(ex)
        except Exception as x:
            print(x)

    def pool_data(self, page):
        try:
            url = self.__base_url.format(page)
            print('=== URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            result_div = soup.find('div', class_='search-results clearfix column')
            if not result_div:
                return

            results = result_div.find_all('div', class_='results-block')
            for result in results:
                try:
                    name, email, url = '', '', ''
                    title_tag = result.find('h3', class_='panel-title')
                    if title_tag:
                        tag = title_tag.find_all('a')[-1]
                        name = tag.text.strip()
                        url = tag.get('href')
                    email_m = re.search(r'<a href="mailto\:([^"]*?)"', str(result), re.MULTILINE)
                    if email_m:
                        email = email_m.group(1)

                    csv_data = [name, email, url]
                    print(csv_data)
                    self.__write_data(csv_data)
                except Exception as ex:
                    print(ex)
        except Exception as x:
            print(x)

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)
        except Exception as x:
            print('Error printing csv output: ' + str(x))
        finally:
            self.__lock.release()

    @staticmethod
    def __write_category(category):
        try:
            with open('category.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __write_region(category):
        try:
            with open('region.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __read_category():
        try:
            categories = []
            with open('category.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    category = line.strip()
                    categories.append(category)
            return categories
        except Exception as x:
            print(x)

    @staticmethod
    def __read_region():
        try:
            regions = []
            with open('region.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    region = line.strip()
                    regions.append(region)
            return regions
        except Exception as x:
            print(x)


if __name__ == '__main__':
    with ToursGrabber('tours_output.csv') as spider:
        spider.grab_data()
