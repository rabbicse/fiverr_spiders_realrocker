from logging.handlers import RotatingFileHandler
import multiprocessing, threading, logging, sys, traceback
import os

class MultiProcessingLog(logging.Handler):
    def __init__(self, name, mode, maxsize, rotate):
        logging.Handler.__init__(self)

        self._handler = RotatingFileHandler(name, mode, maxsize, rotate)
        self.queue = multiprocessing.Queue(-1)

        t = threading.Thread(target=self.receive)
        t.daemon = True
        t.start()

    def setFormatter(self, fmt):
        logging.Handler.setFormatter(self, fmt)
        self._handler.setFormatter(fmt)

    def receive(self):
        while True:
            try:
                record = self.queue.get()
                self._handler.emit(record)
                print('received on pid {}'.format(os.getpid()))
            except (KeyboardInterrupt, SystemExit):
                raise
            except EOFError:
                break
            except:
                traceback.print_exc(file=sys.stderr)

    def send(self, s):
        self.queue.put_nowait(s)

    def _format_record(self, record):
        # ensure that exc_info and args have been stringified. Removes any
        # chance of unpickleable things inside and possibly reduces message size
        # sent over the pipe
        if record.args:
            record.msg = record.msg % record.args
            record.args = None
        if record.exc_info:
            dummy = self.format(record)
            record.exc_info = None

        return record

    def emit(self, record):
        try:
            s = self._format_record(record)
            self.send(s)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def close(self):
        self._handler.close()
        logging.Handler.close(self)


import logging
from multiprocessing import Pool
import os

# logger = logging.getLogger(__name__)


# def test(value):
#     msg = '[{}] value {}'.format(os.getpid(), value)
#     logger.info(msg)


class TestPool:
    def __init__(self):
        self.logger = MultiProcessingLog('log.log', 'a', 2 * 1024, 30)
        # self.logger = logging.getLogger(__name__)
        # self.logger.setLevel(logging.DEBUG)
        #
        # # create formatter and add it to the handlers
        # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        #
        # # create file handler which logs even debug messages
        # fh = RotatingFileHandler('log.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        # fh.setLevel(logging.INFO)
        # fh.setFormatter(formatter)
        #
        # # create console handler with a higher log level
        # ch = logging.StreamHandler()
        # ch.setLevel(logging.INFO)
        # ch.setFormatter(formatter)
        #
        # # add the handlers to the logger
        # self.logger.addHandler(fh)
        # self.logger.addHandler(ch)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def do_pool(self):
        with Pool(5) as pool:
            pool.map(self.do_process, range(1000))

    def do_process(self, n):
        try:
            # print(n)
            self.logger.info(n)
        except:
            pass


if __name__ == '__main__':
    # import logging.config
    # import yaml

    # path = 'logging.yaml'
    # if os.path.exists(path):
    #     with open(path, 'rt') as f:
    #         config = yaml.load(f.read())
    #     logging.config.dictConfig(config)

    # logger.setLevel(logging.DEBUG)
    #
    # # create formatter and add it to the handlers
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    #
    # # create file handler which logs even debug messages
    # fh = RotatingFileHandler('log.log', maxBytes=2 * 1024 * 1024, backupCount=5)
    # fh.setLevel(logging.INFO)
    # fh.setFormatter(formatter)
    #
    # # create console handler with a higher log level
    # ch = logging.StreamHandler()
    # ch.setLevel(logging.INFO)
    # ch.setFormatter(formatter)
    #
    # # add the handlers to the logger
    # logger.addHandler(fh)
    # logger.addHandler(ch)

    # test('begin')
    #
    # p = Pool(4)
    # p.map(test, [1, 2, 3, 4])
    #
    # test('end')

    with TestPool() as tp:
        tp.do_pool()
