# -*- coding: utf-8 -*-
import csv
import json
import logging
import os
import re
import time
import urllib.parse
import uuid
from collections import OrderedDict
from multiprocessing import Semaphore, Value
from multiprocessing.pool import ThreadPool
import random

import requests
from easy_spider import Spider

logger = logging.getLogger(__name__)


class UrlGeneratorUpdated(Spider):
    """
    nohup python3.6 url_generator.py >/dev/null 2>&1 &
    """
    # &t=[1-100]
    __proxy_uri = 'http://api.scraperapi.com'
    __proxy_key = 'a075e0ac04ccbe07119123932dba1e47'
    __search_url = 'http://www.upc.ch/services/commerce-services.checkServiceAbility?rfsStreetName={}&rfsStreetNo={}&rfsCityZip={}'
    __api_url = 'https://apishop.business.upc.ch/v1/footprintcheck'
    __url_cache = []
    __page_cache = []
    __total = Value('i', 0)
    __lock = Semaphore()
    __MIN_COUNT = 500
    __MAX_THREAD = 1
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'

    # HEADERS = {
    #     'User-Agent': __USER_AGENT,
    #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    #     'Accept-Encoding': 'gzip, deflate, br',
    #     # 'Accept-Language': 'en-US,en;q=0.9',
    #     'Connection': 'keep-alive',
    #     # 'Pragma': 'no-cache',
    #     # 'Cache-Control': 'no-cache',
    #     'Upgrade-Insecure-Requests': '1'
    # }

    def __init__(self, input_csv, output_csv):
        Spider.__init__(self, log_file='url_processor.log')
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv
        self.MAX_RETRY = 20
        self.TIMEOUT = 60
        self.HTTP_RETRY_CODES = [302, 303, 400, 403, 404, 408, 413, 500, 502, 503, 504]

    def __enter__(self):
        self.__init_full()
        return self

    def __init_full(self):
        hdr = [('id', 'ID'),
               ('street_id', 'Street ID'),
               ('street_name', 'Street Name'),
               ('zip', 'Zip'),
               ('availableServices', 'availableServices'),
               ('status', 'status'),
               ('value', 'value'),
               ('street', 'street'),
               ('streetNo', 'streetNo'),
               ('postCode', 'postCode'),
               ('city', 'city'),
               ('addressId', 'addressId'),
               ('optedAddress', 'optedAddress'),
               ('tauglichkeitsId', 'tauglichkeitsId'),
               ('admId', 'admId'),
               ('partnerNetworkType', 'partnerNetworkType'),
               ('productsAvailable', 'productsAvailable'),
               ('guid', 'Guid'),
               ('url', 'Url'),
               ('original_address', 'Original Address'),
               ('original_get_url', 'Original Get URL'),
               ('original_get_response', 'Original Get Response'),
               ('multi_get_response', 'Multi Get Response'),
               ('post_param', 'Post Param'),
               ('post_param_multi', 'Post Param Multiple Address'),
               ('post_response_multi', 'Post Response Multiple Address')]
        # ('id_per_zip', '#id per ZIP')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+') as f:
                reader = csv.DictReader(f, self.__field_names, dialect='excel', delimiter=';')
                for row in reader:
                    self.__url_cache.append(row['id'])

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        self.__prepare_proxies()

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

    def __prepare_proxies(self):
        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.proxies = json_data

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Total records: {}'.format(self.__total.value))
        logger.info('Finish!!!')
        del self

    def process_data(self):
        try:

            # if os.path.exists(self.__input_csv):
            #     with open(self.__input_csv, 'r+') as f:
            #         reader = csv.DictReader(f, self.__field_names, dialect='excel', quoting=csv.QUOTE_ALL)
            #         for row in reader:
            #             req = {'id': row['id'],
            #                    'zip': row['zip'],
            #                    'street_id': row['street_id'],
            #                    'street_name': row['street_name'],
            #                    'id_per_zip': ''}
            #             self.__process_address(req)
            # return
            # response = self.fetch_data('http://api.scraperapi.com?api_key=2e26ff19882689ef2ecf317e3956c388&url={}'.format('http://www.upc.ch/services/commerce-services.checkServiceAbility?rfsStreetName=Imfangstrasse&rfsStreetNo=6&rfsCityZip=6005'))
            # print(response)
            # return
            if os.path.exists(self.__input_csv):
                search_requests = []
                with open(self.__input_csv, 'r+') as f:
                    reader = csv.reader(f, quoting=csv.QUOTE_ALL, delimiter=';')
                    i = 0
                    for row in reader:
                        if i == 0 or len(row) < 4:
                            i += 1
                            continue

                        # if row[0].strip() in self.__url_cache:
                        #     logger.warning('ID: {} already exists!'.format(row[0]))
                        #     continue

                        req = {'id': row[0].strip(),
                               'zip': row[3].strip()}

                        if re.match(r'^[0-9\.]+.*$', row[1].strip()):
                            req['street_id'] = row[1].strip()
                            req['street_name'] = row[2].strip()
                        elif re.match(r'^[0-9\.]+.*$', row[2].strip()):
                            req['street_id'] = row[2].strip()
                            req['street_name'] = row[1].strip()
                        else:
                            req['street_id'] = row[1].strip()
                            req['street_name'] = row[2].strip()

                        req['street_id'] = re.sub(r'\.', '', req['street_id'])

                        req['street_name'] = str(req['street_name']).encode('utf-8').decode('utf-8')
                        if len(row) > 4:
                            req['id_per_zip'] = row[4]
                        else:
                            req['id_per_zip'] = ''

                        search_requests.append(req)
                        break
                        i += 1
                        if len(search_requests) % 1024 == 0:
                            with ThreadPool(self.__MAX_THREAD) as pool:
                                pool.map(self.__process_address, search_requests)
                                search_requests = []

                with ThreadPool(self.__MAX_THREAD) as pool:
                    pool.map(self.__process_address, search_requests)
        except Exception as x:
            logger.error('Error when process data: {}'.format(x))

    def __process_address(self, req):
        try:
            logger.info('Request address: {}'.format(req))
            search_uri = self.__search_url.format(urllib.parse.quote_plus(req['street_name']),
                                                  req['street_id'],
                                                  req['zip'])

            if search_uri in self.__url_cache:
                logger.warning('URL: {} already exists!'.format(search_uri))
                return

            # get req
            response = self.__get_address(req)
            if not response:
                return

            return
            if 'status' not in response:
                return

            guid = str(uuid.uuid4())
            item = {'guid': guid,
                    'id': req['id'],
                    'street_id': req['street_id'],
                    'street_name': req['street_name'],
                    'zip': req['zip'],
                    'original_get_url': search_uri}
            if response['status'] == 'MULTI_ADDRESSES':
                post_req_multi = {'guid': guid}
                if 'postCode' in response:
                    post_req_multi['zip'] = response['postCode']

                if 'city' in response:
                    post_req_multi['city'] = response['city']

                if 'street' in response:
                    post_req_multi['street'] = response['street']

                if 'streetNo' in response:
                    post_req_multi['streetNumber'] = response['streetNo']

                if 'optedAddress' in response:
                    post_req_multi['fullstreet'] = response['optedAddress']

                item['post_param_multi'] = str(post_req_multi)
                item['original_address'] = False

                itm = item.copy()
                for k, v in dict(response).items():
                    if k in self.__field_names:
                        itm[k] = v
                itm['original_address'] = True
                post_response_multi = self.__post_address(post_req_multi)
                if post_response_multi:
                    item['post_response_multi'] = str(post_response_multi)
                    if post_response_multi:
                        for k, v in dict(post_response_multi).items():
                            if k in itm.keys():
                                continue
                            if k in self.__field_names:
                                itm[k] = v
                itm['multi_get_response'] = str(response)
                self.__write_item(itm)

                # another get request
                if 'street' in response:
                    req['street_name'] = response['street']
                if 'streetNo' in response:
                    req['street_id'] = response['streetNo']
                if 'postCode' in response:
                    req['zip'] = response['postCode']

                response = self.__get_address(req)
                if not response:
                    return
                if response['status'] != 'MULTI_ADDRESSES':
                    for k, v in dict(response).items():
                        if k in self.__field_names:
                            item[k] = v
                    item['original_address'] = False
                else:
                    return
            else:
                for k, v in dict(response).items():
                    if k in self.__field_names:
                        item[k] = v
                item['original_address'] = True

            post_req = {'guid': guid}
            if 'postCode' in response:
                post_req['zip'] = response['postCode']

            if 'city' in response:
                post_req['city'] = response['city']

            if 'street' in response:
                post_req['street'] = response['street']

            if 'streetNo' in response:
                post_req['streetNumber'] = response['streetNo']

            if 'optedAddress' in response:
                post_req['fullstreet'] = response['optedAddress']

            post_response = self.__post_address(post_req)
            if post_response:
                for k, v in dict(post_response).items():
                    if k in item.keys():
                        continue
                    if k in self.__field_names:
                        item[k] = v

            item['post_param'] = str(post_req)
            item['original_get_response'] = str(response)
            # item['id_per_zip'] = req['id_per_zip']
            self.__write_item(item)
        except Exception as x:
            logger.error('Error when parse details page: {}'.format(x))

    def __get_address(self, req, retry=0):
        try:
            search_uri = self.__search_url.format(urllib.parse.quote_plus(req['street_name']),
                                                  req['street_id'],
                                                  req['zip'])

            # payload = {'api_key': self.__proxy_key,
            #            'url': search_uri}

            logger.info('Get address: {}'.format(search_uri))

            resp = requests.get(
                'http://falcon.proxyrotator.com:51337/?apiKey=rB9fvLQjuJ6HX3cTRGMtwsVhPmFxZ4Ub&country=HU')
            data = json.loads(resp.text)
            # {'proxy': '2.139.187.123:3128', 'ip': '2.139.187.123', 'port': '3128', 'connectionType': 'Residential', 'asn': '12430', 'isp': 'Vodafone Spain', 'type': 'elite', 'lastChecked': 1562770762, 'get': True, 'post': True, 'cookies': True, 'referer': True, 'userAgent': True, 'city': 'Borox', 'state': 'TO', 'country': 'ES', 'randomUserAgent': 'Mozilla/5.0 (Android Mobile rv:29.0) Gecko/29.0 Firefox/29.0', 'requestsRemaining': 49}

            print(data)
            if 'proxy' not in data:
                return

            HEADERS = {
                'User-Agent': data['randomUserAgent']}
            response = requests.get(search_uri, proxies={'https': 'https://{}'.format(data['proxy'])}, headers=HEADERS,
                                    timeout=self.TIMEOUT)
            print(response.status_code)
            if not response:
                print('not ok...1')
                return

            json_data = json.loads(response.text)
            if not json_data:
                print('not ok...2')
                return

            logger.info('Get Data: {}'.format(json_data))
            if json_data['status'] == 'MULTI_ADDRESSES':
                itm = {'status': json_data['status'],
                       'street': json_data['street'],
                       'streetNo': json_data['streetNo'],
                       'postCode': json_data['postCode'],
                       'city': json_data['city'],
                       'addressId': json_data['addressId'],
                       'url': search_uri}
                return itm
            else:
                json_data['url'] = search_uri
                return json_data

        except Exception as x:
            logger.error('Error when get address details: {}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__get_address(req, retry=retry + 1)

    def __post_address(self, post_req, retry=0):
        try:
            logger.info('Post request: {}'.format(post_req))
            # response = self.fetch_data(self.__api_url, post_data=post_req)
            payload = {'api_key': self.__proxy_key,
                       'url': self.__api_url}

            response = requests.post(self.__proxy_uri, params=payload, data=post_req, timeout=self.TIMEOUT)
            if not response:
                return

            json_data = json.loads(response.text)
            if not json_data:
                return

            logger.info('Post Data: {}'.format(json_data))
            if 'data' not in json_data:
                return

            return json_data['data']
        except Exception as x:
            logger.error('Error when post address details: {}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__post_address(post_req, retry=retry + 1)

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL, dialect='excel',
                                        delimiter=';')
                writer.writerow(row)

                with self.__total.get_lock():
                    self.__total.value += 1
                    logger.info('Total: {}'.format(self.__total.value))
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    input_csv = '500k_input.csv'  # input('Please specify Input csv: ')
    output_csv = '500k_output.csv'  # input('Please specify output csv: ')

    # mode = input('Please specify Mode (1 for save all urls; 0 for parse details from previously saved pages): ')
    # input_csv = input('Please specify Input csv: ')
    # output_csv = input('Please specify output csv: ')
    with UrlGeneratorUpdated(input_csv, output_csv) as crawler:
        crawler.process_data()
