import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from datetime import datetime
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from bs4 import BeautifulSoup


class FootballSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url_hkjc = 'http://bet.hkjc.com'
    __start_url_hkjc = 'http://bet.hkjc.com/football/odds/odds_inplay.aspx?lang=EN'
    __match_details_url = 'http://bet.hkjc.com/football/odds/odds_allodds.aspx?lang=EN&tmatchid={}'
    __match_details_in_play_url = 'http://bet.hkjc.com/football/odds/odds_inplay_all.aspx?lang=EN&tmatchid={}'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 20
    # __output_csv_format = './football_output/{}{}_{}_{}_vs_{}.csv'
    __output_csv_format = '{}_{}_{}_{}_{}_vs_{}.csv'
    __match_hash = {}

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('football.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__lock = Lock()
        self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(30)

        hdr = [
            # ('match_start_time_yyyy_mm_dd_hh_mm_local_computer_time',
            #     'Match Start Time yyyy/mm/dd/hh:mm (local computer time)'),
            ('matchid_hkjc', 'MatchID_HKJC'),
            ('match_number_hkjc', 'MatchNumber_HKJC'),
            ('match_league_hkjc', 'MatchLeague_HKJC'),
            ('match_home_team_hkjc', 'MatchHomeTeam_HKJC'),
            ('match_away_team_hkjc', 'MatchAwayTeam_HKJC'),
            ('match_league_bet365', 'MatchLeague_bet365'),
            ('match_home_team_bet365', 'MatchHomeTeam_bet365'),
            ('match_away_team_bet365', 'MatchAwayTeam_bet365'),
            ('time_local_computer', 'TimeLocalComputer'),
            ('time_bet365', 'Time_bet365'),
            ('match_status_hkjc', 'MatchStatus_HKJC'),
            ('score_hkjc', 'Score_HKJC'),
            ('score_bet365', 'Score_bet365'),
            ('home_away_draw_home_odd_hkjc', 'HomeAwayDraw_HomeOdd_HKJC'),
            ('home_away_draw_draw_odd_hkjc', 'HomeAwayDraw_DrawOdd_HKJC'),
            ('home_away_draw_away_odd_hkjc', 'HomeAwayDraw_AwayOdd_HKJC'),
            ('hilo_line_1_hkjc', 'HiLo_Line1_HKJC'),
            ('hilo_line_1_high_odd_hkjc', 'HiLo_Line1_HighOdd_HKJC'),
            ('hilo_line_1_low_odd_hkjc', 'HiLo_Line1_LowOdd_HKJC'),
            ('hilo_line_2_hkjc', 'HiLo_Line2_HKJC'),
            ('hilo_line_2_high_odd_hkjc', 'HiLo_Line2_HighOdd_HKJC'),
            ('hilo_line_2_low_odd_hkjc', 'HiLo_Line2_LowOdd_HKJC'),
            ('hilo_line_3_hkjc', 'HiLo_Line3_HKJC'),
            ('hilo_line_3_high_odd_hkjc', 'HiLo_Line3_HighOdd_HKJC'),
            ('hilo_line_3_low_odd_hkjc', 'HiLo_Line3_LowOdd_HKJC'),
            ('total_corner_hkjc', 'TotalCorner_HKJC'),
            ('corner_line_1_hkjc', 'Corner_Line1_HKJC'),
            ('corner_line_1_high_odd_hkjc', 'Corner_Line1_HighOdd_HKJC'),
            ('corner_line_1_low_odd_hkjc', 'Corner_Line1_LowOdd_HKJC'),
            ('corner_line_2_hkjc', 'Corner_Line2_HKJC'),
            ('corner_line_2_high_odd_hkjc', 'Corner_Line2_HighOdd_HKJC'),
            ('corner_line_2_low_odd_hkjc', 'Corner_Line2_LowOdd_HKJC'),
            ('corner_line_3_hkjc', 'Corner_Line3_HKJC'),
            ('corner_line_3_high_odd_hkjc', 'Corner_Line3_High_Odd_HKJC'),
            ('corner_line_3_low_odd_hkjc', 'Corner_Line3_LowOdd_HKJC'),
            ('next_team_to_score_home_odd_hkjc', 'NextTeamtoScore_HomeOdd_HKJC'),
            ('next_team_to_score_no_goals_odd_hkjc', 'NextTeamtoScore_NoGoalsOdd_HKJC'),
            ('next_team_to_score_away_odd_hkjc', 'NextTeamtoScore_AwayOdd_HKJC'),
            ('home_team_corner_bet365', 'HomeTeam_Corner_bet365'),
            ('home_team_dangerous_attack_bet365', 'HomeTeam_DangerousAttack_bet365'),
            ('home_team_yellow_card_bet365', 'HomeTeam_YellowCard_bet365'),
            ('home_team_red_card_bet365', 'HomeTeam_RedCard_bet365'),
            ('away_team_corner_bet365', 'AwayTeam_Corner_bet365'),
            ('away_team_dangerous_attack_bet365', 'AwayTeam_DangerousAttack_bet365'),
            ('away_team_yellow_card_bet365', 'AwayTeam_YellowCard_bet365'),
            ('away_team_red_card_bet365', 'AwayTeam_RedCard_bet365')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # if not os.path.exists('./football_output/'):
        #     os.mkdir('./football_output/')

        if os.path.exists('./cache.json'):
            try:
                with open('cache.json', 'r+') as fp:
                    self.__match_hash = json.load(fp)
            except Exception as x:
                self.__logger.error('Error when load cache. Please delete cache.json file! \nError: {}'.format(x))

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:
            while True:
                self.__grab_data_hkjc()

                sleep_time = 15
                if os.path.exists('config.txt'):
                    with open('config.txt', 'r+', encoding='utf-8') as f:
                        txt = f.read().strip()
                        sleep_time = int(txt)

                with open('cache.json', 'w+', encoding='utf-8') as f:
                    # print(self.__match_hash)
                    json.dump(self.__match_hash, f)

                self.__logger.info('Script is going to sleep for {} seconds...'.format(sleep_time))
                time.sleep(sleep_time)
        except Exception as x:
            print(x)

    def __grab_data_hkjc(self, retry=0):
        try:
            self.__logger.info('=== BET HKJC URL: {} ==='.format(self.__start_url_hkjc))
            opener = self.__create_opener()
            data = opener.open(self.__start_url_hkjc).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            tr_list = soup.find_all('tr', id=re.compile(r'^rmid\d+$'))
            for tr in tr_list:
                try:
                    # parse match id
                    match_id = re.sub(r'[^0-9]', '', tr.get('id')).strip()
                    item = {'matchid_hkjc': match_id}
                    # hkjc match no
                    match_no_td = tr.find('td', class_='cday ttgR2')
                    if match_no_td:
                        item['match_number_hkjc'] = match_no_td.text.strip()

                    # hkjc match leage
                    match_leage_td = tr.find('td', class_='cflag ttgR2')
                    if match_leage_td:
                        ml_img = match_leage_td.find('img')
                        if ml_img and ml_img.has_attr('title'):
                            item['match_league_hkjc'] = ml_img.get('title').strip()

                    # hkjc teams
                    teams_td = tr.find('td', class_='cteams ttgR2')
                    if teams_td:
                        teams = teams_td.find_all('span', class_='teamname')
                        item['match_home_team_hkjc'] = teams[0].text.strip()
                        item['match_away_team_hkjc'] = teams[1].text.strip()

                        vs_tag = teams_td.find('span', class_='nolnk span_vs')
                        if vs_tag and 'vs' not in vs_tag.text:
                            item['score_hkjc'] = vs_tag.text.strip()

                    match_st_td = tr.find('td', class_='cesst')
                    if match_st_td:
                        item['match_status_hkjc'] = match_st_td.text.strip()

                        if match_id not in self.__match_hash.keys():
                            match_start_time_txt = re.sub(r'[^0-9\/\: ]', '', match_st_td.text.strip())
                            match_start_time_txt = match_start_time_txt.strip().strip(':').strip()

                            if re.match(r'^\d+\/\d+ \d+\:\d+$', match_start_time_txt):
                                match_start_time_txt = datetime.now().strftime('%Y/') + match_start_time_txt
                                match_st_time = datetime.strptime(match_start_time_txt, '%Y/%d/%m %H:%M')
                                # datetime.now().strftime('%Y%m%d_%H:%M:%S') + '_'
                                directory_structure = './{}/{}/'.format(item['match_league_hkjc'].replace(' ', '_'),
                                                                        match_st_time.strftime('%Y_%m'))
                                if not os.path.exists(directory_structure):
                                    os.mkdir('./{}/'.format(item['match_league_hkjc'].replace(' ', '_')))
                                    os.mkdir(directory_structure)

                                match_start_time = match_st_time.strftime('%Y%m%d_%H%M')
                                output_csv_file = self.__output_csv_format.format(
                                    match_id,
                                    match_start_time,
                                    item['match_number_hkjc'],
                                    item['match_league_hkjc'],
                                    item['match_home_team_hkjc'],
                                    item['match_away_team_hkjc'])
                                output_csv_file = re.sub(r'\s+', ' ', output_csv_file)
                                output_csv_file = re.sub(r'\s', '_', output_csv_file)
                                self.__match_hash[match_id] = directory_structure + output_csv_file  # match_start_time

                        # if 'Expected In Play start selling time:' in match_st_td.text.strip():
                        #     continue
                        #
                        # if 'Half Time' in match_st_td.text.strip():
                        #     continue

                    if match_id not in self.__match_hash:
                        match_start_time = datetime.now().strftime('%Y%m%d_%H%M')
                        directory_structure = './{}/{}/'.format(item['match_league_hkjc'].replace(' ', '_'),
                                                                datetime.now().strftime('%Y_%m'))
                        if not os.path.exists(directory_structure):
                            os.mkdir('./{}/'.format(item['match_league_hkjc'].replace(' ', '_')))
                            os.mkdir(directory_structure)

                        output_csv_file = self.__output_csv_format.format(
                            match_id,
                            match_start_time,
                            item['match_number_hkjc'],
                            item['match_league_hkjc'],
                            item['match_home_team_hkjc'],
                            item['match_away_team_hkjc'])
                        output_csv_file = re.sub(r'\s+', ' ', output_csv_file)
                        output_csv_file = re.sub(r'\s', '_', output_csv_file)
                        self.__match_hash[match_id] = directory_structure + output_csv_file  # match_start_time

                    odd_tds = tr.find_all('td', class_='codds')
                    if odd_tds and len(odd_tds) >= 3:
                        item['home_away_draw_home_odd_hkjc'] = self.__clean_data(odd_tds[0].text.strip())
                        item['home_away_draw_draw_odd_hkjc'] = self.__clean_data(odd_tds[1].text.strip())
                        item['home_away_draw_away_odd_hkjc'] = self.__clean_data(odd_tds[2].text.strip())

                    item['time_local_computer'] = datetime.now().strftime('%Y/%m/%d %H:%M:%S')

                    match_details_url = self.__match_details_in_play_url.format(
                        match_id)  # self.__match_details_url.format(match_id)
                    status = self.__grab_hkjc_details(match_details_url, item, self.__match_hash[match_id])

                    if not status:
                        self.__logger.info('Data: {}'.format(item))
                        if not os.path.exists(self.__match_hash[match_id]):
                            self.__write_data(self.__match_hash[match_id], self.__csv_header)
                        self.__write_data(self.__match_hash[match_id], item)

                        # match_details_a = match_no_td.find('a')
                        # if match_details_a:
                        #     hkjc_details_url = self.__base_url_hkjc + match_details_a.get('href')
                        #     self.__grab_hkjc_details(hkjc_details_url, item, match_start_time)
                        # else:
                        #     output_csv_file = self.__output_csv_format.format(match_start_time, item['match_number_hkjc'],
                        #                                                       item['match_league_hkjc'],
                        #                                                       item['match_home_team_hkjc'],
                        #                                                       item['match_away_team_hkjc'])
                        #
                        #     self.__logger.info('Data: {}'.format(item))
                        #     if not os.path.exists(output_csv_file):
                        #         self.__write_data(output_csv_file, self.__csv_header)
                        #
                        #     self.__write_data(output_csv_file, item)
                except Exception as ex:
                    self.__logger.error('Error when parse data for each match: {}'.format(ex))
            # print(self.__match_hash)
            self.__logger.info('HKJC in play record search finished!')
        except Exception as x:
            self.__logger.error('Error when grab bet hkjc page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_data_hkjc(retry + 1)

    def __grab_hkjc_details(self, url, item, output_csv_file, retry=0):
        try:
            self.__logger.info('=== BET HKJC Details URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            div_hilo = soup.find('div', id='dHIL')
            if div_hilo:
                hilo_tr_list = div_hilo.find_all('tr')
                i = 0
                for hilo_tr in hilo_tr_list[1:]:
                    tds = hilo_tr.find_all('td')
                    if tds and len(tds) >= 3:
                        if i == 0:
                            item['hilo_line_1_hkjc'] = self.__clean_data(tds[0].text.strip())
                            item['hilo_line_1_high_odd_hkjc'] = self.__clean_data(tds[1].text.strip())
                            item['hilo_line_1_low_odd_hkjc'] = self.__clean_data(tds[2].text.strip())
                        elif i == 1:
                            item['hilo_line_2_hkjc'] = self.__clean_data(tds[0].text.strip())
                            item['hilo_line_2_high_odd_hkjc'] = self.__clean_data(tds[1].text.strip())
                            item['hilo_line_2_low_odd_hkjc'] = self.__clean_data(tds[2].text.strip())
                        elif i == 2:
                            item['hilo_line_3_hkjc'] = self.__clean_data(tds[0].text.strip())
                            item['hilo_line_3_high_odd_hkjc'] = self.__clean_data(tds[1].text.strip())
                            item['hilo_line_3_low_odd_hkjc'] = self.__clean_data(tds[2].text.strip())
                    i += 1

                div_corner_hilo = soup.find('div', id='dCHL')
                if div_corner_hilo:
                    total_corner_span = div_corner_hilo.find('span', class_='spTotalCorner')
                    if total_corner_span:
                        item['total_corner_hkjc'] = self.__clean_data(total_corner_span.text.strip())
                    hilo_tr_list = div_corner_hilo.find_all('tr')
                    i = 0
                    for hilo_tr in hilo_tr_list[1:]:
                        tds = hilo_tr.find_all('td')
                        if tds and len(tds) >= 3:
                            if i == 0:
                                item['corner_line_1_hkjc'] = self.__clean_data(tds[0].text.strip())
                                item['corner_line_1_high_odd_hkjc'] = self.__clean_data(tds[1].text.strip())
                                item['corner_line_1_low_odd_hkjc'] = self.__clean_data(tds[2].text.strip())
                            elif i == 1:
                                item['corner_line_2_hkjc'] = self.__clean_data(tds[0].text.strip())
                                item['corner_line_2_high_odd_hkjc'] = self.__clean_data(tds[1].text.strip())
                                item['corner_line_2_low_odd_hkjc'] = self.__clean_data(tds[2].text.strip())
                            elif i == 2:
                                item['corner_line_3_hkjc'] = self.__clean_data(tds[0].text.strip())
                                item['corner_line_3_high_odd_hkjc'] = self.__clean_data(tds[1].text.strip())
                                item['corner_line_3_low_odd_hkjc'] = self.__clean_data(tds[2].text.strip())
                        i += 1

                div_next_team_to_score = soup.find('div', id='dNTS')
                if div_next_team_to_score:
                    hilo_tr_list = div_next_team_to_score.find_all('tr')
                    for hilo_tr in hilo_tr_list[1:]:
                        tds = hilo_tr.find_all('td')
                        if tds and len(tds) >= 3:
                            item['next_team_to_score_home_odd_hkjc'] = self.__clean_data(tds[0].text.strip())
                            item['next_team_to_score_no_goals_odd_hkjc'] = self.__clean_data(tds[1].text.strip())
                            item['next_team_to_score_away_odd_hkjc'] = self.__clean_data(tds[2].text.strip())

            # # match_start_time = datetime.now().strftime('%Y%m%d_%H:%M:%S')
            # output_csv_file = self.__output_csv_format.format(match_start_time, item['match_number_hkjc'],
            #                                                   item['match_league_hkjc'], item['match_home_team_hkjc'],
            #                                                   item['match_away_team_hkjc'])

            self.__logger.info('Data: {}'.format(item))
            if not os.path.exists(output_csv_file):
                self.__logger.info('No csv file created yet for: {}!'.format(output_csv_file))
                self.__write_data(output_csv_file, self.__csv_header, 'w+')
            self.__write_data(output_csv_file, item)
            return True
        except Exception as x:
            self.__logger.error('Error when grab bet hkjc details page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_hkjc_details(url, item, output_csv_file, retry + 1)

    def __clean_data(self, text):
        try:
            text = re.sub(r'\[', '', text)
            text = re.sub(r'\]', '', text)
            return text
        except Exception as x:
            self.__logger.error('Error cleaning data: {}'.format(x))
        return text

    def __write_data(self, output_csv, row, mode='a+'):
        try:
            with open(output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'football.csv'
    with FootballSpider(output_file) as spider:
        spider.grab_data()
