import csv
import os
import re
import socket
import urllib.parse
import urllib.request
from bs4 import BeautifulSoup
from multiprocessing.pool import ThreadPool
from multiprocessing import Lock
import logging
import readline

readline.parse_and_bind("control-v: paste")


class AgenticitySpider:
    __base_url = 'http://www.agentincity.com'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        # 'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __url_cache = []

    def __init__(self, state_link, output_csv):
        self.__state_link = state_link
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __enter__(self):
        self.__setup_logger()
        self.__lock = Lock()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket
        socket.setdefaulttimeout(100)

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.reader(f)
                for row in reader:
                    self.__url_cache.append(row[8])

        csv_header = ['First Name', 'Last Name', 'Company Name', 'Real State Types', 'Contact Address', 'City',
                      'Phone Number', 'Email Address', 'URL']
        if csv_header[-1] not in self.__url_cache:
            self.__write_data(csv_header)
        return self

    def __setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler('agenticity.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data_by_state(self):
        try:
            # print('=== State URL: {} ==='.format(self.__state_link))
            self.__logger.info('=== State URL: {} ==='.format(self.__state_link))

            req = urllib.request.Request(self.__state_link, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            csrf_token_tag = soup.find('input', {'name': 'csrf_agentincity'})
            if not csrf_token_tag:
                return

            csrf_token = csrf_token_tag.get('value')
            state_code = self.__state_link.split('/')[-1]

            page = 1
            post_data = {'csrf_agentincity': csrf_token, 'page': page, 'statecode': state_code}
            has_next = self.__grab_cities(post_data)
            while has_next:
                page += 1
                post_data['page'] = page
                has_next = self.__grab_cities(post_data)
        except Exception as x:
            self.__logger.info('{}'.format(x))

    def __grab_cities(self, post_data):
        try:
            post_url = 'http://www.agentincity.com/home/getcitylist'
            self.__logger.info('post data: {}'.format(post_data))
            # print(post_data)
            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            req = urllib.request.Request(post_url, headers=self.__headers, data=post_req)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            state_list_tag = soup.find('div', class_='listingBg')
            if not state_list_tag:
                return
            city_link_tags = state_list_tag.find_all('a')
            if not city_link_tags or len(city_link_tags) == 0:
                return

            for city_link in city_link_tags:
                city = city_link.text.strip()
                city_url = city_link.get('href')
                next_link = self.grab_data_by_city(city_url, city)
                while next_link and str(next_link).startswith('http'):
                    next_link = self.grab_data_by_city(next_link, city)
            return True

        except Exception as x:
            self.__logger.info('{}'.format(x))

    def grab_all_data_by_city(self, city_url, city):
        next_link = self.grab_data_by_city(city_url, city)
        while next_link:
            next_link = self.grab_data_by_city(next_link, city)

    def grab_data_by_city(self, city_link, city):
        try:
            # print('=== City URL: {} ==='.format(city_link))
            self.__logger.info('=== City URL: {} ==='.format(city_link))
            req = urllib.request.Request(city_link, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # city = str(self.__city_link.split('/')[-1]).split('-')[0].replace('_', ' ').strip()
            link_tags = soup.find_all('div', class_=re.compile(r'^\s*?profile-listing-sbox\s*?$'))
            if link_tags and len(link_tags) > 0:
                url_list = []
                for link_tag in link_tags:
                    link = link_tag.find('a')
                    if link:
                        url = link.get('href')
                        full_name = link.text.strip()
                        if url not in self.__url_cache:
                            # self.__grab_details(url, city, full_name)
                            url_list.append((url, city, full_name))
                            self.__url_cache.append(url)
                        else:
                            self.__logger.warning('Already grabbed URL: {}.'.format(url))

                with ThreadPool(16) as p:
                    p.map(self.__grab_details, url_list)

            next_link = None
            pagination_tag = soup.find('div', id='pagination')
            if pagination_tag:
                links = pagination_tag.find_all('a')
                if links and len(links) > 0:
                    for link in links:
                        if '>' in link.text:
                            next_link = link.get('href')
                            break

            if next_link:
                return next_link
                # return self.grab_data_by_city(next_link, city)
        except Exception as x:
            self.__logger.info('{}'.format(x))

    def __grab_details(self, args):
        try:
            print(args)
            (url, city, full_name) = args
            # print('=== Details URL: {} ==='.format(url))
            self.__logger.info('=== Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            first_name = ''
            last_name = ''

            if full_name:
                name_parts = full_name.split(' ')
                first_name = name_parts[0]
                last_name = ' '.join(name_parts[1:]) if len(name_parts) > 1 else ''

            company_name = ''
            real_state_types = ''
            contact_address = ''
            phone = ''
            email = ''

            agent_data_tag = soup.find('ul', class_='agent-data')
            if not agent_data_tag:
                return

            agent_data_list = agent_data_tag.find_all('li')
            if not agent_data_list or len(agent_data_list) == 0:
                return

            for agent_data in agent_data_list:
                span_list = agent_data.find_all('span')
                if not span_list or len(span_list) < 2:
                    continue
                key = span_list[0].text.strip()
                val = span_list[1].text.strip()

                if 'Company Name:' in key:
                    company_name = val
                if 'Real State Types:' in key:
                    real_state_types = val
                if 'Contact Address:' in key:
                    contact_address = val
                if 'Phone Number:' in key:
                    phone = val
                if 'E-Mail:' in key:
                    email = val

            csv_data = [first_name, last_name, company_name, real_state_types, contact_address, city, phone, email, url]
            # print(csv_data)
            self.__logger.info('Data: {}'.format(csv_data))
            self.__write_data(csv_data)
        except Exception as x:
            self.__logger.info('{}'.format(x))

    __total = 0

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)

                # print('Total: {}'.format(self.__total))
                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.info('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


def main():
    try:
        # u1 = 'http://www.agentincity.com/realestateagents/Adelanto-CA'
        # with AgenticitySpider('', '') as spider:
        #     spider.grab_data_by_city(u1, 'a1')
        # return
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            # 'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive'}
        url = 'http://www.agentincity.com/'
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read()
        data = data.decode('utf-8', errors='ignore')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        footer_div = soup.find('div', id='footer')
        if not footer_div:
            return

        li_list = footer_div.find_all('li')
        for li in li_list:
            state_url = li.find('a').get('href').strip()
            output_file = 'agenticity_output_state_{}.csv'.format(state_url.split('/')[-1])

            try:
                with AgenticitySpider(state_url.strip(), output_file.strip()) as spider:
                    spider.grab_data_by_state()
            except Exception as e:
                print(e)
    except Exception as x:
        print(x)


if __name__ == '__main__':
    main()
    # city_state = input('Please specify State/City - Y/N:')
    # if city_state.strip() == 'Y'.lower():
    #     state_link = input('Please specify State URL: ')
    #     output_file = 'agenticity_output_state_{}.csv'.format(state_link.split('/')[-1])
    #     with AgenticitySpider(state_link.strip(), output_file.strip()) as spider:
    #         spider.grab_data_by_state()
