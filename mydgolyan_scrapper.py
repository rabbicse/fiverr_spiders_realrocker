# -*- coding: utf-8 -*-

import csv
import gzip
import json
import logging
import os
import random
import readline
import socket
import string
import sys
import time
import urllib.parse
import urllib.request
from multiprocessing import Lock

from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings, QWebEngineProfile
from PyQt5.QtWidgets import QApplication
from bs4 import BeautifulSoup

# import socks

readline.parse_and_bind("control-v: paste")


class Browser(QWebEngineView):
    __redirected_url = None
    __html = None
    __clicked = False

    def __init__(self, app):
        QWebEngineView.__init__(self)
        self.__app = app
        # self.__setup_browser()
        self.loadFinished.connect(self._load_finished)
        self.__loaded = False

    def __setup_browser(self):
        random_dir = ''.join(random.choice(string.ascii_lowercase) for _ in range(6))
        cache = '/tmp/' + random_dir + '/cache'
        storage = '/tmp/' + random_dir + '/storage'
        os.makedirs(storage)
        os.makedirs(cache)
        self.page().profile().setPersistentCookiesPolicy(QWebEngineProfile.NoPersistentCookies)
        self.page().settings().setAttribute(QWebEngineSettings.LocalStorageEnabled, False)
        self.page().profile().setCachePath(cache)
        self.page().profile().setPersistentStoragePath(storage)

    def fetch(self, url, next_page=False, page_index=0, page_type=0):
        try:
            self.__next_page = next_page
            self.__page_index = page_index
            self.__page_type = page_type

            # request = QWebEngineHttpRequest()
            # request.setUrl(QUrl(url))
            #
            self.timeout_timer = QTimer()
            self.timeout_timer.timeout.connect(self._request_timed_out)
            #
            self.timeout_timer.start(120 * 1000)
            # self.load(request)

            self.setUrl(QUrl(url))
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            print(x)
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            print('Custom request timeout value exceeded.')
            self.timeout_timer.stop()
            # self.stop()
            # self.loadFinished.emit(False)
            if not self.__loaded:
                print('Not loaded...')
            else:
                print('loaded...')

            self.__app.quit()
        except Exception as x:
            print(x)

    def _load_finished(self):
        try:
            self.__loaded = True
            current_url = str(self.page().url().toString())
            self.__redirected_url = current_url
            print('Page just loaded. waiting for ajax data...')

            # find name div if details page
            if self.__page_type == 0:
                self.__run_js_details()
            elif self.__page_type == 1:
                self.__run_js_category()

                # time.sleep(10)

                # if self.__next_page:
                #     print('Next page...')
                #     for i in range(self.__page_index):
                #         self.page().runJavaScript("""document.getElementsByClassName('next page-numbers')[0].click()""")
                #         self.__next_page = False
                #         time.sleep(5)
                # self.page().toHtml(self.processHtml)
        except Exception as x:
            print(x)
            self.__app.quit()

    def __run_js_details(self):
        js_details = """document.getElementsByClassName("product_title entry-title").length"""
        self.page().runJavaScript(js_details, self.__js_callback)

    def __run_js_category(self):
        try:
            # print('run js for category...')
            js_category = """document.getElementsByClassName("inbox").length"""
            self.page().runJavaScript(js_category, self.__js_callback)
        except Exception as x:
            print('Error when category js: {}'.format(x))

    def __js_callback(self, js_data):
        try:
            if js_data > 0:
                if self.__page_type == 1 and self.__page_index > 0:
                    print('data loaded. tried to click ')
                    js = """document.querySelectorAll('li[data-wiz-grid-hash="{}"]')[0].click()""".format(
                        '#c/?page={}&pi=16&gid=&sb=&so=&filter='.format(self.__page_index))
                    self.page().runJavaScript(js)
                    self.__clicked = True

                    js1 = """document.querySelector('li[data-wiz-next-page]').outerHTML"""
                    self.page().runJavaScript(js1, self.__js_click_callback)
                    # self.__run_js_category()
                else:
                    time.sleep(2)
                    self.page().toHtml(self.processHtml)
            else:
                time.sleep(0.5)
                if self.__page_type == 0:
                    self.__run_js_details()
                elif self.__page_type == 1:
                    self.__run_js_category()
        except Exception as x:
            print('Error when js callback: {}'.format(x))

    def __js_click_callback(self, data):
        # print('click callback found...')
        # print(data)
        if data:
            soup = BeautifulSoup(data, 'lxml')
            page = soup.find('li').get('data-wiz-grid-hash')
            if 'page={}'.format(self.__page_index + 1) in page:
                self.page().toHtml(self.processHtml)
            else:
                # print('loading click...')
                js1 = """document.querySelector('li[data-wiz-next-page]').outerHTML"""
                self.page().runJavaScript(js1, self.__js_click_callback)
        else:
            self.page().toHtml(self.processHtml)


    def processHtml(self, html):
        self.__html = html
        self.__app.quit()


class MydgolyanCrawler:
    __base_url = 'http://www.mydgolyan.com/'
    __start_url = 'http://www.mydgolyan.com/'
    __ajax_url = 'http://www.mydgolyan.com/wp-admin/admin-ajax.php'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __proxies = []
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__app = QApplication(sys.argv)
        self.__browser = Browser(self.__app)
        self.__lock = Lock()
        self.__setup_logger('mydgolyan.log')
        socket.setdefaulttimeout(30)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        # proxy_handler = urllib.request.ProxyHandler(self.__proxy_dict)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # opener = urllib.request.build_opener(proxy_handler)
        # urllib.request.install_opener(opener)
        self.__field_names = ['url', 'category', 'sub_category', 'name', 'item_key', 'price', 'dimentions', 'color',
                              'available_package_of', 'big_image_url', 'small_image_url']
        csv_header = {'url': 'URL',
                      'category': 'Category',
                      'sub_category': 'Sub Category',
                      'name': 'Name',
                      'item_key': 'Item Key',
                      'price': 'Price',
                      'dimentions': 'Dimentions',
                      'color': 'Color',
                      'available_package_of': 'Available Package Of',
                      'big_image_url': 'Big Image URL',
                      'small_image_url': 'Small Image URL'}

        # self.__write_data_new(csv_header)

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

                    # if row['name'] == '':
                    #     continue
                    #
                    # self.__write_data_new(row)

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)

        # self.__load_proxies()

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__browser.close()
        self.__app.exit()

    def __load_proxies(self):
        try:
            if os.path.exists('proxies.json'):
                with open('proxies.json') as inFile:
                    self.__proxies = json.load(inFile)
        except Exception as x:
            self.__logger.error('Error when loading proxies.{}'.format(x))

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy_url = str(random.choice(self.__proxies))

            if not proxy_url.startswith('http://'):
                proxy_url = 'http://' + proxy_url

            return {
                "http": proxy_url,
                "https": proxy_url
            }
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                cookie_handler = urllib.request.HTTPCookieProcessor()
                opener = urllib.request.build_opener(cookie_handler, urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def grab_data(self):
        try:
            u = 'http://www.mydgolyan.com/%D7%A7%D7%98%D7%92%D7%95%D7%A8%D7%99%D7%95%D7%AA/%D7%9B%D7%9C%D7%99-%D7%94%D7%92%D7%A9%D7%94/%D7%A4%D7%95%D7%A8%D7%A6%D7%9C%D7%9F/'
            c = 'יודאיקה'
            sc = 'כוסות קידוש'
            self.__grab_data_by_sub_category_pages(u, c, sc)
            return
            # d = {'small_image_url': 'http://www.mydgolyan.com/wp-content/uploads/wizshop/7964-300x300.jpg',
            #      'color': 'לבן',
            #      'big_image_url': 'http://www.mydgolyan.com/wp-content/uploads/wizshop/7964-700x700.jpg',
            #      'item_key': 'מפתח הפריט: 7964', 'price': '26 ש"ח', 'name': 'פרח שן הארי', 'sub_category': 'פרחים',
            #      'url': 'http://www.mydgolyan.com/מוצרים/7964', 'dimentions': '86 ס"מ', 'category': 'מוצרי נוי',
            #      'available_package_of': '12'}
            # self.__grab_details('http://www.mydgolyan.com/%D7%9E%D7%95%D7%A6%D7%A8%D7%99%D7%9D/7091/')
            # return
            self.__logger.info('Start URL: {}'.format(self.__start_url))
            opener = self.__create_opener()
            data = opener.open(self.__start_url).read()

            # data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            menu_div = soup.find('div', class_='nav-main-inner')
            if not menu_div:
                return

            sub_cat_url_list = []
            sub_menu_list = menu_div.find_all('ul', class_='sub-menu')
            if sub_menu_list:
                for sub_menu in sub_menu_list:
                    menu = sub_menu.find_previous_sibling('a')
                    if not menu:
                        continue

                    if not menu.has_attr('title'):
                        continue

                    category = menu.get('title').strip()
                    # print(category)
                    # print(sub_menu)
                    sub_cat_list = sub_menu.find_all('a')
                    for sub_cat in sub_cat_list:
                        if not sub_cat.has_attr('title'):
                            continue

                        sub_category = sub_cat.get('title')
                        sub_cat_url = urllib.parse.unquote_plus(sub_cat.get('href'))
                        sub_cat_url = sub_cat_url.replace(self.__base_url, '')
                        sub_cat_url = urllib.parse.quote_plus(sub_cat_url)
                        sub_cat_url = self.__base_url + sub_cat_url

                        if sub_cat_url not in sub_cat_url_list:
                            # page_index = 0
                            # next_page = self.__grab_data_by_sub_category(sub_cat_url, category, sub_category)
                            # while next_page:
                            #     page_index += 1
                            #     next_page = self.__grab_data_by_sub_category(sub_cat_url, category, sub_category,
                            #                                                  next_page=True, page_index=page_index)
                            self.__grab_data_by_sub_category_pages(sub_cat_url, category, sub_category)
                            sub_cat_url_list.append(sub_cat_url)
                        else:
                            self.__logger.info('Sub Category: {} already grabbed!'.format(sub_cat_url))
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def grab_details_from_file(self, filename):
        try:
            if os.path.exists(filename):
                with open(filename, 'r+', encoding='utf-8') as f:
                    reader = csv.DictReader(f, fieldnames=['url', 'category', 'sub_category'], quoting=csv.QUOTE_ALL)
                    for row in reader:
                        if row['url'] not in self.__url_cache:
                            self.__grab_details(row['url'], row['category'], row['sub_category'])
                        else:
                            self.__logger.info('URL: {} already exists!'.format(row['url']))

                            # product_urls = []
                            # for row in reader:
                            #     if row[0] != '' and row[0].strip() not in self.__url_cache:
                            #         product_urls.append(row[0].strip())
                            #
                            #     if len(product_urls) == 128:
                            #         with ThreadPool(16) as p:
                            #             p.map(self.__grab_details, product_urls)
                            # product_urls = []

            self.__logger.info('==== Finish ====')

        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __grab_data_by_sub_category_pages(self, url, category, sub_category):
        try:
            print(url)
            print(category)
            print(sub_category)
            page_index = 0
            next_page = self.__grab_data_by_sub_category(url, category, sub_category)
            while next_page:
                page_index += 1
                next_page = self.__grab_data_by_sub_category(url, category, sub_category, next_page=True,
                                                             page_index=page_index)
        except Exception as x:
            print(x)

    __d_urls = []

    def __grab_data_by_sub_category(self, url, category, sub_category, next_page=False, page_index=0, retry=0):
        try:
            self.__logger.info('=== Sub Category URL: {} ==='.format(url))
            r, data = self.__browser.fetch(url, next_page, page_index=page_index, page_type=1)
            if not data:
                return
            # print(r)
            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return
            product_divs = soup.find_all('div', class_='inbox')
            for product_div in product_divs:
                product_link_tag = product_div.find('a', class_='caption')
                if not product_link_tag:
                    continue
                details_url = product_link_tag.get('href')
                details_url = details_url.replace(self.__base_url, '')
                details_url = urllib.parse.quote_plus(details_url)
                details_url = self.__base_url + details_url

                item = {'url': urllib.parse.unquote_plus(details_url), 'category': category,
                        'sub_category': sub_category}

                if urllib.parse.unquote_plus(details_url) not in self.__d_urls:
                    self.__d_urls.append(urllib.parse.unquote_plus(details_url))
                else:
                    print('Warning: already exists: {}'.format(urllib.parse.unquote_plus(details_url)))

                print('Total d urls: {}'.format(len(self.__d_urls)))
                # self.__write_json_data(item)

                # if urllib.parse.unquote_plus(details_url) not in self.__url_cache:
                #     self.__grab_details(details_url, category, sub_category)
                # else:
                #     self.__logger.info('URL: {} already exists!'.format(details_url))

            next_page = soup.find('a', class_='next page-numbers')
            if next_page:
                return True
        except Exception as x:
            self.__logger.error('Error grab search by category and location page: {}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_data_by_sub_category(url, category, sub_category, next_page, page_index, retry + 1)

    def __grab_details(self, url, category, sub_category, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            r, data = self.__browser.fetch(url)
            print(r)

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # print(soup)

            item = {'url': urllib.parse.unquote_plus(url), 'category': category, 'sub_category': sub_category}
            name_tag = soup.find('h1', class_='product_title entry-title')
            if name_tag:
                item['name'] = name_tag.text.strip()

            item_key_tag = soup.find('p', class_='key')
            if item_key_tag:
                item['item_key'] = item_key_tag.text.strip()

            price_key_tag = soup.find('p', class_='price')
            if price_key_tag:
                price = price_key_tag.text.strip()
                price = price.split('\n')[0]
                price = price.strip()
                item['price'] = price

            tab_additional_info = soup.find('div', id='tab-additional_information')
            if tab_additional_info:
                div_list = tab_additional_info.find_all('div')
                for div in div_list:
                    if not div.has_attr('data-wiz-item-prop'):
                        continue

                    # print(div)
                    if 'מידות' in div.text:
                        item['dimentions'] = div.text.replace('מידות', '').replace(':', '').strip()

                    if 'מגיע בצבע' in div.text:
                        item['color'] = div.text.replace('מגיע בצבע', '').replace(':', '').strip()

                    if 'ניתן לרכוש באריזות של' in div.text:
                        item['available_package_of'] = div.text.replace('ניתן לרכוש באריזות של', '').replace(':',
                                                                                                             '').strip()

            img_tag = soup.find('img', class_='attachment-shop_catalog wp-post-image')
            if img_tag:
                img_big_url = img_tag.get('src')
                item['big_image_url'] = img_big_url
                item['small_image_url'] = img_big_url.replace('700x700', '300x300')

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(urllib.parse.unquote_plus(url))
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

            return True

        except Exception as x:
            self.__logger.error('Error grab details page: {}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, category, sub_category, retry + 1)
            else:
                return False

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()

    def __write_data_new(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv + '_new.csv', mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()

    def __write_json_data(self, item, mode='a+'):
        try:
            self.__lock.acquire()
            with open('results_updated.csv', mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=['url', 'category', 'sub_category'], quoting=csv.QUOTE_ALL)
                writer.writerow(item)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    output_file = 'mydgolyan_output_final.csv'
    with MydgolyanCrawler(output_file) as spider:
        # spider.grab_details_from_file('results.csv')
        spider.grab_data()

        # input('Press any key to exit...')
