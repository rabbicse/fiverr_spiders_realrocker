import urllib.request
import json

from tail_call import tail_call_optimized

top_url = 'http://www.australia.com/en/places/australias-cities.html'
sitemap_url = 'http://www.australia.com/en/sitemap.xml'

json_links = [
    'http://www.australia.com/content/australia/en/places/australias-cities/_jcr_content/mainParsys/atdwsearchcomplex.find.atdwonclick.json?page={}&locale=en&location=&location_name=Australia&searchMode=&category=Accommodation&verticalClassifications=&ratingAAA=']


@tail_call_optimized
def grab_data(url, page=1):
    try:
        json_url = url.format(page)
        print('===== Page: {} ===='.format(page))
        print('URL: {}'.format(json_url))
        data = urllib.request.urlopen(json_url).read().decode('utf-8')
        if not data:
            return

        json_data = json.loads(data)
        results = json_data['results']
        for result in results:
            if 'email' in result:
                write_email(result['email'])

        last_page = json_data['lastPage']
        if last_page != page:
            grab_data(url, page + 1)

    except Exception as x:
        print('error...')
        print(x)


def write_email(email):
    try:
        with open('aus_email_output_accommodation.txt', 'a+', encoding='utf-8') as f:
            f.write(email + '\n')
    except Exception as x:
        print('Error write data for {}'.format(email))
        print(x)


def process_crawler():
    for json_link in json_links:
        grab_data(json_link)


if __name__ == '__main__':
    process_crawler()
