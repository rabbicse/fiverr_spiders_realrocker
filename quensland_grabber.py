import csv
import gzip
import socket
import urllib.request
import urllib.parse
import json

import socks
from bs4 import BeautifulSoup

socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket
socket.setdefaulttimeout(60)

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
    'Host': 'www.queensland.com',
    'Origin': 'https://www.queensland.com',
    'X-Requested-With': 'XMLHttpRequest',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive'}

headers_1 = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
    'Host': 'www.queensland.com',
    'Origin': 'https://www.queensland.com',
    # 'X-Requested-With': 'XMLHttpRequest',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive'}

base_url = 'https://www.queensland.com'


def grab_data(url, post_data, page=1):
    try:
        print('URL: ' + url)

        # todo format.
        post_data['Page'] = str(page)
        print(post_data)
        post_req = urllib.parse.urlencode(post_data).encode('utf-8')

        req = urllib.request.Request(url, headers=headers, data=post_req)
        data = urllib.request.urlopen(req).read()
        data = data.decode('utf-8')
        if not data:
            return

        json_data = json.loads(data)
        # print(json_data)
        count = 0
        for result in json_data['Results']:
            if 'ReadMoreUrl' in result:
                details_link = '{}{}'.format(base_url, result['ReadMoreUrl'])
                print(details_link)
                write_link(details_link)
            count += 1

        if count >= 100:
            return True
    except Exception as x:
        print('error...')
        print(x)
    return False


def write_link(link):
    try:
        with open('queensland_new1.txt', 'a+', encoding='utf-8') as f:
            f.write(link + '\n')
    except Exception as x:
        print('Error write data for {}'.format(link))
        print(x)


def write_email(email):
    try:
        with open('aus_email_output_accommodation.txt', 'a+', encoding='utf-8') as f:
            f.write(email + '\n')
    except Exception as x:
        print('Error write data for {}'.format(email))
        print(x)


def grab_email(url):
    try:
        req = urllib.request.Request(url, headers=headers_1)
        data = urllib.request.urlopen(req).read()
        data = gzip.decompress(data).decode('utf-8')
        if not data:
            return
        # product-email col-xs-12
        # contact-detail
        soup = BeautifulSoup(data, 'lxml')
        title = soup.find('h1', class_='atdw-product-name').text.strip()
        email = soup.find('div', class_='product-email col-xs-12').find('a').get('href')
        csv_row = [email, title, url]
        print(csv_row)
        write_data(csv_row)
    except Exception as x:
        print(x)


def write_data(row, mode='a'):
    try:
        with open('queensland_updated_output.csv', mode, newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(row)
    except Exception as x:
        print('Error printing csv output: ' + str(x))


def grab_top_links(url):
    try:
        data = urllib.request.urlopen(url).read().decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        if not soup:
            return

        search_form = soup.find('div', class_='search-parameters')
        if not search_form:
            return

        TargetItemId = search_form.find('input', {'name': 'TargetItemId'}).get('value').strip()
        PageItemId = search_form.find('input', {'name': 'PageItemId'}).get('value').strip()
        SearchGroups = search_form.find('input', {'name': 'SearchGroups'}).get('value').strip()

        form_data = {
            'TargetItemId': TargetItemId,
            'PageItemId': PageItemId,
            'Page': '1',
            # 'pageTotal': pageTotal,
            'SearchGroups': SearchGroups,
            'OrderBy1': '',
            'PageSize': '100',
            'searchPanelAnchor': '#spanel',
            'filterToggleShowLabel': 'Show',
            'filterToggleHideLabel': 'Hide',
            'destinations': '',
            'maxprice': '4000',
            'minprice': '1',
            'pricefrom': '',
            'priceto': '',
            'starratingfilter': '',
            'keywords': ''
        }

        page = 1
        has_next = grab_data('https://www.queensland.com/api/Search/ResultsList', form_data, page=page)
        while has_next:
            page += 1
            has_next = grab_data('https://www.queensland.com/api/Search/ResultsList', form_data, page=page)
            # if page > 11:
            #     break
    except Exception as x:
        print(x)


def process_crawler():
    # url = 'https://www.queensland.com/en-au/accommodation'
    # url = 'https://www.queensland.com/en-au/events'
    urls = [
        # 'https://www.queensland.com/en-in/plan-your-trip/getting-here',
        #     'https://www.queensland.com/en-in/plan-your-trip/holiday-type/driving-holidays',
        #     'https://www.queensland.com/en-in/plan-your-trip/visitor-information-centres',
        #     'https://www.queensland.com/en-in/things-to-see-and-do/diving-and-snorkelling'
        'https://www.queensland.com/en-in/explore-queensland/the-whitsundays/things-to-see-and-do/sailing',
        'https://www.queensland.com/en-in/plan-your-trip/holiday-type/driving-holidays',
        'https://www.queensland.com/en-in/things-to-see-and-do/camping-and-4wding/glamping',
        'https://www.queensland.com/en-in/things-to-see-and-do/camping-and-4wding/camping-and-caravanning'
        'https://www.queensland.com/en-in/things-to-see-and-do/national-parks/waterfalls-and-natural-swimming-holes'
    ]
    for url in urls:
        grab_top_links(url)
    # write_data(['Email', 'Page Title', 'Source Url'], mode='w')
    with open('queensland_new1.txt', 'r', encoding='utf-8') as f:
        for line in f.readlines():
            grab_email(line.strip())
            # break
            # grab_email('https://www.queensland.com/accommodation/riviera-mackay')


if __name__ == '__main__':
    process_crawler()
