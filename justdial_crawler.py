import csv
import json
import logging
import os
import re
import urllib.parse
from collections import OrderedDict
from multiprocessing import Semaphore, Value
from multiprocessing.pool import Pool, ThreadPool
from bs4 import BeautifulSoup
from easy_spider import Spider

logger = logging.getLogger(__name__)


class JustDialCrawler(Spider):
    __start_url = 'http://www.allitebooks.in/page/{}/'
    __api_url = 'https://t.justdial.com/api/india_api_write/01jan2018/searchziva.php?'
    __url_cache = []
    __page_cache = []
    __total = Value('i', 0)
    __lock = Semaphore()
    __MIN_COUNT = 500

    def __init__(self, input_csv, output_csv, mode=0):
        Spider.__init__(self, log_file='justdial.log')
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv
        self.__mode = mode

    def __enter__(self):
        if self.__mode == 1:
            self.__init_partial()
        else:
            self.__init_full()
        return self

    def __init_full(self):
        hdr = [('url', 'Url'),
               ('name', 'Name'),
               ('email', 'Email'),
               ('phone_number', 'Phone Number'),
               ('website', 'Website'),
               ('address', 'Address'),
               ('area', 'Area'),
               ('latitude', 'Latitude'),
               ('longitude', 'Longitude'),
               ('rating', 'Rating'),
               ('total_reviews', 'Total Reviews'),
               ('year_establish', 'Year Establish'),
               ('practice_areas', 'Practice Areas'),
               ('hours_of_operations', 'Hours Of Operations'),
               ('description', 'Description'),
               ('city', 'City'),
               ('keyword', 'Keyword'),
               ('thumbnail', 'Thumbnail Url'),
               ('images', 'Gallery Images')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        self.__prepare_proxies()

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

    def __init_partial(self):
        hdr = [('url', 'Url'),
               ('keyword', 'Keyword'),
               ('thumbnail', 'Thumbnail Url')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        self.__prepare_proxies()

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

    def __prepare_proxies(self):
        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.proxies = json_data

        if os.path.exists('proxylist.csv'):
            with open('proxylist.csv', 'r+', encoding='utf-8') as f:
                self.proxies = []
                reader = csv.reader(f)
                for row in reader:
                    if not row:
                        continue

                    if len(row) <= 0:
                        continue

                    p = row[0]
                    https = 'no'
                    if 'https://' in p:
                        https = 'yes'

                    p = p.replace('https://', '')
                    p = p.replace('http://', '')
                    p_list = p.split(':')
                    if len(p_list) < 2:
                        continue

                    ip = p_list[0] #+ ':' + p_list[1]
                    # proxy = {"ip": ''.join(p_list[0:-1]), "port": p_list[-1], "https": "no"}
                    proxy = {"ip": ip, "port": p_list[-1], "https": https}
                    self.proxies.append(proxy)

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Total records: {}'.format(self.__total.value))
        logger.info('Finish!!!')
        del self

    def process_data(self):
        try:
            # ur = 'https://www.justdial.com/Delhi/Fun-Rangers-Near-Peepal-Chowk-Niti-Khand-2-Indirapuram/011PXX11-XX11-170918111801-X3K9_BZDET?xid=RGVsaGkgUHJlc2Nob29scyBJbmRpcmFwdXJhbQ=='
            # self._grab_details((ur, 'b', 'c'))
            # return
            # u = 'https://www.justdial.com/Ghaziabad/search?q=Salons'
            # self.do_process(u)
            #
            # with ThreadPool(32) as pool:
            #     pool.map(self.do_process, self.__page_cache)
            # return
            if self.__mode == 1:
                urls = []
                if os.path.exists(self.__input_csv):
                    with open(self.__input_csv, 'r', errors='ignore') as f:
                        reader = csv.reader(f, quoting=csv.QUOTE_ALL)
                        for row in reader:
                            if not row:
                                continue

                            if len(row) <= 0:
                                continue

                            u = row[0].strip()
                            if u != '':
                                u = urllib.parse.urlparse(u)
                                urls.append(u.geturl())
                with ThreadPool(32) as pool:
                    pool.map(self.do_process, urls)

                with ThreadPool(32) as pool:
                    pool.map(self.do_process, self.__page_cache)
            else:
                urls = []
                if os.path.exists(self.__input_csv):
                    with open(self.__input_csv, 'r+') as f:
                        reader = csv.reader(f, quoting=csv.QUOTE_ALL)
                        for row in reader:
                            if len(row) < 2:
                                continue

                            u = row[0].strip()
                            k = row[1].strip()
                            t = row[2].strip()
                            if u != '' and u not in self.__url_cache:
                                urls.append((u, k, t))
                            else:
                                logger.info('{} already exists!'.format(u))

                with ThreadPool(32) as pool:
                    pool.map(self._grab_details, urls)

        except Exception as x:
            logger.error('Error when process data: {}'.format(x))

    def do_process(self, url):
        try:
            page = 1
            response = self.__grab_pages(url)

            if not response:
                return

            next_page, opener, count, area_pages = response
            while next_page:
                page += 1
                logger.info('All Record Count: {} for Base URL: {}'.format(count, url))
                res = self.__grab_pages(next_page, pageno=page, opener=opener, count=count)

                if res:
                    next_page, opener, count = res
                else:
                    break

            logger.info('Records Count: {}'.format(count))
            if count >= self.__MIN_COUNT:
                if area_pages and len(area_pages) > 0:
                    for area_page in area_pages:
                        if area_page not in self.__page_cache:
                            self.__page_cache.append(area_page)

        except Exception as x:
            logger.error('Error when get pages: {}'.format(x))

    def __grab_pages(self, url, pageno=1, opener=None, count=0):
        try:
            response = self.fetch_data(url, opener=opener)
            if not response:
                return

            data, redirected_url, opener = response

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            kw = urllib.parse.unquote_plus(url.split('/')[-2]) if 'page' in url.split('/')[
                -1] else urllib.parse.unquote_plus(url.split('/')[-1])

            # get detail pages...
            detail_pages = []
            li_list = soup.find_all('li', class_='cntanr')
            if li_list and len(li_list) > 0:
                logger.info('Total records found: {}'.format(len(li_list)))
                for li in li_list:
                    if li.has_attr('data-href'):
                        detail_page_link = li.get('data-href')

                        if detail_page_link not in self.__url_cache:
                            thumb_url = ''
                            thumb_tag = li.find('img', class_='altImgcls')
                            if thumb_tag:
                                thumb_url = thumb_tag.get('data-src')
                            detail_pages.append((detail_page_link, kw, thumb_url))
                        else:
                            logger.info('{} already exists!'.format(detail_page_link))

                        count += 1

            # span_list = soup.find_all('span', class_='jcn')
            # if span_list and len(span_list) > 0:
            #     for span in span_list:
            #         link_a = span.find('a')
            #         if not link_a:
            #             continue
            #
            #         detail_page_link = link_a.get('href')
            #
            #         if detail_page_link not in self.__url_cache and detail_page_link not in detail_pages:
            #             detail_pages.append((detail_page_link, kw))
            #         else:
            #             logger.info('{} already exists!'.format(detail_page_link))

            # with ThreadPool(32) as pool:
            #     pool.map(self._grab_details, detail_pages)
            for detail_page in detail_pages:
                item = {'url': detail_page[0], 'keyword': detail_page[1], 'thumbnail': detail_page[2]}
                self.__write_item(item)

            next_url = None
            next_a = soup.find('a', {'rel': 'next'})
            if next_a and next_a.has_attr('href'):
                next_url = next_a.get('href')

            if pageno == 1:
                area_pages = []
                area_wrap = soup.find('div', class_='filter-common-wrap popular-area-wrap')
                if area_wrap:
                    links = area_wrap.find_all('a', class_='lng_commn')
                    for link in links:
                        area_page = link.get('href')
                        area_pages.append(area_page)

                    if next_url:
                        return next_url, opener, count, area_pages
            elif next_url:
                return next_url, opener, count
            # elif 'page-' in redirected_url.split('/')[-1]:
            #     sp_arr = redirected_url.split('/')
            #     sp_arr[-1] = 'page-{}'.format(pageno + 1)
            #     u = '/'.join(sp_arr)
            #     return u, opener, count

        except Exception as x:
            logger.error('Error when parse next page: {}'.format(x))

    def _grab_details(self, obj):
        try:
            (url, keyword, thumbnail) = obj
            logger.info('Details page: {}'.format(url))
            response = self.fetch_data(url)
            if not response:
                return

            data, redirected_url, opener = response

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            item = {'url': url, 'keyword': keyword, 'thumbnail': thumbnail}
            name_tag = soup.find('span', class_='fn')
            if name_tag:
                item['name'] = name_tag.text.strip()

            address_tag = soup.find('span', id='fulladdress')
            if address_tag:
                ad_tag = address_tag.find('span', class_='lng_add')
                if ad_tag:
                    item['address'] = ad_tag.text.strip()
            else:
                address_tag = soup.find('span', class_='lng_add')
                if address_tag:
                    item['address'] = address_tag.text.strip()

            web_i = soup.find('i', class_='web_ic sprite_wb comp-icon')
            if web_i:
                web_span = web_i.find_next_sibling('span')
                if web_span:
                    web_a = web_span.find('a')
                    if web_a:
                        item['website'] = web_a.get('href')

            practical_areas = []
            practical_area_a_list = soup.find_all('a', class_='lng_als_lst')
            for practical_area_a in practical_area_a_list:
                if not practical_area_a.has_attr('title'):
                    continue
                practical_areas.append(practical_area_a.get('title').strip())
            item['practice_areas'] = ', '.join(practical_areas)

            hours_ul = soup.find('ul', id='statHr')
            if hours_ul:
                hours = []
                li_list = hours_ul.find_all('li')
                for li in li_list:
                    spans = li.find_all('span')
                    if len(spans) < 2:
                        continue

                    hours.append(spans[0].text.strip() + ': ' + spans[1].text.strip())

                item['hours_of_operations'] = ', '.join(hours)

            desc_div = soup.find('div', class_='col-sm-12 businfo seoshow')
            if not desc_div:
                desc_div = soup.find('div', class_='col-sm-12 businfo seoshow ')
            if desc_div:
                desc_span = desc_div.find('span')
                item['description'] = desc_span.text.strip()

            if 'description' not in item:
                meta_desc = soup.find('meta', {'name': 'description'})
                if meta_desc and meta_desc.has_attr('content'):
                    item['description'] = meta_desc.get('content')

            # images = []
            # ul = soup.find('ul', class_='catyimgul')
            # if ul:
            #     imags = ul.find_all('img')
            #     for img in imags:
            #         image_url = img.get('src')
            #         images.append(image_url)
            #     item['images'] = ', '.join(images)

            id_input = soup.find('input', id='docid')
            if id_input:
                try:
                    id = id_input.get('value')
                    api_url = self.__api_url + 'docid=' + id + '&case=detail'
                    res = self.fetch_data(api_url, cache=True, opener=opener)
                    if res:
                        d, r, o = res
                        json_response = json.loads(d)
                        # with open('j.json', 'w+') as f:
                        #     json.dump(json_response, f, indent=4)
                        if 'results' in json_response:
                            json_data = json_response['results']
                            # logger.info(str(json_data['disp_pic']))
                            # print(json_data['photos'])
                            # for k in json_data:
                            #     print(k)
                            try:
                                images = []
                                j_detail = json.loads(json_data['ldJson']['detail'])
                                photos = j_detail['photos']['url']
                                if photos and len(photos) > 0:
                                    for photo in photos:
                                        if not photo or len(photo.strip()) > 0:
                                            images.append(photo)

                                imgs = ', '.join(images)
                                imgs = imgs.strip().strip(',').strip()
                                item['images'] = imgs
                            except:
                                pass

                            if 'contact' in json_data and json_data['contact'].strip() != '':
                                phone = json_data['contact']
                                # print('Phone: {}'.format(phone))
                                phone = re.sub(r'\+', '', phone)
                                phone = re.sub(r'=', '', phone)
                                item['phone_number'] = phone

                            if 'phone_number' not in item and 'VNumber' in json_data:
                                phone = json_data['VNumber']
                                # print('Phone: {}'.format(phone))
                                phone = re.sub(r'\+', '', phone)
                                phone = re.sub(r'=', '', phone)
                                item['phone_number'] = phone

                            # print(item['phone_number'])

                            if 'rating' in json_data:
                                item['rating'] = json_data['rating']

                            if 'totalReviews' in json_data:
                                item['total_reviews'] = json_data['totalReviews']

                            if 'YOE' in json_data:
                                item['year_establish'] = json_data['YOE']

                            if 'area' in json_data:
                                item['area'] = json_data['area']
                            if 'complat' in json_data:
                                item['latitude'] = json_data['complat']
                            if 'complong' in json_data:
                                item['longitude'] = json_data['complong']
                            if 'email' in json_data:
                                item['email'] = json_data['email'].strip().strip(',').strip()
                            if 'data_city' in json_data:
                                item['city'] = json_data['data_city']
                except Exception as ex:
                    logger.error('Error when api call: {}'.format(ex))

            self.__write_item(item)
        except Exception as x:
            logger.error('Error when parse details page: {}'.format(x))

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                with self.__total.get_lock():
                    self.__total.value += 1
                    logger.info('Total: {}'.format(self.__total.value))
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # mode = 1
    # input_csv = 'all_city_jd.csv'  # input('Please specify Input csv: ')
    # output_csv = 'all_city_jd_p.csv'  # input('Please specify output csv: ')

    # mode = 0
    # input_csv = 'all_city_jd_p.csv'  # input('Please specify Input csv: ')
    # output_csv = 'all_city_jd_output.csv'  # input('Please specify output csv: ')
    # with JustDialCrawler('jd.csv', 'justdial_output_updated.csv') as crawler:

    mode = input('Please specify Mode (1 for save all urls; 0 for parse details from previously saved pages): ')
    input_csv = input('Please specify Input csv: ')
    output_csv = input('Please specify output csv: ')
    with JustDialCrawler(input_csv, output_csv, mode=int(mode)) as crawler:
        crawler.process_data()
