import csv


def write_data(row, mode='a'):
    try:
        with open('trip_advisor_final.csv', mode, newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(row)
    except Exception as x:
        print('Error printing csv output: ' + str(x))


def clean_data():
    try:
        with open('trip_output_final1.csv', 'r', newline='', encoding='utf-8') as f:
            reader = csv.reader(f)
            for row in reader:
                if 'about:blank' in row[5]:
                    row[5] = ''
                write_data(row)
    except Exception as x:
        print(x)


clean_data()