import csv
import gzip
import json
import logging
import os
import random
import re
import readline
import socket
import urllib.parse
import urllib.request
# import socks

from multiprocessing.pool import ThreadPool

import time
from bs4 import BeautifulSoup
from multiprocessing import Lock

readline.parse_and_bind("control-v: paste")


class EbayGrabber:
    # __start_url = 'http://stores.ebay.com/toysrus/_i.html?rt=nc&_sop={}&_pgn={}&_ipg=192'
    __start_url = 'http://stores.ebay.com/OfficialBestBuy/_i.html?rt=nc&_sasi={}&_pgn={}'
    # __search_url = 'https://www.ebay.com/sch/i.html?_from=R40&_trksid=m570.l1313&_nkw={}&_pgn={}&_sacat=0'
    # __search_url = 'https://www.ebay.com/sch/i.html?_from=R40&_sacat=0&_nkw={}&_pgn={}&_sop=15'
    __search_url = 'https://www.ebay.com/sch/i.html?_from=R40&_sacat=0&_nkw={}&_sop=15'
    __proxies = {
        # 'http': '83.149.70.159:13042',
        # 'https': '37.48.118.90:13042'
        'http': '108.59.14.208:13040',
        'https': '108.59.14.203:13040'
    }
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __total = 0
    __url_cache = []
    __current_keyword = ''

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('ebay.log')
        socket.setdefaulttimeout(15)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        proxy_handler = urllib.request.ProxyHandler(self.__proxies)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        opener = urllib.request.build_opener(proxy_handler)
        # urllib.request.install_opener(opener)
        self.__field_names = ['url', 'item_title', 'condition', 'img_src', 'category_string', 'desc', 'upc', 'mpn',
                              'brand', 'orig_price', 'sale_price', 'shipping_cost', 'keyword']
        csv_header = {'url': 'Product page URL',
                      'item_title': 'Item Title',
                      'condition': 'Condition',
                      'img_src': 'Image source url',
                      'category_string': 'Category string',
                      'desc': 'Item description',
                      'upc': 'UPC',
                      'mpn': 'MPN',
                      'brand': 'Brand',
                      'orig_price': 'Original price',
                      'sale_price': 'Sale Price',
                      'shipping_cost': 'Shipping cost',
                      'keyword': 'Keyword'
                      }

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            # sop_list = [1, 2, 3, 10, 15, 16]
            sop_list = [0, 1, 2]
            for sop in sop_list:
                next_page = self.__grab_page(self.__start_url.format(sop, 1))
                page_no = 2
                while next_page:
                    next_page = self.__grab_page(self.__start_url.format(sop, page_no))
                    page_no += 1
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def grab_details_from_file(self, filename):
        try:

            if os.path.exists(filename):
                with open(filename, 'r+', encoding='utf-8') as f:
                    reader = csv.reader(f)
                    product_urls = []
                    for row in reader:
                        if row[0] != '' and row[0].strip() not in self.__url_cache:
                            product_urls.append(row[0].strip())

                        if len(product_urls) == 128:
                            with ThreadPool(16) as p:
                                p.map(self.__grab_details, product_urls)
                            product_urls = []

                self.__logger.info('==== Finish ====')
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def grab_data_by_keyword(self, filename):
        try:

            if os.path.exists(filename):
                with open(filename, 'r+', encoding='utf-8') as f:
                    reader = csv.reader(f)
                    for row in reader:
                        keyword = row[0].strip()
                        if keyword == 'upc':
                            continue

                        keyword = keyword.strip('\'')
                        self.__current_keyword = keyword
                        # for i in range(1, 2):
                        # kw_url = self.__search_url.format(urllib.parse.quote_plus(keyword), 1)
                            # if not self.__grab_page(kw_url):
                            #     break

                        kw_url = self.__search_url.format(urllib.parse.quote_plus(keyword))
                        self.__grab_page(kw_url)

                self.__logger.info('==== Finish ====')
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __grab_page(self, url, retry=0):
        try:
            self.__logger.info('=== URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                self.__logger.error('No results found!')
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                self.__logger.error('No results found!')
                return

            product_tags = soup.find_all('div', class_='span3 item-cell')
            if product_tags and len(product_tags) > 0:
                self.__logger.info('Total products found on this page: {}'.format(len(product_tags)))
                product_urls = []
                for product_tag in product_tags:
                    if len(product_urls) == 10:
                        self.__logger.warning('Max record limit reached!')
                        break

                    product_link_tag = product_tag.find('a', class_='vi-url')
                    if product_link_tag:
                        product_url = product_link_tag.get('href')
                        if product_url not in self.__url_cache:
                            product_urls.append(product_url)
                            # self.__grab_details(product_url)
                            self.__url_cache.append(product_url)
                        else:
                            self.__logger.warning('Already grabbed URL: {}.'.format(product_url))

                with ThreadPool(10) as p:
                    p.map(self.__grab_details, product_urls)
                return

            product_tags = soup.find_all('div', class_='tpitem_g')
            if product_tags and len(product_tags) > 0:
                self.__logger.info('Total products found on this page: {}'.format(len(product_tags)))
                product_urls = []
                for product_tag in product_tags:
                    if len(product_urls) == 10:
                        self.__logger.warning('Max record limit reached!')
                        break

                    product_link_tag = product_tag.find('a', class_='v4lnk')
                    if product_link_tag:
                        product_url = product_link_tag.get('href')
                        if product_url not in self.__url_cache:
                            product_urls.append(product_url)
                            # self.__grab_details(product_url)
                            # self.__url_cache.append(product_url)
                        else:
                            self.__logger.warning('Already grabbed URL: {}.'.format(product_url))

                with ThreadPool(16) as p:
                    p.map(self.__grab_details, product_urls)
                return True

            product_tags = soup.find_all('a', class_='vip')
            if product_tags and len(product_tags) > 0:
                self.__logger.info('Total products found on this page: {}'.format(len(product_tags)))
                product_urls = []
                for product_tag in product_tags:
                    if len(product_urls) == 10:
                        self.__logger.warning('Max record limit reached!')
                        break
                    product_url = product_tag.get('href')
                    if 'https://www.ebay.com' not in product_url:
                        continue
                    if product_url not in self.__url_cache:
                        product_urls.append(product_url)
                        # self.__grab_details(product_url)
                        # self.__url_cache.append(product_url)
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(product_url))

                with ThreadPool(16) as p:
                    p.map(self.__grab_details, product_urls)
                return True
            else:
                self.__logger.error('No item found!!')

                if retry < 5:
                    sleep_time = random.randint(20, 30)
                    self.__logger.info(
                        'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                    time.sleep(sleep_time)
                    return self.__grab_page(url, retry + 1)
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

            if retry < 5:
                sleep_time = random.randint(20, 30)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_page(url, retry + 1)

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))

            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            item = {'url': url}
            title_tag = soup.find('h1', id='itemTitle')
            if title_tag:
                item['item_title'] = title_tag.text.strip()

            images = re.search(r'\"imgArr\"\s*?\:\s*?(\[[^\]]*?\])', data, re.MULTILINE)
            if images:
                img_array = images.group(1)
                try:
                    img_array = urllib.parse.unquote_plus(img_array.encode('utf-8').decode('unicode-escape', 'ignore'))
                    json_data = json.loads(img_array)
                    if json_data:
                        for json_img in json_data:
                            if 'maxImageUrl' in json_img:
                                item['img_src'] = json_img['maxImageUrl']
                                break
                except Exception as ex:
                    self.__logger.error('Error parsing large image: {}'.format(ex))
            else:
                img_src_tag = soup.find('img', id='icImg')
                if img_src_tag:
                    item['img_src'] = img_src_tag.get('src').strip()

            desc_tag = soup.find('meta', {'name': 'description'})
            if desc_tag and desc_tag.has_attr('content'):
                item['desc'] = desc_tag.get('content').strip()

            upc_tag = soup.find('h2', {'itemprop': 'gtin13'})
            if upc_tag:
                item['upc'] = upc_tag.text.strip()

            mpn_tag = soup.find('h2', {'itemprop': 'mpn'})
            if mpn_tag:
                item['mpn'] = mpn_tag.text.strip()

            brand_tag = soup.find('h2', {'itemprop': 'brand'})
            if brand_tag:
                item['brand'] = brand_tag.text.strip()

            orig_price_tag = soup.find('span', id='orgPrc')
            if orig_price_tag:
                price = orig_price_tag.text.strip()
                price = re.sub('[^\d\.]', '', price)
                item['orig_price'] = price.strip()

            sale_price_tag = soup.find('span', id='prcIsum')
            if sale_price_tag:
                price = sale_price_tag.text.strip()
                price = re.sub('[^\d\.]', '', price)
                item['sale_price'] = price.strip()

            if 'orig_price' not in item and 'sale_price' in item:
                item['orig_price'] = item['sale_price']
                item['sale_price'] = ''

            item_condition_tag = soup.find('div', {'itemprop': 'itemCondition'})
            if item_condition_tag:
                item['condition'] = item_condition_tag.text.replace('(see details)', '').strip()

            shipping_cost_tag = soup.find('span', id='fshippingCost')
            if shipping_cost_tag:
                item['shipping_cost'] = shipping_cost_tag.text.strip()

            category_string_tag = soup.find('a', class_='scnd')
            if category_string_tag:
                item['category_string'] = category_string_tag.text.strip()

            item['keyword'] = self.__current_keyword
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

        except Exception as x:
            self.__logger.error('Error grab details page: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    # keyword = input('Please specify Keyword: ')
    # location = input('Please specify Location: ')
    # output_file = input('Please specify output file name: ')

    # keyword = 'plumber'
    # location = 'toronto'
    output_file = 'ebay_output_upc_1_page.csv'
    with EbayGrabber(output_file) as spider:
        spider.grab_data_by_keyword('upc.csv')
        # spider.grab_data_by_keyword('Keywords.csv')
        # spider.grab_details_from_file('target.csv')
        # spider.grab_data()
