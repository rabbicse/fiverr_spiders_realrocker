import csv
import json
import logging
import os
import random
import re
import time
from collections import OrderedDict
from multiprocessing import Semaphore, Value

from bs4 import BeautifulSoup
from easy_spider import Spider

logger = logging.getLogger(__name__)


# http://tsdr.uspto.gov/statusview/rn5754716
class TsdrCrawler(Spider):
    __start_url = 'https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=0000789019&type=&dateb=&owner=only&start=0&count=100'
    __base_uri = 'https://www.sec.gov'
    __search_uri = 'http://tsdr.uspto.gov/statusview/{}'
    __url_cache = []
    __page_cache = []
    __total = Value('i', 0)
    __lock = Semaphore()
    __MIN_COUNT = 500

    def __init__(self, input_csv, output_csv):
        Spider.__init__(self, log_file='sec_gov.log')
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __enter__(self):
        self.__init_full()
        return self

    def __init_full(self):
        hdr = [('url', 'Url'),
               ('title_mark', 'Title/Mark'),
               ('app_no', 'App No.'),
               ('reg_no', 'Reg No.'),
               ('mark', 'Mark'),
               ('us_serial_number', 'US serial Number'),
               ('reg_num', 'Registration number'),
               ('attorney_name', 'Attorney Name'),
               ('attorney_primary_email_address', 'Attorney Primary Email Address'),
               ('correspondent_name_address', 'Correspondent Name/Address'),
               ('correspondent_email', 'Correspondent Email')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        ##If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        self.__prepare_proxies()

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

    def __prepare_proxies(self):
        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.proxies = json_data

        # if os.path.exists('proxylist.csv'):
        #     with open('proxylist.csv', 'r+', encoding='utf-8') as f:
        #         self.proxies = []
        #         reader = csv.reader(f)
        #         for row in reader:
        #             if not row:
        #                 continue
        #
        #             if len(row) <= 0:
        #                 continue
        #
        #             p = row[0]
        #             https = 'no'
        #             if 'https://' in p:
        #                 https = 'yes'
        #
        #             p = p.replace('https://', '')
        #             p = p.replace('http://', '')
        #             p_list = p.split(':')
        #             if len(p_list) < 2:
        #                 continue
        #
        #             ip = p_list[0]  # + ':' + p_list[1]
        #             # proxy = {"ip": ''.join(p_list[0:-1]), "port": p_list[-1], "https": "no"}
        #             proxy = {"ip": ip, "port": p_list[-1], "https": https}
        #             self.proxies.append(proxy)

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Total records: {}'.format(self.__total.value))
        logger.info('Finish!!!')
        del self

    def process_data(self):
        try:
            if os.path.exists(self.__input_csv):
                with open(self.__input_csv, 'r', errors='ignore') as f:
                    reader = csv.reader(f, quoting=csv.QUOTE_ALL)

                    i = 0
                    items = []
                    for row in reader:
                        if i == 0:
                            i += 1
                            continue

                        reg_no = row[-1].strip('\xa0')
                        details_uri = ''
                        if reg_no.strip() != '':
                            details_uri = self.__search_uri.format('rn' + reg_no)

                        details_uri1 = ''
                        app_no = re.sub(r'[^0-9]', '', row[1])
                        if app_no.strip() != '':
                            details_uri1 = self.__search_uri.format('sn' + app_no)

                        if details_uri != '' and details_uri not in self.__url_cache and details_uri1 not in self.__url_cache:
                            if not self._grab_details(details_uri, row[0], row[1], row[2]):
                                self._grab_details(details_uri1, row[0], row[1], row[2])
                        elif details_uri1 != '' and details_uri1 not in self.__url_cache:
                            self._grab_details(details_uri1, row[0], row[1], row[2])
                        else:
                            logger.warning('URL: [{}] already processed!'.format(details_uri))

                    # with ThreadPool(1) as pool:
                    #     pool.map(self._grab_details, items)

        except Exception as x:
            logger.error('Error when process data: {}'.format(x))

    def _grab_details(self, url, title, app_no, reg_no):
        try:

            time.sleep(random.randint(2, 5))

            response = self.fetch_data(url)
            if not response:
                return

            data, redirected_url, opener = response
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            item = {'url': url,
                    'title_mark': title,
                    'app_no': app_no,
                    'reg_no': reg_no}
            mark_div = soup.find('div', class_='value markText')
            if mark_div:
                item['mark'] = mark_div.text.strip()

            rows = soup.find_all('div', class_='row')
            for row in rows:
                div_key = row.find('div', class_='key')
                if not div_key:
                    continue

                div_value = row.find('div', class_='value')
                if not div_value:
                    continue

                if 'US Serial Number:' in div_key.text:
                    item['us_serial_number'] = div_value.text.strip()

                if 'US Registration Number:' in div_key.text:
                    item['reg_num'] = div_value.text.strip()

                if 'Attorney Name:' in div_key.text:
                    item['attorney_name'] = div_value.text.strip()

                if 'Attorney Primary Email Address:' in div_key.text:
                    item['attorney_primary_email_address'] = div_value.text.strip()

                if 'Correspondent Name/Address:' in div_key.text:
                    item['correspondent_name_address'] = div_value.text.strip()

                if 'Correspondent e-mail:' in div_key.text:
                    item['correspondent_email'] = div_value.text.strip()

            self.__write_item(item)
            return True
        except Exception as x:
            logger.error('Error when parse details page: {}'.format(x))
        return False

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        """
        :param row:
        :param mode:
        :return:
        """
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                with self.__total.get_lock():
                    self.__total.value += 1
                    logger.info('Total: {}'.format(self.__total.value))
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # mode = 1
    input_csv = 'tsdr_new1.csv'  # input('Please specify Input csv: ')
    output_csv = 'tsdr_output_updated1.csv'  # input('Please specify output csv: ')

    # mode = input('Please specify Mode (1 for save all urls; 0 for parse details from previously saved pages): ')
    # input_csv = input('Please specify Input csv: ')
    # output_csv = input('Please specify output csv: ')
    with TsdrCrawler(input_csv, output_csv) as crawler:
        crawler.process_data()
