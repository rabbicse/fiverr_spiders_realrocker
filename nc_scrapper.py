import csv
import gzip
import json
import logging
import os
import random
import re
import readline
import socket
import urllib.parse
import urllib.request
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool

import sys
from OpenGL import GL
from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings, QWebEngineProfile
from PyQt5.QtWidgets import QApplication

import time
from bs4 import BeautifulSoup

readline.parse_and_bind("control-v: paste")


class ReonomyBrowser(QWebEngineView):
    __redirected_url = None
    __html = None

    def __init__(self, app):
        QWebEngineView.__init__(self)
        self.__app = app
        self.settings().setAttribute(QWebEngineSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        # self.urlChanged.connect(self.__url_changed)
        self.loadFinished.connect(self._load_finished)
        self.__loaded = False

    def fetch(self, search_key, logger):
        try:
            self.__search_key = search_key
            self.__login_url = 'https://app.reonomy.com/login'
            self.__logger = logger
            self.timeout_timer = QTimer()
            self.timeout_timer.timeout.connect(self._request_timed_out)
            self.timeout_timer.start(120 * 1000)

            self.__logger.info('Trying login: {}'.format(self.__login_url))
            self.setUrl(QUrl(self.__login_url))
            self.resize(1920, 1080)
            self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            self.__logger.error('Error when fetch data: {}'.format(x))
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            self.__logger.warning('Custom request timeout value exceeded.')
            self.timeout_timer.stop()
            self.__app.quit()
        except Exception as x:
            self.__logger.error('Request timeout: {}'.format(x))

    # def __url_changed(self, url):
    #     try:
    #         current_url = str(url.toString())
    #         if current_url != self.__login_url:
    #             print(current_url)
    #             self.__redirected_url = current_url
    #         if
    #             # self.__app.quit()
    #     except Exception as x:
    #         self.__logger.error('Error when load response: {}'.format(x))
    #         self.__app.quit()

    def _load_finished(self):
        try:
            current_url = str(self.page().url().toString())
            if current_url == self.__login_url:
                self.__run_js_login()

            if current_url == 'https://app.reonomy.com/search':
                self.__logger.info('Logged in!')
                print(current_url)
                self.__redirected_url = current_url

                count_js = """document.querySelector('input[name="address"]').outerHTML"""
                self.page().runJavaScript(count_js, self.__js_click_callback)
        except Exception as x:
            self.__logger.error('Error when load response: {}'.format(x))
            self.__app.quit()

    def __js_click_callback(self, data):
        print('click callback found...')
        print(data)
        if not data:
            count_js = """document.querySelector('input[name="address"]').count"""
            self.page().runJavaScript(count_js, self.__js_click_callback)
        else:
            search_js = """document.querySelector('input[name="address"]').value='{}'""".format(
                self.__search_key)
            submit_js = """document.querySelector('button[type="submit"]').click()"""
            self.page().runJavaScript(search_js)
            self.page().runJavaScript(submit_js)

    def __run_js_login(self):
        fill_user = """document.getElementById("email").value='{}'""".format('avi@blackmountainp.com')
        fill_pass = """document.getElementById("password").value='{}'""".format('March52018')
        submit = """document.getElementById("login").click()"""
        self.page().runJavaScript(fill_user)
        self.page().runJavaScript(fill_pass)
        self.page().runJavaScript(submit)

    def process_html(self, html):
        self.__html = html
        self.__app.quit()


class Browser(QWebEngineView):
    __redirected_url = None
    __html = None
    timerScreen = None
    timeout_timer = None

    def __init__(self, app):
        QWebEngineView.__init__(self)
        self.__app = app
        self.settings().setAttribute(QWebEngineSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.loadFinished.connect(self._load_finished)
        self.__loaded = False

    def fetch(self, url, logger):
        try:
            self.__logger = logger
            self.timeout_timer = QTimer()
            self.timeout_timer.timeout.connect(self._request_timed_out)
            self.timeout_timer.start(120 * 1000)

            self.__logger.info('Trying search via: {}'.format(url))
            self.setUrl(QUrl(url))
            self.resize(1920, 1080)
            self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            self.__logger.error('Error when fetch data: {}'.format(x))
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            self.__logger.warning('Custom request timeout value exceeded.')
            self.timeout_timer.stop()
            self.close()
            self.__app.quit()
        except Exception as x:
            self.__logger.error('Request timeout: {}'.format(x))

    def _load_finished(self):
        try:
            self.timeout_timer.stop()
            self.__search_js()
            # self.page().toHtml(self.process_html)
        except Exception as x:
            self.__logger.error('Error when load response: {}'.format(x))
            self.close()
            self.__app.quit()

    def __search_js(self):
        if self.timerScreen:
            self.timerScreen.stop()

        search_js = """document.querySelector('html').outerHTML"""
        self.page().runJavaScript(search_js, self.__search_result_callback)

    def __search_result_callback(self, data):
        if data:
            soup = BeautifulSoup(data, 'html5lib')
            search_result_tag = soup.find('div', class_='rc')
            if search_result_tag:
                self.page().toHtml(self.process_html)
                return

        self.__logger.info('Waiting for result...')
        self.timerScreen = QTimer()
        self.timerScreen.timeout.connect(self.__search_js)
        self.timerScreen.start(2 * 1000)

    def process_html(self, html):
        self.__html = html
        if self.timerScreen:
            self.timerScreen.stop()
            self.timerScreen = None
        if self.timeout_timer:
            self.timeout_timer.stop()
            self.timeout_timer = None

        self.close()
        self.__app.quit()


class NcGrabber:
    """
    https://nursinghomereport.org/
    https://nursinghomereport.org/state-NC/
    https://nursinghomereport.org/abbotts-creek-center-345333/
    Avante at Wilkesboro
    https://nursinghomereport.org/avante-at-wilkesboro-345133/
    https://www.hipaaspace.com/medical_billing/coding/national_provider_identifier/codes/npi_1255416293.aspx
    
    """
    __base_url = 'https://nursinghomereport.org'
    __category_url = 'https://www.houzz.com/photos/{}'
    __product_url = 'https://www.houzz.com/product/{}'
    __embedded_url = 'https://www.houzz.com/j/pxy/getEmbedSpaceCode'

    __nc_url = 'https://nursinghomereport.org/state-NC/'
    __search_google_url = 'https://www.google.com/search?{}'

    __proxies = (
        '108.59.14.208:13040',
        '108.59.14.203:13040'
    )
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        # 'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}
    __total = 0
    __url_cache = []
    __input_data = []

    def __init__(self, input_csv, output_csv):
        self.__input_csv = input_csv
        self.__output_csv = output_csv

    def __enter__(self):
        self.__app = QApplication(sys.argv)
        self.__browser = Browser(self.__app)
        self.__rbrowser = ReonomyBrowser(self.__app)
        self.__lock = Lock()
        self.__setup_logger('houzz.log')
        socket.setdefaulttimeout(30)

        self.__field_names = ['federalprovidernumber', 'providername', 'provideraddress', 'providercity',
                              'providerstate', 'providerzipcode', 'ownershiptype',
                              'ownertypeindividualotherthanoperationalmanagerialcontrol', 'numberofcertifiedbeds',
                              'occupancy', 'operatorlegalbusinessname', 'nhr', 'hipaa', 'propertylegalname', 'sosid',
                              'manager1', 'member1']
        csv_header = {'federalprovidernumber': 'Federal Provider Number',
                      'providername': 'Provider Name',
                      'provideraddress': 'Provider Address',
                      'providercity': 'Provider City',
                      'providerstate': 'Provider State',
                      'providerzipcode': 'Provider Zip Code',
                      'ownershiptype': 'Ownership Type',
                      'ownertypeindividualotherthanoperationalmanagerialcontrol': 'Owner Type-Individual (Other than OPERATIONAL/MANAGERIAL CONTROL)',
                      'numberofcertifiedbeds': 'Number of Certified Beds',
                      'occupancy': 'Occupancy %',
                      'operatorlegalbusinessname': 'Operator Legal Business Name',
                      'nhr': 'NHR',
                      'hipaa': 'Hipaa',
                      'propertylegalname': 'Property Legal Name',
                      'sosid': 'SOSid',
                      'manager1': 'Manager 1',
                      'member1': 'Member 1'
                      }

        if os.path.exists(self.__input_csv):
            with open(self.__input_csv, 'r+', encoding='utf-8') as f:
                reader = csv.reader(f, self.__field_names)
                for row in reader:
                    item = {'federalprovidernumber': row[0],
                            'providername': row[1],
                            'provideraddress': row[2],
                            'providercity': row[3],
                            'providerstate': row[4],
                            'providerzipcode': row[5],
                            'ownershiptype': row[6],
                            'ownertypeindividualotherthanoperationalmanagerialcontrol': row[7],
                            'numberofcertifiedbeds': row[8],
                            'occupancy': row[9],
                            'operatorlegalbusinessname': row[10],
                            'nhr': row[11],
                            'hipaa': row[12],
                            'propertylegalname': row[13],
                            'sosid': row[14],
                            'manager1': row[15],
                            'member1': row[16]}
                    self.__input_data.append(item)

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['federalprovidernumber'])

        if csv_header['federalprovidernumber'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler(log_file_name, maxBytes=1 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __get_random_proxy(self):
        try:
            return None
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy_url = str(random.choice(self.__proxies))

            if not proxy_url.startswith('http://'):
                proxy_url = 'http://' + proxy_url

            return {
                "http": proxy_url,
                "https": proxy_url
            }
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__browser.close()
        self.__rbrowser.close()
        self.__app.exit()
        del self.__browser
        del self.__rbrowser
        del self.__app
        del self.__logger
        del self

    def grab_data(self):
        try:
            if len(self.__input_data) == 0:
                return

            # grab all nc data
            nc_list = self.__search_data_nc()
            for row in self.__input_data[1:]:
                item = row.copy()

                # process nhr
                nhr = [x.popitem() for x in nc_list if item['providername'].upper() in x.keys()]
                if nhr and len(nhr) > 0:
                    item['nhr'] = nhr[0][1]

                # process google search
                google_search_key = item['operatorlegalbusinessname'] + '+ site:hipaaspace.com'
                gs = self.__search_data_google(google_search_key)
                if gs:
                    item['hipaa'] = gs


                    # # login and get data from reonomy
                    # search_key = item['providername'] + ' ' + item['provideraddress'] + ' ' + item['providercity'] + ' ' + item['providerstate'] + ' ' + item['providerzipcode']
                    # self.__rbrowser.fetch(search_key, self.__logger)
                    #
                    # break

                self.__logger.info('Data: {}'.format(item))
                self.__write_data(item)
        except Exception as x:
            self.__logger.error('Error when processing: {}'.format(x))

    def __search_data_nc(self, retry=0):
        opener = None
        try:
            self.__logger.info('=== Search URL: {} ==='.format(self.__nc_url))
            opener = self.__create_opener()
            data = opener.open(self.__nc_url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            nc_list = []
            tr_tag_list = soup.find_all('tr')
            for tr in tr_tag_list[1:]:
                td_list = tr.find_all('td')
                if len(td_list) == 0:
                    continue

                td = td_list[0]
                a_tag = td.find('a')
                if not a_tag:
                    continue

                k = a_tag.text.strip().upper()
                v = self.__base_url + a_tag.get('href')
                item = {k: v}
                nc_list.append(item)
            return nc_list
        except Exception as x:
            self.__logger.error('Error grab category: {}'.format(x))
            if retry < 5:
                return self.__search_data_nc(retry + 1)
        finally:
            if opener:
                opener.close()
                del opener

    def __search_data_google(self, search_key, retry=0):
        try:
            post_data = {'q': search_key}
            post_req = urllib.parse.urlencode(post_data)
            url = self.__search_google_url.format(post_req)

            r, data = self.__browser.fetch(url, self.__logger)
            soup = BeautifulSoup(data, 'html5lib')
            search_result_tag = soup.find('div', class_='rc')
            if not search_result_tag:
                return
            search_url_tag = search_result_tag.find('a')
            if search_url_tag:
                return search_url_tag.get('href')
        except Exception as x:
            self.__logger.error('Error grab category: {}'.format(x))
            if retry < 5:
                return self.__search_data_nc(search_key, retry + 1)

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    input_file = 'nc_input.csv'
    output_file = 'nc_output.csv'
    with NcGrabber(input_file, output_file) as spider:
        spider.grab_data()
    print('Script finished!')
