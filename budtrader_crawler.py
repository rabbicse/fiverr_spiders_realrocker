import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from datetime import datetime, timedelta
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool

from bs4 import BeautifulSoup


class BudTraderSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url = 'https://budtrader.com/'
    __start_url = 'https://budtrader.com/'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 5

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('budtrader.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__lock = Lock()
        self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(60)

        hdr = [('name', 'Name'),
               ('price', 'Price'),
               ('desc', 'Description'),
               ('itm_address', 'Item Address'),
               ('location', 'Location'),
               ('image_urls', 'Image URLS'),
               ('url', 'URL')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # if os.path.exists(self.__output_csv):
        #     with open(self.__output_csv, 'r+', encoding='utf-8') as f:
        #         reader = csv.DictReader(f, self.__field_names)
        #         for row in reader:
        #             self.__url_cache.append(row['url'])

        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__output_csv, self.__csv_header)

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:
            city_urls = self.__grab_all_cities()
            if city_urls and len(city_urls) > 0:
                with ThreadPool(4) as p:
                    p.map(self.__grab_all_data_by_city, city_urls)
                    # for city_url in city_urls:
                    #     self.__grab_all_data_by_city(city_url)
        except Exception as x:
            print(x)

    def process_output(self, out_csv):
        try:

            hdr = [('name', 'Name'),
                   ('price', 'Price'),
                   ('desc', 'Description'),
                   ('phone', 'Phone'),
                   ('city', 'City'),
                   ('zip', 'Zip/Postal Code'),
                   ('website', 'Website'),
                   ('listed', 'Listed'),
                   ('expires', 'Expires'),
                   ('location', 'Location'),
                   ('image_urls', 'Image URLS'),
                   ('url', 'URL')]

            out_header = OrderedDict(hdr)
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)

                self.__field_names = list(out_header.keys())
                self.__write_data(out_csv, out_header, mode='w+')

                for row in reader:
                    item = row.copy()

                    if item['url'] in self.__url_cache or item['url'] == 'URL':
                        self.__logger.warning('Item already exists! {}'.format(item['url']))
                        continue

                    itm_address = item['itm_address']
                    itm_address = itm_address.replace('City:', '\nCity:')
                    itm_address = itm_address.replace('Zip/Postal Code:', '\nZip/Postal Code:')
                    itm_address = itm_address.replace('Website:', '\nWebsite:')

                    itms = itm_address.split('\n')
                    for itm in itms:
                        if itm.strip() == '':
                            continue

                        if 'Phone Number:' in itm.strip():
                            item['phone'] = itm.replace('Phone Number:', '').strip()

                        if 'City:' in itm.strip():
                            item['city'] = itm.replace('City:', '').strip()

                        if 'Zip/Postal Code:' in itm.strip():
                            item['zip'] = itm.replace('Zip/Postal Code:', '').strip()

                        if 'Website:' in itm.strip():
                            item['website'] = itm.replace('Website:', '').strip()

                        if 'Listed:' in itm.strip():
                            item['listed'] = itm.replace('Listed:', '').strip()

                        if 'Expires:' in itm.strip():
                            item['expires'] = itm.replace('Expires:', '').strip()

                    del item['itm_address']
                    try:
                        self.__logger.info('Data: {}'.format(item))
                        self.__lock.acquire()
                        self.__write_data(out_csv, item)
                        self.__url_cache.append(item['url'])
                    except Exception as ex:
                        self.__logger.error('Error grab details page 2: {}'.format(ex))
                    finally:
                        self.__lock.release()


        except Exception as x:
            print(x)

    def __grab_all_cities(self, retry=0):
        try:
            self.__logger.info('=== Main URL: {} ==='.format(self.__start_url))
            opener = self.__create_opener()
            data = opener.open(self.__start_url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            city_urls = []
            city_a_list = soup.find_all('a', class_='btn btn-city hvr-float-shadow')
            for city_a in city_a_list:
                city_urls.append(city_a.get('href'))
            return city_urls
        except Exception as x:
            self.__logger.error('Error when grab all cities.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_all_cities(retry + 1)

    def __grab_all_data_by_city(self, url):
        try:
            next_link = self.__grab_data_by_city(url)
            while next_link:
                next_link = self.__grab_data_by_city(next_link)
        except Exception as x:
            self.__logger.error('Error when grab all data by city. {}'.format(x))

    def __grab_data_by_city(self, url, retry=0):
        try:
            self.__logger.info('=== City URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            url_list = []
            ad_url_tds = soup.find_all('td', class_='listingPro')
            for ad_url_td in ad_url_tds:
                ad_url_a = ad_url_td.find('a')
                if not ad_url_a:
                    continue

                details_url = ad_url_a.get('href')
                if details_url not in self.__url_cache:
                    url_list.append(details_url)
                else:
                    self.__logger.warning('Link: {} already grabbed.'.format(details_url))

            with ThreadPool(16) as p:
                p.map(self.__grab_details, url_list)

            next_link_a = soup.find('a', class_='nextpostslink')
            if next_link_a:
                return next_link_a.get('href')
        except Exception as x:
            self.__logger.error('Error when grab all cities.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_data_by_city(url, retry + 1)

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            item = {'url': url}

            name_h1 = soup.find('h1', class_='single-listing')
            if name_h1:
                item['name'] = name_h1.text.strip()

            price_p = soup.find('p', class_='post-price')
            if price_p:
                item['price'] = price_p.text.strip()

            desc_h3 = soup.find('h3', class_='description-area')
            if desc_h3:
                desc_p = desc_h3.find_next_sibling('p')
                if desc_p:
                    item['desc'] = desc_p.text.strip()

            address_div = soup.find('div', class_='ad_address')
            if address_div:
                address_ul = address_div.find('ul')
                if address_ul:
                    address = address_ul.text.strip()
                    address = re.sub(r'\t+', ' ', address)
                    item['itm_address'] = address

            location_m = re.search(r'new\s*?google\.maps\.LatLng\(([0-9-\.]+)\,\s*?([0-9-\.]+)\)', data)
            if location_m:
                lat = location_m.group(1)
                lon = location_m.group(2)
                item['location'] = 'Latitude: {}; Longitude: {}'.format(lat, lon)

            images_div = soup.find('div', id='main-pic')
            if images_div:
                images_a = images_div.find_all('a')
                if images_a and len(images_a) > 0:
                    image_urls = []
                    for image_a in images_a:
                        image_urls.append(image_a.get('href'))
                    item['image_urls'] = '; '.join(image_urls)

            try:
                self.__logger.info('Data: {}'.format(item))
                self.__lock.acquire()
                self.__write_data(self.__output_csv, item)
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()
        except Exception as x:
            self.__logger.error('Error when grab bet hkjc details page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __write_data(self, output_csv, row, mode='a+'):
        try:
            with open(output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'budtrader_output.csv'
    with BudTraderSpider(output_file) as spider:
        spider.process_output('budtrader_output_final.csv')
        # spider.grab_data()
