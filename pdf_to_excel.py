import PyPDF2

with open('EWG_License_Program_Restricted_List_C01.pdf', 'rb') as f:
    pdfFileReader = PyPDF2.PdfFileReader(f)
    # Get the specified pdf page object.
    pdfPage = pdfFileReader.getPage(1)

    # Get pdf page text.
    text = pdfPage.extractText()
    print(text)