import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import time
import urllib.request
import urllib.parse
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class SuperpagesUpdatedGrabber:
    __lock = Lock()
    MAX_PAGE = 69024
    __base_url = 'http://www.superpages.com.au/listings/search/page:{}?keyword=&hometypecat=1&loc_address=&location_id=&location_type='
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    __proxies = None
    __url_cache = []
    __total = 0

    def __init__(self, output_csv):
        self.__output_csv = output_csv
        socket.setdefaulttimeout(120)

    def __setup_logger(self):
        """
        Script will create log file and write std output to terminal to track bug or status
        :return:
        """
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('superpages.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        """
        Initialize script and load required modules or initial setup of script.
        :return:
        """
        # lock object will need when we'll apply multi-threaded operation
        self.__lock = Lock()

        # Initialize/setup logger
        self.__setup_logger()

        hdr = [('name', 'Name'),
               ('email', 'Email'),
               ('website', 'Website'),
               ('contact', 'Contact Number'),
               ('address', 'Address'),
               ('city', 'City'),
               ('state', 'State'),
               ('zip_code', 'Zip Code'),
               ('business_category', 'Business Category'),
               ('keywords', 'Keywords'),
               ('fax', 'Fax'),
               ('social', 'Social URLS'),
               ('url', 'URL')]
        csv_header = OrderedDict(hdr)
        self.__field_names = list(csv_header.keys())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        # if os.path.exists('proxy.json'):
        #     with open('proxy.json', 'r+', encoding='utf-8') as f:
        #         json_data = json.load(f)
        #         self.__proxies = json_data

        # If header not yet written then write csv header first
        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)
            self.__url_cache.append(csv_header['url'])

        # Calculate previous records for debugging purpose
        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            if proxy['https'] == 'yes':
                return {'https': '{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': '{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                self.__logger.info('Proxy: {}'.format(proxy))
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def clean_data(self):
        try:
            with open(self.__output_csv, 'r', newline='', encoding='utf-8') as f:
                reader = csv.reader(f)
                for row in reader:
                    if any(word in str(row[0]).strip().lower() for word in
                           ['school', 'church', 'kindergarten', 'kindy']):
                        print('skipping...')
                        continue
                    self.__write_data(row)
        except Exception as x:
            print(x)

    def grab_data(self):
        try:
            # item = {'url': 'http://www.superpages.com.au/listings/details/crc-craig-reddish-constructions-1905'}
            # self.__grab_details(item)
            # return
            # self.pool_data(1)
            for i in range(1, self.MAX_PAGE, 16):
                # start 4 worker processes
                # with Pool(processes=8) as pool:
                with ThreadPool(16) as pool:
                    pool.map(self.pool_data, range(i, i + 16))
        except Exception as x:
            print(x)

    @tail_recursive
    def pool_data(self, page, retry=0):
        try:
            url = self.__base_url.format(page)
            self.__logger.info('=== URL: {} ==='.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()
            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            result_div = soup.find('div', class_='search-results clearfix column')
            if not result_div:
                return

            results = result_div.find_all('div', class_='results-block')
            for result in results:
                try:
                    item = {}
                    title_tag = result.find('h3', class_='panel-title')
                    if title_tag:
                        tag = title_tag.find_all('a')[-1]
                        name = tag.text.strip()
                        details_url = tag.get('href')

                        if details_url in self.__url_cache:
                            self.__logger.warning('URL: {} already grabbed!'.format(details_url))
                            continue

                        item['url'] = details_url
                        item['name'] = name

                    address_info_tag = result.find('div', class_='address-info scolumn')
                    if address_info_tag:
                        li_list = address_info_tag.find_all('li')
                        for li in li_list:
                            has_phone_tag = li.find('i', class_='ic-phone')
                            if has_phone_tag:
                                span_tags = li.find_all('span')
                                if span_tags and len(span_tags) > 0:
                                    item['contact'] = span_tags[-1].text.strip()

                            has_web_tag = li.find('i', class_='ic-globe')
                            if has_web_tag:
                                web_tag = li.find('a')
                                if web_tag:
                                    item['website'] = web_tag.get('href')

                    email_m = re.search(r'<a href="mailto\:([^"]*?)"', str(result), re.MULTILINE)
                    if email_m:
                        item['email'] = email_m.group(1)

                    address_tag = result.find('div', class_='address-line')
                    if address_tag:
                        address_text = address_tag.text.strip()

                        address = re.sub(r'[,]+', ',', address_text)
                        address = re.sub(r'\n', '\t', address)
                        address = re.sub(r'\t+', ' ', address)
                        address = re.sub(r'[ ]+', ' ', address)
                        item['address'] = address

                        csz = address_text.split('\n')[-1]
                        csz_parts = csz.split(',')
                        if csz_parts and len(csz_parts) > 1:
                            csz_text = csz_parts[-1]
                            # print(csz_text)
                            zip_m = re.search(r'(\d+)$', csz_text)
                            if zip_m:
                                zip = zip_m.group(1).strip()
                                item['zip_code'] = zip
                                csz_text = csz_text.replace(zip, '').strip()

                            city_state_parts = csz_text.split()
                            if city_state_parts and len(city_state_parts) > 0:
                                state = city_state_parts[-1]
                                city = ' '.join(city_state_parts[:-1])
                                item['state'] = state
                                item['city'] = city

                    # get business categories, keywords and social links
                    self.__grab_details(item)

                    # Finally write item
                    self.__write_item(item)
                except Exception as ex:
                    self.__logger.error('Error when parse data: {}'.format(ex))

            del data
            del soup
        except Exception as x:
            self.__logger.error('Error pool data: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.pool_data(page, retry + 1)

    @tail_recursive
    def __grab_details(self, item, retry=0):
        try:
            url = item['url']
            self.__logger.info('=== Details URL: {} ==='.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()
            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            del data

            cat_a_list = soup.find_all('a', class_='label label-default')
            if cat_a_list and len(cat_a_list) > 0:
                item['business_category'] = ', '.join([cat.text.strip() for cat in cat_a_list])

            ctrl_groups = soup.find_all('div', class_='control-group')
            if ctrl_groups and len(ctrl_groups) > 0:
                for ctrl_group in ctrl_groups:
                    label = ctrl_group.find('label', class_='control-label')
                    if not label:
                        continue

                    ctrl_text = ctrl_group.find('div', class_='controls text_line')
                    if not ctrl_text:
                        continue

                    if 'Keywords' in label.text:
                        item['keywords'] = ctrl_text.text.strip()

            content = soup.find('div', class_='addressareas')
            if content:
                con_div = content.find('div', class_='col-md-8')
                if con_div:
                    p_list = con_div.find_all('p')
                    if p_list and len(p_list) > 0:
                        for p in p_list:
                            strong = p.find('strong')
                            if not strong:
                                continue
                            if 'Social links' in strong.text:
                                s_list = p.find_all('a')
                                if s_list and len(s_list) > 0:
                                    item['social'] = ', '.join([s.get('href') for s in s_list])
            del soup
        except Exception as x:
            self.__logger.error('Error grab details data: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(item, retry + 1)

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        """
        Write data to csv file as dictionary object,
        quoting=csv.QUOTE_ALL, delimiter=';'
        :param row:
        :param mode:
        :return:
        """
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            pass
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    with SuperpagesUpdatedGrabber('superpages_output_09_18.csv') as spider:
        spider.grab_data()
