import csv
import gzip
import re
import socket
import urllib.request
import urllib.parse
from bs4 import BeautifulSoup
import logging
import readline

readline.parse_and_bind("control-v: paste")


class SkiresortGrabber:
    __start_url = 'http://www.skiresort.info/ski-resorts/usa/page/{}/'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __total = 0

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__setup_logger('skiresort.log')
        socket.setdefaulttimeout(60)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

        csv_header = ['Name', 'Address', 'City', 'State', 'Phone', 'Email', 'Website', 'Latitude', 'Longitude', 'URL']
        self.__write_data(csv_header)

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def clean_data(self):
        try:
            with open(self.__output_csv, 'r', newline='', encoding='utf-8') as f:
                reader = csv.reader(f)
                for row in reader:
                    if any(word in str(row[0]).strip().lower() for word in
                           ['school', 'church', 'kindergarten', 'kindy']):
                        print('skipping...')
                        continue
                    self.__write_data(row)
        except Exception as x:
            print(x)

    def grab_data(self):
        try:
            for i in range(1, 5):
                url = self.__start_url.format(i)
                self.__grab_pages(url)
        except Exception as x:
            print(x)

    def __grab_pages(self, url):
        try:
            self.__logger.info('=== URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            resort_list = soup.find_all('div', id=re.compile(r'resort\d+'))
            if not resort_list:
                return
            for resort in resort_list:
                link_tag = resort.find('a', class_='h3')
                if not link_tag:
                    continue

                state = ''
                state_tag = resort.find('div', class_='sub-breadcrumb')
                if state_tag:
                    state = state_tag.find_all('a')[-1].text

                link = link_tag.get('href') + 'lift-operator/'
                name = link_tag.text.strip()
                self.__grab_details(link, name, state)
        except Exception as x:
            print(x)

    def __grab_details(self, url, name, state):
        try:
            print('=== URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            address = ''
            city = ''
            phone = ''
            email = ''
            web = ''
            latitude = ''
            longitude = ''

            lon_lat_m = re.search(r'latitude\=([\d\.\-]+)\&amp\;longitude\=([\d\.\-]+)', data)
            if lon_lat_m:
                latitude = lon_lat_m.group(1)
                longitude = lon_lat_m.group(2)

            # name_tag = soup.find('div', class_='panel-heading h5')
            # if name_tag:
            #     name = name_tag.text.strip()

            city_tag = soup.find('span', class_='regionname')
            if city_tag:
                city = city_tag.text.strip()

            contacts_div = soup.find('div', class_='panel-body middle-padding')
            if not contacts_div:
                return

            phone_tag = contacts_div.find('a', class_='phonenumber')
            if phone_tag:
                phone = phone_tag.get('href')
                phone = re.sub(r'tel\:', '', phone)

            email_tags = contacts_div.find_all('a')
            if email_tags and len(email_tags) > 0:
                for email_tag in email_tags:
                    if 'mailto:' in email_tag.get('href'):
                        email = email_tag.get('href')
                        email = re.sub(r'mailto\:', '', email)
                        break

            web_tag = contacts_div.find('a', class_='btn btn-default btn-xs next-link')
            if web_tag:
                web_url = web_tag.get('href')
                web = self.__get_web_url(web_url)

            address_tag = contacts_div.find('p')
            if address_tag:
                nodes = [node for node in address_tag.descendants if (not node.name and len(node.strip()))]
                address_list = []
                for node in nodes:
                    if 'Tel.:' in node or 'Email:' in node or 'Go to Website' in node:
                        break
                    address_list.append(node.strip())
                address = ', '.join(address_list)

            csv_data = [name, address, city, state, phone, email, web, latitude, longitude, url]
            self.__logger.info('Data: {}'.format(csv_data))
            self.__write_data(csv_data)
        except Exception as x:
            print(x)

    def __get_web_url(self, url):
        try:
            print('=== Web URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            response = urllib.request.urlopen(req)
            return response.geturl()
        except Exception as x:
            self.__logger.error('Error getting web address: {}'.format(x))
        return url

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv + '.csv', mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            print('Error printing csv output: ' + str(x))


if __name__ == '__main__':
    # input_file = input('Please specify input txt: ')
    # output_file = input('Please specify output csv file name: ')
    output_file = 'skiresort_output_updated.csv'
    with SkiresortGrabber(output_file) as spider:
        spider.grab_data()
