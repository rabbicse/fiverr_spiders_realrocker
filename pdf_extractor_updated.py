# -*- coding: utf-8 -*-
"""
Functions that encapsulate "usual" use-cases for pdfminer, for use making
bundled scripts and for using pdfminer as a module for routine tasks.
"""
import csv
import html
import io
import os
import re

import pdfminer
import six
import sys

from PyPDF2 import PdfFileMerger, PdfFileReader
from bs4 import BeautifulSoup
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.image import ImageWriter


def extract_text_to_fp(inf, outfp, output_type='text', codec='utf-8', laparams=None,
                       maxpages=0, page_numbers=None, password="", scale=1.0, rotation=0,
                       layoutmode='normal', output_dir=None, strip_control=False,
                       debug=False, disable_caching=False, **other):
    """
    Parses text from inf-file and writes to outfp file-like object.
    Takes loads of optional arguments but the defaults are somewhat sane.
    Beware laparams: Including an empty LAParams is not the same as passing None!
    Returns nothing, acting as it does on two streams. Use StringIO to get strings.

    output_type: May be 'text', 'xml', 'html', 'tag'. Only 'text' works properly.
    codec: Text decoding codec
    laparams: An LAParams object from pdfminer.layout.
        Default is None but may not layout correctly.
    maxpages: How many pages to stop parsing after
    page_numbers: zero-indexed page numbers to operate on.
    password: For encrypted PDFs, the password to decrypt.
    scale: Scale factor
    rotation: Rotation factor
    layoutmode: Default is 'normal', see pdfminer.converter.HTMLConverter
    output_dir: If given, creates an ImageWriter for extracted images.
    strip_control: Does what it says on the tin
    debug: Output more logging data
    disable_caching: Does what it says on the tin
    """
    # if six.PY2 and sys.stdin.encoding:
    #     password = password.decode(sys.stdin.encoding)
    try:
        laparams = pdfminer.layout.LAParams()
        for param in ("all_texts", "detect_vertical", "word_margin", "char_margin", "line_margin", "boxes_flow"):
            paramv = locals().get(param, None)
            if paramv is not None:
                setattr(laparams, param, paramv)

        imagewriter = None
        if output_dir:
            imagewriter = ImageWriter(output_dir)

        rsrcmgr = PDFResourceManager(caching=not disable_caching)

        if output_type == 'text':
            device = TextConverter(rsrcmgr, outfp, codec=codec, laparams=laparams,
                                   imagewriter=imagewriter)

        if six.PY3 and outfp == sys.stdout:
            outfp = sys.stdout.buffer

        if output_type == 'xml':
            device = XMLConverter(rsrcmgr, outfp, codec=codec, laparams=laparams,
                                  imagewriter=imagewriter,
                                  stripcontrol=strip_control)
        elif output_type == 'html':
            device = HTMLConverter(rsrcmgr, outfp, codec=codec, scale=scale,
                                   layoutmode=layoutmode, laparams=laparams,
                                   imagewriter=imagewriter)
        elif output_type == 'tag':
            device = TagExtractor(rsrcmgr, outfp, codec=codec)

        interpreter = PDFPageInterpreter(rsrcmgr, device)
        for page in PDFPage.get_pages(inf,
                                      page_numbers,
                                      maxpages=maxpages,
                                      password=password,
                                      caching=not disable_caching,
                                      check_extractable=True):
            page.rotate = (page.rotate + rotation) % 360
            interpreter.process_page(page)

        device.close()
        return outfp.getvalue().decode('utf-8')
    except Exception as x:
        print(x)


def parse_data(data, output_csv):
    try:
        # data = re.sub(r'<br\/?>', '', data)
        sp = re.split(r'\>\!\<\/span\>', data)
        print('Total data: {}'.format(len(sp)))

        template_type = 0
        if not data or len(sp) < 2:
            id_list = re.findall(r'\>\[ID\:\s*\d+\]', data)
            if id_list and len(id_list) > 0:
                template_type = 3
            else:
                template_type = 2

        i = 0
        with open(output_csv, 'a+') as csvfile:
            fieldnames = ['name', 'address', 'postcode', 'phone', 'email']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            # writer.writeheader()

            if template_type == 3:
                sp = re.split(r'\>\[ID\:\s*\d+\]', data)
                for s in sp[1:]:
                    soup = BeautifulSoup(s, 'lxml')
                    if not soup:
                        continue

                    csv_dict = {}
                    name_tag = soup.find('p')
                    if name_tag:
                        csv_dict['name'] = html.unescape(name_tag.text).strip()

                    div_list = soup.find_all('div')
                    for div in div_list:
                        span_tag = div.find_all('span')
                        if not span_tag or len(span_tag) < 2:
                            continue
                        key = html.unescape(span_tag[0].text).strip()
                        values = []
                        for span_val in span_tag[1:]:
                            values.append(html.unescape(span_val.text).strip())

                        if 'Address:' in key:
                            address = ''.join(values)
                            csv_dict['address'] = address
                            post_code_m = re.search(r'(\d{4,})$', address.strip())
                            if post_code_m:
                                csv_dict['postcode'] = post_code_m.group(1)
                        if 'Phone:' in key:
                            phones = []
                            for v in values:
                                p = re.sub(r'[^\d]', '', v)
                                p = p.strip()
                                if p != '':
                                    phones.append(p)
                            csv_dict['phone'] = ', '.join(phones)
                        if 'Email:' in key:
                            csv_dict['email'] = ''.join(values)
                    print(csv_dict)
                    i += 1
                    writer.writerow(csv_dict)
                print('Total: {}'.format(i))
            elif template_type == 2:
                soup = BeautifulSoup(data, 'lxml')
                names = []
                name_spans = soup.find_all('span',
                                           {'style': re.compile(r'font\-family\: [A-Z]+\+Tahoma\; font\-size\:17px')})
                for name_span in name_spans:
                    name = html.unescape(name_span.text).strip()
                    contact = ''
                    next_span = name_span.find_next_sibling('span')
                    if next_span:
                        contact = next_span.text.strip()
                    name_contact = (name, contact)
                    names.append(name_contact)

                contact_list = []
                contact_spans = soup.find_all('span', {
                    'style': re.compile(r'font\-family\: [A-Z]+\+Tahoma\; font\-size\:11px')})
                prev_div = None
                for contact_span in contact_spans:
                    div = contact_span.find_parent('div')
                    if prev_div == div:
                        continue
                    # if '@' not in contact_span and not re.search(r'\d{4,}', contact_span.text):
                    if '@' not in div.text:
                        continue
                    email, phone = '', ''
                    contact_text = div.text
                    contacts = contact_text.split('\n')
                    if len(contacts) == 1 and '@' in contacts[0]:
                        email = contacts[0].strip()
                    elif len(contacts) == 1 and re.search(r'\d{4,}', contacts[0]):
                        phone = contacts[0].strip()
                    elif len(contacts) > 1:
                        email = contacts[0].strip()
                        phone = ', '.join(contacts[1:])
                    contact_list.append((email, phone))
                    prev_div = div

                for i in range(len(names)):
                    csv_dict = {'name': names[i][0],
                                'address': names[i][1]}
                    if i < len(contact_list):
                        csv_dict['email'] = contact_list[i][0]
                        csv_dict['phone'] = contact_list[i][1]
                    print(csv_dict)
                    i += 1
                    writer.writerow(csv_dict)
                print('Total: {}'.format(i))
            else:
                for s in sp:
                    soup = BeautifulSoup(s, 'lxml')
                    if not soup:
                        continue

                    if not 'ID:' in soup.text:
                        continue

                    spans = soup.find_all('span')
                    id_span = next(iter([s for s in spans if 'ID:' in s.text]))
                    if not id_span:
                        continue

                    csv_dict = {}

                    name = re.sub(r'ID\:\d+\s*\-*', '', html.unescape(id_span.text))
                    name = name.strip()

                    template_type = 0
                    # template 1
                    if 'Managed By:' in soup.text:
                        csv_dict['name'] = name
                        template_type = 1
                    else:
                        names = name.split('\n')
                        csv_dict['name'] = names[0]
                        csv_dict['address'] = (' '.join(names[1:]) if len(names) > 1 else '').strip()

                    div_list = soup.find_all('div')
                    for div in div_list:
                        span_tag = div.find_all('span')

                        if template_type == 1:
                            if not span_tag or len(span_tag) < 2:
                                continue

                            key = html.unescape(span_tag[0].text).strip()
                            values = []
                            for span_val in span_tag[1:]:
                                values.append(html.unescape(span_val.text).strip())

                            # if 'Managed By:' in key:
                            #     csv_dict['name'] = ''.join(values)
                            if 'Address:' in key:
                                address = ''.join(values)
                                csv_dict['address'] = address
                                post_code_m = re.search(r'(\d{4,})$', address.strip())
                                if post_code_m:
                                    csv_dict['postcode'] = post_code_m.group(1)
                            if 'Phone:' in key:
                                phones = []
                                for v in values:
                                    p = re.sub(r'[^\d]', '', v)
                                    p = p.strip()
                                    if p != '':
                                        phones.append(p)
                                csv_dict['phone'] = ', '.join(phones)
                            if 'Email:' in key:
                                csv_dict['email'] = ''.join(values)
                        else:
                            span_text = ' '.join([s.text.strip() for s in span_tag])
                            if '@' in span_text and 'email' not in csv_dict:
                                csv_dict['email'] = span_text.strip()
                            if re.search(r'[0-9 \+]{6,}', span_text):
                                phone = span_text.strip()
                                phone = re.sub('\n', ', ', phone)
                                csv_dict['phone'] = phone

                    if len(csv_dict.keys()) == 0:
                        continue

                    print(csv_dict)
                    writer.writerow(csv_dict)

                    i += 1
                print('Total: {}'.format(i))
        if i < 1:
            os.remove(output_csv)
    except Exception as x:
        print(x)


# def main_operation():
#     try:
#         pdf_files = [pdf for pdf in os.listdir('./pdfs/') if pdf.endswith('pdf')]
#         for pdf_file in pdf_files:
#             input_pdf_file = './pdfs/{}'.format(pdf_file)
#             output_csv_file = './csvs_final/{}.csv'.format(pdf_file)
#             # html_file_name = './html1/{}.html'.format(pdf_file)
#             print('=== Processing PDF file: {}. ==='.format(input_pdf_file))
#             with open(input_pdf_file, 'rb') as f:
#                 output = io.BytesIO()
#                 html = extract_text_to_fp(f, output, output_type='html')
#                 # with open(html_file_name, mode='w', encoding='utf-8') as f:
#                 #     f.write(html)
#                 parse_data(html, output_csv_file)
#     except Exception as x:
#         print(x)


def main_operation_on_html():
    try:
        html_files = [pdf for pdf in os.listdir('./html1/') if pdf.endswith('html')]
        for html_file in html_files:
            input_html_file = './html1/{}'.format(html_file)
            output_csv_file = './csv1/{}.csv'.format(html_file)
            html_file_name = './htmls/{}.html'.format(html_file)
            print('=== Processing HTML file: {}. ==='.format(input_html_file))
            with open(input_html_file, 'r', encoding='utf-8') as f:
                html = f.read()
                parse_data(html, output_csv_file)
    except Exception as x:
        print(x)


def merge_pdf_to_single(pdf_list, output_file):
    try:
        merger = PdfFileMerger()
        for fname in pdf_list:
            merger.append(PdfFileReader(open(fname, 'rb')))

        merger.write(output_file)
    except Exception as x:
        print(x)


def main_operation():
    try:
        output_csv_file = './pdf_output_final.csv'
        with open(output_csv_file, 'a+') as csvfile:
            fieldnames = ['name', 'address', 'postcode', 'phone', 'email']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

        pdfs_to_merge = []
        pdf_files = [pdf for pdf in os.listdir('./pdfs/') if pdf.endswith('pdf')]
        for pdf_file in sorted(pdf_files):
            input_pdf_file = './pdfs/{}'.format(pdf_file)
            pdfs_to_merge.append(input_pdf_file)

            print('=== Processing PDF file: {}. ==='.format(input_pdf_file))
            with open(input_pdf_file, 'rb') as f:
                output = io.BytesIO()
                html = extract_text_to_fp(f, output, output_type='html')
                parse_data(html, output_csv_file)
                del output

        # merge pdf
        print('==== merging pdf files ===')
        merge_pdf_to_single(pdfs_to_merge, './pdf_final_output.pdf')
    except Exception as x:
        print(x)


if __name__ == '__main__':
    # pdf_files = [pdf for pdf in os.listdir('./pdfs/') if pdf.endswith('pdf')]
    # csv_files = [pdf.replace('.csv', '').replace('.html', '') for pdf in os.listdir('./csvs_final/') if pdf.endswith('csv')]
    # for pdf in pdf_files:
    #     if pdf not in csv_files:
    #         print(pdf)
    main_operation()
    # main_operation_on_html()
