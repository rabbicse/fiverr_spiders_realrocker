import csv
import gzip
import json
import logging
import os
import random
import re
import readline
import socket
import urllib.parse
import urllib.request
# import socks

from multiprocessing.pool import ThreadPool

import time
from bs4 import BeautifulSoup
from multiprocessing import Lock

readline.parse_and_bind("control-v: paste")


class OnlineGrabber:
    __base_url = 'http://www.140online.com'
    __category_url_en = 'http://www.140online.com/moreclasses.aspx?Lang=En'
    __proxies = {
        # 'http': '83.149.70.159:13042',
        # 'https': '37.48.118.90:13042'
        # 'http': '37.48.118.90:13042',
        # 'https': '83.149.70.159:13042'
        'http': '108.59.14.208:13040',
        'https': '108.59.14.203:13040'
    }
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Host': 'www.140online.com',
        'Upgrade-Insecure-Requests': '1'}
    __total = 0
    __url_cache = []
    __current_keyword = ''
    __current_referer = ''

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('140online.log')
        socket.setdefaulttimeout(30)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        proxy_handler = urllib.request.ProxyHandler(self.__proxies)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        opener = urllib.request.build_opener(proxy_handler)
        urllib.request.install_opener(opener)
        self.__field_names = ['name', 'address', 'telephones', 'fax', 'about', 'keywords', 'website', 'email',
                              'facebook_id', 'twitter_id', 'branches', 'categories', 'url', 'category_url']
        csv_header = {'name': 'Name',
                      'address': 'Address',
                      'telephones': 'Telephones',
                      'fax': 'Fax',
                      'about': 'About',
                      'keywords': 'Keywords',
                      'website': 'Website',
                      'email': 'Email',
                      'facebook_id': 'Facebook ID',
                      'twitter_id': 'Twitter ID',
                      'branches': 'Branches',
                      'categories': 'Categories',
                      'url': 'URL',
                      'category_url': 'Category URL'}

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            self.__grab_all_categories_en()
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __grab_all_categories_en(self):
        try:
            self.__logger.info('=== Category all English URL: {} ==='.format(self.__category_url_en))

            req = urllib.request.Request(self.__category_url_en, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            cat_div = soup.find('div', class_='areamid')
            if cat_div:
                links = cat_div.find_all('a')
                for link in links:
                    if not link.has_attr('title'):
                        continue

                    category_url = self.__base_url + '/' + re.sub(' ', '%20', link.get('href'))
                    self.grab_all_data_by_category_en(category_url)
        except Exception as x:
            print(x)

    def grab_all_data_by_category_en(self, url):
        try:
            page = 1
            url_parts = url.split('/')
            url_parts.insert(-3, 'pages')
            url_parts.insert(-1, '{}')
            category_url = '/'.join(url_parts)
            self.__current_cat_referer = url
            has_more_page = self.__grab_data_by_category_en(category_url.format(page))
            while has_more_page:
                self.__current_cat_referer = category_url.format(page)
                page += 1
                has_more_page = self.__grab_data_by_category_en(category_url.format(page))
        except Exception as x:
            print(x)

    def __grab_data_by_category_en(self, url, retry=0):
        try:
            self.__logger.info('=== Category English URL: {} ==='.format(url))
            self.__current_referer = url

            custom_header = self.__headers.copy()
            # Referer:http://www.140online.com/Class/En/133/Airlines
            # Upgrade-Insecure-Requests:1
            custom_header['Referer'] = self.__current_cat_referer

            req = urllib.request.Request(url, headers=custom_header)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            data_tags = soup.find_all('table', class_='tablcom0')
            if not data_tags or len(data_tags) == 0:
                return

            links = []
            for data_tag in data_tags:
                link_tag = data_tag.find('a')
                if not link_tag:
                    continue

                link = self.__base_url + re.sub(' ', '%20', link_tag.get('href'))
                if link not in self.__url_cache:
                    # item = {'url': link}
                    # links.append(link)
                    if not self.__grab_details(link):
                        self.__logger.warning('Failed to grab details...')
                        name = link_tag.text.strip()
                        self.__parse_details(data_tag, link, name)
                else:
                    self.__logger.warning('Already grabbed URL: {}.'.format(link))

            # with ThreadPool(4) as p:
            #     p.map(self.__grab_details, links)

            return True
        except Exception as x:
            self.__logger.error('Error grab category page: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying for category...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __parse_details(self, soup, url, name):
        try:
            item = {'url': url, 'name': name, 'category_url': self.__current_referer}

            address_tag = soup.find('span', id=re.compile(r'ctl\d+_resRep_ctl\d+_lblAddress'))
            if address_tag:
                item['address'] = address_tag.text.strip()

            keyword_tag = soup.find('span', id=re.compile(r'ctl\d+_resRep_ctl\d+_lblKeyWord'))
            if keyword_tag:
                item['keywords'] = keyword_tag.text.strip()

            # telephone_tag = soup.find('span', id=re.compile(r'ctl\d+_lblTel'))
            # if telephone_tag:
            #     item['telephones'] = telephone_tag.text.strip()

            # fax_tag = soup.find('span', id=re.compile(r'ctl\d+_lblFax'))
            # if fax_tag:
            #     item['fax'] = fax_tag.text.strip()

            website_tag = soup.find('a', id=re.compile(r'ctl\d+_resRep_ctl\d+_lnkwebsite'))
            if website_tag:
                item['website'] = website_tag.get('href').strip()

            # email_tag = soup.find('a', id=re.compile(r'ctl\d+_lnkEmail'))
            # if email_tag:
            #     item['email'] = re.sub(r'mailto\:', '', email_tag.get('href')).strip()

            fb_tag = soup.find('a', id=re.compile(r'ctl\d+_resRep_ctl\d+_lnkfacebook'))
            if fb_tag:
                item['facebook_id'] = fb_tag.get('href').strip()

            twitter_tag = soup.find('a', id=re.compile(r'ctl\d+_resRep_ctl\d+_lnktwitter'))
            if twitter_tag:
                item['twitter_id'] = twitter_tag.get('href').strip()

            about_tag = soup.find('span', id=re.compile(r'ctl\d+_resRep_ctl\d+_lblCompanyTxt'))
            if about_tag:
                about = urllib.parse.unquote_plus(about_tag.text.strip())
                about_more_tag = soup.find('span', id=re.compile(r'ctl\d+_resRep_ctl\d+_lblCompanyTxtMore'))
                if about_more_tag:
                    about_more = urllib.parse.unquote_plus(about_more_tag.text.strip())
                    item['about'] = about + ' ' + about_more

            categories_tag = soup.find('span', id=re.compile(r'ctl\d+_resRep_ctl\d+_lblClass'))
            if categories_tag:
                item['categories'] = categories_tag.text.strip()

            branches = []
            branch_tags_table = soup.find('span', id=re.compile(r'ctl\d+_resRep_ctl\d+_repGov'))
            if branch_tags_table:
                branch_tags = branch_tags_table.find_all('a',
                                                         id=re.compile(r'ctl\d+_resRep_ctl\d+_repGov_ctl\d+_Branch'))
                if branch_tags:
                    for branch_tag in branch_tags:
                        branches.append(branch_tag.text.strip())
                item['branches'] = ', '.join(branches)

            self.__logger.info('Data From Category page: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()
        except Exception as x:
            print(x)

    def grab_all_data_from_basic(self):
        try:
            if os.path.exists('./140online_output_basic.csv'):
                with open('./140online_output_basic.csv', 'r+', encoding='utf-8') as f:
                    reader = csv.DictReader(f, self.__field_names)
                    for row in reader:
                        url = row['url']
                        if 'http' not in url:
                            continue

                        if url in self.__url_cache:
                            self.__logger.warning('Already grabbed URL: {}.'.format(url))
                            continue

                        self.__current_referer = row['category_url']

                        if not self.__grab_details(url):
                            self.__logger.info('Data Basic: {}'.format(row))
                            self.__write_data(row)

                            try:
                                self.__lock.acquire()
                                self.__url_cache.append(url)

                                sleep_time = random.randint(5, 15)
                                self.__logger.info(
                                    'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                                time.sleep(sleep_time)
                            except Exception as ex:
                                self.__logger.error('Error grab details page 2: {}'.format(ex))
                            finally:
                                self.__lock.release()
        except Exception as x:
            self.__logger.error('Error grab details from basic: {}'.format(x))

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            custom_header = self.__headers.copy()
            # Referer:http://www.140online.com/Class/En/133/Airlines
            # Upgrade-Insecure-Requests:1
            custom_header['Referer'] = self.__current_referer

            req = urllib.request.Request(url, headers=custom_header)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            item = {'url': url}
            name_tag = soup.find('span', id=re.compile(r'ctl\d+_lblCompanyName'))
            if not name_tag:
                return
            item['name'] = name_tag.text.strip()

            address_tag = soup.find('span', id=re.compile(r'ctl\d+_lblAddress'))
            if address_tag:
                item['address'] = address_tag.text.strip()

            keyword_tag = soup.find('span', id=re.compile(r'ctl\d+_lblKeyWord'))
            if keyword_tag:
                item['keywords'] = keyword_tag.text.strip()

            telephone_tag = soup.find('span', id=re.compile(r'ctl\d+_lblTel'))
            if telephone_tag:
                item['telephones'] = telephone_tag.text.strip()

            fax_tag = soup.find('span', id=re.compile(r'ctl\d+_lblFax'))
            if fax_tag:
                item['fax'] = fax_tag.text.strip()

            website_tag = soup.find('a', id=re.compile(r'ctl\d+_lnkwebsite'))
            if website_tag:
                item['website'] = website_tag.get('href').strip()

            email_tag = soup.find('a', id=re.compile(r'ctl\d+_lnkEmail'))
            if email_tag:
                item['email'] = re.sub(r'mailto\:', '', email_tag.get('href')).strip()

            fb_tag = soup.find('a', id=re.compile(r'ctl\d+_lnkfacebook'))
            if fb_tag:
                item['facebook_id'] = fb_tag.get('href').strip()

            twitter_tag = soup.find('a', id=re.compile(r'ctl\d+_lnktwitter'))
            if twitter_tag:
                item['twitter_id'] = twitter_tag.get('href').strip()

            about_tag = soup.find('span', id=re.compile(r'ctl\d+_lblCompanyTxt'))
            if about_tag:
                item['about'] = urllib.parse.unquote_plus(about_tag.text.strip())

            categories_tag = soup.find('span', id=re.compile(r'ctl\d+_lblClass'))
            if categories_tag:
                item['categories'] = categories_tag.text.strip()

            branches = []
            branch_tags_table = soup.find('table', class_='tablebranches')
            if branch_tags_table:
                branch_tags = branch_tags_table.find_all('a')
                if branch_tags:
                    for branch_tag in branch_tags:
                        branches.append(branch_tag.text.strip())
                item['branches'] = ', '.join(branches)

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

            return True

        except Exception as x:
            self.__logger.error('Error grab details page: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)
            else:
                return False

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    # output_file = '140online_output_basic_modified.csv'
    output_file = '140online_output_upgraded.csv'
    with OnlineGrabber(output_file) as spider:
        # spider.grab_all_data_from_basic()
        spider.grab_data()
        # spider.grab_all_data_by_category_en('http://www.140online.com/Class/En/133/Airlines')
