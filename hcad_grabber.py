import csv
import gzip
import random
import time
import logging
import os
import re
import socket
import readline
import urllib.parse
import urllib.request
from bs4 import BeautifulSoup

readline.parse_and_bind("control-v: paste")


class HcadGrabber:
    __start_url = 'https://public.hcad.org/records/QuickRecord.asp'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Host': 'public.hcad.org',
        'Origin': 'https://public.hcad.org',
        'Referer': 'https://public.hcad.org/records/QuickSearch.asp',
        'Upgrade-Insecure-Requests': '1'}
    __total = 0
    __url_cache = []

    def __init__(self, input_xlsx, output_csv):
        self.__input_xlsx = input_xlsx
        self.__output_csv = output_csv

    def __enter__(self):
        self.__setup_logger('hcad.log')
        socket.setdefaulttimeout(60)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

        self.__field_names = ['location', 'field1', 'field2', 'field3', 'create_date', 'closed_date', 'due_date',
                              'case_type', 'case_number', 'owner_name', 'address', 'city', 'market_area',
                              'appraised', 'year_built', 'bedroom', 'bath', 'impr_sqft', 'quality']
        csv_header = {'location': 'Location',
                      'field1': '',
                      'field2': '',
                      'field3': '',
                      'create_date': 'Create Date',
                      'closed_date': 'Closed Date',
                      'due_date': 'Due Date',
                      'case_type': 'Case Type',
                      'case_number': 'Case Number',
                      'owner_name': 'OWNER',
                      'address': 'MAILING ADDRESS',
                      'city': 'MAILING CITY/ STATE/ ZIP',
                      'market_area': 'MARKET AREA',
                      'appraised': 'APPRAISED VALUE',
                      'year_built': 'YEAR BUILT',
                      'bedroom': 'BEDROOM',
                      'bath': 'BATH',
                      'impr_sqft': 'IMPR SQ FT',
                      'quality': 'QUALITY'
                      }

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['location'])

        if csv_header['location'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            if os.path.exists(self.__input_xlsx):
                with open(self.__input_xlsx, 'r+') as f:
                    reader = csv.reader(f)
                    for row in reader:
                        row_sp = row[0].split()
                        if len(row_sp) < 2:
                            continue

                        kwargs = {
                            'year': '2017',
                            'stnum': row_sp[0],
                            'stname': ' '.join(row_sp[1:])
                        }

                        default_item = {
                            'location': row[0],
                            'field1': row[1],
                            'field2': row[2],
                            'field3': row[3],
                            'create_date': row[4],
                            'closed_date': row[5],
                            'due_date': row[6],
                            'case_type': row[7],
                            'case_number': row[8]
                        }

                        if row[0] in self.__url_cache:
                            continue

                        item = self.search_data(**kwargs)
                        if item:
                            final_item = {**default_item, **item}
                            self.__write_data(final_item)
                            self.__url_cache.append(row[0])
                        else:
                            self.__write_data(default_item)
                            self.__url_cache.append(row[0])
                        self.__sleep_script()
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def search_data(self, **kwargs):
        try:
            post_data = {'TaxYear': kwargs['year'],
                         'stnum': kwargs['stnum'],
                         'stname': kwargs['stname']}
            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            self.__logger.info('=== Search URL: {} ==='.format(self.__start_url))
            req = urllib.request.Request(self.__start_url, headers=self.__headers, data=post_req)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            return self.__parse_data(data)
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __parse_data(self, data):
        try:
            if not data:
                self.__logger.error('No results found!')
                return

            item = {}

            # <!-- ---------- OWNER NAME ---------- -->
            #                                     COMIN RICHARD L<br />
            owner_name_m = re.search(r'\<\!\-\-[\s\-]+OWNER NAME[\s\-]+\-\-\>([^<]*?)<', data, re.MULTILINE | re.DOTALL)
            if owner_name_m:
                item['owner_name'] = self.__cleanup(owner_name_m.group(1).strip())

            # <!-- ---------- MAILING ADDRESS (ADDR1 AND ADDR2) ---------- -->
            #                                             2723 PIRATES GOLD CIR<br />
            mailing_address_m = re.search(r'<!--[\s\-]+MAILING ADDRESS \(ADDR1 AND ADDR2\)[\s\-]+-->([^<]*?)<',
                                          data, re.MULTILINE | re.DOTALL)
            if mailing_address_m:
                item['address'] = self.__cleanup(mailing_address_m.group(1).strip())

            # <!-- ---------- MAILING ADDRESS (CITY-STATE-ZIP OR COUNTRY)---------- -->
            #                                             FRIENDSWOOD&nbsp;TX&nbsp;77546-7418<br />
            mailing_city_m = re.search(
                r'<!--[\s\-]+MAILING ADDRESS \(CITY\-STATE\-ZIP OR COUNTRY\)[\s\-]+-->([^<]*?)<', data,
                re.MULTILINE | re.DOTALL)
            if mailing_city_m:
                item['city'] = self.__cleanup(mailing_city_m.group(1).strip())

            # use bs4 to parse
            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                self.__logger.error('No results found!')
                return

            tr_list = soup.find_all('tr')
            for tr in tr_list:

                td_index = 0
                td_list = tr.find_all('td')
                for td in td_list:
                    # parse market area
                    if 'Market Area' in td.text:
                        next_tr = tr.find_next_sibling('tr')
                        if not next_tr:
                            continue

                        next_tr_td_list = next_tr.find_all('td')
                        item['market_area'] = self.__cleanup(next_tr_td_list[td_index].text)

                    # parse Appraised
                    if 'Appraised' in td.text:
                        table = tr.find_parent('table')
                        ap_tr = table.find_all('tr')[-2]
                        ap_td = ap_tr.find_all('td')
                        if ap_td:
                            appraised = self.__cleanup(ap_td[-1].text)
                            appraised = re.sub(r'[^\d]', '', appraised)
                            item['appraised'] = appraised.strip()

                    if 'Bedroom' in td.text:
                        next_td = td.find_next_sibling('td')
                        if next_td:
                            item['bedroom'] = self.__cleanup(next_td.text)

                    if 'Full Bath' in td.text:
                        next_td = td.find_next_sibling('td')
                        if next_td:
                            item['bath'] = self.__cleanup(next_td.text)

                    td_index += 1

                th_index = 0
                th_list = tr.find_all('th')
                for th in th_list:
                    # parse year built
                    if 'Year Built' in th.text:
                        next_tr = tr.find_next_sibling('tr')
                        if not next_tr:
                            continue

                        next_tr_td_list = next_tr.find_all('td')
                        item['year_built'] = self.__cleanup(next_tr_td_list[th_index].text)

                    # parse quality
                    if 'Quality' in th.text:
                        next_tr = tr.find_next_sibling('tr')
                        if not next_tr:
                            continue

                        next_tr_td_list = next_tr.find_all('td')
                        item['quality'] = self.__cleanup(next_tr_td_list[th_index].text)

                    # parse Impr Sq Ft
                    if 'Impr Sq Ft' in th.text:
                        next_tr = tr.find_next_sibling('tr')
                        if not next_tr:
                            continue

                        next_tr_td_list = next_tr.find_all('td')
                        impr_sqft = self.__cleanup(next_tr_td_list[th_index].text)
                        impr_sqft = re.sub(r'[^\d]', '', impr_sqft)
                        item['impr_sqft'] = impr_sqft.strip()

                    th_index += 1

            self.__logger.info('Data: {}'.format(item))
            return item

        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __cleanup(self, data):
        try:
            result = urllib.parse.unquote_plus(data.encode('utf-8').decode('unicode-escape', 'ignore'))
            result = re.sub(r'&nbsp;', ' ', result)
            result = result.strip()
            return result
        except Exception as x:
            self.__logger.error('Error cleanup: {}'.format(x))
        return data

    def __sleep_script(self, min_sleep=1, max_sleep=5):
        sleep_time = random.randint(min_sleep, max_sleep)
        self.__logger.info(
            'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
        time.sleep(sleep_time)

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    input_file = input('Please specify input csv: ')
    output_file = input('Please specify output csv: ')

    # input_file = 'input.csv'
    # output_file = 'hcad_output.csv'
    with HcadGrabber(input_file, output_file) as spider:
        spider.grab_data()

    input('Press any key to exit...')
