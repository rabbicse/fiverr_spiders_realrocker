import csv
import gzip
import logging
import os
import random
import re
import readline
import socket
import time
import urllib.parse
import urllib.request
import sys

import socks
from OpenGL import GL
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QApplication
from bs4 import BeautifulSoup

readline.parse_and_bind("control-v: paste")


class Browser(QWebEngineView):
    __redirected_url = None
    __html = None

    def __init__(self, url, keyword, location):
        QWebEngineView.__init__(self)
        self.__url = url
        self.__keyword = keyword
        self.__location = location

    def __enter__(self):
        self.loadFinished.connect(self._load_finished)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def fetch(self):
        self.setUrl(QUrl(self.__url))
        app.exec_()
        return self.__redirected_url, self.__html

    def _load_finished(self):
        try:
            current_url = str(self.page().url().toString())
            print('Current URL: {}'.format(current_url))

            if current_url == self.__url:
                self.page().runJavaScript("""document.getElementById('whatwho').value = '{}'""".format(self.__keyword))
                self.page().runJavaScript("""document.getElementById('where').value = '{}'""".format(self.__location))
                self.page().runJavaScript("""document.getElementsByName('search_button')[0].click()""")
            else:
                self.__redirected_url = current_url
                self.page().toHtml(self.processHtml)
        except Exception as x:
            print(x)
            app.quit()

    def processHtml(self, html):
        self.__html = html
        app.quit()


class YellowPagesCaGrabber:
    __start_url = 'https://www.yellowpages.ca/'
    __base_url = 'https://www.yellowpages.ca'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__setup_logger('yellow_pages_ca.log')
        socket.setdefaulttimeout(60)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # urllib.request.install_opener(opener)

        self.__field_names = ['name', 'address', 'city', 'state', 'zipcode', 'phone', 'website', 'business_category',
                              'ratings', 'num_of_reviews', 'opening_hours', 'url']
        csv_header = {'name': 'Name',
                      'address': 'Address',
                      'city': 'City',
                      'state': 'State',
                      'zipcode': 'Zip Code',
                      'phone': 'Phone',
                      'website': 'Website',
                      'business_category': 'Business Category',
                      'ratings': 'Ratings',
                      'num_of_reviews': '# of reviews',
                      'opening_hours': 'Opening Hours',
                      'url': 'URL'
                      }

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self.__logger
        del self

    def grab_data(self, keyword, location):
        try:
            self.__keyword = keyword
            self.__location = location
            # self.__grab_page('https://www.yellowpages.ca/search/si/35/Dentist/Vancouver%2C British Columbia')
            # sys.exit(0)

            url, data = None, None
            with Browser(self.__start_url, self.__keyword, self.__location) as browser:
                url, data = browser.fetch()
                self.__logger.info(url)

            if url and data:
                page = 2
                has_next = self.__parse_search_page(data)
                while has_next:
                    url_arr = url.split('/')
                    url_arr[-3] = '{}'.format(page)
                    page_url = '/'.join(url_arr)

                    # sleep_time = random.randint(5, 15)
                    # self.__logger.info(
                    #     'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                    # time.sleep(sleep_time)

                    # checking if there are more data
                    has_next = self.__grab_page(page_url)
                    page += 1
        except Exception as x:
            print(x)

    def __grab_page(self, url):
        try:
            self.__logger.info('=== URL: {} ==='.format(url))

            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            return self.__parse_search_page(data)
        except Exception as x:
            self.__logger.error('Error grab main page: {}'.format(x))

    def __parse_search_page(self, data):
        try:
            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            item_list = soup.find_all('div', class_='listing__content listing__content--ltr listingInfo ctaMap2')
            if item_list and len(item_list) > 0:
                for item in item_list:
                    title_tag = item.find('div', 'listing__title--wrap')
                    if not title_tag:
                        continue

                    link_tag = title_tag.find('a')
                    if not link_tag:
                        continue

                    # parse item details
                    data_item = {}
                    data_item['name'] = link_tag.text.strip()
                    data_item['url'] = self.__base_url + link_tag.get('href')

                    address_full_tag = item.find('span', class_='listing__address--full')
                    if address_full_tag:
                        data_item['address'] = address_full_tag.text.strip()
                        city_tag = address_full_tag.find('span', {'itemprop': 'addressLocality'})
                        if city_tag:
                            data_item['city'] = city_tag.text.strip()
                        state_tag = address_full_tag.find('span', {'itemprop': 'addressRegion'})
                        if state_tag:
                            data_item['state'] = state_tag.text.strip()

                        postcode_tag = address_full_tag.find('span', {'itemprop': 'postalCode'})
                        if postcode_tag:
                            data_item['zipcode'] = postcode_tag.text.strip()

                    rating_tag = item.find('span', class_='ypStars')
                    if rating_tag and rating_tag.has_attr('data-rating'):
                        data_item['ratings'] = rating_tag.get('data-rating').replace('rating', '').strip()

                    website_tag = item.find('li', class_='mlr__item mlr__item--website ')
                    if not website_tag:
                        website_tag = item.find('li', class_='mlr__item mlr__item--website')

                    if website_tag:
                        website_a_tag = website_tag.find('a')
                        if website_a_tag:
                            web_url = website_a_tag.get('href').strip()
                            web_url = re.sub(r'\/goframe\/\d+\?', '', web_url)
                            web_url = re.sub(r'\/gourl\?', '', web_url)
                            web_url = urllib.parse.unquote_plus(web_url, encoding='utf-8')
                            data_item['website'] = web_url

                    phone_tag = item.find('li', class_='mlr__item mlr__item--more mlr__item--phone jsMapBubblePhone')
                    if phone_tag:
                        phones_tag = phone_tag.find_all('li', class_='mlr__submenu__item')
                        phones = []
                        for phone in phones_tag:
                            phones.append(phone.text.strip())
                        data_item['phone'] = ', '.join(phones)

                    # grab details page
                    if data_item['url'] not in self.__url_cache:
                        # sleep_time = random.randint(1, 10)
                        # self.__logger.info(
                        #     'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                        # time.sleep(sleep_time)

                        json_data = self.__grab_details(**data_item)
                        if json_data:
                            data_item = json_data

                        self.__logger.info('Data: {}'.format(data_item))
                        self.__write_data(data_item)

                        self.__url_cache.append(data_item['url'])
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(data_item['url']))
                return True
        except Exception as x:
            self.__logger.error('Error grab main page: {}'.format(x))

    def __grab_details(self, **kwargs):
        try:
            print('=== Details URL: {} ==='.format(kwargs['url']))
            req = urllib.request.Request(kwargs['url'], headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            if 'website' not in kwargs:
                website_tag = soup.find('li', class_='mlr__item mlr__item--website')
                if not website_tag:
                    website_tag = soup.find('li', class_='mlr__item mlr__item--website ')
                if not website_tag:
                    website_tag = soup.find('li', class_='mlr__item mlr__item--website mlr__item--more')

                if website_tag:
                    website_a_tag = website_tag.find('a')
                    if website_a_tag:
                        web_url = website_a_tag.get('href').strip()
                        web_url = re.sub(r'\/goframe\/\d+\?', '', web_url)
                        web_url = re.sub(r'\/gourl\?', '', web_url)
                        web_url = urllib.parse.unquote_plus(web_url, encoding='utf-8')
                        kwargs['website'] = web_url
            if 'website' not in kwargs:
                self.__logger.warning('No website url found for url: ' + kwargs['url'])

            review_count_tag = soup.find('span', class_='jsYPReviewCount')
            if review_count_tag:
                review_count_text = review_count_tag.text.strip()
                review_count_m = re.search(r'(?i)(\d+)\s*?reviews$', review_count_text)
                if review_count_m:
                    kwargs['num_of_reviews'] = review_count_m.group(1)

            opening_hours_tag = soup.find('table', class_='openHours-table')
            if opening_hours_tag:
                kwargs['opening_hours'] = opening_hours_tag.text.strip()

            business_categories_tags = soup.find_all('div',
                                                     class_='business__details business__details--inline jsParentContainer')
            for business_category_tag in business_categories_tags:
                if 'Categories' in business_category_tag.text:
                    category_tags = business_category_tag.find_all('a')
                    categories = []
                    for category_tag in category_tags:
                        if category_tag.has_attr('title'):
                            categories.append(category_tag.get('title').strip())
                    kwargs['business_category'] = ', '.join(categories)
                    break

            return kwargs
        except Exception as x:
            self.__logger.error('Error grab details: {}'.format(x))

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # keyword = input('Please specify Keyword: ')
    # location = input('Please specify Location: ')
    # output_file = input('Please specify output file name: ')

    # keyword = 'plumber'
    # location = 'toronto'
    # output_file = 'yellow_pages_ca_output.csv'

    app = QApplication(sys.argv)
    # global app

    output_file = 'yellow_pages_ca_output_final.csv'
    with YellowPagesCaGrabber(output_file) as spider:
        with open('categories.txt', 'r+') as rf:
            for kw_line in rf.readlines():
                keyword = kw_line.strip()

                with open('locations.txt', 'r+') as rf:
                    for loc_line in rf.readlines():
                        location = loc_line.strip()
                        print('==== Keyword: {}; Location: {} ==='.format(keyword, location))
                        spider.grab_data(keyword, location)

                        sleep_time = random.randint(5, 15)
                        print('Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                        time.sleep(sleep_time)
    print('Script finished!')
    # sys.exit(app.exec_())
