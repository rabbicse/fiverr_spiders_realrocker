import csv
import gzip
import json
import logging
import os
import random
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup
import readline

readline.parse_and_bind("control-v: paste")


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class YellowPagesSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url = 'https://www.yellowpages.com'
    __search_url = 'https://www.yellowpages.com/search?search_terms={}&geo_location_terms=+{}'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'Content-Type': 'application/json; charset=utf-8'}

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 30

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('yellow_pages.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)
        return logger

    def __enter__(self):
        self.__lock = Lock()
        self.__logger = self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(15)

        hdr = [
            ('url', 'Url'),
            ('name', 'Name'),
            ('category', 'Category'),
            ('telephone', 'Telephone'),
            ('email', 'Email'),
            ('website', 'Website'),
            ('description', 'Description'),
            ('locality', 'Locality'),
            ('region', 'Region'),
            ('street_address', 'Street Address'),
            ('postal_code', 'Postal Code'),
            ('country', 'Country'),
            ('latitude', 'Latitude'),
            ('longitude', 'Longitude'),
            ('rating_count', 'Rating Count'),
            ('rating', 'Rating'),
            ('bbb_rating_value', 'BBB Rating Value'),
            ('bbb_rating_link', 'BBB Rating Link'),
            ('brands', 'Brands'),
            ('payment', 'Payment'),
            ('years_in_business', 'Years In Business'),
            ('opening_hours', 'Opening Hours'),
            ('aka', 'AKA'),
            ('amenities', 'Amenities'),
            ('association', 'Association'),
            ('neighborhood', 'Neighborhood'),
            ('social_links', 'Social Links'),
            ('other_inforamtion', 'Other Inforamtion'),
            ('preferred_label', 'Preferred Label')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.__proxies = json_data

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:
            # self.__grab_details('https://www.yellowpages.com/bronx-ny/mip/pelham-plumbing-heating-5693548?lid=170595459')
            # return
            search_urls = []
            for category in self.__read_categories():
                for location in self.__read_locations():
                    search_url = self.__search_url.format(urllib.parse.quote_plus(category.strip(' \t\n\r')), urllib.parse.quote_plus(location.strip(' \t\n\r')))
                    search_urls.append(search_url)
                    # break
                # break

            # Process all records with multi-thread
            with ThreadPool(8) as p:
                p.map(self.__grab_ad_search_results, search_urls)

        except Exception as x:
            self.__logger.error('Error when grab data. Error details: {}'.format(x))

    @tail_recursive
    def __grab_ad_search_results(self, url, page=0, retry=0):
        try:
            self.__logger.info('Search URL: {}'.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            # After creating soup object simply dispose data to free memory
            del data

            business_urls = []
            ad_results = soup.find_all('div', class_='result flash-ad')
            for ad_result in ad_results:
                business_tag = ad_result.find('a', class_='business-name')
                if not business_tag:
                    continue

                business_url = business_tag.get('href')
                if not business_url.startswith('http'):
                    business_url = self.__base_url + business_url
                elif business_url.startswith('http') and 'www.yellowpages.com' not in business_url:
                    continue

                if business_url in self.__url_cache:
                    self.__logger.warning('URL: {} already exists inside output csv!'.format(business_url))
                    continue

                business_urls.append(business_url)

            other_ad_results = soup.find_all('div', class_='listing flash-ad')
            for ad_result in other_ad_results:
                business_tag = ad_result.find('a', class_='business-name')
                if not business_tag:
                    continue

                business_url = business_tag.get('href')
                if not business_url.startswith('http'):
                    business_url = self.__base_url + business_url
                elif business_url.startswith('http') and 'www.yellowpages.com' not in business_url:
                    continue

                if business_url in self.__url_cache:
                    self.__logger.warning('URL: {} already exists inside output csv!'.format(business_url))
                    continue

                business_urls.append(business_url)
                # break

            # Process all records with multi-thread
            with ThreadPool(32) as p:
                p.map(self.__grab_details, business_urls)

            if page < 1:
                next_page_tag = soup.find('a', class_='next ajax-page')
                if next_page_tag:
                    next_url = self.__base_url + next_page_tag.get('href')
                    page = page + 1
                    return self.__grab_ad_search_results(next_url, page=page)
        except Exception as x:
            self.__logger.error('Error when get search results for ad. Error details: {}'.format(x))
            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_ad_search_results(url, page, retry + 1)

    @tail_recursive
    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('Details URL: {}'.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            # After creating soup object simply dispose data to free memory
            del data

            item = {'url': url}
            script_tags = soup.find_all('script', {'type': 'application/ld+json'})
            for script_tag in script_tags:
                if '"longitude":' not in script_tag.text:
                    continue

                json_data = json.loads(script_tag.text)

                if 'name' in json_data:
                    item['name'] = json_data['name']

                if 'telephone' in json_data:
                    item['telephone'] = json_data['telephone']

                if 'aggregateRating' in json_data:
                    if 'ratingValue' in json_data['aggregateRating']:
                        item['rating'] = json_data['aggregateRating']['ratingValue']
                    if 'reviewCount' in json_data['aggregateRating']:
                        item['rating_count'] = json_data['aggregateRating']['reviewCount']

                if 'address' in json_data:
                    if 'streetAddress' in json_data['address']:
                        item['street_address'] = json_data['address']['streetAddress']
                    if 'addressLocality' in json_data['address']:
                        item['locality'] = json_data['address']['addressLocality']
                    if 'addressRegion' in json_data['address']:
                        item['region'] = json_data['address']['addressRegion']
                    if 'postalCode' in json_data['address']:
                        item['postal_code'] = json_data['address']['postalCode']
                    if 'addressCountry' in json_data['address']:
                        item['country'] = json_data['address']['addressCountry']

                if 'geo' in json_data:
                    if 'longitude' in json_data['geo']:
                        item['longitude'] = json_data['geo']['longitude']
                    if 'latitude' in json_data['geo']:
                        item['latitude'] = json_data['geo']['latitude']

            if 'name' not in item:
                name_tag_div = soup.find('div', class_='sales-info')
                if name_tag_div:
                    name_tag = name_tag_div.find('h1')
                    if name_tag:
                        item['name'] = name_tag.text.strip()
            if 'telephone' not in item:
                phone_tag = soup.find('p', class_='phone')
                if phone_tag:
                    item['telephone'] = phone_tag.text.strip()

            web_tag = soup.find('a', class_='secondary-btn website-link')
            if web_tag:
                item['website'] = web_tag.get('href')

            email_tag = soup.find('a', class_='email-business')
            if email_tag:
                item['email'] = email_tag.get('href').replace('mailto:', '')

            bbb_rating_val_tag = soup.find('span', class_='bbb-no-link')
            if bbb_rating_val_tag:
                item['bbb_rating_value'] = bbb_rating_val_tag.text.strip()

            bbb_rating_link_tag = soup.find('a', class_='bbb-link')
            if bbb_rating_link_tag:
                item['bbb_rating_link'] = bbb_rating_link_tag.get('href')

            payment_tag = soup.find('dd', class_='payment')
            if payment_tag:
                item['payment'] = payment_tag.text.strip()

            desc_tag = soup.find('dd', class_='general-info')
            if desc_tag:
                item['description'] = desc_tag.text.strip()

            associations_tag = soup.find('dd', class_='associations')
            if associations_tag:
                item['association'] = associations_tag.text.strip()

            brands_tag = soup.find('dd', class_='brands')
            if brands_tag:
                item['brands'] = brands_tag.text.strip()

            year_in_business_div = soup.find('div', class_='years-in-business')
            if year_in_business_div:
                year_in_business_number_tag = year_in_business_div.find('div', class_='number')
                if year_in_business_number_tag:
                    item['years_in_business'] = year_in_business_number_tag.text.strip()

            aka_tag = soup.find('dd', class_='aka')
            if aka_tag:
                item['aka'] = aka_tag.text.strip()

            amenities_tag = soup.find('dd', class_='amenities')
            if amenities_tag:
                item['amenities'] = amenities_tag.text.strip()

            opening_hours_tag = soup.find('div', class_='open-details')
            if opening_hours_tag:
                time_tags = opening_hours_tag.find_all('time')
                opening_hours = []
                for time_tag in time_tags:
                    time_label_tag = time_tag.find('span', class_='day-label')
                    if not time_label_tag:
                        continue
                    time_val_tag = time_tag.find('span', class_='day-hours')
                    if not time_val_tag:
                        continue

                    opening_hour = '{}: {}'.format(time_label_tag.text.strip(), time_val_tag.text.strip())
                    opening_hours.append(opening_hour)
                item['opening_hours'] = ', '.join(opening_hours)

                social_tag = soup.find('dd', class_='social-links')
                if social_tag:
                    item['social_links'] = social_tag.text.strip()

                neighborhoods_tag = soup.find('dd', class_='neighborhoods')
                if neighborhoods_tag:
                    item['neighborhood'] = neighborhoods_tag.text.strip()

                categories_tag = soup.find('dd', class_='categories')
                if categories_tag:
                    item['category'] = categories_tag.text.strip()

                otherinfo_tag = soup.find('dd', class_='other-information')
                if otherinfo_tag:
                    item['other_inforamtion'] = otherinfo_tag.text.strip()

            item['preferred_label'] = 'False'
            preferred_label = soup.find('div', class_='preferred-label')
            if preferred_label:
                item['preferred_label'] = 'True'

            self.__write_item(item)
        except Exception as x:
            self.__logger.error('Error when get business details. Error details: {}'.format(x))
            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __read_categories(self):
        try:
            categories = []
            with open('categories.txt', 'r+', encoding='utf-8') as f:
                for line in f:
                    category = line.strip()
                    category = urllib.parse.quote_plus(category)
                    categories.append(category)
            return categories
        except Exception as x:
            self.__logger.error('Error when read all categories. Error details: {}'.format(x))

    def __read_locations(self):
        try:
            regions = []
            with open('locations.txt', 'r+', encoding='utf-8') as f:
                for line in f:
                    region = line.strip()
                    region = urllib.parse.quote_plus(region)
                    regions.append(region)
            return regions
        except Exception as x:
            self.__logger.error('Error when read all locations. Error details: {}'.format(x))

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # output_file = 'yellow_pages_ad_output_updated.csv'
    output_file = input('Please specify output csv: ')
    with YellowPagesSpider(output_file) as spider:
        spider.grab_data()
