import contextlib
import multiprocessing
import threading
from multiprocessing.pool import Pool
import logging


def daemon(log_queue):
    while True:
        try:
            record_data = log_queue.get()
            if record_data is None:
                break
            record = logging.makeLogRecord(record_data)

            logger = logging.getLogger(record.name)
            if logger.isEnabledFor(record.levelno):
                logger.handle(record)
        except (KeyboardInterrupt, SystemExit):
            raise
        except EOFError:
            break
        except:
            logging.exception('Error in log handler.')

def logged_call(log_queue, func, *args, **kwargs):
    MultiprocessingLogger.log_queue = log_queue
    logging.setLoggerClass(MultiprocessingLogger)
    # monkey patch root logger and already defined loggers
    logging.root.__class__ = MultiprocessingLogger
    for logger in logging.Logger.manager.loggerDict.values():
        if not isinstance(logger, logging.PlaceHolder):
            logger.__class__ = MultiprocessingLogger
    func(*args, **kwargs)


@contextlib.contextmanager
def open_queue():
    m = multiprocessing.Manager()
    log_queue = m.Queue() #multiprocessing.Queue()
    daemon_thread = threading.Thread(target=daemon, args=(log_queue,))
    daemon_thread.start()
    yield log_queue
    log_queue.put(None)

class MultiprocessingLogger(logging.Logger):
    log_queue = None

    def isEnabledFor(self, level):
        return True

    def handle(self, record):
        ei = record.exc_info
        if ei:
            # to get traceback text into record.exc_text
            logging._defaultFormatter.format(record)
            record.exc_info = None  # not needed any more
        d = dict(record.__dict__)
        d['msg'] = record.getMessage()
        d['args'] = None
        self.log_queue.put(d)


# -*- coding: utf-8 -*-
'''
Copyright (c) 2013 Qin Xuye <qin@qinxuye.me>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
Created on 2013-5-31
@author: Chine
'''

import socket
import logging.handlers
import struct

try:
    import cPickle as pickle
except ImportError:
    import pickle


class Log(object):
    def __init__(self, name, default_level=logging.DEBUG):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(default_level)
        self.formatter = logging.Formatter(
            '%(asctime)s - %(module)s.%(funcName)s.%(lineno)d - %(levelname)s - %(message)s')

    def add_stream_log(self, level=logging.DEBUG, format_=False):
        stream_handler = logging.StreamHandler()
        if format_:
            stream_handler.setFormatter(self.formatter)
        stream_handler.setLevel(level)
        self.logger.addHandler(stream_handler)

    def add_file_log(self, filename, level=logging.INFO):
        handler = logging.FileHandler(filename)
        handler.setFormatter(self.formatter)
        handler.setLevel(level)
        self.logger.addHandler(handler)

    def add_remote_log(self, server, level=logging.INFO):
        if ':' in server:
            server, port = tuple(server.split(':', 1))
            port = int(port)
        else:
            port = logging.handlers.DEFAULT_TCP_LOGGING_PORT

        socket_handler = logging.handlers.SocketHandler(server, port)
        socket_handler.setLevel(level)
        self.logger.addHandler(socket_handler)

    def get_logger(self):
        return self.logger


def get_logger(name='cola', filename=None, server=None, is_master=False,
               basic_level=logging.DEBUG):
    log = Log(name, basic_level)
    log.add_stream_log(basic_level)

    if filename is not None:
        level = logging.INFO
        if is_master:
            level = logging.ERROR
        log.add_file_log(filename, level)

    if server is not None:
        log.add_remote_log(server, logging.INFO)

    return log.get_logger()










class PoolDemo:
    def __init__(self):
        self.logger = get_logger('cola_job_command')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def do_pool(self):
        with Pool(5) as pool:
            pool.map(self.do_process, range(1000))

    def do_process(self, n):
        try:
            print(n)
        except:
            pass


if __name__ == '__main__':
    with PoolDemo() as demo:
        demo.do_pool()
