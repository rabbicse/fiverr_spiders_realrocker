import csv
import gzip
import json
import os
import platform
import re
import socket
import urllib.parse
import urllib.request
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import ActionChains, DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import logging
import readline

readline.parse_and_bind("control-v: paste")


class TripAdvisorUpdatedSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url = 'https://www.tripadvisor.com'
    __start_url = 'https://www.tripadvisor.com/Restaurants-g60763-New_York_City_New_York.html'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __url_cache = []

    def __init__(self, country_link, output_csv):
        self.__country_link = country_link
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler('tripadvisor.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(60)

        # csv_header = ['Name', 'Address', 'Country', 'Phone', 'Email', 'Website', 'URL', 'Image URL', 'Image File',
        #               'Average Prices', 'Cuisine', 'Ratings', '# of reviews', 'Menu', 'Business Category']
        csv_header = ['Name', 'Address', 'Phone', 'Email', 'Website', 'Cuisine', 'URL']
        self.__write_data(csv_header)

        if not os.path.exists('./images'):
            os.mkdir('./images')
        return self

    def __build_browser(self):
        try:
            cap = webdriver.DesiredCapabilities.PHANTOMJS
            cap["phantomjs.page.settings.javascriptEnabled"] = True
            cap['phantomjs.page.settings.userAgent'] = self.__user_agent
            driver_path = './driver/phantomjs'
            if str(platform.system()).startswith('Windows'):
                driver_path = './driver/phantomjs.exe'

            # browser = webdriver.PhantomJS(driver_path, desired_capabilities=cap,
            #                               service_args=['--ignore-ssl-errors=true'])


            # options = webdriver.ChromeOptions(set_headless=True)
            # options.set_headless(True)
            # options.add_argument('ignore-certifcate-errors')

            chrome_options = Options()
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--window-size=800x600")
            browser = webdriver.Chrome('./driver/chromedriver', desired_capabilities=DesiredCapabilities.CHROME,
                                       chrome_options=chrome_options)
            return browser
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data_by_country(self, country_link=None):
        try:
            if not country_link:
                country_link = self.__country_link
            # print('=== Country URL: {} ==='.format(country_link))
            self.__logger.info('=== Country URL: {} ==='.format(country_link))
            req = urllib.request.Request(country_link, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            link_tags = soup.find_all('div', class_='geo_name')
            if link_tags and len(link_tags) > 0:
                for link_tag in link_tags:
                    link = link_tag.find('a')
                    if link:
                        url = self.__base_url + link.get('href')
                        self.grab_all_data(url)

            link_ul = soup.find('ul', class_='geoList')
            if link_ul:
                link_tags = link_ul.find_all('li')
                if link_tags and len(link_tags) > 0:
                    for link_tag in link_tags:
                        link = link_tag.find('a')
                        if link:
                            url = self.__base_url + link.get('href')
                            self.grab_all_data(url)

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

            next_page_tag = soup.find('a', class_='guiArw sprite-pageNext ')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

        except Exception as x:
            print(x)

    def grab_all_data(self, url):
        next_page = self.grab_data(url)
        while next_page:
            next_page = self.grab_data(next_page)

    def grab_data(self, url):
        try:
            # print('=== Main URL: {} ==='.format(url))
            self.__logger.info('=== Main URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', errors='ignore')
            except Exception as e:
                data = data.decode('utf-8', errors='ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            divs = soup.find_all('div', id=re.compile(r'^eatery_\d+$'))
            for div in divs:
                link_tag = div.find('a', class_='property_title')
                if link_tag:
                    link = self.__base_url + link_tag.get('href')
                    if link not in self.__url_cache:
                        is_done = self.__grab_details(link)
                        if is_done:
                            self.__url_cache.append(link)
                    else:
                        self.__logger.warning('Link: {} already grabbed.'.format(link))

            divs = soup.find_all('div', class_='attraction_element')
            for div in divs:
                link_tag = div.find('div', class_='listing_title ')
                if link_tag:
                    link_tag_a = link_tag.find('a')
                    if link_tag_a:
                        link = self.__base_url + link_tag_a.get('href')
                        if link not in self.__url_cache:
                            is_done = self.__grab_details(link)
                            if is_done:
                                self.__url_cache.append(link)
                        else:
                            self.__logger.warning('Link: {} already grabbed.'.format(link))

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                return self.__base_url + next_page_tag.get('href').strip()
        except Exception as x:
            print(x)

    def __grab_details(self, url):
        try:
            # print('=== Details URL: {} ==='.format(url))
            self.__logger.info('=== Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            name = ''
            address = ''
            country = ''
            phone = ''
            email = ''
            web = ''
            image_url = ''
            image_file = ''
            average_prices = ''
            cuisine = ''
            ratings = ''
            num_of_reviews = ''
            menu = ''
            business_category = ''

            name_tag = soup.find('h1', id='HEADING')
            if name_tag:
                name = name_tag.text.strip()

            address_tag = soup.find('span', class_='format_address')
            if address_tag:
                address = address_tag.text.strip()

            json_tag = soup.find('script', {'type': 'application/ld+json'})
            if json_tag:
                json_data = json.loads(json_tag.text)
                if 'address' in json_data and 'addressCountry' in json_data['address'] and 'name' in \
                        json_data['address']['addressCountry']:
                    country = json_data['address']['addressCountry']['name']

                if 'address' in json_data and address == '':
                    address_list = []
                    if 'streetAddress' in json_data['address']:
                        address_list.append(json_data['address']['streetAddress'])
                    if 'streetAddress' in json_data['address']:
                        address_list.append(json_data['address']['streetAddress'])
                    if 'addressLocality' in json_data['address']:
                        address_list.append(json_data['address']['addressLocality'])
                    if 'addressRegion' in json_data['address']:
                        address_list.append(json_data['address']['addressRegion'])
                    if 'postalCode' in json_data['address']:
                        address_list.append(json_data['address']['postalCode'])
                    address_list.append(country)
                    address = ', '.join(address_list)

                if 'aggregateRating' in json_data:
                    if 'ratingValue' in json_data['aggregateRating']:
                        ratings = json_data['aggregateRating']['ratingValue']
                    if 'reviewCount' in json_data['aggregateRating']:
                        num_of_reviews = json_data['aggregateRating']['reviewCount']
                if 'image' in json_data:
                    image_url = json_data['image']
                    image_file = image_url.split('/')[-1]

                    try:
                        urllib.request.urlretrieve(image_url, './images/{}'.format(image_file))
                    except Exception as ex:
                        print('Error when download image from {}.'.format(image_url))
                        print(ex)

            phone_tag = soup.find('div', class_='blEntry phone')
            if phone_tag and not 'Add phone number' in phone_tag.text:
                phone = phone_tag.text

            if phone == '':
                phone_tag = soup.find('div', class_='blEntry phone directContactInfo')
                if phone_tag and not 'Add phone number' in phone_tag.text:
                    phone = phone_tag.text

            email_tag = soup.find('span', class_='ui_icon email')
            if email_tag:
                email_link_tag = email_tag.find_next_sibling('a')
                if email_link_tag:
                    email = email_link_tag.get('href')
                    email = email.replace('mailto:', '')
                    email = email.strip()

            details_tags = soup.find_all('div', class_='row')
            if details_tags and len(details_tags) > 0:
                for row in details_tags:
                    title = row.find('div', class_='title')
                    if not title:
                        continue
                    if 'Cuisine' in title.text:
                        contant_tag = row.find('div', class_='content')
                        if contant_tag:
                            cuisine = contant_tag.text.strip()

            menu_tab = soup.find('div', id='RESTAURANT_MENU')
            if menu_tab:
                menus = []
                menu_tags = menu_tab.find_all('div', class_=re.compile(r'(?:^menuItemTitle$)|(?:^menuItem$)'))
                for menu_tag in menu_tags:
                    menus.append(menu_tag.text.strip())
                menu = ', '.join(menus)

            business_category_tag = soup.find('span', class_='header_detail attraction_details')
            if business_category_tag:
                business_category = business_category_tag.text.strip()
                business_category = business_category.replace('More', '')
                business_category = business_category.strip()
                business_category = business_category.strip(',')

            web_tag = soup.find('div', class_='blEntry website')
            if web_tag and web_tag.has_attr('data-ahref') and len(web_tag.get('data-ahref').strip()) > 0:
                web = self.grab_web_url(url)
                self.__logger.info('Web site: {}'.format(web))
            else:
                self.__logger.debug('No web address present for {}'.format(url))

            csv_data = [name, address, country, phone, email, web, url, image_url, image_file, average_prices, cuisine,
                        ratings, num_of_reviews, menu, business_category]
            # print(csv_data)
            self.__logger.info('{}'.format(csv_data))
            self.__write_data(csv_data)
            return True
        except Exception as x:
            print(x)

    def grab_web_url(self, url):
        browser = None
        try:
            self.__logger.info('Trying to get web site address. URL: {}'.format(url))
            browser = self.__build_browser()
            browser.get(url)
            wait = WebDriverWait(browser, 60)
            wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

            # Get web address
            element = browser.find_element_by_xpath('//div[@class="blEntry website"]')
            if element:
                actionChains = ActionChains(browser)
                actionChains.move_to_element(element)
                actionChains.click(element)
                actionChains.perform()
                # element.click()

                print(browser.window_handles)
                # Switch tab to the new tab, which we will assume is the next one on the right
                browser.switch_to_window(browser.window_handles[-1])
                web = browser.current_url
                print('Web url: ' + web)

                # wait = WebDriverWait(browser, 60)
                # wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
                # web = browser.current_url
                return web if web.startswith('http') else ''
        except Exception as x:
            print(x)
        finally:
            try:
                if browser:
                    browser.quit()
                    browser = None
                    del browser
            except:
                pass
        return ''

    __total = 0

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            print('Error printing csv output: ' + str(x))


if __name__ == '__main__':
    # country_link = input('Please specify URL: ')
    # output_file = input('Please specify output file name: ')
    country_link = 'https://www.tripadvisor.com/Restaurants-g294200-Egypt.html'
    output_file = 'trip_output_final_new.csv'
    with TripAdvisorUpdatedSpider(country_link, output_file) as spider:
        web_url = spider.grab_web_url(
            'https://www.tripadvisor.com/Restaurant_Review-g60763-d423304-Reviews-Bouley_Restaurant-New_York_City_New_York.html')
        print(web_url)
        # spider.grab_data_by_country()
        # with open('urls.txt', 'r+') as rf:
        #     for url_line in rf.readlines():
        #         url = url_line.strip()
        #         print('==== URL from file: {} ==='.format(url))
        #         spider.grab_all_data(url)
