import csv
import gzip
import os
import random
import re
import socket
import urllib.request
import urllib.parse
from stem import Signal
from stem.control import Controller
import socks
import time
from bs4 import BeautifulSoup
import logging
import readline

readline.parse_and_bind("control-v: paste")


class LocalHarvestGrabber:
    # __start_url = 'https://www.localharvest.org/search.jsp?m&lat=44.1&lon=-121.2&scale=5&st=39&ty=6&p={}'
    # __start_url = 'https://www.localharvest.org/search.jsp?lat=44.1&lon=-121.2&scale=5&st=39&ty=0&p={}'
    # __start_url = 'https://www.localharvest.org/search.jsp?lat=45.7&lon=-115.2&scale=5&st=14&ty=6&p={}'
    # __start_url = 'https://www.localharvest.org/search.jsp?lat=45.7&lon=-115.2&scale=5&st=14&ty=0&p={}'
    # __start_url = 'https://www.localharvest.org/search.jsp?lat=39.0&lon=-105.5&scale=5&st=6&ty=6&p={}'
    # __start_url = 'https://www.localharvest.org/search.jsp?lat=39.0&lon=-105.5&scale=5&st=6&ty=0&p={}'
    __base_url = 'https://www.localharvest.org'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __total = 0
    __url_cache = []

    def __init__(self, url, output_csv):
        self.__start_url = url + '&p={}'
        self.__output_csv = output_csv

    def __enter__(self):
        self.__setup_logger('localharvest.log')
        socket.setdefaulttimeout(30)
        socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        socket.socket = socks.socksocket
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        # self.__grab_proxies()
        # self.__proxy_list = []
        # with open('proxy.txt', 'r+') as rf:
        #     for line in rf.readlines():
        #         proxy_line = line.strip()
        #         proxy_parts = proxy_line.split()
        #         http = proxy_parts[-1]
        #         proxy = proxy_parts[0] + ':' + proxy_parts[1]
        #         proxy_dict = {http: http + '://' + proxy}
        #         self.__proxy_list.append(proxy_dict)

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.reader(f)
                for row in reader:
                    self.__url_cache.append(row[-1])

        csv_header = ['Name', 'Street Address', 'City', 'State', 'Zip Code', 'URL']
        if csv_header[-1] not in self.__url_cache:
            self.__write_data(csv_header)

        return self

    def __grab_proxies(self):
        try:
            url = 'https://free-proxy-list.net/'
            # url = 'https://www.socks-proxy.net/'
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', 'ignore')
            soup = BeautifulSoup(data, 'lxml')

            proxy_table = soup.find('table', id='proxylisttable').find_all('tr')
            if proxy_table:
                with open('proxy.txt', 'w+') as f:
                    for proxy in proxy_table[1:]:
                        columns = proxy.find_all('td')

                        if len(columns) < 8:
                            continue

                        http = 'http'
                        if columns[-2].text == 'yes':
                            http = 'https'

                        f.write(columns[0].text.strip() + ' ' + columns[1].text.strip() + ' ' + http + '\n')
        except Exception as x:
            print(x)

    def __set_random_proxy(self):
        try:
            current_proxy = random.choice(self.__proxy_list)
            self.__logger.info('Current proxy: {}'.format(current_proxy))
            proxy_handler = urllib.request.ProxyHandler(current_proxy)
            opener = urllib.request.build_opener(proxy_handler)
            urllib.request.install_opener(opener)
        except Exception as x:
            self.__logger.error('Error setting proxy: {}'.format(x))

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __get_proxy_address(self):
        try:
            url = 'http://ifconfig.me/ip'
            req = urllib.request.Request(url)
            res = urllib.request.urlopen(req).read()
            self.__logger.info('=== Proxy IP: {} ==='.format(res.decode('utf-8').strip()))
        except Exception as x:
            pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            url = self.__start_url.format(1)
            next_url = self.__grab_pages(url)
            while next_url:
                sleep_time = random.randint(30, 60)
                self.__logger.info('Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                time.sleep(sleep_time)

                next_url = self.__grab_pages(next_url)
        except Exception as x:
            print(x)

    def __grab_pages(self, url):
        try:
            self.__logger.info('=== URL: {} ==='.format(url))
            # self.__get_proxy_address()
            # self.__set_random_proxy()
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            item_list = soup.find_all('div', class_='membercell')
            if not item_list:
                return
            for item in item_list:
                link_tag = item.find('h4', class_='inline')
                if not link_tag:
                    continue

                city, state = '', ''
                city_state_tag = item.find('a', {'rel': 'nofollow'})
                if city_state_tag:
                    city_state = city_state_tag.text.strip()
                    city_state_sp = city_state.split(',')
                    state = city_state_sp[-1].strip()
                    city = ' '.join(city_state_sp[:-1])

                link_tag = link_tag.find('a')
                if not link_tag:
                    continue

                link = self.__base_url + link_tag.get('href')

                contact_url = ''
                name = link_tag.text.strip()
                contact_tags = item.find_all('a')
                if contact_tags and len(contact_tags) > 0:
                    for contact_tag in contact_tags:
                        if 'more' in contact_tag.text:
                            contact_url = self.__base_url + contact_tag.get('href')
                            break

                if contact_url != '':
                    if contact_url not in self.__url_cache:
                        sleep_time = random.randint(10, 20)
                        self.__logger.info(
                            'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                        time.sleep(sleep_time)
                        self.__grab_details(contact_url, name, city, state)
                        self.__url_cache.append(contact_url)
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(contact_url))
                else:
                    if link not in self.__url_cache:
                        sleep_time = random.randint(10, 20)
                        self.__logger.info(
                            'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                        time.sleep(sleep_time)
                        self.__grab_details_page(link, name, city, state)
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(contact_url))

            # pagination
            pagination_div = soup.find('div', class_='pagination')
            if pagination_div:
                link_tag = pagination_div.find('a', text=re.compile(r'\s*?\>\>\s*?'))
                if link_tag:
                    return self.__base_url + link_tag.get('href').strip()

        except Exception as x:
            self.__logger.error('Error grab pages: {}'.format(x))

    def __grab_details_page(self, url, *args):
        try:
            print('=== Main URL: {} ==='.format(url))
            # self.__get_proxy_address()
            # self.__set_random_proxy()
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            details_link = ''
            a_tag_list = soup.find_all('a')
            for a_tag in a_tag_list:
                if 'Visit our Listing' in a_tag.text:
                    details_link = self.__base_url + a_tag.get('href').strip()
                    break

            if details_link != '':
                if details_link not in self.__url_cache:
                    sleep_time = random.randint(10, 20)
                    self.__logger.info(
                        'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                    time.sleep(sleep_time)
                    self.__grab_details(details_link, *args)
                    self.__url_cache.append(details_link)
                else:
                    self.__logger.warning('Already grabbed URL: {}.'.format(details_link))
            else:
                csv_data = [args[0], '', args[1], args[2], '', url]
                self.__logger.info('Data: {}'.format(csv_data))
                self.__write_data(csv_data)
        except Exception as x:
            self.__logger.error('Error grab main page: {}'.format(x))

    def __grab_details(self, url, *args):
        try:
            print('=== Details URL: {} ==='.format(url))
            # self.__get_proxy_address()
            # self.__set_random_proxy()
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            name = args[0]
            address = ''
            city = args[1]
            state = args[2]
            zipcode = ''

            location_tag = soup.find('h5', text=re.compile(r'(?i)Location|Mailing Address'))
            if location_tag:
                address_tag = location_tag.find_next_sibling('div')
                if address_tag:
                    st_address = address_tag.text.strip()
                    st_address = re.sub(r'(?i)Address\:', '', st_address)
                    st_address = st_address.strip()
                    st_address = re.sub(r'{}\,?'.format(city), '', st_address)
                    st_address = re.sub(r'{}'.format(state), '', st_address)
                    zipcode_m = re.search(r'(\d+)$', st_address.strip())
                    if zipcode_m:
                        zipcode = zipcode_m.group(1).strip()
                    st_address = re.sub(r'{}'.format(zipcode), '', st_address)
                    address = st_address.strip()
                    address = re.sub(r'\n+', ' ', address)
                    address = re.sub(r'\s+', ' ', address)

            csv_data = [name, address, city, state, zipcode, url]
            self.__logger.info('Data: {}'.format(csv_data))
            self.__write_data(csv_data)
        except Exception as x:
            self.__logger.error('Error grab details: {}'.format(x))

    def __get_web_url(self, url):
        try:
            print('=== Web URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            response = urllib.request.urlopen(req)
            return response.geturl()
        except Exception as x:
            self.__logger.error('Error getting web address: {}'.format(x))
        return url

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            print('Error printing csv output: ' + str(x))


if __name__ == '__main__':
    with open('local_harvest_input.txt', 'r+') as rf:
        i = 0
        for line in rf.readlines():
            proxy_line = line.strip()
            proxy_parts = proxy_line.split(': ')
            url = proxy_parts[-1].strip()
            suffix = proxy_parts[0].strip().replace(' ', '_') + '_{}'.format(i)

            output_file = 'local_harvest_output_{}.csv'.format(suffix)
            with LocalHarvestGrabber(url, output_file) as spider:
                spider.grab_data()

            i += 1
