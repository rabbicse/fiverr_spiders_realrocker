import csv
import gzip
import html
import random
import re
import time
import readline
import urllib.parse
import urllib.request
from multiprocessing.pool import ThreadPool
from multiprocessing import Lock

import datetime

readline.parse_and_bind("control-v: paste")

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Host': 'www.facebook.com',
    'Referer': 'https://www.facebook.com/',
    'Connection': 'keep-alive',
    'Accept-Encoding': 'gzip',
    'Upgrade-Insecure-Requests': '1'}

lock = Lock()
proxies = []
count = 0

redirect_handler = urllib.request.HTTPRedirectHandler()
opener = urllib.request.build_opener(redirect_handler)
urllib.request.install_opener(opener)


def print_count():
    global count
    count += 1
    print('Processed {}'.format(count))


def grab_fb_data(url, proxy=None):
    try:
        # print(url)
        # grab_about_page(url, proxy)
        about_url = url.strip('/')
        about_url = about_url.replace('#!/', '')
        about_url = re.sub(r'\?[^\$]*?$', '', about_url)
        about_url = about_url.strip('/')

        about_url = about_url + '/about/?ref=page_internal'
        print('=== URL: ' + about_url + ' ===')
        if proxy:
            print('Proxy: ' + proxy)

        # 1-5 sec random sleep
        time.sleep(random.randint(1, 5))

        req = urllib.request.Request(about_url, headers=headers)
        if proxy:
            req.set_proxy(proxy, 'https')

        response = urllib.request.urlopen(req).read()
        try:
            data = gzip.decompress(response).decode('utf-8', 'ignore')
        except Exception as e:
            print(e)
            data = response.decode('utf-8', 'ignore')

        return data
    except Exception as x:
        print('Error when grab data.' + str(x))


def grab_about_page(url, proxy=None):
    try:
        req = urllib.request.Request(url, headers=headers)
        if proxy:
            req.set_proxy(proxy, 'https')

        response = urllib.request.urlopen(req)
        response_data = response.read()

        print(response.geturl())
        print(response.info())
        try:
            data = gzip.decompress(response_data).decode('utf-8', 'ignore')
        except Exception as e:
            print(e)
            data = response_data.decode('utf-8', 'ignore')

            # print(data)
    except Exception as x:
        print(x)


def parse_email(data):
    try:
        pattern = r'<a href="mailto\:([^\?]*?)\?'
        m = re.search(pattern, data, re.MULTILINE)
        if m:
            email = m.group(1)
            email = urllib.parse.unquote_plus(email, encoding='utf-8')
            email = html.unescape(email)
            return email.strip()
    except Exception as x:
        print('Error when parse email from FB: ' + str(x))


def fb_operation(input_csv, output_csv, proxy_file=None):
    try:
        if proxy_file:
            with open(proxy_file, 'r', encoding='utf-8') as f:
                for line in f:
                    proxy = line.strip()
                    print(proxy)
                    proxies.append(proxy)

        with open(output_csv, 'w', newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(['Facebook URL', 'Email'])

        # pool = Pool()
        # pool.starmap(process_url,
        #              [(url, output_csv, proxy) for (url, proxy) in read_input_csv(input_csv) if url is not None])
        #

        with ThreadPool(16) as p:
            p.map(process_run,
                  [(url, output_csv, proxy) for (url, proxy) in read_input_csv(input_csv) if url is not None])
        print('Finish!!!')

        # for row in read_input_csv(input_csv):
        #     # grab data with proxy
        #     proxy = None
        #     if len(proxies) > 0:
        #         proxy = proxies[random.randint(0, len(proxies) - 1)]
        #
        #     url = str(row[0]).strip()
        #
        #     process_url(url, output_csv, proxy)
        #     # Process(target=process_url, args=(url, output_csv, proxy)).start()
        #
        #     time.sleep(random.randint(1, 5))

        # print('Finish!!')
    except Exception as x:
        print('Error when processing urls: ' + str(x))


def read_input_csv(input_csv):
    try:
        with open(input_csv, 'r', encoding='utf-8') as f:
            reader = csv.reader(f)
            for row in reader:
                if not row or len(row) == 0:
                    print('Invalid row...')
                    continue

                if 'https' not in row[0]:
                    print('wrong data...')
                    continue
                url = str(row[0]).strip()
                proxy = proxies[random.randint(0, len(proxies) - 1)] if len(proxies) > 0 else None
                yield url, proxy
    except Exception as x:
        print('Error when read csv file: ' + str(x))


def process_run(*args):
    print(args)
    process_url(args[0][0], args[0][1], args[0][2])


def process_url(url, output_csv, proxy):
    try:
        data = grab_fb_data(url, proxy=proxy)
        if not data:
            print('Failed to grab facebook data.')
            write_data(output_csv, [url, ''])
            return

        email = parse_email(data)
        if email:
            print('Writing email address to csv: ' + email)
            write_data(output_csv, [url, email])
            return True
        else:
            print('Failed to parse email address from url: ' + url)
            write_data(output_csv, [url, ''])
    except Exception as x:
        print('Error when processing data: ' + str(x))


def write_data(file_name, row):
    try:
        lock.acquire()
        with open(file_name, 'a', newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(row)
            print_count()
    except Exception as x:
        print('Error printing csv output: ' + str(x))
    finally:
        lock.release()


if __name__ == '__main__':
    try:
        input_csv = input('Please Specify Input CSV: ')
        # output_csv = input('Please Specify Output CSV: ')
        # fb_operation(input_csv, output_csv, 'proxy.txt')
        output_csv = './fb_output_' + datetime.datetime.today().strftime('%Y_%m_%d.') + 'csv'
        proxy_file = './proxy.txt'
        fb_operation(input_csv, output_csv, proxy_file)

        print('Press any key to exit!')
        input()
    except Exception as x:
        print(x)
