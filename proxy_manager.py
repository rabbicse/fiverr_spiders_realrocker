import json

# https://checkerproxy.net/
# txt = 'http://{}:{}@{}:{}\n'
with open('prx.txt', 'r+') as f:
    with open('proxy.txt', 'w+') as w:
        for p in f.readlines():
            w.write('https://{}'.format(p))
            # p = p.strip()
            # ps = p.split(':')
            # l = txt.format(ps[2], ps[3], ps[0], ps[1])
            # print(l)
            # w.write(l)

        # with open('cat.txt', 'r+') as f:
        #     for line in f.readlines():
        #         print('"{}",'.format(line.strip()))


data = []
with open('prx.txt', 'r+') as f:
    for p in f.readlines():
        line = p.strip()
        ip, port = line.split(':')
        data.append({'ip': ip, 'port': port, 'https': 'yes'})

with open('proxy.json', 'w+') as outfile:
    json.dump(data, outfile)


# txt = '{}:{}'
# data = []
# with open('proxy.txt', 'r+') as f:
#     for p in f.readlines():
#         line = p.strip()
#         usr, p, port = line.split(':')
#         ip = txt.format(usr, p)
#         data.append({'ip': ip, 'port': port, 'https': 'yes'})
#
# with open('proxy.json', 'w+') as outfile:
#     json.dump(data, outfile)
