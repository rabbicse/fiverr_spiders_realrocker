# Ubuntu instruction
- libXcomposite.so, libsmime3.so, libXcursor.so, libXtst.so, libXrandr.so, libasound.so
- **libnss3:** apt-get install libnss3
- **libxcomposite-dev:**: apt-get install libxcomposite-dev
- **libxcursor-dev:** apt-get install libxcursor-dev
- **libxtst-dev:** apt-get install libxtst-dev
- **libxrandr-dev:** apt-get install libxrandr-dev
- **libasound2-dev:** apt-get install libasound2-dev

# Prerequisite
- **pyqt5:** pip3 install pyqt5 version: Version: 5.10.1
- **pyopengl:** pip3 install pyopengl
- **wheel:** pip3 install wheel
- **sqlalchemy:** pip3 install sqlalchemy
- **beautifulsoup4:** pip3 install beautifulsoup4
- **lxml:** pip3 install lxml
- **html5lib:** pip3 install html5lib

# Run command
```xvfb-run --server-args='-screen 0 1920x1080x24' python3 quantopian_scrapper.py```

# Deployment
- Open terminal and run the following command:
```ssh root@192.243.96.179```
- Enter password: "Loopcap_123"
- Keep project folder "quantopian" to a directory. For example now it's on: /root/spiders/quantopian/
- Under this folder, edit "credentials.json" file for editing algorithm_path, user, pass, email etc.
- During make another project copy please delete the "quantopian.db" file under "quantopian" directory.
- Edit cronjob by the following command:
```crontab -e```
- You should see a line as follows. If not exists add line like this.
```*/10 * * * * /bin/sh /root/spiders/quantopian/quantopian.sh```
where, /root/spiders/quantopian/quantopian.sh is the script path with directory.
"*/10 * * * *" means run script in every 10 minutes.
- Restart cron service by: ```/etc/init.d/cron restart```
- To check cron log: ```tail -f /var/log/syslog```