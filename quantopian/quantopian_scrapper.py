import email
import json
import logging
import os
import re
import smtplib
import socket
import sys
import time
from datetime import datetime
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock

from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings, QWebEngineProfile, QWebEnginePage
from PyQt5.QtWidgets import QApplication
from bs4 import BeautifulSoup
from sqlalchemy import Column, Integer, Float, create_engine, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DeclarativeBase = declarative_base()


def db_connect():
    """Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance.
    """
    engine = create_engine('sqlite:///quantopian.db', echo=False)
    return engine


def create_table(engine):
    """"""
    DeclarativeBase.metadata.create_all(engine)


class Positions(DeclarativeBase):
    __tablename__ = 'positions'

    id = Column(Integer, primary_key=True)
    pos_id = Column(Integer)
    algorithm_id = Column(String)
    security = Column(String)
    shares = Column(Integer)
    price = Column(Float)
    avg_cost = Column(Float)
    pos_value = Column(Float)
    unrealized = Column(Float)

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])


class WebEnginePage(QWebEnginePage):
    def acceptNavigationRequest(self, url, _type, isMainFrame):
        if _type == QWebEnginePage.NavigationTypeLinkClicked:
            return True
        return QWebEnginePage.acceptNavigationRequest(self, url, _type, isMainFrame)


class Browser(QWebEngineView):
    __redirected_url = None
    __html = None
    __clicked = False
    _timeout_timer = None
    __timer_screen = None
    __logger = None
    __details_url = None
    __login_page = None
    __username = None
    __password = None
    __login_success = False

    def __init__(self, app, logger, login_url, username, password, *args, **kwargs):
        QWebEngineView.__init__(self, *args, **kwargs)
        self.__logger = logger
        self.__app = app
        self.__login_url = login_url
        self.__username = username
        self.__password = password
        self.settings().setAttribute(QWebEngineSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.AllowRunningInsecureContent, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptCanAccessClipboard, True)
        self.loadFinished.connect(self._load_finished)

        profile = QWebEngineProfile("storage", self)
        profile.setCachePath('./cookies')
        profile.setPersistentStoragePath('./cookies')
        self.setPage(WebEnginePage(profile, self))
        self.__loaded = False

    def createWindow(self, wintype):
        return self

    # def login(self):
    #     try:
    #         self.__login_page = True
    #         self._timeout_timer = QTimer()
    #         self._timeout_timer.timeout.connect(self._request_timed_out)
    #         self._timeout_timer.start(120 * 1000)
    #
    #         self.__logger.info('Trying to login via: {}'.format(self.__login_url))
    #         self.setUrl(QUrl(self.__login_url))
    #         self.resize(1920, 1080)
    #         self.show()
    #         self.__app.exec_()
    #         return self.__login_success
    #     except Exception as x:
    #         self.__logger.error('Error when login: {}'.format(x))

    def clear(self):
        try:
            self.__html = None
            self.__redirected_url = None
            self._timeout_timer = QTimer()
            self._timeout_timer.timeout.connect(self._request_timed_out)
            self._timeout_timer.start(120 * 1000)

            self.__logger.info('Trying to clear...')
            self.setUrl(QUrl('https://www.quantopian.com/posts'))
            self.resize(1920, 1080)
            self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            self.__logger.error('Error when clear data: {}'.format(x))
            self.__dispose()
        return self.__redirected_url, self.__html

    def fetch(self, details_url):
        try:
            self.__html = None
            self.__redirected_url = None
            self.__details_url = details_url
            self._timeout_timer = QTimer()
            self._timeout_timer.timeout.connect(self._request_timed_out)
            self._timeout_timer.start(120 * 1000)

            self.__logger.info('Trying to get algorithm details: {}'.format(self.__details_url))
            self.setUrl(QUrl(details_url))
            self.resize(1920, 1080)
            self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            self.__logger.error('Error when fetch data: {}'.format(x))
            self.__dispose()
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            self.__logger.warning('Custom request timeout value exceeded.')
            self.__dispose()
        except Exception as x:
            self.__logger.error('Request timeout: {}'.format(x))
            self.__dispose()

    def _load_finished(self):
        try:

            if self._timeout_timer:
                self._timeout_timer.stop()
                self._timeout_timer = None

            current_url = str(self.page().url().toString())
            self.__logger.info('Redirected URL: {}'.format(current_url))
            if current_url == 'https://www.quantopian.com/posts':
                self.__dispose()
            elif current_url == self.__login_url or current_url == 'https://www.quantopian.com/users/sign_in':
                self.__run_js_login()
            elif current_url == self.__details_url:
                self.__run_js_position_count()
            else:
                self.setUrl(QUrl(self.__details_url))

        except Exception as x:
            self.__logger.error('Error when load response: {}'.format(x))
            self.__dispose()

    def __run_js_login(self):
        fill_user = """document.getElementById("user_email").value='{}'""".format(self.__username)
        fill_pass = """document.getElementById("user_password").value='{}'""".format(self.__password)
        remember_me = """document.getElementById("remember-checkbox").click()"""
        submit = """document.getElementById("login-button").click()"""
        self.page().runJavaScript(fill_user)
        self.page().runJavaScript(fill_pass)
        self.page().runJavaScript(remember_me)
        self.page().runJavaScript(submit)

    def __run_js_position_count(self):
        if self.__timer_screen:
            self.__timer_screen.stop()
            self.__timer_screen = None

        # algorithm_js = """document.querySelector('div[id="positions_count"]').outerHTML"""
        algorithm_js = """document.querySelector('html').outerHTML"""
        self.page().runJavaScript(algorithm_js, self.__js_positions_result_callback)

    def __js_positions_result_callback(self, data):
        self.__logger.info('Searching positions table result from algorithm page...')
        if data:
            self.__logger.info('Found positions table result!')
            soup = BeautifulSoup(data, 'html5lib')
            cnt = soup.find('div', id='positions_count').text.strip()
            cnt = re.sub(r'[^0-9]', '', cnt).strip()
            if cnt == '' or int(cnt) == 0:
                # time.sleep(1)
                self.__timer_screen = QTimer()
                self.__timer_screen.timeout.connect(self.__run_js_position_count)
                self.__timer_screen.start(1 * 1000)
                # self.__run_js_position_count()
            else:
                success = False
                tabs = soup.find_all('div', class_=re.compile(r'^ui-widget-content.*'))
                for tab in tabs:
                    item = {}
                    columns = tab.find_all('div', class_=re.compile(r'^slick-cell.*?$'))
                    if len(columns) < 6:
                        continue

                    column_1 = columns[0]
                    security_id_tag = column_1.find('span')
                    if not security_id_tag:
                        continue

                    security_text = security_id_tag.text.strip()
                    if re.match(r'^\d+$', security_text):
                        success = False
                    else:
                        success = True
                    break
                if success:
                    # self.__run_js_position_count()
                    self.__timer_screen = QTimer()
                    self.__timer_screen.timeout.connect(self.__process_data)
                    self.__timer_screen.start(1 * 1000)
                else:
                    self.__timer_screen = QTimer()
                    self.__timer_screen.timeout.connect(self.__run_js_position_count)
                    self.__timer_screen.start(5 * 1000)

    def __process_data(self):
        self.page().toHtml(self.process_html)

    def process_html(self, html):
        self.__html = html
        current_url = str(self.page().url().toString())
        self.__redirected_url = current_url
        self.__dispose()

    def __dispose(self):
        try:
            self.__details_url = None
            if self._timeout_timer:
                self._timeout_timer.stop()
                self._timeout_timer = None
            if self.__timer_screen:
                self.__timer_screen.stop()
                self.__timer_screen = None
            self.close()
            self.__app.quit()
        except Exception as x:
            self.__logger.error('Error when dispose. {}'.format(x))


class QuantopianGrabber:
    __login_form_url = 'https://www.quantopian.com/signin'
    __login_url = 'https://www.quantopian.com/users/sign_in'
    __algorithm_urls = []

    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}
    __total = 0
    __cache = []

    def __init__(self):
        self.__algorithm_urls = []
        self.__quser = None
        self.__qpass = None
        self.__username = None
        self.__password = None
        self.__from_address = None
        self.__to_address = None
        self.__from_header = None
        self.__to_header = None
        self.__subject = ''

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('quantopian.log')
        socket.setdefaulttimeout(120)
        self.__app = QApplication(sys.argv)


        if os.path.exists('credentials.json'):
            with open('credentials.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                if json_data:
                    self.__algorithm_urls = json_data['algorithm_url']
                    self.__quser = json_data['q_user']
                    self.__qpass = json_data['q_pass']
                    self.__username = json_data['username']
                    self.__password = json_data['password']
                    self.__from_address = json_data['from_address']
                    self.__to_address = json_data['to_addresses']
                    self.__from_header = json_data['from_header']
                    self.__to_header = json_data['to_header']
                    self.__subject = json_data['subject']

        self.__browser = Browser(self.__app, self.__logger, self.__login_form_url, self.__quser, self.__qpass)
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler(log_file_name, maxBytes=2 * 1024 * 1024, backupCount=30)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__browser.close()
        self.__app.exit()
        self.__logger.info('========== SCRIPT FINISHED ==========')
        del self.__browser
        del self.__app
        del self.__logger
        del self

    def operations(self):
        try:
            # login_status = self.__browser.login(self.__login_form_url, self.__quser, self.__qpass)
            # if not login_status:
            #     self.__logger.error('Logged in failed!')
            #     return
            for algorithm_url in self.__algorithm_urls:
                self.operation(algorithm_url)
                sleep_time = 5
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)

        except Exception as x:
            self.__logger.error('Error when operations. {}'.format(x))

    def operation(self, algorithm_url):
        try:

            r, d = self.__browser.clear()

            algorithm_id = algorithm_url.split('/')[-1].strip()
            redirected_url, data = self.__browser.fetch(algorithm_url)
            if not redirected_url or not data:
                return

            if not redirected_url == algorithm_url:
                return

                # print(data)
            items = self.__parse_data(data, algorithm_id)
            if items:
                session = self.Session()
                try:
                    changes = []
                    for item in items:
                        item['algorithm_id'] = algorithm_id
                        self.__logger.info('Item: {}'.format(item))
                        q = {'pos_id': item['pos_id'], 'algorithm_id': algorithm_id}
                        q_data = session.query(Positions).filter_by(**q).first()
                        if not q_data:
                            positions = Positions(**item)
                            session.add(positions)
                            session.commit()
                            self.__logger.info('Item[{}] inserted successfully!'.format(item['pos_id']))
                            changes.append('{} shares of {} has been added!'.format(item['shares'], item['security']))
                        else:
                            data_changes = False
                            if int(q_data.shares) != int(item['shares']):
                                self.__logger.info(
                                    'Share changes. OLD Share: {}; NEW Share: {}'.format(q_data.shares, item['shares']))
                                data_changes = True

                                share_changes = int(item['shares']) - int(q_data.shares)

                                if share_changes < 0:
                                    changes.append(
                                        '{} shares of {} has been sold!'.format(abs(share_changes), item['security']))
                                else:
                                    changes.append(
                                        '{} shares of {} has been bought!'.format(abs(share_changes), item['security']))
                            if float(q_data.price) != float(item['price']) or \
                                            float(q_data.avg_cost) != float(item['avg_cost']) or \
                                            float(q_data.pos_value) != float(item['pos_value']) or \
                                            float(q_data.unrealized) != float(item['unrealized']):
                                data_changes = True

                            if data_changes:
                                session.delete(q_data)
                                session.commit()
                                positions = Positions(**item)
                                session.add(positions)
                                session.commit()

                    # New Changes if no data from result:
                    q = {'algorithm_id': algorithm_id}
                    q_data_iter = session.query(Positions).filter_by(**q).all()
                    for q_data in q_data_iter:
                        if not any([item for item in items if int(q_data.pos_id) == int(item['pos_id'])]):
                            self.__logger.info('Item[{}] completely sold.'.format(q_data.pos_id))
                            changes.append(
                                'Shares of {} has been completely sold!'.format(q_data.security))
                            session.delete(q_data)
                            session.commit()

                    # Send email on test basis first
                    if len(changes) > 0:
                        body = 'Quantopian status for algorithm: {} on {}.\n\n' \
                            .format(algorithm_id, datetime.now().strftime('%y-%m-%d %H:%M:%S'))
                        body += '\n'.join(changes)
                        self.__send_email(body, algorithm_id)
                except Exception as ex:
                    self.__logger.error('Error: {}'.format(ex))
                    session.rollback()
                finally:
                    session.close()
        except Exception as x:
            self.__logger.error('Error when full operation: {}'.format(x))

    def __parse_data(self, data, algorithm_id):
        try:
            soup = BeautifulSoup(data, 'html5lib')

            algo_id_tag = soup.find('input', id='live_algo_id')
            if not algo_id_tag:
                return

            if str(algo_id_tag.get('value')) != algorithm_id:
                return

            tabs = soup.find_all('div', class_=re.compile(r'^ui-widget-content.*'))

            items = []
            for tab in tabs:
                item = {}
                columns = tab.find_all('div', class_=re.compile(r'^slick-cell.*?$'))
                if len(columns) < 6:
                    continue

                column_1 = columns[0]
                security_id_tag = column_1.find('span')
                if not security_id_tag:
                    continue

                security_id = security_id_tag.get('class')
                security_id = re.sub(r'[^\d]', '', str(security_id))

                item['pos_id'] = security_id
                item['security'] = security_id_tag.text.strip()
                item['shares'] = columns[1].text.strip()
                item['price'] = re.sub(r'[^\d\.]', '', columns[2].text.strip())
                item['avg_cost'] = re.sub(r'[^\d\.]', '', columns[3].text.strip())
                item['pos_value'] = re.sub(r'[^\d\.]', '', columns[4].text.strip())
                item['unrealized'] = re.sub(r'[^\d\.]', '', columns[5].text.strip())
                items.append(item)

            return items
        except Exception as x:
            self.__logger.error('Error when parse result set.{}'.format(x))

    def __send_email(self, body, algorithm_id):
        try:
            self.__logger.info('Sending email...')
            msg = MIMEMultipart()
            msg['Subject'] = Header(self.__subject.format(algorithm_id), 'utf-8')
            msg['From'] = email.utils.formataddr((self.__from_header['name'], self.__from_header['email']))
            msg['To'] = COMMASPACE.join(['{} <{}>'.format(kv['name'], kv['email']) for kv in self.__to_header])
            msg['Date'] = formatdate(localtime=True)

            msg.attach(MIMEText(body, 'plain', 'utf-8'))
            msg_body = msg.as_string()

            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.set_debuglevel(True)
            server.ehlo()
            # If we can encrypt this session, do it
            if server.has_extn('STARTTLS'):
                server.starttls()
                server.login(self.__username, self.__password)

            # send email
            server.sendmail(self.__from_address, self.__to_address, msg_body)
            server.quit()
            self.__logger.info('Email sent successfully.')
        except Exception as x:
            self.__logger.error('Error when sending email: {}'.format(x))


if __name__ == '__main__':
    with QuantopianGrabber() as spider:
        spider.operations()
    logging.info('Script finished!')
