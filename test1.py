import html
import re
import csv
from bs4 import BeautifulSoup

pattern = r'<span style=\"font-family\: b\'PWNEJI\+RenetMobile\'; font-size\:9px\"\>!<\/span\>'
# pattern = r'\<span\s*?style\=\"font\-family\:\s*?b\'KDMVJK\+Tahoma\-Bold\'\;\s*?font\-size\:12px\"\>ID\:[^<]*?<br\>\s*?<\/span\>'
with open('./html1/706.pdf.html', 'r', encoding='utf-8') as f:
    data = f.read()
    # data = re.sub(r'<br\/?>', '', data)
    # data = re.sub(r'\n+', '\s', data)
    # data = re.sub(r'\s+', ' ', data)

    template_type = 0
    sp = re.split(r'\>\!\<\/span\>', data)
    if len(sp) < 2:
        id_list = re.findall(r'\>\[ID\:\s*\d+\]', data)
        if id_list and len(id_list) > 0:
            template_type = 3
        else:
            template_type = 2

    if template_type == 3:
        sp = re.split(r'\>\[ID\:\s*\d+\]', data)
        for s in sp[1:]:
            soup = BeautifulSoup(s, 'lxml')
            if not soup:
                continue

            csv_dict = {}
            name_tag = soup.find('p')
            if name_tag:
                csv_dict['name'] = html.unescape(name_tag.text).strip()

            div_list = soup.find_all('div')
            for div in div_list:
                span_tag = div.find_all('span')
                if not span_tag or len(span_tag) < 2:
                    continue
                key = span_tag[0].text.strip()
                values = []
                for span_val in span_tag[1:]:
                    values.append(span_val.text.strip())

                if 'Address:' in key:
                    address = ''.join(values)
                    csv_dict['address'] = address
                    post_code_m = re.search(r'(\d{4,})$', address.strip())
                    if post_code_m:
                        csv_dict['postcode'] = post_code_m.group(1)
                if 'Phone:' in key:
                    phones = []
                    for v in values:
                        p = re.sub(r'[^\d]', '', v)
                        p = p.strip()
                        if p != '':
                            phones.append(p)
                    csv_dict['phone'] = ', '.join(phones)
                if 'Email:' in key:
                    csv_dict['email'] = ''.join(values)
            print(csv_dict)
    elif template_type == 2:
        soup = BeautifulSoup(data, 'lxml')
        names = []
        name_spans = soup.find_all('span', {'style': re.compile(r'font\-family\: [A-Z]+\+Tahoma\; font\-size\:17px')})
        for name_span in name_spans:
            name = name_span.text.strip()
            contact = ''
            next_span = name_span.find_next_sibling('span')
            if next_span:
                contact = next_span.text.strip()
            name_contact = (name, contact)
            names.append(name_contact)

        contact_list = []
        contact_spans = soup.find_all('span',
                                      {'style': re.compile(r'font\-family\: [A-Z]+\+Tahoma\; font\-size\:11px')})
        prev_div = None
        for contact_span in contact_spans:
            div = contact_span.find_parent('div')
            if prev_div == div:
                continue
            # if '@' not in contact_span and not re.search(r'\d{4,}', contact_span.text):
            if '@' not in div.text:
                continue
            email, phone = '', ''
            contact_text = div.text
            contacts = contact_text.split('\n')
            if len(contacts) == 1 and '@' in contacts[0]:
                email = contacts[0].strip()
            elif len(contacts) == 1 and re.search(r'\d{4,}', contacts[0]):
                phone = contacts[0].strip()
            elif len(contacts) > 1:
                email = contacts[0].strip()
                phone = ', '.join(contacts[1:])
            contact_list.append((email, phone))
            prev_div = div

        for i in range(len(names)):
            csv_dict = {'name': names[i][0],
                        'address': names[i][1]}
            if i < len(contact_list):
                csv_dict['email'] = contact_list[i][0]
                csv_dict['phone'] = contact_list[i][1]
            print(csv_dict)

    else:
        i = 0
        with open('PDF_Example.pdf.csv', 'a+') as csvfile:
            fieldnames = ['name', 'address', 'postcode', 'phone', 'email']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            for s in sp:
                soup = BeautifulSoup(s, 'lxml')
                if not soup:
                    continue

                if not 'ID:' in soup.text:
                    continue

                spans = soup.find_all('span')
                id_span = next(iter([s for s in spans if 'ID:' in s.text]))
                if not id_span:
                    continue

                csv_dict = {}

                name = re.sub(r'ID\:\d+\s*\-*', '', id_span.text)
                name = name.strip()

                # template 1
                if 'Managed By:' in soup.text:
                    csv_dict['name'] = name
                    template_type = 1
                else:
                    names = name.split('\n')
                    csv_dict['name'] = names[0]
                    csv_dict['address'] = (' '.join(names[1:]) if len(names) > 1 else '').strip()

                div_list = soup.find_all('div')
                for div in div_list:
                    span_tag = div.find_all('span')

                    if template_type == 1:
                        if not span_tag or len(span_tag) < 2:
                            continue

                        key = span_tag[0].text.strip()
                        values = []
                        for span_val in span_tag[1:]:
                            values.append(span_val.text.strip())

                        # if 'Managed By:' in key:
                        #     csv_dict['name'] = ''.join(values)
                        if 'Address:' in key:
                            address = ''.join(values)
                            csv_dict['address'] = address
                            post_code_m = re.search(r'(\d{4,})$', address.strip())
                            if post_code_m:
                                csv_dict['postcode'] = post_code_m.group(1)
                        if 'Phone:' in key:
                            phones = []
                            for v in values:
                                p = re.sub(r'[^\d]', '', v)
                                p = p.strip()
                                if p != '':
                                    phones.append(p)
                            csv_dict['phone'] = ', '.join(phones)
                        if 'Email:' in key:
                            csv_dict['email'] = ''.join(values)
                    else:
                        span_text = ' '.join([s.text.strip() for s in span_tag])
                        if '@' in span_text and 'email' not in csv_dict:
                            csv_dict['email'] = span_text.strip()
                        if re.search(r'[0-9 \+]{6,}', span_text):
                            phone = span_text.strip()
                            phone = re.sub('\n', ', ', phone)
                            csv_dict['phone'] = phone

                if len(csv_dict.keys()) == 0:
                    continue

                print(csv_dict)
                writer.writerow(csv_dict)

                i += 1
                print('Total: {}'.format(i))
