import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from urllib.error import HTTPError

from bs4 import BeautifulSoup
import time
import readline

readline.parse_and_bind("control-v: paste")


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


def isbn_strip(isbn):
    """Strip whitespace, hyphens, etc. from an ISBN number and return
the result."""
    short = re.sub("\W", "", isbn)
    return re.sub("\D", "X", short)


def convert(isbn):
    """Convert an ISBN-10 to ISBN-13 or vice-versa."""
    short = isbn_strip(isbn)
    if (isValid(short) == False):
        raise Exception("Invalid ISBN")
    if len(short) == 10:
        stem = "978" + short[:-1]
        return stem + check(stem)
    else:
        if short[:3] == "978":
            stem = short[3:-1]
            return stem + check(stem)
        else:
            raise Exception("ISBN not convertible")


def isValid(isbn):
    """Check the validity of an ISBN. Works for either ISBN-10 or ISBN-13."""
    short = isbn_strip(isbn)
    if len(short) == 10:
        return isI10(short)
    elif len(short) == 13:
        return isI13(short)
    else:
        return False


def check(stem):
    """Compute the check digit for the stem of an ISBN. Works with either
    the first 9 digits of an ISBN-10 or the first 12 digits of an ISBN-13."""
    short = isbn_strip(stem)
    if len(short) == 9:
        return checkI10(short)
    elif len(short) == 12:
        return checkI13(short)
    else:
        return False


def checkI10(stem):
    """Computes the ISBN-10 check digit based on the first 9 digits of a
stripped ISBN-10 number."""
    chars = list(stem)
    sum = 0
    digit = 10
    for char in chars:
        sum += digit * int(char)
        digit -= 1
    check = 11 - (sum % 11)
    if check == 10:
        return "X"
    elif check == 11:
        return "0"
    else:
        return str(check)


def isI10(isbn):
    """Checks the validity of an ISBN-10 number."""
    short = isbn_strip(isbn)
    if (len(short) != 10):
        return False
    chars = list(short)
    sum = 0
    digit = 10
    for char in chars:
        if (char == 'X' or char == 'x'):
            char = "10"
        sum += digit * int(char)
        digit -= 1
    remainder = sum % 11
    if remainder == 0:
        return True
    else:
        return False


def checkI13(stem):
    """Compute the ISBN-13 check digit based on the first 12 digits of a
    stripped ISBN-13 number. """
    chars = list(stem)
    sum = 0
    count = 0
    for char in chars:
        if (count % 2 == 0):
            sum += int(char)
        else:
            sum += 3 * int(char)
        count += 1
    check = 10 - (sum % 10)
    if check == 10:
        return "0"
    else:
        return str(check)


def isI13(isbn):
    """Checks the validity of an ISBN-13 number."""
    short = isbn_strip(isbn)
    if (len(short) != 13):
        return False
    chars = list(short)
    sum = 0
    count = 0
    for char in chars:
        if (count % 2 == 0):
            sum += int(char)
        else:
            sum += 3 * int(char)
        count += 1
    remainder = sum % 10
    if remainder == 0:
        return True
    else:
        return False


def toI10(isbn):
    """Converts supplied ISBN (either ISBN-10 or ISBN-13) to a stripped
ISBN-10."""
    if (isValid(isbn) == False):
        raise Exception("Invalid ISBN")
    if isI10(isbn):
        return isbn_strip(isbn)
    else:
        return convert(isbn)


def toI13(isbn):
    """Converts supplied ISBN (either ISBN-10 or ISBN-13) to a stripped
ISBN-13."""
    if (isValid(isbn) == False):
        raise Exception("Invalid ISBN")
    if isI13(isbn):
        return isbn_strip(isbn)
    else:
        return convert(isbn)


class AmazonPriceSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
    __amazon_url = 'https://www.amazon.de/gp/offer-listing/{}'
    __momox_url = 'https://www.momox.de/offer/{}'
    __momex_api_url = 'https://api.momox.de/api/latest/find/media/ean/{}/'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Upgrade-Insecure-Requests': '1'}

    __proxies = None
    __cache = []
    __total = 0
    MAX_RETRY = 50
    PROXY_FILE = 'proxy.json'

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('amazon_price_comparer.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__lock = Lock()
        self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(30)

        hdr = [('title', 'Title'),
               ('isbn', 'ISBN'),
               ('amazon_price', 'Amazon Price'),
               ('momex_price', 'Momex Price'),
               ('price_diff', 'Price Difference'),
               ('status', 'Status')]

        csv_header = OrderedDict(hdr)
        self.__field_names = list(csv_header.keys())

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__cache.append(row['isbn'])

        if os.path.exists(self.PROXY_FILE):
            with open(self.PROXY_FILE, 'r+', encoding='utf-8') as f:
                proxies = json.load(f)
                self.__proxies = proxies

        if csv_header['isbn'] not in self.__cache:
            self.__write_data(csv_header)

        self.__total = len(self.__cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def start_compare(self, filename):
        try:
            # self.process_price(['', '9789995943042'])
            # return
            # self.grab_amazon_data('3662556499')
            # self.grab_momox_data('3662556499')
            # return
            if os.path.exists(filename):
                with open(filename, 'r+', encoding='utf-8') as f:
                    reader = csv.reader(f)

                    rows = []
                    for row in reader:
                        if len(row) == 0:
                            continue
                        # if row[1] == '' or not re.match(r'^[0-9\-]+$', row[1]):
                        #     continue
                        #
                        # if row[1] in self.__cache:
                        #     self.__logger.warning('Already processed ISBN: {}.'.format(row[1]))
                        #     continue
                        if row[0] == '' or not re.match(r'^[0-9\-]+$', row[0]):
                            continue

                        if row[0] in self.__cache:
                            self.__logger.warning('Already processed ISBN: {}.'.format(row[0]))
                            continue

                        rows.append(row)
                        if len(rows) == 2048:
                            with ThreadPool(32) as p:
                                p.map(self.process_price, rows)
                            rows = []

                    if len(rows) > 0:
                        with ThreadPool(32) as p:
                            p.map(self.process_price, rows)
        except Exception as x:
            self.__logger.error('Error when process isbn from file. {}'.format(x))

    def process_price(self, row):
        try:
            # item = {'title': row[0],
            #         'isbn': row[1]}
            #
            # isbn10 = toI10(row[1])

            item = {'title': '',
                    'isbn': row[0]}

            isbn10 = toI10(row[0])

            # grab amazon price
            amazon_price = self.grab_amazon_data(isbn10)
            amazon_price = amazon_price if amazon_price else 0

            # grab momex price
            # if amazon price is 0 then skip momex price
            # If price not found on Amazon then no need to check Momox
            # If price is given in Amazon then and only then check for momox
            momox_price = self.grab_momox_data(isbn10) if amazon_price > 0 else 0
            momox_price = momox_price if momox_price else 0

            price_difference = amazon_price - momox_price
            status = 'False' if price_difference >= 0 else True

            item['amazon_price'] = amazon_price if amazon_price > 0 else ''
            item['momex_price'] = momox_price if momox_price > 0 else ''
            item['price_diff'] = price_difference if price_difference > 0 else ''
            item['status'] = status

            try:
                self.__logger.info('Data: {}'.format(item))
                self.__lock.acquire()
                self.__write_data(item)
                self.__cache.append(item['isbn'])
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

        except Exception as x:
            print(x)

    @tail_recursive
    def grab_amazon_data(self, isbn, retry=0):
        opener = None
        try:
            self.__logger.info('=== Amazon URL: {} ==='.format(self.__amazon_url.format(isbn)))
            opener = self.__create_opener()
            # opener.addheaders.append(('authority', 'www.amazon.de'))
            # opener.addheaders.append(('accept-language', 'en-US,en;q=0.9'))
            # opener.addheaders.append(('cache-control', 'max-age=0'))
            # opener.addheaders.append(('scheme', 'https'))
            # opener.addheaders.append(('method', 'GET'))
            # opener.addheaders.append(('path', '/gp/offer-listing/{}'.format(isbn)))
            response = opener.open(self.__amazon_url.format(isbn))

            if response.getcode() != 200:
                return

            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            del data

            offer_div = soup.find('div', id='olpOfferList')
            if not offer_div:
                return

            offer_price_div = offer_div.find('div', class_='a-column a-span2 olpPriceColumn')
            if not offer_price_div:
                return

            offer_price_span = offer_price_div.find('span',
                                                    class_='a-size-large a-color-price olpOfferPrice a-text-bold')
            if not offer_price_span:
                return

            offer_price = offer_price_span.text.strip()
            offer_price = re.sub(r'\,', '\.', offer_price)
            offer_price = re.sub(r'[^0-9\.]', '', offer_price)
            if not re.match(r'^[0-9][0-9\.]*$', offer_price):
                return
            offer_price = float(offer_price.strip('.'))

            shipping_price = 0
            shipping_price_p = offer_price_div.find('p', class_='olpShippingInfo')
            if shipping_price_p:
                shipping_price_text = shipping_price_p.text.strip()
                shipping_price_text = re.sub(r'\,', '\.', shipping_price_text)
                shipping_price_text = re.sub(r'[^0-9\.]', '', shipping_price_text)
                if re.match(r'^[0-9][0-9\.]*$', shipping_price_text):
                    shipping_price = float(shipping_price_text.strip('.'))

            offer_price += float(shipping_price)
            return offer_price
        except HTTPError as h:
            if h.code == 404:
                return
            elif h.code == 503:
                self.__logger.error('HTTP Error when grab amazon page.{}'.format(h))
                if retry < self.MAX_RETRY:
                    sleep_time = random.randint(5, 15)
                    self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                    time.sleep(sleep_time)
                    return self.grab_amazon_data(isbn, retry + 1)
        except Exception as x:
            self.__logger.error('Error when grab amazon page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_amazon_data(isbn, retry + 1)

        finally:
            if opener:
                del opener

    @tail_recursive
    def grab_momox_data(self, isbn, retry=0):
        try:
            self.__logger.info('=== Momex URL: {} ==='.format(self.__momox_url.format(isbn)))
            # time.sleep(random.randrange(1, 5))
            opener = self.__create_opener()
            data = opener.open(self.__momox_url.format(isbn)).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            client_key_m = re.search(r'root\.MX_WEBAPP\.clientKey \= \"(.*?)\"', data, re.MULTILINE)
            api_key = ''
            if client_key_m:
                api_key = client_key_m.group(1).strip()

            opener.addheaders.append(('x-api-token', api_key))
            opener.addheaders.append(('x-marketplace-id', 'momox_de'))

            api_url = self.__momex_api_url.format(isbn)
            self.__logger.info('Momex Api url: {}'.format(api_url))
            res = opener.open(api_url).read()
            try:
                res = gzip.decompress(res).decode('utf-8', 'ignore')
            except:
                res = res.decode('utf-8', 'ignore')

            json_data = json.loads(res)
            offer_price = json_data['price']
            offer_price = float(offer_price.strip('.'))
            return offer_price
        except Exception as x:
            self.__logger.error('Error when grab momex page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_momox_data(isbn, retry + 1)

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # input_file = 'ISBN_s11.csv'
    # output_file = input_file + '_out.csv'# 'price_comparer.csv'
    input_file = input('Input CSV: ')
    output_file = input('Output CSV: ')

    with AmazonPriceSpider(output_file) as spider:
        spider.start_compare(input_file)
