# -*- coding: utf-8 -*-
import csv
import math
import os
import random
import re
import socket
from queue import Queue
from threading import Thread

import requests
from lxml import html

socket.setdefaulttimeout(30)

concurrent = 100
global junkProxies
global proxies
global pageUrls

junkProxies = []
proxies = []
websiteUrl = 'https://www.justdial.com'

try:
    if os.path.exists('proxylist.csv'):
        with open('proxylist.csv', 'r+') as f:
            reader = csv.reader(f)
            for row in reader:
                proxies.append(row[0])
except Exception as x:
    print(x)

categories = []
categoryFile = input('Enter file that contains category urls: ')
try:
    f = open(categoryFile, 'r+')
    reader = csv.reader(f)
    for row in reader:
        categories.append(row[0])
finally:
    f.close()

myfile = open('Pages.csv', 'w+')
wrtr = csv.writer(myfile, delimiter=',', quotechar='"')
wrtr.writerow(['Category Url', 'Page Url'])


def GetProxy(proxies):
    proxy_url = random.choice(proxies)
    if not str(proxy_url).startswith('http://'):
        proxy_url = 'http://' + proxy_url
    return {
        "http": proxy_url,
        "https": proxy_url
    }


def doWork():
    while True:
        url = q.get()
        url, response = getResponseFromUrl(url)
        getDataFromPage(url, response)
        q.task_done()


def getResponseFromUrl(url):
    proxy = GetProxy(proxies)
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, sdch, br",
        "Accept-Language": "en-US,en;q=0.8",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
    }

    if len(junkProxies) > len(proxies) / 2:
        del junkProxies[:]

    response = None
    while response is None:
        try:
            while proxy in junkProxies:
                proxy = GetProxy(proxies)
            response = requests.get(url, timeout=30, headers=headers, proxies=proxy)
            if response != None:
                if response.status_code != 200:
                    response = None
        except Exception as e:
            junkProxies.append(proxy)

    print(response.url)
    return response.url, response.content


def getDataFromPage(url, content):
    try:
        tree = html.fromstring(content)
        totalRecords = tree.xpath('//li[@class="lstEmt "]//span[@class="lng_crcum"]/text()')[0]
        totalRecords = re.findall(r'\d+', totalRecords)
        totalRecords = int(totalRecords[0])

        totalPages = math.ceil(totalRecords / 10)

        if totalRecords > 500:
            totalPages = 50

        for x in range(1, totalPages + 1):
            pageUrl = url + '/' + 'page-' + str(x)
            wrtr.writerow([url, pageUrl])
    except Exception as e:
        pass
    myfile.flush()


print('Collecting Pages URL...\n')

q = Queue(concurrent)
for i in range(concurrent):
    t = Thread(target=doWork)
    t.daemon = True
    t.start()

for category in categories:
    q.put(category)

q.join()
myfile.close()

input('Done, Please Enter to continue...')
