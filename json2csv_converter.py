import csv
import json
from collections import OrderedDict


class Json2CsvConverter:

    __cache = []

    def __init__(self, input_json, output_csv):
        self.__input_json = input_json
        self.__output_csv = output_csv

    def __enter__(self):
        hdr = [('repo_name', 'Repository Name'),
               ('acc_name', 'Account Name'),
               ('repo_url', 'Repository URL'),
               ('desc', 'Description'),
               ('name', 'Name'),
               ('email', 'Email'),
               ('star_count', 'Star Count'),
               ('fork_count', 'Fork Count'),
               ('commit_id', 'Commit ID'),
               ('url', 'URL')]
        csv_header = OrderedDict(hdr)
        self.__field_names = list(csv_header.keys())
        self.__write_data(csv_header)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def convert(self):
        try:
            with open(self.__input_json) as f:
                json_data = json.load(f)
                for j_data in json_data:
                    # if not 'email' in j_data:
                    #     continue

                    # self.__write_data(j_data)
                    if not j_data['email'] in self.__cache:
                        self.__write_data(j_data)
                        if not j_data['email'] == '':
                            self.__cache.append(j_data['email'])
        except Exception as x:
            print('Error when converting: {}'.format(x))

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)
        except Exception as x:
            print('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    input_json = 'output_neural_network.json'
    output_csv = 'output_neural_network.json.csv'
    with Json2CsvConverter(input_json, output_csv) as converter:
        converter.convert()

    print('=== Finished ===')
