# -*- coding: utf-8 -*-
import csv
import os
import random
import sys
from queue import Queue
from threading import Thread

import requests
from lxml import html

concurrent = 200
global junkProxies
global proxies
global remainingCount

junkProxies = []
proxies = []
websiteUrl = 'https://www.justdial.com'

try:
    if os.path.exists('proxylist.csv'):
        with open('proxylist.csv', 'r+') as f:
            reader = csv.reader(f)
            for row in reader:
                proxies.append(row[0])
except Exception as x:
    print('Error reading proxy: {}'.format(x))

filename = input('Enter File name that contains links: ')

pages = set()
try:
    if os.path.exists(filename):
        with open(filename, 'r+') as f:
            next(f)
            reader = csv.reader(f)
            for row in reader:
                try:
                    pages.add(row[1])
                except Exception as e:
                    pass
except Exception as x:
    print('Error when read pages form csv file. {}'.format(x))

donePages = set()


try:
    if os.path.exists("Records.csv"):
        with open('Records.csv', 'r+') as f:
            next(f)
            reader = csv.reader(f)
            try:
                for row in reader:
                    donePages.add(row[0])
            except Exception as e:
                pass
except Exception as x:
    print('Error when read records. {}'.format(x))

remaining = pages - donePages
remaining = list(remaining)
remainingCount = len(remaining)

print('Total Pages: ' + str(len(pages)))
print('Done Pages: ' + str(len(donePages)))
print('Remaining Pages: ' + str(len(remaining)))

pages = set()
donePages = set()

print('Collecting Records URLs...\n')

if os.path.exists('Records.csv'):
    myfile = open('Records.csv', 'a+')
    wrtr = csv.writer(myfile, delimiter=',', quotechar='"')
else:
    myfile = open('Records.csv', 'w+')
    wrtr = csv.writer(myfile, delimiter=',', quotechar='"')
    headerRow = ['Page Url', 'Record Url']
    wrtr.writerow(headerRow)


def GetProxy(proxies):
    proxy_url = random.choice(proxies)

    if not str(proxy_url).startswith('http://'):
        proxy_url = 'http://' + proxy_url

    return {
        "http": proxy_url,
        "https": proxy_url
    }


def doWork():
    while True:
        url = q.get()
        url, response = getResponseFromUrl(url)
        getDataFromPage(url, response)
        q.task_done()


def getResponseFromUrl(url):
    global remainingCount
    proxy = GetProxy(proxies)
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, sdch, br",
        "Accept-Language": "en-US,en;q=0.8",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
    }

    if len(junkProxies) > (len(proxies) / 2):
        del junkProxies[:]

    response = None
    while response is None:
        try:
            while proxy in junkProxies:
                proxy = GetProxy(proxies)
            response = requests.get(url, timeout=30, headers=headers, proxies=proxy)
            if response != None:
                if response.status_code != 200:
                    response = None
        except Exception as e:
            junkProxies.append(proxy)

    sys.stdout.write("%d | %s \t\t\t\t\n" % (remainingCount, response.url))
    remainingCount -= 1
    sys.stdout.flush()
    return response.url, response.content


def getDataFromPage(url, content):
    try:
        tree = html.fromstring(content)
        try:
            for recordUrl in tree.xpath('//h2[@class="store-name"]//a/@href'):
                wrtr.writerow([url, recordUrl])
        except Exception as e:
            pass
        myfile.flush()
    except Exception as e:
        pass


q = Queue(concurrent)
for i in range(concurrent):
    t = Thread(target=doWork)
    t.daemon = True
    t.start()
try:
    for url in remaining:
        q.put(url)
    q.join()
except KeyboardInterrupt:
    sys.exit(1)

myfile.close()
input('Done, Please Enter to continue...')
