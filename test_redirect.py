import socket
import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

url = 'https://www.tripadvisor.com/Restaurant_Review-g60763-d4578404-Reviews-Los_Tacos_No_1-New_York_City_New_York.html'

user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
socket.setdefaulttimeout(60)
cap = webdriver.DesiredCapabilities.PHANTOMJS
cap["phantomjs.page.settings.javascriptEnabled"] = True
cap['phantomjs.page.settings.userAgent'] = user_agent
driver_path = './driver/phantomjs'

browser = webdriver.PhantomJS(driver_path, desired_capabilities=cap, service_args=['--ignore-ssl-errors=true'])
# browser = webdriver.Chrome('./driver/chromedriver')
browser.get(url)

print('Try to get search results...')
wait = WebDriverWait(browser, 60)
# wait.until(EC.url_changes(url))
wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
print('element ready...')

element = browser.find_element_by_xpath('//div[@class="blEntry website"]')
if element:
    element.click()
    print('clicked on element...')

    # Switch tab to the new tab, which we will assume is the next one on the right
    # browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.TAB)
    browser.switch_to_window(browser.window_handles[1])
    print(browser.current_url)


