# -*- coding: utf-8 -*-
import argparse
import csv
import glob
import logging
import os
from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__name__)


def setup_logger():
    logger.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    fh = RotatingFileHandler('cleaner.log', maxBytes=10 * 1024 * 1024, backupCount=5)
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)

    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)


class YellowPagesCleaner:
    __base_directory = './yp_clean_data'

    def __init__(self, input_csv):
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv

    def __enter__(self):
        if not os.path.exists(self.__base_directory):
            os.mkdir(self.__base_directory)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Finish!!!')
        del self.__input_csv
        del self

    def process_data(self):
        try:
            if os.path.exists(self.__input_csv):
                logger.info('Processing csv file: {}.'.format(self.__input_csv))
                with open(self.__input_csv, 'r+', encoding='utf-8') as f:
                    reader = csv.reader(f)
                    header = []
                    index = 0
                    for index, row in enumerate(reader):
                        if index == 0:
                            item = row.copy()
                            item.pop(26)
                            item.pop(20)
                            item.pop(18)
                            item.pop(17)
                            item.pop(16)
                            item.pop(15)
                            item.pop(14)
                            header = item.copy()
                            continue

                        item = row.copy()
                        item.pop(26)
                        item.pop(20)
                        item.pop(18)
                        item.pop(17)
                        item.pop(16)
                        item.pop(15)
                        item.pop(14)

                        if item[2] != '':
                            cat = item[2].split(',')[0]
                            item[2] = cat
                            output_csv = '{}/{}.csv'.format(self.__base_directory, cat.strip())
                            if not os.path.exists(output_csv):
                                self.__write_data(output_csv, header)

                            self.__write_data(output_csv, item)

                    logger.info('Total rows processed: [{}].'.format(index))
        except Exception as x:
            logger.error('Error when grab data. Error details: {}'.format(x))

    def __write_data(self, output_csv, row, mode='a+'):
        try:
            with open(output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f, quoting=csv.QUOTE_ALL)
                writer.writerow(row)
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required=True, help='Please specify "Input csv file"!')
    args = parser.parse_args()

    # directory = input('Please specify directory: ')
    setup_logger()
    directory = args.input
    for input_file in glob.glob('{}/*.csv'.format(directory)):
        # input_file = 'sample.csv'
        # input_file = args.input
        with YellowPagesCleaner(input_file) as spider:
            spider.process_data()
