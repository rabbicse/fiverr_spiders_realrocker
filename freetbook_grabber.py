import csv
import gzip
import html
import logging
import os
import random
import re
import readline
import socket
import time
import urllib.parse
import urllib.request

import datetime
from bs4 import BeautifulSoup

readline.parse_and_bind("control-v: paste")


class FreetbookGrabber:
    __login_page = 'https://www.freetobook.com/auth/login.php'
    __success_redirected_url = 'https://www.freetobook.com/diary/profile/index.php?p'
    # __start_url = 'https://www.freetobook.com/diary/bookings/reviews/index.php?published=NA'
    __start_url = 'https://www.freetobook.com/diary/bookings/reviews/index.php?published=A'
    __review_details_url = 'https://www.freetobook.com/diary/shared/bookings/panel/manage.php'

    __base_url = 'https://www.yellowpages.ca'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}

    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__setup_logger('freetbook.log')
        socket.setdefaulttimeout(60)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

        self.__min_date = datetime.datetime.strptime('01 Jan 2016', "%d %b %Y")

        self.__field_names = ['name', 'email', 'phone', 'address', 'url']
        csv_header = {'name': 'Name',
                      'email': 'Email',
                      'phone': 'Phone',
                      'address': 'Address',
                      'url': 'URL'}

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            # login
            if not self.__login():
                return
            self.__logger.info('Login SUCCESS!')

            # grab review page
            page_index = 0
            has_next = self.__grab_review_page(page_index)
            while has_next:
                self.__sleep_script()
                page_index += 20
                has_next = self.__grab_review_page(page_index)

        except Exception as x:
            print(x)

    def __login(self):
        try:
            self.__logger.info('Trying to login via: {}'.format(self.__login_page))
            post_data = {'username': 'amoore1',
                         'password': 'Newstart2014janandadam!',
                         'login': 'login'}
            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            req = urllib.request.Request(self.__login_page, headers=self.__headers, data=post_req)
            response = urllib.request.urlopen(req)
            redirected_url = response.geturl()
            return redirected_url == self.__success_redirected_url
        except Exception as x:
            self.__logger.error('Error when login page: {}'.format(x))

    def __grab_review_page(self, page_index):
        try:
            post_data = {'startIndex': page_index,
                         'generate': 'next>'}
            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            self.__logger.info('=== Reviews URL: {} ==='.format(self.__start_url))
            req = urllib.request.Request(self.__start_url, headers=self.__headers, data=post_req)
            response = urllib.request.urlopen(req)
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            review_list = soup.find_all('tr', class_='header')
            if not review_list:
                return

            for review_tag in review_list:
                td_list = review_tag.find_all('td', class_='text-field')
                if td_list and len(td_list) < 4:
                    continue

                review_time = td_list[0].text.strip()
                review = td_list[-1].text.strip()
                self.__logger.info('Review Date: {}; Review: {}'.format(review_time, review))
                # todo check review and review date
                try:
                    rvw = float(review)
                    if rvw < 4.8:
                        self.__logger.warning('Review not in range!')
                        continue
                except:
                    self.__logger.warning('Review not in range!')
                    continue

                try:
                    review_date = datetime.datetime.strptime(review_time, "%d %b %Y")
                    if review_date < self.__min_date:
                        self.__logger.warning('Review time not in range!')
                        continue
                except:
                    self.__logger.warning('Review time not in range!')
                    continue

                next_tr = review_tag.find_next_sibling('tr')
                if not next_tr:
                    continue

                review_table = next_tr.find('table', class_='reviewtable')
                if not review_table:
                    continue

                review_id_th = review_table.find('th', class_='right')
                if not review_id_th:
                    continue

                review_id_link_tag = review_id_th.find('a')
                if not review_id_link_tag or not review_id_link_tag.has_attr('onclick'):
                    continue

                review_id_text = review_id_link_tag.get('onclick')
                m = re.search(r'launchSummaryPopup\(\'([^\']*)\'\)', review_id_text)
                if m:
                    review_id = m.group(1).strip()
                    review_url = 'https://www.freetobook.com/diary/shared/bookings/panel/summary.php?bookId={}'.format(
                        review_id)
                    if review_url not in self.__url_cache:
                        self.__sleep_script(1, 5)
                        self.__grab_review_details(review_id, review_url)
                        self.__url_cache.append(review_url)
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(review_url))
            return True
        except Exception as x:
            self.__logger.error('Error when grab review page: {}'.format(x))

    def __grab_review_details(self, review_id, url):
        try:
            post_data = {'bookId': review_id}
            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            self.__logger.info('=== Review Details URL: {} ==='.format(self.__review_details_url))
            self.__logger.info('=== Post Data: {} ==='.format(post_data))

            req = urllib.request.Request(self.__review_details_url, headers=self.__headers, data=post_req)
            response = urllib.request.urlopen(req)
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            book_panels = soup.find_all('div', class_='bookPanelTitle')
            for book_panel in book_panels:
                if 'Lead Guest and Contact Details' in book_panel.text:
                    table = book_panel.find_next_sibling('table')
                    if not table:
                        return

                    tr_list = table.find_all('tr')
                    if not tr_list:
                        return

                    item = {'url': url}
                    for tr in tr_list:
                        td_list = tr.find_all('td')
                        if not td_list or len(td_list) < 2:
                            return

                        key = td_list[0].text.strip()
                        val = self.__cleanup(td_list[1].text)
                        if 'Name:' in key:
                            item['name'] = val
                        if 'Phone:' in key:
                            item['phone'] = val
                        if 'Email:' in key:
                            item['email'] = val.split(' ')[0] if val else ''
                        if 'Address:' in key:
                            item['address'] = val

                    self.__logger.info('Data: {}'.format(item))
                    self.__write_data(item)
                    return
        except Exception as x:
            self.__logger.error('Error when grab review details: {}'.format(x))

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))

    def __cleanup(self, data):
        try:
            result = html.unescape(data)
            result = urllib.parse.unquote_plus(result.encode('utf-8').decode('unicode-escape', 'ignore'))
            result = result.strip()
            return result
        except Exception as x:
            self.__logger.error('Error cleanup: {}'.format(x))
        return data

    def __sleep_script(self, min_sleep=5, max_sleep=15):
        sleep_time = random.randint(min_sleep, max_sleep)
        self.__logger.info(
            'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
        time.sleep(sleep_time)


if __name__ == '__main__':
    # keyword = input('Please specify Keyword: ')
    # location = input('Please specify Location: ')
    # output_file = input('Please specify output file name: ')

    # keyword = 'plumber'
    # location = 'toronto'
    output_file = 'freetbook_output_published.csv'
    with FreetbookGrabber(output_file) as spider:
        spider.grab_data()
