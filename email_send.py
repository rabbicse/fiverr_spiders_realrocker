import smtplib
import email.utils
from email import encoders
from email.header import Header
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class MailTest:
    def send_email(self):
        try:
            print('Sending email...')
            msg = MIMEMultipart()

            msg['From'] = email.utils.formataddr(('Support', 'support@chrisquantserver.com'))
            msg['To'] = email.utils.formataddr(('Alpesh Kalathiya', 'kalathiyaalpesh@gmail.com'),
                                               ('Mehedi', 'rabbi.se@gmail.com'))
            # [email.utils.formataddr(('Mehedi', 'rabbi.se@gmail.com')),

            msg['Subject'] = Header('Quantopian Status.', 'utf-8')

            body = 'Please check updated status.\nFYI'

            msg.attach(MIMEText(body, 'plain', 'utf-8'))

            # filename = self.__output_csv.split('/')[-1]
            # attachment = open(self.__output_csv, "rb")

            # part = MIMEBase('application', 'octet-stream')
            # part.set_payload((attachment).read())
            # encoders.encode_base64(part)
            # part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

            # msg.attach(part)

            # server = smtplib.SMTP('localhost', smtplib.SMTP_PORT)
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.set_debuglevel(True)
            server.ehlo()
            # server.starttls()
            # server.login(self.__from_address, self.__email_pass)
            # If we can encrypt this session, do it
            if server.has_extn('STARTTLS'):
                server.starttls()
                server.ehlo()  # re-identify ourselves over TLS connection
            text = msg.as_string()
            server.sendmail('support@chrisquantserver.com', ['kalathiyaalpesh@gmail.com', 'rabbi.se@gmail.com'], text)
            server.quit()
            print('Email send successfully.')
        except Exception as x:
            print(x)


mt = MailTest()
mt.send_email()
