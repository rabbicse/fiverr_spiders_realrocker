import csv
import gzip
import json
import os
import re
import socket
import urllib.parse
import urllib.request
from bs4 import BeautifulSoup
import readline
readline.parse_and_bind("control-v: paste")


class TripAdvisorSpider:
    __base_url = 'https://www.tripadvisor.com'
    __start_url = 'https://www.tripadvisor.com/Restaurants-g60763-New_York_City_New_York.html'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}

    def __init__(self, country_link, output_csv):
        self.__country_link = country_link
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __enter__(self):
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(60)

        csv_header = ['Name', 'Address', 'Country', 'Phone', 'Email', 'Website', 'URL', 'Image URL', 'Image File',
                      'Average Prices', 'Cuisine', 'Ratings', '# of reviews', 'Menu']
        self.__write_data(csv_header)

        if not os.path.exists('./images'):
            os.mkdir('./images')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data_by_country(self, country_link=None):
        try:
            # self.__grab_details('https://www.tripadvisor.com/Restaurant_Review-g60763-d598053-Reviews-Del_Posto-New_York_City_New_York.html')
            # return
            if not country_link:
                country_link = self.__country_link
            print('=== Country URL: {} ==='.format(country_link))
            req = urllib.request.Request(country_link, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            link_tags = soup.find_all('div', class_='geo_name')
            if link_tags and len(link_tags) > 0:
                for link_tag in link_tags:
                    link = link_tag.find('a')
                    if link:
                        url = self.__base_url + link.get('href')
                        self.grab_all_data(url)

            link_ul = soup.find('ul', class_='geoList')
            if link_ul:
                link_tags = link_ul.find_all('li')
                if link_tags and len(link_tags) > 0:
                    for link_tag in link_tags:
                        link = link_tag.find('a')
                        if link:
                            url = self.__base_url + link.get('href')
                            self.grab_all_data(url)

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

            next_page_tag = soup.find('a', class_='guiArw sprite-pageNext ')
            if next_page_tag:
                next_url = self.__base_url + next_page_tag.get('href')
                return self.grab_data_by_country(next_url)

        except Exception as x:
            print(x)

    def grab_all_data(self, url):
        next_page = self.grab_data(url)
        while next_page:
            next_page = self.grab_data(next_page)

    # def get_web_url(self, url):
    #     try:
    #         browser = RoboBrowser(user_agent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
    #         )
    #         browser.open(url)
    #         links = browser.select('div.ui_icon laptop')
    #         print(links)
    #         browser.follow_link(links[0])
    #         print(browser.url)
    #         # browser.follow_link("login")
    #     except Exception as x:
    #         print(x)

    def grab_data(self, url):
        try:
            print('=== Main URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            divs = soup.find_all('div', id=re.compile(r'^eatery_\d+$'))
            for div in divs:
                link_tag = div.find('a', class_='property_title')
                if link_tag:
                    link = self.__base_url + link_tag.get('href')
                    self.__grab_details(link)

            next_page_tag = soup.find('a', class_='nav next rndBtn ui_button primary taLnk')
            if next_page_tag:
                return self.__base_url + next_page_tag.get('href').strip()
        except Exception as x:
            print(x)

    def __grab_details(self, url):
        try:
            print('=== Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            name = ''
            address = ''
            country = ''
            phone = ''
            email = ''
            web = ''
            image_url = ''
            image_file = ''
            average_prices = ''
            cuisine = ''
            ratings = ''
            num_of_reviews = ''
            menu = ''

            name_tag = soup.find('h1', id='HEADING')
            if name_tag:
                name = name_tag.text.strip()

            address_tag = soup.find('span', class_='format_address')
            if address_tag:
                address = address_tag.text.strip()

            json_tag = soup.find('script', {'type': 'application/ld+json'})
            if json_tag:
                json_data = json.loads(json_tag.text)
                if 'address' in json_data and 'addressCountry' in json_data['address'] and 'name' in \
                        json_data['address']['addressCountry']:
                    country = json_data['address']['addressCountry']['name']

                if 'aggregateRating' in json_data:
                    if 'ratingValue' in json_data['aggregateRating']:
                        ratings = json_data['aggregateRating']['ratingValue']
                    if 'reviewCount' in json_data['aggregateRating']:
                        num_of_reviews = json_data['aggregateRating']['reviewCount']

            phone_tag = soup.find('div', class_='blEntry phone')
            if phone_tag:
                phone = phone_tag.text

            email_tag = soup.find('span', class_='ui_icon email')
            if email_tag:
                email_link_tag = email_tag.find_next_sibling('a')
                if email_link_tag:
                    email = email_link_tag.get('href')
                    email = email.replace('mailto:', '')
                    email = email.strip()

            details_tags = soup.find_all('div', class_='row')
            if details_tags and len(details_tags) > 0:
                for row in details_tags:
                    title = row.find('div', class_='title')
                    if not title:
                        continue
                    if 'Cuisine' in title.text:
                        contant_tag = row.find('div', class_='content')
                        if contant_tag:
                            cuisine = contant_tag.text.strip()

            menu_tab = soup.find('div', id='RESTAURANT_MENU')
            if menu_tab:
                menus = []
                menu_tags = menu_tab.find_all('div', class_=re.compile(r'(?:^menuItemTitle$)|(?:^menuItem$)'))
                for menu_tag in menu_tags:
                    menus.append(menu_tag.text.strip())
                menu = ', '.join(menus)

            print(menu)

            image_url_tag = soup.find_all('div', class_='prw_rup prw_common_centered_image photo')
            if image_url_tag:
                image_url_link = image_url_tag[-1].find('img', class_='centeredImg')
                if image_url_link:
                    image_url = image_url_link.get('src').strip()
                    image_file = image_url.split('/')[-1]

                    try:
                        urllib.request.urlretrieve(image_url, './images/{}'.format(image_file))
                    except Exception as ex:
                        print('Error when download image from {}.'.format(image_url))
                        print(ex)
            csv_data = [name, address, country, phone, email, web, url, image_url, image_file, average_prices, cuisine,
                        ratings, num_of_reviews, menu]
            print(csv_data)
            self.__write_data(csv_data)
        except Exception as x:
            print(x)

    __total = 0

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)

                print('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            print('Error printing csv output: ' + str(x))


if __name__ == '__main__':
    # country_link = input('Please specify URL: ')
    # output_file = input('Please specify output file name: ')
    country_link = 'https://www.tripadvisor.com/Restaurants-g294200-Egypt.html'
    output_file = 'trip_output.csv'
    with TripAdvisorSpider(country_link, output_file) as spider:
        spider.get_web_url('https://www.tripadvisor.com/Restaurant_Review-g294201-d2619464-Reviews-Fayruz_Lebanese_Restaurant-Cairo_Cairo_Governorate.html')
        # spider.grab_data_by_country()
