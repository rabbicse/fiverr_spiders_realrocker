import csv
import re
import string
import urllib.request
from bs4 import BeautifulSoup
import socket
import datetime
import xlsxwriter

socket.setdefaulttimeout(20)

base_url = 'http://www.prosoccer.gr/en/'
# 2016/05
# 2016-05-01
formatted_url = 'http://www.prosoccer.gr/en/{}/soccer-predictions-{}.html'

headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}

css_styles = {
    'ao10': '#EBF4F7',
    'pl25': '#F4F4E3',
    'pl30': '#E7E7C2',
    'pl35': '#E7E7C2',
    'pl45': '#C2E7C2',
    'pl55': '#A1DAA1',
    'pl65': '#A1DAA1',
    'pl75': '#73C873',
    'pr24': '#ffffcc',
    'pr25': '#F4F4E3',
    'pr30': '#E7E7C2',
    'pr35': '#E7E7C2',
    'pr45': '#C2E7C2',
    'pr55': '#A1DAA1',
    'pr65': '#A1DAA1',
    'pr75': '#73C873',
    'pli25': '#F4F4E3',
    'pli30': '#E7E7C2',
    'pli35': '#E7E7C2',
    'pli45': '#C2E7C2',
    'pli55': '#A1DAA1',
    'pli65': '#A1DAA1',
    'pli75': '#73C873',
    'BL': '#f98d7f',
    'LL': '#fdcac3',
    'DD': '#FBF951',
    'LW': '#E4FBD3',
    'SW': '#8DF371',
    'BL2': '#f98d7f',
    'LL2': '#fdcac3',
    'DD2': '#FBF951',
    'LW2': '#E4FBD3',
    'SW2': '#8DF371',
    'R2': '#FFFFff',
    'R5': '#DDEEDD',
    'FC2': '#FFFFff',
    'FC1': '#e8e8e8',
    'FC3': '#D9F19E',
    'FC4': '#D9F19E',
    'Fb2': '#A1DAA1',
    # 'f11w': '#C2E7C2',
    'Fa3': '#FFFFff',
    'f31': '#EBF4F7',
    'ao20': '#EBF4F7',
    'ao30': '#D7E9EF',
    'ao40': '#C9E2EA',
    'ao50': '#A8D0DD',
    'ao55': '#85BDCF',
    'ao60': '#85BDCF',
    'al10': '#EBF4F7',
    'al20': '#EBF4F7',
    'al30': '#D7E9EF',
    'al40': '#C9E2EA',
    'al50': '#A8D0DD',
    'al55': '#85BDCF',
    'al60': '#85BDCF',
    'ar10': '#EBF4F7',
    'ar20': '#EBF4F7',
    'ar30': '#D7E9EF',
    'ar40': '#C9E2EA',
    'ar50': '#A8D0DD',
    'ar55': '#85BDCF',
    'ar60': '#85BDCF',
    'po25': '#F4F4E3',
    'po30': '#E7E7C2',
    'po35': '#E7E7C2',
    'po45': '#C2E7C2',
    'po55': '#A1DAA1',
    'po65': '#A1DAA1',
    'po75': '#73C873',
    'fmr': '#F4F4E3',
    'pu4': '#E5E5CE',
    'pu3': '#CEE5CE',
    'pu2': '#ADD8AD',
    'pu1': '#7BC67B',
    'pu5': '#F4F4E3',
    'ou5': '#EBF4F7',
    'ou4': '#D0EAF2',
    'ou3': '#C3DBE2',
    'ou2': '#A8D0DD',
    'ou1': '#85BDCF'}


def generate_urls():
    start_date = '2016-05-01'
    current_date = datetime.datetime.strptime(start_date, '%Y-%m-%d')
    while current_date <= datetime.datetime.now():
        yy_mm = current_date.strftime('%Y/%m')
        yy_mm_dd = current_date.strftime('%Y-%m-%d')
        yield yy_mm_dd, formatted_url.format(yy_mm, yy_mm_dd)
        current_date = current_date + datetime.timedelta(days=1)


def grab_details(url):
    try:
        # url = 'http://www.prosoccer.gr/en/2017/08/soccer-predictions-2017-08-25.html'
        print('URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read().decode('utf-8')
        file_name = url.split('/')[-1]
        with open('./prosoccer_html/{}'.format(file_name), 'wb') as f:
            f.write(data.encode('utf-8'))
    except Exception as x:
        print(x)


def parse_details(d, url):
    global row

    file = url.split('/')[-1]
    file_name = './prosoccer_html/{}'.format(file)
    print(file_name)
    with open(file_name, 'rb') as f:
        data = f.read()
        soup = BeautifulSoup(data, 'lxml')
        table = soup.find('table', id='tblPredictions')
        if not table:
            return

        data_list = []
        tr_list = table.find_all('tr')
        for tr in tr_list:
            td_list = tr.find_all('td')
            if not td_list or len(td_list) == 0:
                continue

            # row += 1
            # col = 0
            # write_to_xlsx(row, col, d)
            # col += 1

            row_data = []
            utc, league = '', ''
            if re.match(r'^\d+\:\d+$', td_list[0].text.strip()):
                utc = td_list[0].text.strip()
                league = td_list[1].text.strip()
            else:
                utc = td_list[1].text.strip()
                league = td_list[0].text.strip()

            row_data.append((d, None))
            row_data.append((league, None))
            row_data.append((utc, None))

            # write_to_xlsx(row, col, league)
            # col += 1
            # write_to_xlsx(row, col, utc)
            # col += 1

            for td in td_list[2:]:
                data = td.text.strip()

                class_names = td.get('class')
                cell_format = None
                if class_names and len(class_names) > 0:
                    class_name = td.get('class')[0]

                    if class_name in css_styles.keys():
                        cell_format = workbook.add_format()
                        cell_format.set_bg_color(css_styles[class_name])
                        # cell_format = select_style(class_name)

                row_data.append((data, cell_format))

                # if cell_format:
                #     print('writing with cell format...')
                #     write_to_xlsx(row, col, data, cell_format)
                # else:
                #     print('writing without cell format...')
                #     write_to_xlsx(row, col, data)
                #
                # col += 1
            data_list.append(row_data)

        # data_list.sort(key=lambda d: datetime.datetime.strptime(d[2][0], '%H:%M'))
        # f = sorted(data_list, key=lambda x: (int(x[2][0].split(':')[0]) * 60) + int(x[2][0].split(':')[1]))
        # for f1 in f:
        #     print(f1)
        # return
        for r in sorted(data_list, key=lambda x: (int(x[2][0].split(':')[0]) * 60) + int(x[2][0].split(':')[1])):
            print(r)
            row += 1
            col = 0
            for c in r:
                write_to_xlsx(row, col, c[0], c[1])
                col += 1


workbook = None
worksheet = None
bg_format = None
row = 0


def create_workbook():
    global workbook
    global worksheet
    global bg_format

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('output_color_final.xlsx')
    worksheet = workbook.add_worksheet()

    bg_format = workbook.add_format()
    bg_format.set_bg_color('#ccddcc')
    bg_format.set_bold(True)


def write_to_xlsx(row, column, cell_data, cell_format=None):
    if cell_format:
        worksheet.write(row, column, cell_data, cell_format)
    else:
        worksheet.write(row, column, cell_data)


def process_spider():
    global row

    heading = ['Date', 'League', 'UTC', 'Match', 'Predict. Prob% 1', 'Predict. Prob% X', 'Predict. Prob% 2', 'Tips',
               'Avg. Odds 1', 'Avg. Odds X', 'Avg. Odds 2', 'Pred. Score 1', 'Pred. Score 2', 'Under 2.5 Goals',
               'Over 2.5 Goals', 'Final Score']

    create_workbook()
    for i in range(len(heading)):
        write_to_xlsx(row, i, heading[i], bg_format)

    # with open('output.csv', 'w', newline='', encoding='utf-8') as f:
    #     writer = csv.writer(f)
    #     writer.writerow()

    for d, url in generate_urls():
        # grab_details(url)
        parse_details(d, url)

    workbook.close()


if __name__ == '__main__':
    # write_to_xlsx()
    process_spider()
