import base64
import csv
import os
import re
import socket
import urllib
from lxml import html, etree
from requests import get
from threading import Semaphore, Thread
from time import sleep
from urllib import urlencode

import socks

ONE_ADDRESS = True
USE_PROXY = False
RESTAURANT = True

QUEUE_SEM = Semaphore(value=1)
P_SEM = Semaphore(value=1)
QUEUE = []
THREADS = 1  # 20
MAX_PAGES = 1

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
}

PROXIES = {
    'http': '83.149.70.159:13042',
    'https': '37.48.118.90:13042'
}

from io import open as iopen

socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket
socket.setdefaulttimeout(60)


def requests_image(file_url, file_name):
    if 'http' not in file_url:
        file_url = 'http:' + file_url
    file_name = file_name + '.jpg'
    if USE_PROXY:
        i = get(file_url, proxies=PROXIES, headers=HEADERS)
    else:
        i = get(file_url, headers=HEADERS)
    with iopen(create_path_in_script_directory(os.path.join('images', file_name)), 'wb') as file:
        file.write(i.content)


def get_reviews(x):
    x = x[-1].replace('(', '').replace(')', '').replace('No reviews', '').strip()
    try:
        return str(int(x.split()[0]))
    except:
        return ''


def get_bus(x):
    x = x[-1].replace('(', '').replace(')', '').replace('No reviews', '').strip()
    try:
        revs = str(int(x.split()[0]))
        return x.replace(x.split()[0], '')
    except:
        return x


def get_w(x):
    try:
        if 'maps' in x[0]:
            return ''
        if x[0][0] == '/':
            if USE_PROXY:
                return get('https://www.google.co.in' + x[0], headers=HEADERS, proxies=PROXIES).url
            else:
                return get('https://www.google.co.in' + x[0], headers=HEADERS).url
        else:
            return x[0]
    except:
        return ''


def get_loc_txt(url, s_i, e_i):
    t = urllib.unquote((url.replace('https://www.google.co.in', '').split('/')[4])).decode('utf8')
    return u''.join(t.split(',')[s_i:e_i]).replace(',', '')


def get_adr(x):
    if 'maps' not in x[0]:
        return ''
    else:
        url = x[0]
    t = urllib.unquote((url.replace('https://www.google.co.in', '').split('/')[4])).decode('utf8').replace(',', '')
    t = t.strip('+').replace('+', ' ').replace('  ', ' ')
    return t


def get_st(x):
    if 'maps' not in x[0]:
        return ''
    else:
        url = x[0]
    t = get_loc_txt(url, -2, -1).strip('+').split('+')
    return t[0]


def get_cty(x):
    if 'maps' not in x[0]:
        return ''
    else:
        url = x[0]
    t = get_loc_txt(url, -3, -2)
    return t.strip('+')


def get_str(x):
    if 'maps' not in x[0]:
        return ''
    else:
        url = x[0]
    t = get_loc_txt(url, 0, -3).replace('+', ' ')
    return t


def get_zp(x):
    if 'maps' not in x[0]:
        return ''
    else:
        url = x[0]
    t = get_loc_txt(url, -2, -1).strip('+').split('+')
    return t[1] if len(t) == 2 else ''


ADAPTERS = {
    'reviews': get_reviews,
    'address': get_adr,
    'street': get_str,
    'city': get_cty,
    'state': get_st,
    'zipcode': get_zp,
    'business ': get_bus,
    'website ': get_w,
}

XPATHS = {
    'name': '//div[@class="_rl"]//text()',
    'rating': '//span[@class="_PXi"]//text()',
    'reviews': '//span[@class="rllt__details lqhpac"]/div[1]/text()',
    'address': '//a[contains(.,"Directions")]/@href',
    'street': '//a[contains(.,"Directions")]/@href',
    'city': '//a[contains(.,"Directions")]/@href',
    'state': '//a[contains(.,"Directions")]/@href',
    'zipcode': '//a[contains(.,"Directions")]/@href',
    'business ': '//span[@class="rllt__details lqhpac"]/div[1]/text()',
    'website': '//a[contains(.,"Website")]/@href',
    'phone': '//span[@class="rllt__details lqhpac"]/div[4]//text()',
}

XPATHS_2 = {
    'website': '//div[@class="_ldf"]//a[contains(.,"Website")]/@href',
    'address ': '//span[@class="_Xbe"]//text()',
    'phone': '//span[@data-dtype="d3ph"]//text()',
    'menu': '//div[@class="_mr" and contains(.,"Menu")]/a[@class="fl"]/@href',
}


def get_params(res):
    params = {
        'ei': find_by_xpath(res.text, '//div[@id="rso"]/@eid')[0],
        'yv': '2',
        'vet': '',
        'lei': find_by_xpath(res.text, '//div[@id="rso"]/@eid')[0],
        'tbs': 'lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:10',
        'tbm': 'lcl',
        'async': 'ludocids:{},f:rlni,lqe:false,id:akp_uid_0,_id:akp_uid_0,_pms:s',
    }
    return params


def get_respages(_id, params):
    params['async'] = params['async'].format(_id)
    if USE_PROXY:
        j = get('https://www.google.co.in/async/lcl_akp', params=urlencode(params), headers=HEADERS,
                proxies=PROXIES).json()
    else:
        j = get('https://www.google.co.in/async/lcl_akp', params=urlencode(params), headers=HEADERS).json()

    entry = {}
    for k in XPATHS_2:
        try:
            entry[k] = adjust_text(find_by_xpath(j[1][1], XPATHS_2[k]), k)
        except Exception as e:
            print e
            print k
            entry[k] = ''
    return entry


def b_print(obj):
    P_SEM.acquire()
    print(obj)
    P_SEM.release()


def adjust_text(_list, k):
    if ADAPTERS.get(k, None):
        return ADAPTERS[k](_list).strip().encode('utf-8', errors='ignore')
    return ' '.join([item.strip() for item in _list]).strip().encode('utf-8', errors='ignore')


# Generates a path in the directory of the script
def create_path_in_script_directory(file_name):
    # Get the script file path
    script_file_path = os.path.realpath(__file__)

    # Gpthe directory path
    script_directory = os.path.dirname(script_file_path)

    # Make sure directory exists if nested
    directory = os.path.join(script_directory, os.path.dirname(file_name))
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Build the file path using the file name and the directory path
    file_path = os.path.join(script_directory, file_name)

    # return
    return file_path


# Find the element in an html text by xpath
def find_by_xpath(element_source, xpath_expression):
    if type(element_source) == type(html.fromstring('<a></a>')):
        element_source = etree.tostring(element_source)
    root = html.fromstring(element_source)
    return root.xpath(xpath_expression)


def get_page(i, keywords):
    url = 'https://www.google.co.in/search?q={}&start={}&tbm=lcl'.format(keywords.replace(' ', '+'), i * 20)
    print(url)
    if USE_PROXY:
        res = get(url, headers=HEADERS, proxies=PROXIES)
    else:
        res = get(url, headers=HEADERS)
    data = []
    # with open('tt.html','w')as f:
    #    f.write(res.text)
    # input()
    if 'unusual traffic from your computer' in res.text:
        b_print('{} , {} :Bad IP : {}'.format(keywords, i,
                                              find_by_xpath(res.text, '(//div[contains(.,"IP address")]//text())[10]')))
        raise Exception()
    for div in find_by_xpath(res.text, '//div[@class="_gt"]'):
        if RESTAURANT:
            _id = find_by_xpath(div, '//a[1]/@data-cid')[0]
            entry = get_respages(_id, get_params(res))
        else:
            entry = {}
        for k in XPATHS:
            try:
                if entry.get(k.strip(), False):
                    continue
                if ONE_ADDRESS:
                    if k in ['street', 'city', 'state', 'zipcode']:
                        continue
                if not ONE_ADDRESS:
                    if k == 'address':
                        continue
                entry[k] = adjust_text(find_by_xpath(div, XPATHS[k]), k)
            except Exception as e:
                entry[k] = ''
        entry['page'] = str(i + 1)
        if RESTAURANT:
            try:
                image_tag = div.xpath('.//img[@class="_VQf"]/@id')
                if image_tag:
                    im_id = image_tag[0]
                    m = re.search(
                        r'_image_ids=\[\'%s\'\];_setImagesSrc\(_image_ids,_image_src\);\}\)\(\);\(function\(\)\{var _image_src=\'([^\']*?)\'' % (
                            im_id),
                        res.text)
                    if m:
                        img = m.group(1)
                        print(img)
                        img_ext_m = re.search(r'data\:image\/([a-zA-Z0-9]+)\;base64\,', img)
                        if img_ext_m:
                            ext = img_ext_m.group(1)
                            img = re.sub(r'data\:image\/[a-zA-Z0-9]+\;base64\,', '', img)
                            print('IMG: ' + img)
                            imgdata = base64.b64decode(img)
                            im_name = entry['name'] + '.' + ext
                            with open('images/' + im_name, 'wb') as f:
                                f.write(imgdata)
                                entry['image_file'] = im_name
                                # requests_image(m.group(1), entry['name'])
                                # requests_image(find_by_xpath(div, './/img[@class="__VQf"]/@src')[0], entry['name'])
            except Exception as e:
                print e
        data.append(entry)

    nxt = len(find_by_xpath(res.text, '//span[text()="Next"]')) > 0
    b_print(keywords + ' , ' + str(nxt))
    return data, nxt


def get_kw():
    while len(QUEUE) > 0:
        QUEUE_SEM.acquire()
        keywords = QUEUE.pop()
        QUEUE_SEM.release()
        try:
            i = 0
            data = []
            nxt = True
            while nxt and i < MAX_PAGES:
                try:
                    b_print('[KEYWORDS] : {} , [PAGE] : {}'.format(keywords, i))
                    sleep(3)
                    n_data, nxt = get_page(i, keywords)
                    data += n_data
                    i += 1
                except  Exception as e:
                    b_print(e)

            rows = [[e[k] for k in e] for e in data]
            rows.insert(0, [k for k in data[0]])
            save(rows, 'output_{}.csv'.format(keywords.replace(' ', '_')))

        except Exception as e:
            b_print(e)
            QUEUE_SEM.acquire()
            QUEUE.append(keywords)
            QUEUE_SEM.release()


def save(rows, path):
    with open(create_path_in_script_directory(path), 'wb') as f:
        csv.writer(f).writerows(rows)


def main():
    with open(create_path_in_script_directory('input.csv')) as f:
        kws = [x.replace('\n', '').strip() for x in f.readlines() if x.replace('\n', '').strip()]

    for x in kws:
        QUEUE.append(x)

    ths = []
    for i in range(THREADS):
        ths.append(Thread(target=get_kw))

    for th in ths:
        th.start()

    for th in ths:
        th.join()


if __name__ == '__main__':
    main()
