#!/bin/sh
SERVICE='tripadvisor_crawler_updated_trip.py'

if ps ax | grep -v grep | grep $SERVICE > /dev/null
then
    echo "$SERVICE service running, everything is fine"
else
    echo "$SERVICE is not running"
    cd /root/spiders/
    /usr/bin/xvfb-run --server-args='-screen 0 1920x1080x24' /usr/bin/python3 tripadvisor_crawler_updated_trip.py
fi
