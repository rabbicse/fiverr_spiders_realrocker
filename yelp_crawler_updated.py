import csv
import gzip
import json
import logging
import os
import random
# import readline
import socket
import string
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool

# import socks
from bs4 import BeautifulSoup

# readline.parse_and_bind("control-v: paste")


class YelpCrawlerUpdated:
    __base_url = 'https://www.yelp.com'
    __search_url = 'https://www.yelp.com/search?find_desc={}&find_loc={}&ns=1'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __proxies = []
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('yelp.log')
        socket.setdefaulttimeout(30)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        # proxy_handler = urllib.request.ProxyHandler(self.__proxy_dict)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # opener = urllib.request.build_opener(proxy_handler)
        # urllib.request.install_opener(opener)
        # self.__field_names = ['url', 'name', 'address', 'city', 'zip_code', 'website', 'phone_number', 'working_hours',
        #                       'latitude', 'longitude', 'category', 'rating', 'reviews_count']

        hdr = [('name', 'Restraunt Name'),
               ('address', 'Address'),
               ('city', 'City'),
               ('zip_code', 'Zip Code'),
               ('country', 'Country'),
               ('website', 'Website'),
               ('phone_number', 'Phone Number'),
               ('working_hours', 'Working Hours'),
               # ('latitude', 'Latitude'),
               # ('longitude', 'Longitude'),
               ('category', 'Category'),
               ('rating', 'Yelp Rating'),
               # ('reviews_count', 'Reviews Count'),
               ('liked_by_vegans', 'Liked by Vegans'),
               ('takes_reservations', 'Takes Reservations'),
               ('delivery', 'Delivery'),
               ('take-out', 'Take-out'),
               ('accepts_apple_pay', 'Accepts Apple Pay'),
               ('accepted_cards', 'Accepted Cards'),
               ('parking', 'Parking'),
               ('bike_parking', 'Bike Parking'),
               ('wheelchair_accessible', 'Wheelchair Accessible'),
               ('good_for_kids', 'Good for Kids'),
               ('good_for_groups', 'Good for Groups'),
               ('attire', 'Attire'),
               ('ambience', 'Ambience'),
               ('noise_level', 'Noise Level'),
               ('good_for_dancing', 'Good For Dancing'),
               ('alcohol', 'Alcohol'),
               ('happy_hour', 'Happy Hour'),
               ('best_nights', 'Best Nights'),
               ('coat_check', 'Coat Check'),
               ('smoking', 'Smoking'),
               ('outdoor_seating', 'Outdoor Seating'),
               ('wi-fi', 'Wi-Fi'),
               ('has_tv', 'Has TV'),
               ('dogs_allowed', 'Dogs Allowed'),
               ('waiter_service', 'Waiter Service'),
               ('caters', 'Caters'),
               ('has_pool_table', 'Has Pool Table'),
               ('url', 'URL')]

        csv_header = OrderedDict(hdr)

        self.__field_names = list(csv_header.keys())

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)

        self.__load_proxies()

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __load_proxies(self):
        try:
            if os.path.exists('proxies.json'):
                with open('proxies.json') as inFile:
                    self.__proxies = json.load(inFile)
        except Exception as x:
            self.__logger.error('Error when loading proxies.{}'.format(x))

    def __get_random_proxy(self):
        try:
            # return
            # return {'https': 'https://47.206.51.67:8080'}

            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy_url = str(random.choice(self.__proxies))

            if not proxy_url.startswith('http://'):
                proxy_url = 'http://' + proxy_url

            return {
                "http": proxy_url,
                "https": proxy_url
            }
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener

        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def grab_data(self):
        try:
            # self.__grab_details(
            #     'https://www.yelp.com/biz/snakes-and-lattes-college-toronto-4?osq=snakes+and+lattes+college')
            # return

            with open('categories.txt', 'r+') as rcf:
                for category_line in rcf.readlines():
                    category = category_line.strip()

                    with open('locations.txt', 'r+') as rlf:
                        for loc_line in rlf.readlines():
                            location = loc_line.strip()
                            self.__logger.info('==== Category: {}; Location: {} ==='.format(category, location))
                            search_url = self.__search_url.format(category, location)
                            next_page = self.__search_data(search_url)
                            while next_page:
                                next_page = self.__search_data(next_page)
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def grab_data_by_search(self, url):
        try:
            next_page = self.__search_data(url)
            while next_page:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds to avoid block.'.format(sleep_time))
                time.sleep(sleep_time)
                next_page = self.__search_data(next_page)
        except Exception as x:
            print(x)

    def __search_data(self, url, retry=0):
        try:
            self.__logger.info('=== Search URL: {} ==='.format(url))

            opener = self.__create_opener()
            data = opener.open(url).read()

            # data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            item_tags = soup.find_all('li', class_='regular-search-result')
            if item_tags:
                # url_list = []
                for item_tag in item_tags:
                    details_tag = item_tag.find('a', class_='biz-name js-analytics-click')
                    if not details_tag:
                        continue

                    details_url = self.__base_url + details_tag.get('href')
                    if details_url not in self.__url_cache:
                        if not self.__grab_details(details_url):
                            self.__logger.warning('Failed to grab details...')
                        # url_list.append(details_url)
                        sleep_time = random.randint(5, 15)
                        self.__logger.info(
                            'Script is going to sleep for {} seconds to avoid block.'.format(sleep_time))
                        time.sleep(sleep_time)
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(details_url))

                # with ThreadPool(16) as p:
                #     p.map(self.__grab_details, url_list)

            next_link_tag = soup.find('a', class_='u-decoration-none next pagination-links_anchor')
            if next_link_tag:
                next_link = next_link_tag.get('href')
                return self.__base_url + next_link
        except Exception as x:
            self.__logger.error('Error grab search by category and location page: {}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__search_data(url, retry + 1)

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            item = {'url': url}
            name_tag_top_shelf = soup.find('div', {'class': 'top-shelf'})
            if name_tag_top_shelf:
                name_tag = name_tag_top_shelf.find('meta', {'itemprop': 'name'})
                if name_tag and name_tag.has_attr('content'):
                    item['name'] = name_tag.get('content').strip()

            if 'name' not in item:
                name_tag = soup.find('h1', {'class': 'biz-page-title embossed-text-white shortenough'})
                if name_tag:
                    item['name'] = name_tag.text.strip()

            if 'name' not in item:
                name_tag = soup.find('h1', {'class': 'biz-page-title embossed-text-white'})
                if name_tag:
                    item['name'] = name_tag.text.strip(string.whitespace)

            street_address_tag = soup.find('span', {'itemprop': 'streetAddress'})
            if street_address_tag:
                item['address'] = street_address_tag.text.strip()

            city_tag = soup.find('span', {'itemprop': 'addressLocality'})
            if city_tag:
                item['city'] = city_tag.text.strip()

            post_code_tag = soup.find('span', {'itemprop': 'postalCode'})
            if post_code_tag:
                item['zip_code'] = post_code_tag.text.strip()

            short_def_list_tag = soup.find('div', class_='short-def-list')
            if short_def_list_tag:
                dl_list = short_def_list_tag.find_all('dl')
                for dl in dl_list:

                    dt = dl.find('dt')
                    if not dt:
                        continue

                    dd = dl.find('dd')
                    if not dd:
                        continue

                    dt_text = dt.text.strip()
                    dd_text = dd.text.strip()

                    # print("""'{}': '{}',""".format(dt_text.lower().replace(' ', '_'), dt_text))

                    #                 txt = """if '{}' in dt_text:
                    # item['{}'] = dd_text""".format(dt_text, dt_text.lower().replace(' ', '_'))
                    #                 print(txt)

                    if 'Liked by Vegans' in dt_text:
                        item['liked_by_vegans'] = dd_text
                    if 'Takes Reservations' in dt_text:
                        item['takes_reservations'] = dd_text
                    if 'Delivery' in dt_text:
                        item['delivery'] = dd_text
                    if 'Take-out' in dt_text:
                        item['take-out'] = dd_text
                    if 'Accepts Apple Pay' in dt_text:
                        item['accepts_apple_pay'] = dd_text
                    if 'Accepted Cards' in dt_text:
                        item['accepted_cards'] = dd_text
                    if 'Parking' in dt_text:
                        item['parking'] = dd_text
                    if 'Bike Parking' in dt_text:
                        item['bike_parking'] = dd_text
                    if 'Wheelchair Accessible' in dt_text:
                        item['wheelchair_accessible'] = dd_text
                    if 'Good for Kids' in dt_text:
                        item['good_for_kids'] = dd_text
                    if 'Good for Groups' in dt_text:
                        item['good_for_groups'] = dd_text
                    if 'Attire' in dt_text:
                        item['attire'] = dd_text
                    if 'Ambience' in dt_text:
                        item['ambience'] = dd_text
                    if 'Noise Level' in dt_text:
                        item['noise_level'] = dd_text
                    if 'Good For Dancing' in dt_text:
                        item['good_for_dancing'] = dd_text
                    if 'Alcohol' in dt_text:
                        item['alcohol'] = dd_text
                    if 'Happy Hour' in dt_text:
                        item['happy_hour'] = dd_text
                    if 'Best Nights' in dt_text:
                        item['best_nights'] = dd_text
                    if 'Coat Check' in dt_text:
                        item['coat_check'] = dd_text
                    if 'Smoking' in dt_text:
                        item['smoking'] = dd_text
                    if 'Outdoor Seating' in dt_text:
                        item['outdoor_seating'] = dd_text
                    if 'Wi-Fi' in dt_text:
                        item['wi-fi'] = dd_text
                    if 'Has TV' in dt_text:
                        item['has_tv'] = dd_text
                    if 'Dogs Allowed' in dt_text:
                        item['dogs_allowed'] = dd_text
                    if 'Waiter Service' in dt_text:
                        item['waiter_service'] = dd_text
                    if 'Caters' in dt_text:
                        item['caters'] = dd_text
                    if 'Has Pool Table' in dt_text:
                        item['has_pool_table'] = dd_text

                        # print(dl.text.strip())

            country_tag = soup.find('address', {'itemprop': 'address'})
            if country_tag:
                country = country_tag.text
                if 'city' in item:
                    country = country.replace(item['city'], '')
                if 'zip_code' in item:
                    country = country.replace(item['zip_code'], '')
                country = country.strip()
                item['country'] = country

            telephone_tag = soup.find('span', {'class': 'biz-phone'})
            if telephone_tag:
                item['phone_number'] = telephone_tag.text.strip()

            try:
                web_url = soup.find('span', {'class': 'biz-website js-biz-website js-add-url-tagging'}).find('a').get(
                    'href')
                o = urllib.parse.urlparse(web_url)
                query = urllib.parse.parse_qs(o.query)
                website = query.get('url')[0]
                item['website'] = website
            except Exception as e:
                website = ''

            try:
                working_hours = soup.find('table', {'class': 'table table-simple hours-table'}).text.replace(
                    '\n', '')
                item['working_hours'] = working_hours.strip()
            except Exception as e:
                workingHours = ''

            categories = []
            try:
                for category in soup.find('span', {'class': 'category-str-list'}).find_all('a'):
                    categoryObj = {}
                    categoryObj['title'] = category.text
                    categoryObj['url'] = category.get('href')
                    categories.append(categoryObj)
            except Exception as e:
                pass

            try:
                forPrintCategories = []
                for cat in categories:
                    forPrintCategories.append(cat.get('title'))
                category = ', '.join(forPrintCategories)
                item['category'] = category
            except Exception as e:
                category = ''

            # try:
            #     coordinatesDiv = json.loads(
            #         str(soup.find('div', {'class': 'lightbox-map hidden'}).get('data-map-state')))
            #     latitude = coordinatesDiv.get('center').get('latitude')
            #     item['latitude'] = latitude
            #     longitude = coordinatesDiv.get('center').get('longitude')
            #     item['longitude'] = longitude
            # except Exception as e:
            #     latitude = ''
            #     longitude = ''

            # try:
            #     ratingDiv = soup.find('div', {'class': 'rating-info clearfix'})
            #     reviewsCount = str(ratingDiv.find('span', {'class': 'review-count'}).text).strip()
            #     item['reviews_count'] = reviewsCount
            # except Exception as e:
            #     reviewsCount = ''

            try:
                ratingDiv = soup.find('div', {'class': 'rating-info clearfix'})
                ratings = ratingDiv.find('div', {'class': 'i-stars'})
                ratings = ratings.get('title').replace('star rating', '')
                item['rating'] = ratings
            except Exception as identifier:
                ratings = ''

            if len(item.keys()) < 3 and retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

            return True

        except Exception as x:
            self.__logger.error('Error grab details page: {}'.format(x))
            if retry < 5:
                sleep_time = random.randint(5, 15)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)
            else:
                return False

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    output_file = 'yelp_output_updated_04_11.csv'
    with YelpCrawlerUpdated(output_file) as spider:
        spider.grab_data_by_search('https://www.yelp.com/search?find_desc=Restaurants&find_loc=toronto&ns=1')
        # spider.grab_data()
        # input('Press any key to exit...')
