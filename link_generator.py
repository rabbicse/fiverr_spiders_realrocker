import csv
import logging
import os
from collections import OrderedDict
from multiprocessing import Semaphore, Value
from multiprocessing.pool import ThreadPool

import requests
from easy_spider.utils import log

logger = logging.getLogger(__name__)


class LinkGenerator:
    """
    nohup python3.6 url_generator.py >/dev/null 2>&1 &
    """
    # &t=[1-100]
    __proxy_uri = 'http://api.scraperapi.com'
    __proxy_key = 'a075e0ac04ccbe07119123932dba1e47'
    __api_url = 'https://api3.geo.admin.ch/rest/services/all/MapServer/identify?geometry={},{}&geometryFormat=geojson&geometryType=esriGeometryPoint&imageDisplay=1536,442,96&lang=de&layers=all:ch.bakom.anbieter-eigenes_festnetz&limit=10&mapExtent=2450000,1050000,2900000,1350000&returnGeometry=true&sr=2056&tolerance=0'
    __url_cache = []
    __total = Value('i', 0)
    __lock = Semaphore()
    __MAX_THREAD = 16
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
    HEADERS = {
        'User-Agent': __USER_AGENT,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'
    }

    def __init__(self, input_csv, output_csv):
        log.setup_stream_logger()
        log.setup_rotating_file_logger('link_generator')
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv
        self.MAX_RETRY = 20
        self.TIMEOUT = 30

    def __enter__(self):
        self.__init_full()
        return self

    def __init_full(self):
        hdr = [('Provider_Code', 'provider_code'),
               ('X_EPSG2056', 'X_EPSG2056'),
               ('Y_EPSG2056', 'Y_EPSG2056'),
               ('response', 'Response'),
               ('url', 'URL')]
        # ('id_per_zip', '#id per ZIP')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names, dialect='excel', delimiter=';')
                for row in reader:
                    self.__url_cache.append(row['url'])

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Total records: {}'.format(self.__total.value))
        logger.info('Finish!!!')
        del self

    def process_data(self):
        try:
            if os.path.exists(self.__input_csv):
                search_requests = []
                with open(self.__input_csv, 'r+') as f:
                    reader = csv.reader(f, quoting=csv.QUOTE_ALL, delimiter=';')
                    i = 0
                    for row in reader:
                        if i == 0 or len(row) < 3:
                            i += 1
                            continue

                        uri = self.__api_url.format(row[1], row[2])
                        if uri in self.__url_cache:
                            logger.warning('ID: {} already exists!'.format(uri))
                            continue

                        req = {'Provider_Code': row[0],
                               'X_EPSG2056': row[1],
                               'Y_EPSG2056': row[2],
                               'url': uri}
                        search_requests.append(req)
                        i += 1

                        if len(search_requests) % 512 == 0:
                            with ThreadPool(self.__MAX_THREAD) as pool:
                                pool.map(self.__process_links, search_requests)
                                search_requests = []

                with ThreadPool(self.__MAX_THREAD) as pool:
                    pool.map(self.__process_links, search_requests)
        except Exception as x:
            logger.error('Error when process data: {}'.format(x))

    def __process_links(self, req):
        try:
            logger.info('Request: {}'.format(req))
            payload = {'api_key': self.__proxy_key,
                       'url': req['url'],
                       'keep_headers': 'true'}

            logger.info('URI: {}'.format(req['url']))
            response = requests.get(self.__proxy_uri, params=payload, headers=self.HEADERS)
            if not response:
                return

            item = dict(req).copy()
            item['response'] = response.text
            self.__write_item(item)
        except Exception as x:
            logger.error('Error when parse details page: {}'.format(x))

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL, dialect='excel',
                                        delimiter=';')
                writer.writerow(row)

                with self.__total.get_lock():
                    self.__total.value += 1
                    logger.info('Total: {}'.format(self.__total.value))
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # input_csv = 'link_input.csv'  # input('Please specify Input csv: ')
    # output_csv = 'link_output.csv'  # input('Please specify output csv: ')

    # mode = input('Please specify Mode (1 for save all urls; 0 for parse details from previously saved pages): ')
    input_csv = input('Please specify Input csv: ')
    output_csv = input('Please specify output csv: ')
    with LinkGenerator(input_csv, output_csv) as crawler:
        crawler.process_data()
