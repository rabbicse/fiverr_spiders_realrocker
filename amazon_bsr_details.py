import csv
import random
import re
import socket
import urllib.request
import sys
import gzip
import datetime
import socks
from bs4 import BeautifulSoup
import time

socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket

socket.setdefaulttimeout(60)
headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
    'Host': 'www.amazon.co.uk',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1'}

base_url = 'https://www.amazon.co.uk'

search_no = 0
file_name = ''


def grab_details(url, args, price, retry=0):
    try:
        global file_name
        global search_no
        print('Product Details URL: ' + url)

        # proxies = {
        #     'http': '83.149.70.159:13042'
        # }

        # proxy = {'http', '37.48.118.90:13042'}
        # FancyURLopener(proxies=proxies) #



        # proxy_handler = urllib.request.ProxyHandler(proxies)
        # opener = urllib.request.build_opener(proxy_handler)
        # # # ...and install it globally so it can be used with urlopen.
        # urllib.request.install_opener(opener)

        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

        req = urllib.request.Request(url, headers=headers)
        # req.set_proxy('127.0.0.1:9050', 'socks5')
        # # req.set_proxy('http', 'socks5://127.0.0.1:9150')
        data = urllib.request.urlopen(req).read()
        data = gzip.decompress(data).decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        # print(soup.prettify())

        # 0 datetime
        # 1 serial
        # 2-6 no. of category= 5
        # 7 price
        # 8 rank
        # 9 weight
        # 10 title
        # 11 ASIN
        # 12 Review count
        # 13 User
        date = datetime.datetime.now().strftime('%y-%m-%d')
        csv_row = [date, '', '', '', '', '', '', price, '', '', '', '', '', 'Realrocker', url]

        if args and len(args) > 0:
            index = 2
            for ar in args:
                csv_row[index] = ar
                index += 1

        title_h1 = soup.find('h1', id='title')
        if title_h1:
            title = title_h1.text.strip()
            csv_row[10] = title

        # user_data = soup.find('a', id='brand')
        # if user_data:
        #     user = user_data.text.strip()
        #     csv_row[13] = user

        bsr = -1
        bsr_tr = soup.find('tr', id='SalesRank')
        if bsr_tr:
            bsr_m = re.search(r'(?i)([\d\,]+)\s*?in\s*?.*?\(.*?See top', bsr_tr.text.strip())
            if bsr_m:
                bsr = int(bsr_m.group(1).replace(',', ''))

        bsr_tr1 = soup.find('li', id='SalesRank')
        if bsr_tr1 and bsr == -1:
            bsr_m = re.search(r'(?i)([\d\,]+)\s*?in\s*?.*?\(.*?See top', bsr_tr1.text.strip())
            if bsr_m:
                bsr = int(bsr_m.group(1).replace(',', ''))

        if bsr == -1 and retry < 2:
            # 1-5 sec random sleep
            time.sleep(random.randint(5, 10))
            return grab_details(url, args, price, retry + 1)

        if not 1 <= bsr <= 4000:
            print('BSR: ' + str(bsr))
            print('BSR not in range of 1-4000', file=sys.stderr)
            csv_row[8] = str(bsr)
        else:
            csv_row[8] = bsr

        weight = 0
        div = soup.find('div', class_='wrapper GBlocale')
        table = soup.find('table', id='productDetailsTable')
        if div:
            divs = div.find_all('div', class_='pdTab')
            if divs:
                for d in divs:
                    trs = d.find_all('tr')
                    for tr in trs:
                        tds = tr.find_all('td')
                        if 'ASIN' in tds[0].text:
                            asin = tds[1].text.strip()
                            csv_row[11] = asin
                        if 'Shipping weight' in tds[0].text.strip() and weight == 0:
                            weight_m = re.search(r'(?i)(\d+)', tds[1].text.strip())
                            if weight_m:
                                weight = float(weight_m.group(1))
                        if 'Item Weight' in tds[0].text.strip():
                            weight_m = re.search(r'(?i)(\d+)', tds[1].text.strip())
                            if weight_m:
                                weight = float(weight_m.group(1))
                        if 'Customer Reviews' in tds[0].text:
                            review_m = re.search(r'(?i)(\d+)\s*?customer\s*?review.*?', tds[1].text.strip(),
                                                 re.MULTILINE)
                            if review_m:
                                review = review_m.group(1)
                                csv_row[12] = review
        elif table:
            li_list = table.find_all('li')
            if li_list:
                for li in li_list:
                    if 'ASIN:' in li.text:
                        asin = li.text.replace('ASIN:', '').strip()
                        csv_row[11] = asin
                    if 'Boxed-product Weight:' in li.text:
                        weight_m = re.search(r'(?i)(\d+)', li.text.strip())
                        if weight_m:
                            weight = float(weight_m.group(1))
                    if 'Average Customer Review:' in li.text:
                        review_m = re.search(r'(?i)(\d+)\s*?customer\s*?review.*?', li.text.strip(),
                                             re.MULTILINE)
                        if review_m:
                            review = review_m.group(1)
                            csv_row[12] = review

        if weight >= 2000:
            print('Weight: ' + str(weight))
            print('Weight exceeds max.', file=sys.stderr)
            csv_row[9] = str(weight)
        else:
            csv_row[9] = weight

        search_no += 1
        csv_row[1] = search_no
        print(csv_row)
        write_data(file_name, csv_row)

    except Exception as x:
        print(x)


def process_amazon_bsr():
    global file_name
    # root = Tree('Root')
    # global tree

    # grab_listing_urls('https://www.amazon.co.uk/Best-Sellers-Amazon-Launchpad-Baby/zgbs/boost/7424492031/ref=zg_bs_nav_2_7424489031/259-4367490-9323446')
    # return

    csv_header = ['Date (yyyy-mm-dd)', 'Search No.', 'Top Level Category', 'Subcategory1', 'Subcategory2',
                  'Subcategory3', 'Subcategory4', 'Price_£', 'Rank(101 - 4000)', 'Weight_g', 'Listing Title', 'ASIN',
                  'Review Count', 'Username']

    for i in range(1, 11):
        file_name = 'amazon_bsr_output_2017_09_10_{}.csv'.format(i)
        # write_data(file_name, csv_header)
        link_csv_file = 'amazon_links_{}.csv'.format(i)
        with open(link_csv_file, 'r', encoding='utf-8') as f:
            print(link_csv_file)
            reader = csv.reader(f)
            for row in reader:
                uri = re.search('(^.*?dp\/[^\/]*)\/.*?', row[-1])
                grab_details(uri.group(1), row[:-2], row[-2])


def write_data(file_name, row, mode='a'):
    try:
        with open(file_name, mode, newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(row)
    except Exception as x:
        print('Error printing csv output: ' + str(x))


if __name__ == '__main__':
    process_amazon_bsr()
    # grab_listing_urls(None,
    #     'https://www.amazon.co.uk/Best-Sellers-Amazon-Launchpad-Baby/zgbs/boost/7424492031/ref=zg_bs_nav_2_7424489031/257-7654953-4872257')
    # grab_details(
    #     'https://www.amazon.co.uk/Wilson-Junior-Hypergrip-Arrow-Football/dp/B00F5X4I9G/ref=zg_bs_5104995031_13/258-6185501-4146814?_encoding=UTF8&psc=1&refRID=HZYVA8XQRE60EZVJF1Z4',
    #     (), None)
