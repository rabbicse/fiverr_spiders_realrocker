# -*- coding: utf-8 -*-
import csv
import json
import os
import random
import re
import sys
from queue import Queue
from threading import Thread
import requests
from lxml import html

concurrent = 500
global junkProxies
global proxies
global remainingCount

junkProxies = []
proxies = []
websiteUrl = 'https://www.justdial.com'
API_URL = 'https://t.justdial.com/api/india_api_write/01jan2018/searchziva.php?'

f = open('proxylist.csv', 'r+')
try:
    reader = csv.reader(f)
    for row in reader:
        proxies.append(row[0])
finally:
    f.close()

filename = input('Enter File name that contains links: ')

pages = set()
f = open(filename, 'rt')
try:
    next(f)
    reader = csv.reader(f)
    for row in reader:
        try:
            pages.add(row[1])
        except Exception as e:
            pass
finally:
    f.close()

donePages = set()

if os.path.exists("Data.csv"):
    f = open('Data.csv', 'r+')
    try:
        next(f)
        reader = csv.reader(f)
        try:
            for row in reader:
                donePages.add(row[0])
        except Exception as e:
            pass
    finally:
        f.close()

remaining = pages - donePages
remaining = list(remaining)
remainingCount = len(remaining)

print('Total Records: ' + str(len(pages)))
print('Done Records: ' + str(len(donePages)))
print('Remaining Records: ' + str(len(remaining)))

pages = set()
donePages = set()

print('Collecting Records Details...\n')

if os.path.exists('Data.csv'):
    myfile = open('Data.csv', 'a+')
    wrtr = csv.writer(myfile, delimiter=',', quotechar='"')
else:
    myfile = open('Data.csv', 'w+')
    wrtr = csv.writer(myfile, delimiter=',', quotechar='"')
    headerRow = [
        'Url',
        'Name',
        'Email',
        'Phone Number',
        'Website',
        'Address',
        'Area',
        'Latitude',
        'Longitude',
        'Rating',
        'Total Reviews',
        'Year Establish',
        'Practice Areas',
        'Hours Of Operations',
        'Description'
    ]
    wrtr.writerow(headerRow)


def GetProxy(proxies):
    proxy_url = random.choice(proxies)

    if not str(proxy_url).startswith('http://'):
        proxy_url = 'http://' + proxy_url

    return {
        "http": proxy_url,
        "https": proxy_url
    }


def doWork():
    while True:
        url = q.get()
        url, response = getResponseFromUrl(url)
        getDataFromPage(url, response)
        q.task_done()


def getResponseFromUrl(url):
    global remainingCount
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, sdch, br",
        "Accept-Language": "en-US,en;q=0.8",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
    }

    if len(junkProxies) > ((len(proxies) / 100) * 90):
        del junkProxies[:]

    response = None
    while response is None:
        try:
            proxy = GetProxy(proxies)
            response = requests.get(url, timeout=15, headers=headers, proxies=proxy)
            if response != None:
                if response.status_code != 200:
                    response = None
        except Exception as e:
            pass

    sys.stdout.write("%d | %s \t\t\t\t\n" % (remainingCount, response.url))
    remainingCount -= 1
    sys.stdout.flush()
    return response.url, response.content


def getResponseFromUrlWithoutCount(url):
    global remainingCount
    headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, sdch, br",
        "Accept-Language": "en-US,en;q=0.8",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
    }

    if len(junkProxies) > ((len(proxies) / 100) * 90):
        del junkProxies[:]

    response = None
    while response is None:
        try:
            proxy = GetProxy(proxies)
            response = requests.get(url, timeout=15, headers=headers, proxies=proxy)
            if response != None:
                if response.status_code != 200:
                    response = None
        except Exception as e:
            pass
    return response.url, response.content


class ScrapingHelper:
    def __init__(self, tree):
        self.tree = tree

    def GetDataFromXPath(self, xpath):
        text = ''
        try:
            text = self.tree.xpath(xpath)[0].strip()
        except Exception as e:
            pass
        return text

    def GetMergedDataFromXPath(self, xpath):
        text = ''
        try:
            text = ''.join(self.tree.xpath(xpath)).strip()
        except Exception as e:
            pass
        return text

    def GetMergedDataFromXPathUsingSeparator(self, xpath, separator):
        text = ''
        try:
            text = separator.join(self.tree.xpath(xpath)).strip()
        except Exception as e:
            pass
        return text


def getDataFromPage(url, content):
    try:
        tree = html.fromstring(content)
        scrapingHelper = ScrapingHelper(tree)

        name = scrapingHelper.GetDataFromXPath('//div[@class="company-details"]//span[@class="fn"]/text()')
        address = scrapingHelper.GetDataFromXPath('//*[@id="fulladdress"]//span[@class="lng_add"]/text()')
        practiceAreas = scrapingHelper.GetMergedDataFromXPathUsingSeparator(
            '//span[text()="Also Listed In"]//following::td/a/@title', ',')
        website = scrapingHelper.GetDataFromXPath('//i[@class="web_ic sprite_wb comp-icon"]/following::a[1]/@href')

        hoursOfOperations = []
        for openHour in tree.xpath('//ul[@id="statHr"]//li[@class="mreinfli"]'):
            hoursOfOperations.append(
                openHour.xpath('.//span[1]/text()')[0].strip() + ': ' + openHour.xpath('.//span[2]/text()')[0].strip())
        hoursOfOperations = ', '.join(hoursOfOperations)

        description = scrapingHelper.GetMergedDataFromXPath(
            '//p[text()="Business Information"]/following::span[1]//text()')

        try:
            docId = scrapingHelper.GetDataFromXPath('//*[@id="docid"]/@value')
            mobileUrl, mobileContent = getResponseFromUrlWithoutCount(API_URL + 'docid=' + docId + '&case=detail')
            jsonResponse = json.loads(mobileContent)['results']

            phoneNumber = jsonResponse['contact']
            if not phoneNumber:
                phoneNumber = jsonResponse['VNumber']

            rating = jsonResponse['rating']
            totalReviews = jsonResponse['totalReviews']
            yearEstablish = jsonResponse['YOE']
            area = jsonResponse['area']
            latitude = jsonResponse['complat']
            longitude = jsonResponse['complong']
            email = jsonResponse['email']
        except Exception as e:
            phoneNumber = ''
            rating = ''
            totalReviews = ''
            yearEstablish = ''
            area = ''
            latitude = ''
            longitude = ''
            email = ''

        dataRow = [
            url,
            name,
            email,
            phoneNumber,
            website,
            address,
            area,
            latitude,
            longitude,
            rating,
            totalReviews,
            yearEstablish,
            practiceAreas,
            hoursOfOperations,
            description
        ]
        wrtr.writerow([re.sub(r'[^\x00-\x7F]+', '', dataEl) for dataEl in dataRow])
        myfile.flush()
    except Exception as e:
        print(e)
        pass


q = Queue(concurrent)
for i in range(concurrent):
    t = Thread(target=doWork)
    t.daemon = True
    t.start()
try:
    for url in remaining:
        q.put(url.strip())
    q.join()
except KeyboardInterrupt:
    sys.exit(1)

myfile.close()
input('Done, Please Enter to continue...')
