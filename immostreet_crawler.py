# -*- coding: utf-8 -*-

import csv
import gzip
import http.cookiejar
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class ImmoStreetSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
    __base_url = 'http://www.immostreet.com'
    # __start_url = 'http://www.immostreet.com/Agency/Search?currentPage={}&placeId=4832011'
    # __start_url = 'http://www.immostreet.com/Agency/Search?currentPage={}&placeId=4828617'
    __start_url = 'http://www.immostreet.com/Agency/Search?currentPage={}&placeId={}'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'Host': 'www.immostreet.com',
        'Pragma': 'no-cache'
    }

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 5
    MAX_THREAD = 32

    def __init__(self, place_id, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv
        self.__place_id = place_id

    def _setup_logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('immostreet.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)
        return logger

    def __enter__(self):
        self.__lock = Lock()
        self.__logger = self._setup_logger()
        socket.setdefaulttimeout(60)

        hdr = [('name', 'Name'),
               ('address', 'Address'),
               ('contact', 'Contact'),
               ('fax', 'Fax'),
               ('email', 'Email'),
               ('website', 'Website'),
               ('url', 'Map URL')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.__proxies = json_data

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    if key not in opener.addheaders:
                        opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                cj = http.cookiejar.CookieJar()
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self, place_id, output_csv):
        try:
            self.__place_id = str(place_id)
            self.__output_csv = output_csv

            # for page in range(0, 1):
            #     self.__grab_data_by_page(page)
            max_page = self.__grab_data_by_page(0)
            self.__logger.info('Maximum page: {}'.format(max_page))

            max_page = 2560

            for i in range(500, max_page, self.MAX_THREAD):
                with ThreadPool(self.MAX_THREAD) as pool:
                    pool.map(self.__grab_data_by_page, range(i, i + self.MAX_THREAD))
        except Exception as x:
            self.__logger.error('Error when grab data. Error details: {}'.format(x))

    @tail_recursive
    def __grab_data_by_page(self, page=0, retry=0):
        try:
            url = self.__start_url.format(page, self.__place_id)
            self.__logger.info('Main URL: {}'.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            data = urllib.parse.unquote_plus(data)
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            del data

            item_divs = soup.find_all('div', class_='panel panel-default')
            if not item_divs and len(item_divs) == 0:
                return

            for item_div in item_divs:
                try:
                    item = {}

                    map_div = item_div.find('div', class_='panel-footer')
                    if map_div:
                        map_a = map_div.find('a')
                        if map_a:
                            map_url = map_a.get('href')
                            if 'http' in map_url.strip() and map_url in self.__url_cache:
                                self.__logger.warning('Map URL: {} already grabbed!'.format(map_url))
                                continue

                            item['url'] = map_url

                    title_div = item_div.find('div', class_='panel-title')
                    if title_div:
                        item['name'] = title_div.text.strip()

                    address = item_div.find('address')
                    if address:
                        p_list = address.find_all('p')

                        if p_list and len(p_list) > 0:
                            item['address'] = p_list[0].text.strip()

                            for p in p_list:
                                if 'Tél :' in p.text:
                                    item['contact'] = p.text.replace('Tél :', '')
                                    continue

                                if 'Fax : ' in p.text:
                                    item['fax'] = p.text.strip()
                                    continue

                                item_a = p.find('a')
                                if item_a and item_a.has_attr('href'):
                                    if 'mailto:' in item_a.get('href'):
                                        item['email'] = item_a.get('href').replace('mailto:', '')
                                    else:
                                        item['website'] = item_a.get('href')

                    self.__write_item(item)
                except Exception as ex:
                    self.__logger.error('Error when parse data: {}'.format(ex))

            if page == 0:
                last_page_tag = soup.find('a', {'aria-label': 'Last'})
                if last_page_tag and last_page_tag.has_attr('href'):
                    last_link = last_page_tag.get('href')
                    last_page_m = re.search(r'currentPage=(\d+)', last_link)
                    if last_page_m:
                        last_num = last_page_m.group(1)
                        return int(last_num) + 1

            # After creating soup object simply dispose data to free memory
            del soup

        except Exception as x:
            self.__logger.error('Error when grab data. Error details: {}'.format(x))
            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_data_by_page(page, retry + 1)

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    place_id = '4832011'
    # output_file = 'immostreet_output_{}.csv'.format(place_id)
    output_file = 'immostreet_output.csv'
    with ImmoStreetSpider(place_id, output_file) as spider:
        # place_ids = [4843039, 4814353, 4817754, 4819114, 4821033, 4823168, 4826451, 4849783, 4830094, 4833408, 4848171,
        #              4845401,
        #              4847000, 4852656, 4815370]
        place_ids = [4832011]
        for pl_id in place_ids:
            # output_file = 'immostreet_output_{}.csv'.format(pl_id)
            output_file = 'immostreet_output.csv'
            spider.grab_data(pl_id, output_file)
