import csv
import gzip
import random
import re
import socket
import urllib.request
import urllib.parse
from multiprocessing import Lock, Pool
import time
from bs4 import BeautifulSoup


class SuperpagesGrabber:
    __lock = Lock()
    MAX_PAGE = 55537
    __base_url = 'http://www.superpages.com.au/listings/search/page:{}?keyword=a&hometypecat=1&loc_address=&location_id=&location_type='
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    def __init__(self, output_csv):
        self.__output_csv = output_csv
        socket.setdefaulttimeout(120)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def clean_data(self):
        try:
            with open(self.__output_csv, 'r', newline='', encoding='utf-8') as f:
                reader = csv.reader(f)
                for row in reader:
                    if any(word in str(row[0]).strip().lower() for word in
                           ['school', 'church', 'kindergarten', 'kindy']):
                        print('skipping...')
                        continue
                    self.__write_data(row)
        except Exception as x:
            print(x)

    def grab_data(self):
        try:
            csv_header = ['Name', 'Email', 'URL']
            self.__write_data(csv_header)
            for i in range(1, self.MAX_PAGE, 8):
                # start 4 worker processes
                with Pool(processes=8) as pool:
                    pool.map(self.pool_data, range(i, i + 8))
                    # url = self.__base_url.format(i)
                    # self.__grab_data(url)
                    #
                    # # 1-5 sec random sleep
                    # sleep_time = random.randint(1, 10)
                    # print('Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
                    # time.sleep(sleep_time)

        except Exception as x:
            print(x)

    def pool_data(self, page):
        try:
            url = self.__base_url.format(page)
            print('=== URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            result_div = soup.find('div', class_='search-results clearfix column')
            if not result_div:
                return

            results = result_div.find_all('div', class_='results-block')
            for result in results:
                try:
                    name, email, url = '', '', ''
                    title_tag = result.find('h3', class_='panel-title')
                    if title_tag:
                        tag = title_tag.find_all('a')[-1]
                        name = tag.text.strip()
                        url = tag.get('href')
                    email_m = re.search(r'<a href="mailto\:([^"]*?)"', str(result), re.MULTILINE)
                    if email_m:
                        email = email_m.group(1)

                    csv_data = [name, email, url]
                    print(csv_data)
                    self.__write_data(csv_data)
                except Exception as ex:
                    print(ex)
        except Exception as x:
            print(x)

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv + '_final.csv', mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)
        except Exception as x:
            print('Error printing csv output: ' + str(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    # /home/mehmet/local_search_output.csv
    with SuperpagesGrabber('search_output_mp.csv') as spider:
    # with SuperpagesGrabber('world_directory_output.csv') as spider:
    #     spider.clean_data()
        spider.grab_data()
