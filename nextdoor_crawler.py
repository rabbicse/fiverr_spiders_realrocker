import csv
import gzip
import json
import logging
import os
import random
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class NextDoorSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url = 'https://nextdoor.com'
    __start_url = 'https://nextdoor.com/find-neighborhood/'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive'}

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 30
    MAX_THREAD = 4

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('next_door.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)
        return logger

    def __enter__(self):
        self.__lock = Lock()
        self.__logger = self._setup_logger()
        socket.setdefaulttimeout(30)

        hdr = [
            ('url', 'Url'),
            ('state', 'State'),
            ('city', 'City'),
            ('neighborhood', 'Neighborhood')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.__proxies = json_data

        # If header not yet written then write csv header first
        if self.__csv_header['url'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['url'])

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:
            self.__logger.info('Main URL: {}'.format(self.__start_url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(self.__start_url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            states = []
            div_hood_list = soup.find_all('div', class_='hood_group')
            for div_hood in div_hood_list:
                a_list = div_hood.find_all('a')
                for a in a_list:
                    state = a.text.strip()
                    state_url = a.get('href')
                    states.append({'url': state_url, 'state': state})

                    # self.__grab_by_state(state_url, state)

            with ThreadPool(self.MAX_THREAD) as p:
                p.map(self.__grab_by_state, states)

            # After creating soup object simply dispose data to free memory
            del data

        except Exception as x:
            self.__logger.error('Error when grab data. Error details: {}'.format(x))

    @tail_recursive
    def __grab_by_state(self, kwargs, retry=0):
        try:
            url = kwargs['url']
            state = kwargs['state']

            self.__logger.info('State URL: {}'.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            cities = []
            div_hood_list = soup.find_all('div', class_='hood_group')
            for div_hood in div_hood_list:
                a_list = div_hood.find_all('a')
                for a in a_list:
                    city = a.text.strip()
                    city_url = a.get('href')
                    cities.append({'url': city_url, 'state': state, 'city': city})

            with ThreadPool(self.MAX_THREAD) as p:
                p.map(self.__grab_by_city, cities)

            # After creating soup object simply dispose data to free memory
            del data
        except Exception as x:
            self.__logger.error('Error when get search results for ad. Error details: {}'.format(x))
            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_by_state(kwargs, retry + 1)

    @tail_recursive
    def __grab_by_city(self, kwargs, retry=0):
        try:
            url = kwargs['url']
            state = kwargs['state']
            city = kwargs['city']

            self.__logger.info('City URL: {}'.format(url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            # After creating soup object simply dispose data to free memory
            del data

            div_hood_list = soup.find_all('div', class_='hood_group')
            for div_hood in div_hood_list:
                a_list = div_hood.find_all('a')
                for a in a_list:
                    neighborhood = a.text.strip()
                    neighborhood_url = a.get('href')

                    if neighborhood_url in self.__url_cache:
                        self.__logger.warning('URL: {} already grabbed!'.format(neighborhood_url))
                        continue

                    item = {'url': neighborhood_url, 'state': state, 'city': city, 'neighborhood': neighborhood}

                    self.__write_item(item)
        except Exception as x:
            self.__logger.error('Error when get business details. Error details: {}'.format(x))
            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_by_city(kwargs, retry + 1)

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'next_door_output.csv'
    with NextDoorSpider(output_file) as spider:
        spider.grab_data()
