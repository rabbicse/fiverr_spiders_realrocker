import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from datetime import datetime, timezone
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from bs4 import BeautifulSoup


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class FootballJsonSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url_hkjc = 'http://bet.hkjc.com'
    __start_url_hkjc = 'http://bet.hkjc.com/football/odds/odds_inplay.aspx?lang=EN'
    __start_url_hkjc_ajax = 'http://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_inplay.aspx'
    __match_details_url = 'http://bet.hkjc.com/football/odds/odds_allodds.aspx?lang=EN&tmatchid={}'
    __match_details_in_play_url = 'http://bet.hkjc.com/football/odds/odds_inplay_all.aspx?lang=EN&tmatchid={}'
    __match_details_in_play_url_ajax = 'http://bet.hkjc.com/football/getJSON.aspx?jsontype=odds_inplay_all.aspx&matchid={}'
    __match_results_url = 'http://bet.hkjc.com/football/getJSON.aspx?jsontype=results.aspx'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json; charset=utf-8'}

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 20
    # __output_csv_format = './football_output/{}{}_{}_{}_vs_{}.csv'
    __output_csv_format = '{}_{}_{}_{}_{}_vs_{}.csv'
    __match_hash = {}
    __result_dict = {}
    __match_cache = {}

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('football.log', maxBytes=20 * 1024 * 1024, backupCount=500)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__lock = Lock()
        self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(30)

        hdr = [
            ('matchid_hkjc', 'MatchID_HKJC'),
            ('match_number_hkjc', 'MatchNumber_HKJC'),
            ('match_league_hkjc', 'MatchLeague_HKJC'),
            ('match_home_team_hkjc', 'MatchHomeTeam_HKJC'),
            ('match_away_team_hkjc', 'MatchAwayTeam_HKJC'),
            ('match_league_bet365', 'MatchLeague_bet365'),
            ('match_home_team_bet365', 'MatchHomeTeam_bet365'),
            ('match_away_team_bet365', 'MatchAwayTeam_bet365'),
            ('time_local_computer', 'TimeLocalComputer'),
            ('time_bet365', 'Time_bet365'),
            ('match_status_hkjc', 'MatchStatus_HKJC'),
            ('score_hkjc', 'Score_HKJC'),
            ('score_bet365', 'Score_bet365'),
            ('home_away_draw_home_odd_hkjc', 'HomeAwayDraw_HomeOdd_HKJC'),
            ('home_away_draw_draw_odd_hkjc', 'HomeAwayDraw_DrawOdd_HKJC'),
            ('home_away_draw_away_odd_hkjc', 'HomeAwayDraw_AwayOdd_HKJC'),
            ('hilo_line_1_hkjc', 'HiLo_Line1_HKJC'),
            ('hilo_line_1_high_odd_hkjc', 'HiLo_Line1_HighOdd_HKJC'),
            ('hilo_line_1_low_odd_hkjc', 'HiLo_Line1_LowOdd_HKJC'),
            ('hilo_line_2_hkjc', 'HiLo_Line2_HKJC'),
            ('hilo_line_2_high_odd_hkjc', 'HiLo_Line2_HighOdd_HKJC'),
            ('hilo_line_2_low_odd_hkjc', 'HiLo_Line2_LowOdd_HKJC'),
            ('hilo_line_3_hkjc', 'HiLo_Line3_HKJC'),
            ('hilo_line_3_high_odd_hkjc', 'HiLo_Line3_HighOdd_HKJC'),
            ('hilo_line_3_low_odd_hkjc', 'HiLo_Line3_LowOdd_HKJC'),
            ('total_corner_hkjc', 'TotalCorner_HKJC'),
            ('corner_line_1_hkjc', 'Corner_Line1_HKJC'),
            ('corner_line_1_high_odd_hkjc', 'Corner_Line1_HighOdd_HKJC'),
            ('corner_line_1_low_odd_hkjc', 'Corner_Line1_LowOdd_HKJC'),
            ('corner_line_2_hkjc', 'Corner_Line2_HKJC'),
            ('corner_line_2_high_odd_hkjc', 'Corner_Line2_HighOdd_HKJC'),
            ('corner_line_2_low_odd_hkjc', 'Corner_Line2_LowOdd_HKJC'),
            ('corner_line_3_hkjc', 'Corner_Line3_HKJC'),
            ('corner_line_3_high_odd_hkjc', 'Corner_Line3_High_Odd_HKJC'),
            ('corner_line_3_low_odd_hkjc', 'Corner_Line3_LowOdd_HKJC'),
            ('next_team_to_score_home_odd_hkjc', 'NextTeamtoScore_HomeOdd_HKJC'),
            ('next_team_to_score_no_goals_odd_hkjc', 'NextTeamtoScore_NoGoalsOdd_HKJC'),
            ('next_team_to_score_away_odd_hkjc', 'NextTeamtoScore_AwayOdd_HKJC'),
            ('home_team_corner_bet365', 'HomeTeam_Corner_bet365'),
            ('home_team_dangerous_attack_bet365', 'HomeTeam_DangerousAttack_bet365'),
            ('home_team_yellow_card_bet365', 'HomeTeam_YellowCard_bet365'),
            ('home_team_red_card_bet365', 'HomeTeam_RedCard_bet365'),
            ('away_team_corner_bet365', 'AwayTeam_Corner_bet365'),
            ('away_team_dangerous_attack_bet365', 'AwayTeam_DangerousAttack_bet365'),
            ('away_team_yellow_card_bet365', 'AwayTeam_YellowCard_bet365'),
            ('away_team_red_card_bet365', 'AwayTeam_RedCard_bet365'),
            ('hometeam_yellowcard_hkjc', 'HomeTeam_YellowCard_HKJC'),
            ('hometeam_redcard_hkjc', 'HomeTeam_RedCard_HKJC'),
            ('AwayTeam_YellowCard_HKJC', 'AwayTeam_YellowCard_HKJC'),
            ('AwayTeam_RedCard_HKJC', 'AwayTeam_RedCard_HKJC'),
            ('ResultCorner_HKJC', 'ResultCorner_HKJC'),
            ('ResultHalfTime_HKJC', 'ResultHalfTime_HKJC'),
            ('ResultFullTime_HKJC', 'ResultFullTime_HKJC')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # if not os.path.exists('./football_output/'):
        #     os.mkdir('./football_output/')

        if os.path.exists('./cache.json'):
            try:
                with open('cache.json', 'r+') as fp:
                    self.__match_hash = json.load(fp)
            except Exception as x:
                self.__logger.error('Error when load cache. Please delete cache.json file! \nError: {}'.format(x))

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:
            while True:

                self.__grab_result_hkjc()

                self.__process_cached_data()

                self.__grab_data_hkjc()

                sleep_time = 15
                if os.path.exists('config.txt'):
                    with open('config.txt', 'r+', encoding='utf-8') as f:
                        txt = f.read().strip()
                        sleep_time = int(txt)

                with open('cache.json', 'w+', encoding='utf-8') as f:
                    # print(self.__match_hash)
                    json.dump(self.__match_hash, f)

                # break
                self.__logger.info('Script is going to sleep for {} seconds...'.format(sleep_time))
                time.sleep(sleep_time)
        except Exception as x:
            print(x)

    def __process_cached_data(self):
        try:
            expired_matches = []
            for match_id in self.__match_hash:
                self.__logger.info(self.__match_hash[match_id])

                if os.path.exists(self.__match_hash[match_id]):
                    with open(self.__match_hash[match_id], 'r+', encoding='utf-8') as f:
                        reader = csv.DictReader(f, self.__field_names)
                        for row in reader:
                            if row['match_status_hkjc'] == 'ResultIn':
                                expired_matches.append(match_id)
                            else:
                                self.__match_cache[match_id] = row

            for expired_match in expired_matches:
                self.__match_cache.pop(expired_match)

            print(self.__result_dict)
            for m_k in self.__match_cache:
                if m_k in self.__result_dict.keys():
                    pass

        except Exception as x:
            self.__logger.error('Error when process cached data: {}!'.format(x))

    @tail_recursive
    def __grab_data_hkjc(self, retry=0):
        try:
            self.__logger.info('=== BET HKJC URL: {} ==='.format(self.__start_url_hkjc_ajax))
            opener = self.__create_opener()
            data = opener.open(self.__start_url_hkjc_ajax).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            json_array = json.loads(data)
            for json_data in json_array:
                try:

                    # print(json_data)

                    # if 'BeforeKickOff' in json_data['liveEvent']['matchstate']:
                    #     continue
                    # if json_data['liveEvent']['matchstate'] not in ['FirstHalf', 'SecondHalf']:
                    #     continue

                    match_id = json_data['matchID']
                    match_id_no_official = json_data['matchIDinofficial']

                    if json_data['liveEvent']['matchstate'] not in ['FirstHalf', 'SecondHalf']:
                        # todo:
                        continue


                    match_time = json_data['matchTime']
                    match_time = re.sub(r'(?<=[+=][0-9]{2}):', '', match_time)
                    m_t = datetime.strptime(match_time, '%Y-%m-%dT%H:%M:%S%z')
                    m_t = m_t.astimezone(timezone.utc)

                    item = {'matchid_hkjc': json_data['matchIDinofficial'],
                            'match_number_hkjc': json_data['matchDay'] + ' ' + json_data['matchNum'],
                            'match_league_hkjc': json_data['league']['leagueNameEN'],
                            'match_home_team_hkjc': json_data['homeTeam']['teamNameEN'],
                            'match_away_team_hkjc': json_data['awayTeam']['teamNameEN'],
                            'match_status_hkjc': json_data['liveEvent']['matchstate'],
                            'time_local_computer': datetime.now().strftime('%Y/%m/%d %H:%M:%S')}

                    if 'livescore' in json_data:
                        item['score_hkjc'] = '{}_{}'.format(json_data['livescore']['home'], json_data['livescore']['away'])

                    if 'hadodds' in json_data:
                        item['home_away_draw_home_odd_hkjc'] = json_data['hadodds']['H'].split('@')[-1]
                        item['home_away_draw_draw_odd_hkjc'] = json_data['hadodds']['D'].split('@')[-1]
                        item['home_away_draw_away_odd_hkjc'] = json_data['hadodds']['A'].split('@')[-1]

                    if match_id not in self.__match_hash.keys():
                        directory_structure = './{}/{}/'.format(item['match_league_hkjc'].replace(' ', '_'),
                                                                m_t.strftime('%Y_%m'))
                        if not os.path.exists(directory_structure):
                            os.mkdir('./{}/'.format(item['match_league_hkjc'].replace(' ', '_')))
                            os.mkdir(directory_structure)

                        match_start_time = m_t.strftime('%Y%m%d_%H%M')
                        output_csv_file = self.__output_csv_format.format(
                            match_start_time,
                            item['match_number_hkjc'],
                            match_id_no_official,
                            item['match_league_hkjc'],
                            item['match_home_team_hkjc'],
                            item['match_away_team_hkjc'])
                        output_csv_file = re.sub(r'\s+', ' ', output_csv_file)
                        output_csv_file = re.sub(r'\s', '_', output_csv_file)
                        self.__match_hash[match_id] = directory_structure + output_csv_file  # match_start_time

                    match_details_url = self.__match_details_in_play_url_ajax.format(match_id)
                    status = self.__grab_hkjc_details(match_details_url, item, self.__match_hash[match_id])
                    if not status:
                        self.__logger.info('Data: {}'.format(item))
                        if not os.path.exists(self.__match_hash[match_id]):
                            self.__write_data(self.__match_hash[match_id], self.__csv_header)
                        self.__write_data(self.__match_hash[match_id], item)
                except Exception as ex:
                    self.__logger.error('Error when parse data for each match: {}'.format(ex))

            self.__logger.info('HKJC in play record search finished!')
        except Exception as x:
            self.__logger.error('Error when grab bet hkjc page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_data_hkjc(retry + 1)

    @tail_recursive
    def __grab_result_hkjc(self, retry=0):
        try:
            self.__logger.info('=== BET HKJC Result URL: {} ==='.format(self.__match_results_url))
            opener = self.__create_opener()
            data = opener.open(self.__match_results_url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            json_array = json.loads(data)
            j_data = None
            for json_data in json_array:
                if 'name' in json_data and json_data['name'] == 'ActiveMatches':
                    j_data = json_data

            if j_data:
                for r_data in j_data['matches']:
                    self.__result_dict[r_data['matchID']] = r_data
        except Exception as x:
            self.__logger.error('Error when grab bet hkjc result page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_result_hkjc(retry + 1)

    @tail_recursive
    def __grab_hkjc_details(self, url, item, output_csv_file, retry=0):
        try:
            self.__logger.info('=== BET HKJC Details URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            json_data = json.loads(data)
            json_details = json_data[0]
            self.__logger.debug('Json Data: {}'.format(json_details))

            if 'hilodds' in json_details and 'LINELIST' in json_details['hilodds']:
                hilo_odds = json_details['hilodds']['LINELIST']
                for hilo_odd in hilo_odds:
                    line = self.__clean_data(hilo_odd['LINE'])
                    lines = line.split('/')
                    if len(lines) == 2 and lines[0].strip() == lines[-1].strip():
                        line = lines[0]

                    if hilo_odd['LINENUM'] == '1':
                        item['hilo_line_1_hkjc'] = line
                        item['hilo_line_1_high_odd_hkjc'] = self.__clean_data(hilo_odd['H'])
                        item['hilo_line_1_low_odd_hkjc'] = self.__clean_data(hilo_odd['L'])
                    elif hilo_odd['LINENUM'] == '2':
                        item['hilo_line_2_hkjc'] = line
                        item['hilo_line_2_high_odd_hkjc'] = self.__clean_data(hilo_odd['H'])
                        item['hilo_line_2_low_odd_hkjc'] = self.__clean_data(hilo_odd['L'])
                    elif hilo_odd['LINENUM'] == '3':
                        item['hilo_line_3_hkjc'] = line
                        item['hilo_line_3_high_odd_hkjc'] = self.__clean_data(hilo_odd['H'])
                        item['hilo_line_3_low_odd_hkjc'] = self.__clean_data(hilo_odd['L'])

            if 'chlodds' in json_details and 'LINELIST' in json_details['chlodds']:
                chl_odds = json_details['chlodds']['LINELIST']
                i = 0
                for chl_odd in chl_odds:
                    line = self.__clean_data(chl_odd['LINE'])
                    lines = line.split('/')
                    if len(lines) == 2 and lines[0].strip() == lines[-1].strip():
                        line = lines[0]

                    # if chl_odd['LINENUM'] == '1':
                    if i == 0:
                        item['corner_line_1_hkjc'] = line
                        item['corner_line_1_high_odd_hkjc'] = self.__clean_data(chl_odd['H'])
                        item['corner_line_1_low_odd_hkjc'] = self.__clean_data(chl_odd['L'])
                    # elif chl_odd['LINENUM'] == '2':
                    elif i == 1:
                        item['corner_line_2_hkjc'] = line
                        item['corner_line_2_high_odd_hkjc'] = self.__clean_data(chl_odd['H'])
                        item['corner_line_2_low_odd_hkjc'] = self.__clean_data(chl_odd['L'])
                    # elif chl_odd['LINENUM'] == '3':
                    elif i == 2:
                        item['corner_line_3_hkjc'] = line
                        item['corner_line_3_high_odd_hkjc'] = self.__clean_data(chl_odd['H'])
                        item['corner_line_3_low_odd_hkjc'] = self.__clean_data(chl_odd['L'])

                    i += 1

            if 'ntsodds' in json_details:
                for nts in json_details['ntsodds']:
                    if 'H' in nts and 'next_team_to_score_home_odd_hkjc' not in item:
                        item['next_team_to_score_home_odd_hkjc'] = self.__clean_data(nts['H'])
                    if 'N' in nts and 'next_team_to_score_no_goals_odd_hkjc' not in item:
                        item['next_team_to_score_no_goals_odd_hkjc'] = self.__clean_data(nts['N'])
                    if 'A' in nts and 'next_team_to_score_away_odd_hkjc' not in item:
                        item['next_team_to_score_away_odd_hkjc'] = self.__clean_data(nts['A'])

            if 'cornerresult' in json_details:
                total_corner = self.__clean_data(json_details['cornerresult'])
                total_corner = '-' if '-' in total_corner else total_corner
                item['total_corner_hkjc'] = total_corner

            self.__logger.info('Data: {}'.format(item))
            if not os.path.exists(output_csv_file):
                self.__logger.info('No csv file created yet for: {}!'.format(output_csv_file))
                self.__write_data(output_csv_file, self.__csv_header, 'w+')
            self.__write_data(output_csv_file, item)
            return True
        except Exception as x:
            self.__logger.error('Error when grab bet hkjc details page.{}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_hkjc_details(url, item, output_csv_file, retry + 1)

    def __clean_data(self, text):
        try:
            text = text.split('@')[-1]
            text = re.sub(r'\[', '', text)
            text = re.sub(r'\]', '', text)
            return text
        except Exception as x:
            self.__logger.error('Error cleaning data: {}'.format(x))
        return text

    def __write_data(self, output_csv, row, mode='a+'):
        try:
            with open(output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'football.csv'
    with FootballJsonSpider(output_file) as spider:
        spider.grab_data()
