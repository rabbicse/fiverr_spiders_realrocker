import csv
import json
import logging
import os
from collections import OrderedDict
from multiprocessing import Semaphore, Value
from multiprocessing.pool import ThreadPool

from bs4 import BeautifulSoup
from easy_spider import Spider

logger = logging.getLogger(__name__)


class AaToolsCrawler(Spider):
    __image_url = 'https://latauskartta.fi/backend.php?action=getImages&lid={}'
    __listing_url = 'https://latauskartta.fi/latauspiste/{}'
    __start_url = 'http://www.aatools.co.uk/shopdisplayproducts.asp?Search=yes&bc=no&queryprefix=No&cname={}&page={}'
    __base_url = 'https://latauskartta.fi'
    __api_url = 'https://t.justdial.com/api/india_api_write/01jan2018/searchziva.php?'

    __url_cache = []
    __total = Value('i', 0)
    __lock = Semaphore()

    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Connection': 'keep-alive',
        'Host': 'www.aatools.co.uk',
        'Origin': 'http://www.aatools.co.uk',
        'Pragma': 'no-cache',
        'Upgrade-Insecure-Requests': '1'}

    def __init__(self, output_csv):
        Spider.__init__(self, log_file='latauskartta.log', headers=self.__headers)
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __enter__(self):
        hdr = [('loc_id', 'Location ID'),
               ('url', 'URL'),
               ('name', 'Name'),
               ('address', 'Address'),
               ('desc', 'Description'),
               ('lat', 'Latitude'),
               ('lon', 'Longitude'),
               ('cat_1', 'Category_1'),
               ('cat_2', 'Category_2'),
               ('cat_3', 'Category_3'),
               ('cat_4', 'Category_4'),
               ('cat_5', 'Category_5'),
               ('cat_6', 'Category_6'),
               ('cat_7', 'Category_7'),
               ('cat_8', 'Category_8'),
               ('image_urls', 'Image Urls'),
               ('image_paths', 'Images')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['loc_id'])

        # If header not yet written then write csv header first
        if self.__csv_header['loc_id'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['loc_id'])

        # if os.path.exists('proxy.json'):
        #     with open('proxy.json', 'r+', encoding='utf-8') as f:
        #         json_data = json.load(f)
        #         self.proxies = json_data

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Total records: {}'.format(self.__total.value))
        logger.info('Finish!!!')
        del self

    def process_data(self):
        try:
            response = self.fetch_data('https://latauskartta.fi/backend.php?idlimit=0&action=getData&editmode=false&chargers_type2=true&chargers_spc=true&chargers_chademo=true&chargers_ccs=true&chargers_tyomaa=false&unverified=false')
            if not response:
                return

            data, redirected_url = response
            json_data = json.loads(data)
            if not json_data:
                return

            records = []
            for loc_key in json_data['locations']:
                loc_data = json_data['locations'][loc_key]
                item = {'loc_id': loc_key,
                        'name': loc_data['title'],
                        'address': loc_data['address'],
                        'desc': loc_data['info'],
                        'lat': loc_data['lat'],
                        'lon': loc_data['lon']}

                chargers = [charger for charger in json_data['chargers'] if charger['location'] == loc_data['id']]
                records.append((item, chargers))

            with ThreadPool(8) as pool:
                pool.map(self.__parse_data, records)
        except Exception as x:
            logger.error('Error when process data: {}'.format(x))

    def __parse_data(self, data):
        try:
            item = data[0]
            chargers = data[1]

            # response = self.fetch_data(self.__listing_url.format(item['loc_id']))
            # if not response:
            #     return
            #
            # soup = BeautifulSoup(response[0], 'html5lib')
            # url_tag = soup.find('meta', {'property': 'og:url'})
            # if url_tag and url_tag.has_attr('content'):
            #     item['url'] = url_tag.get('content')
            # else:
            item['url'] = self.__listing_url.format(item['loc_id'])

            index = 1
            for charger in chargers:
                item['cat_{}'.format(index)] = '{}x Type: {} {}'.format(charger['kpl'], charger['type'],
                                                                        charger['info'])
                index += 1
                item['cat_{}'.format(index)] = '{} kW'.format(charger['kw'])

            # download images...
            if not os.path.exists('images/'):
                os.mkdir('images/')

            image_dir = 'images/{}/'.format(item['loc_id'])
            if not os.path.exists(image_dir):
                os.mkdir(image_dir)

            img_url = self.__image_url.format(item['loc_id'])
            res = self.fetch_data(img_url)
            if res:
                s = BeautifulSoup(res[0], 'html5lib')
                img_srcs = s.find_all('img')

                if img_srcs and len(img_srcs) > 0:
                    images = []
                    image_paths = []
                    for i, img_src in enumerate(img_srcs):
                        img = self.__base_url + img_src.get('data-original-src')
                        images.append(img)

                    with ThreadPool(8) as pool:
                        results = pool.map(self.__download_images, [(i, img, image_dir) for i, img in enumerate(images)])
                        if results:
                            for res in results:
                                if res:
                                    image_paths.append(res)
                    item['image_urls'] = '; '.join(images)
                    item['image_paths'] = '; '.join(image_paths)

            # logger.info('Data: {}'.format(item))
            self.__write_item(item)
        except Exception as x:
            logger.error(x)

    def __download_images(self, img_data):
        try:
            (i, img, image_dir) = img_data
            res = self.download_data(img, cache=True)
            if not res:
                return
            d, r = res
            index = i + 1
            img_path = '{}{}.jpg'.format(image_dir, index)
            # abs_img_path = os.path.abspath(img_path)
            with open(img_path, 'wb') as f:
                f.write(d)
                return img_path
        except Exception as ex:
            logger.error('Error download image: {}'.format(ex))

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            logger.info('Data: {}'.format(item))
            if item['loc_id'] not in self.__url_cache:
                self.__write_data(item)
                self.__url_cache.append(item['loc_id'])
        except Exception as ex:
            logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                with self.__total.get_lock():
                    self.__total.value += 1
                    logger.info('Total: {}'.format(self.__total.value))
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    with AaToolsCrawler('latauskartta_output.csv') as crawler:
        crawler.process_data()
