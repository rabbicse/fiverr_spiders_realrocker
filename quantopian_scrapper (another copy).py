import email
import json
import logging
import os
import re
import smtplib
import socket
import time
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options as FOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from sqlalchemy import Column, Integer, Float, create_engine, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import sys
from OpenGL import GL
from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings, QWebEngineProfile
from PyQt5.QtWidgets import QApplication

DeclarativeBase = declarative_base()


def db_connect():
    """Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance.
    """
    engine = create_engine('sqlite:///quantopian.db', echo=False)
    return engine


def create_table(engine):
    """"""
    DeclarativeBase.metadata.create_all(engine)


class Positions(DeclarativeBase):
    __tablename__ = 'positions'

    id = Column(Integer, primary_key=True)
    security = Column(String)
    shares = Column(Integer)
    price = Column(Float)
    avg_cost = Column(Float)
    pos_value = Column(Float)
    unrealized = Column(Float)

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])


class Browser(QWebEngineView):
    __redirected_url = None
    __html = None
    __clicked = False

    def __init__(self, app):
        QWebEngineView.__init__(self)
        self.__app = app
        self.settings().setAttribute(QWebEngineSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.loadFinished.connect(self._load_finished)
        self.__loaded = False

    def fetch(self, login_url, details_url):
        try:
            self.__details_url = details_url
            self.__login_page = True
            self.timeout_timer = QTimer()
            self.timeout_timer.timeout.connect(self._request_timed_out)
            #
            self.timeout_timer.start(120 * 1000)

            self.setUrl(QUrl(login_url))
            self.resize(1280, 1024)
            self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            print(x)
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            print('Custom request timeout value exceeded.')
            self.timeout_timer.stop()
            # self.stop()
            # self.loadFinished.emit(False)
            if not self.__loaded:
                print('Not loaded...')
            else:
                print('loaded...')

            self.__app.quit()
        except Exception as x:
            print(x)

    def _load_finished(self):
        try:
            if self.__login_page:
                self.__run_js_login()
                return

            current_url = str(self.page().url().toString())
            print('Redirected URL: {}'.format(current_url))
            if current_url == 'https://www.quantopian.com/posts':
                self.setUrl(QUrl(self.__details_url))
            else:
                time.sleep(5)
                self.__run_js_position_count()
        except Exception as x:
            print(x)
            self.__app.quit()

    def __run_js_login(self):
        fill_user = """document.getElementById("user_email").value='{}'""".format('chrisquantserver@gmail.com')
        fill_pass = """document.getElementById("user_password").value='{}'""".format('Loopcap1')
        submit = """document.getElementById("login-button").click()"""
        self.page().runJavaScript(fill_user)
        self.page().runJavaScript(fill_pass)

        self.page().runJavaScript(submit)
        self.__login_page = False

    def __run_js_position_count(self):
        algo_js = """document.querySelector('div[id="positions_count"]').outerHTML"""
        self.page().runJavaScript(algo_js, self.__js_click_callback)

    def __js_click_callback(self, data):
        print('click callback found...')
        print(data)
        if data:
            soup = BeautifulSoup(data, 'lxml')
            cnt = soup.find('div').text.strip()
            cnt = re.sub(r'[^0-9]', '', cnt).strip()
            if cnt == '' or int(cnt) == 0:
                time.sleep(1)
                self.__run_js_position_count()
            else:
                print(cnt)
                self.page().toHtml(self.processHtml)

                # page = soup.find('li').get('data-wiz-grid-hash')
                # if 'page={}'.format(self.__page_index + 1) in page:
                #     self.page().toHtml(self.processHtml)
                # else:
                #     # print('loading click...')
                #     js1 = """document.querySelector('li[data-wiz-next-page]').outerHTML"""
                #     self.page().runJavaScript(js1, self.__js_click_callback)
                # else:
                #     self.page().toHtml(self.processHtml)

    def processHtml(self, html):
        self.__html = html
        self.__app.quit()


class QuantopianGrabber:
    __login_form_url = 'https://www.quantopian.com/signin'
    __login_url = 'https://www.quantopian.com/users/sign_in'
    __algorithm_url = 'https://www.quantopian.com/live_algorithms/5a8ed9a04867d05e979e7ef4'
    __user = 'chrisquantserver@gmail.com'  # 'arthchrisjr@yahoo.com'
    __pass = 'Loopcap1'

    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}
    __total = 0
    __cache = []

    def __init__(self):
        self.__username = None
        self.__password = None
        self.__from_address = None
        self.__to_address = None
        self.__from_header = None
        self.__to_header = None

    def __enter__(self):
        self.__app = QApplication(sys.argv)
        self.__browser = Browser(self.__app)
        self.__lock = Lock()
        self.__setup_logger('quantopian.log')
        socket.setdefaulttimeout(120)

        if os.path.exists('credentials.json'):
            with open('credentials.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                if json_data:
                    self.__username = json_data['username']
                    self.__password = json_data['password']
                    self.__from_address = json_data['from_address']
                    self.__to_address = json_data['to_addresses']
                    self.__from_header = json_data['from_header']
                    self.__to_header = json_data['to_header']

        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler(log_file_name, maxBytes=2 * 1024 * 1024, backupCount=30)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__browser.close()
        self.__app.exit()
        del self.__logger
        del self

    def __build_browser(self):
        try:
            # phantomjs
            # cap = webdriver.DesiredCapabilities.PHANTOMJS
            # cap["phantomjs.page.settings.javascriptEnabled"] = True
            # cap['phantomjs.page.settings.userAgent'] = self.__user_agent
            # driver_path = './driver/phantomjs'
            #
            # browser = webdriver.PhantomJS(driver_path, desired_capabilities=cap,
            #                               service_args=['--ignore-ssl-errors=true'])

            # chrome browser
            chrome_options = Options()
            # chrome_options.add_argument("--no-startup-window")
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--no-sandbox")
            # chrome_options.add_argument('--user-data-dir="/root/chrome-profile/"')
            # chrome_options.add_argument("--disable-gpu")

            ex_path = './driver/chromedriver'
            # ex_path = '/root/spiders/driver/chromedriver'
            browser = webdriver.Chrome(executable_path=ex_path, desired_capabilities=DesiredCapabilities.CHROME,
                                       chrome_options=chrome_options, service_log_path='./selenium.log')

            # firefox
            # firefox_options = FOptions()
            # # chrome_options.add_argument("--no-startup-window")
            # firefox_options.add_argument("--headless")
            # # chrome_options.add_argument("--no-sandbox")
            # # chrome_options.add_argument('--user-data-dir="/root/chrome-profile/"')
            # # chrome_options.add_argument("--disable-gpu")
            #
            # ex_path = './driver/geckodriver'
            # # ex_path = '/root/spiders/driver/chromedriver'
            # browser = webdriver.Firefox(executable_path=ex_path, capabilities=DesiredCapabilities.FIREFOX,
            #                             firefox_options=firefox_options, log_path='./selenium.log')

            print(browser.service.service_url)
            return browser
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def operation(self):
        browser = None
        try:
            r, data = self.__browser.fetch(self.__login_form_url, self.__algorithm_url)
            # print(data)
            items = self.__parse_data(data)
            if items:
                session = self.Session()
                try:
                    changes = []
                    for item in items:
                        self.__logger.info('Item: {}'.format(item))
                        # changes.append(
                        #     'Security: [{}] added! Amount of shares: [{}]'.format(item['security'], item['shares']))
                        q = {'id': item['id']}
                        q_data = session.query(Positions).filter_by(**q).first()
                        if not q_data:
                            positions = Positions(**item)
                            session.add(positions)
                            session.commit()
                            self.__logger.info('Item[{}] inserted successfully!'.format(item['id']))
                            changes.append('{} shares of {} has been added!'.format(item['shares'], item['security']))
                        else:
                            data_changes = False
                            if int(q_data.shares) != int(item['shares']):
                                self.__logger.info(
                                    'Share changes. OLD Share: {}; NEW Share: {}'.format(q_data.shares, item['shares']))
                                data_changes = True
                                changes.append('{} shares of {} has been sold'.format(item['shares'], item['security']))

                            if q_data.price != item['price'] or \
                                            q_data.avg_cost != item['avg_cost'] or \
                                            q_data.pos_value != item['pos_value'] or \
                                            q_data.unrealized != item['unrealized']:
                                data_changes = True

                            if data_changes:
                                session.delete(q_data)
                                session.commit()
                                positions = Positions(**item)
                                session.add(positions)
                                session.commit()

                    # Send email on test basis first
                    if len(changes) > 0:
                        body = '\n'.join(changes)
                        self.__send_email(body)
                except Exception as ex:
                    self.__logger.error('Error: {}'.format(ex))
                    session.rollback()
                finally:
                    session.close()


            return

            self.__lock.acquire()
            self.__logger.info('Trying to Login. URL: {}'.format(self.__login_form_url))
            browser = self.__build_browser()
            self.__logger.info('Browser created: {}'.format(browser))
            browser.get(self.__login_form_url)
            wait = WebDriverWait(browser, 120)
            wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

            # print(browser.find_elements_by_xpath('//html'))

            # login
            elemu = browser.find_element_by_id('user_email')
            elemu.send_keys(self.__username)
            elemp = browser.find_element_by_id('user_password')
            elemp.send_keys(self.__password)
            elemp.send_keys(Keys.RETURN)

            wait = WebDriverWait(browser, 120)
            wait.until(EC.url_changes)

            self.__logger.info('Redirected URL: {}'.format(browser.current_url))
            if browser.current_url == 'https://www.quantopian.com/posts':
                self.__logger.info('Successfully logged in!')
            else:
                self.__logger.error('Logged in Failed!')
                return

            self.__logger.info('Trying to get results from: {}'.format(self.__algorithm_url))
            browser.get(self.__algorithm_url)
            wait = WebDriverWait(browser, 120)
            wait.until(EC.presence_of_element_located((By.XPATH, '//div[@class="no-positions hidden"]')))
            time.sleep(2)

            self.__logger.info('Entered result page...')
            element = browser.find_element_by_xpath('//div[@id="position-table"]')
            data = element.get_attribute('innerHTML')

            items = self.__parse_data(data)
            if items:
                session = self.Session()
                try:
                    changes = []
                    for item in items:
                        self.__logger.info('Item: {}'.format(item))
                        # changes.append(
                        #     'Security: [{}] added! Amount of shares: [{}]'.format(item['security'], item['shares']))
                        q = {'id': item['id']}
                        q_data = session.query(Positions).filter_by(**q).first()
                        if not q_data:
                            positions = Positions(**item)
                            session.add(positions)
                            session.commit()
                            self.__logger.info('Item[{}] inserted successfully!'.format(item['id']))
                            changes.append('{} shares of {} has been added!'.format(item['shares'], item['security']))
                        else:
                            data_changes = False
                            if int(q_data.shares) != int(item['shares']):
                                self.__logger.info(
                                    'Share changes. OLD Share: {}; NEW Share: {}'.format(q_data.shares, item['shares']))
                                data_changes = True
                                changes.append('{} shares of {} has been sold'.format(item['shares'], item['security']))

                            if q_data.price != item['price'] or \
                                            q_data.avg_cost != item['avg_cost'] or \
                                            q_data.pos_value != item['pos_value'] or \
                                            q_data.unrealized != item['unrealized']:
                                data_changes = True

                            if data_changes:
                                session.delete(q_data)
                                session.commit()
                                positions = Positions(**item)
                                session.add(positions)
                                session.commit()

                    # Send email on test basis first
                    if len(changes) > 0:
                        body = '\n'.join(changes)
                        self.__send_email(body)
                except Exception as ex:
                    self.__logger.error('Error: {}'.format(ex))
                    session.rollback()
                finally:
                    session.close()
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))
        finally:
            try:
                if browser:
                    browser.quit()
                    browser = None
                    del browser
            except:
                pass

    def __parse_data(self, data):
        try:
            soup = BeautifulSoup(data, 'lxml')
            tabs = soup.find_all('div', class_=re.compile(r'^ui-widget-content.*'))

            items = []
            for tab in tabs:
                item = {}
                columns = tab.find_all('div', class_=re.compile(r'^slick-cell.*?$'))
                if len(columns) < 6:
                    continue

                column_1 = columns[0]
                security_id_tag = column_1.find('span')
                if not security_id_tag:
                    continue

                security_id = security_id_tag.get('class')
                security_id = re.sub(r'[^\d]', '', str(security_id))

                item['id'] = security_id
                item['security'] = security_id_tag.text.strip()
                item['shares'] = columns[1].text.strip()
                item['price'] = re.sub(r'[^\d\.]', '', columns[2].text.strip())
                item['avg_cost'] = re.sub(r'[^\d\.]', '', columns[3].text.strip())
                item['pos_value'] = re.sub(r'[^\d\.]', '', columns[4].text.strip())
                item['unrealized'] = re.sub(r'[^\d\.]', '', columns[5].text.strip())
                items.append(item)

            return items
        except Exception as x:
            self.__logger.error('Error when parse result set.{}'.format(x))

    def __send_email(self, body):
        try:
            self.__logger.info('Sending email...')
            msg = MIMEMultipart()
            msg['Subject'] = Header('Quantopian Status.', 'utf-8')
            # msg['From'] = email.utils.formataddr(('Support', 'support@chrisquantserver.com'))

            msg['From'] = email.utils.formataddr((self.__from_header['name'], self.__from_header['email']))
            # msg['To'] = email.utils.formataddr(('Alpesh Kalathiya', 'kalathiyaalpesh@gmail.com'),
            #                                    ('Mehedi', 'rabbi.se@gmail.com'))
            msg['To'] = COMMASPACE.join(['{} <{}>'.format(kv['name'], kv['email']) for kv in self.__to_header])
            msg['Date'] = formatdate(localtime=True)

            msg.attach(MIMEText(body, 'plain', 'utf-8'))
            msg_body = msg.as_string()

            # server = smtplib.SMTP('localhost', smtplib.SMTP_PORT)
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.set_debuglevel(True)
            server.ehlo()
            # If we can encrypt this session, do it
            if server.has_extn('STARTTLS'):
                server.starttls()
                server.login(self.__username, self.__password)
                # server.ehlo()  # re-identify ourselves over TLS connection

            # send email
            # from_address = 'support@chrisquantserver.com'
            # to_address = ['kalathiyaalpesh@gmail.com', 'rabbi.se@gmail.com']
            server.sendmail(self.__from_address, self.__to_address, msg_body)
            server.quit()
            self.__logger.info('Email sent successfully.')
        except Exception as x:
            self.__logger.error('Error when sending email: {}'.format(x))


if __name__ == '__main__':
    with QuantopianGrabber() as spider:
        spider.operation()
    print('Script finished!')
