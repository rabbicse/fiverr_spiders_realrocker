import csv
import gzip
import logging
import os
import random
import re
import readline
import socket
import time
import urllib.parse
import urllib.request
import sys
from multiprocessing.pool import ThreadPool

# import socks
from bs4 import BeautifulSoup
from multiprocessing import Lock

readline.parse_and_bind("control-v: paste")


class AmazonGrabber:
    __start_url = 'https://www.amazon.com/EnviroCare/b/ref=w_bl_hsx_s_ho_web_2591340011?ie=UTF8&node=2591340011&field-lbr_brands_browse-bin=EnviroCare'
    __base_url = 'https://www.amazon.com'
    __product_url = 'https://www.amazon.com/gp/product/{}'
    __proxies = {
        # 'http': '83.149.70.159:13042',
        # 'https': '37.48.118.90:13042'
        # 'http': '37.48.118.90:13042',
        # 'https': '83.149.70.159:13042'
        'http': '37.48.118.90:13042',
        'https': '83.149.70.159:13042'
    }
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('amazon.log')
        socket.setdefaulttimeout(60)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        # proxy_handler = urllib.request.ProxyHandler(self.__proxies)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # urllib.request.install_opener(opener)
        # opener = urllib.request.build_opener(proxy_handler)
        # urllib.request.install_opener(opener)

        self.__field_names = ['url', 'asin', 'title', 'buy_box', 'rank', 'rank_type', 'amazon', 'divys_ark']
        csv_header = {'url': 'URL',
                      'asin': 'Asin',
                      'title': 'Title',
                      'buy_box': 'Buy Box',
                      'rank': 'Rank',
                      'amazon': 'Amazon',
                      'divys_ark': 'Divys Ark'
                      }

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self.__logger
        del self

    def grab_data(self):
        try:
            self.__grab_details('https://www.amazon.com/gp/product/B00631WSHS')
            return
            for dept_url in self.__grab_all_departments():
                self.__grab_department(dept_url)
                break
                # self.__grab_details(
                #     'https://www.yellowpages.com.eg/en/profile/yellow-media,-an-egypt-yellow-pages-company/44735?b=2&k=advertising-agencies&l=category')
                # return
                # next_page = self.__grab_all_categories(self.__start_url)
                # while next_page:
                #     next_page = self.__grab_all_categories(next_page)

        except Exception as x:
            print(x)

    def __grab_all_departments(self):
        try:
            self.__logger.info('=== All Dept URL: {} ==='.format(self.__start_url))
            req = urllib.request.Request(self.__start_url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            dept_tag_list = soup.find_all('span', class_='a-list-item')
            for dept_tag in dept_tag_list:
                dept_a_tag = dept_tag.find('a')
                if not dept_a_tag:
                    continue

                dept_h4_tag = dept_a_tag.find('h4')
                if not dept_h4_tag:
                    continue

                yield self.__base_url + dept_a_tag.get('href').strip()

        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __grab_department(self, url):
        try:
            self.__logger.info('=== Dept URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            results = soup.find_all('li', id=re.compile(r'result_\d+'))
            for result in results:
                if not result.has_attr('data-asin'):
                    continue
                pid = result.get('data-asin')
                product_url = self.__product_url.format(pid)
                self.__grab_details(product_url)
                break
        except Exception as x:
            self.__logger.error('Error when grab dept: {}'.format(x))

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # print(data)

            item = {'url': url}
            title_tag = soup.find('h1', id='title')
            if title_tag:
                item['title'] = title_tag.text.strip()

            # <input type="hidden" id="ASIN" name="ASIN" value="B0088DISDM">
            asin_tag = soup.find('input', id='ASIN')
            if asin_tag:
                item['asin'] = asin_tag.get('value').strip()

            th_list = soup.find_all('th', class_='a-color-secondary a-size-base prodDetSectionEntry')
            for th in th_list:
                if 'Best Sellers Rank' in th.text.strip():
                    ctd = th.find_next_sibling('td')
                    if ctd:
                        rank_text = ctd.text.strip()
                        rank_text = rank_text.split('\n')[0].strip()
                        rank_val = re.search(r'\#([\d\,]+) in ([^\(]*?)\(', rank_text)
                        if rank_val:
                            print(rank_val.group(1))
                            print(rank_val.group(2))

            self.__logger.info('Data: {}'.format(item))
            # self.__write_data(item)
            #
            # try:
            #     self.__lock.acquire()
            #     self.__url_cache.append(url)
            # except Exception as ex:
            #     self.__logger.error('Error grab details page 2: {}'.format(ex))
            # finally:
            #     self.__lock.release()

            return True
        except Exception as x:
            self.__logger.error('Error grab details: {}'.format(x))
            if retry < 5:
                return self.__grab_details(url, retry + 1)

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'amazon_output.csv'
    with AmazonGrabber(output_file) as spider:
        spider.grab_data()
    print('Script finished!')
