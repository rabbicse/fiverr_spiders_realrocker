import csv
import gzip
import http.cookiejar
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from datetime import datetime
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class DayworkJobSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
    __base_url = 'https://www.daywork123.com'
    __start_url = 'https://www.daywork123.com/JobAnnouncementList.aspx'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        # 'Host': 'www.daywork123.com',
        # 'Origin': 'https://www.daywork123.com',
        # 'Pragma': 'no-cache',
        # 'Referer': 'https://www.daywork123.com/JobAnnouncementList.aspx'
    }

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 30
    MAX_THREAD = 4

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('daywork.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)
        return logger

    def __enter__(self):
        self.__lock = Lock()
        self.__logger = self._setup_logger()
        socket.setdefaulttimeout(30)

        hdr = [('id', 'ID'),
                ('date', 'Date Desired'),
                ('employer', 'Employer'),
                ('work_type', 'Work Type'),
                ('port', 'Port'),
                ('phone', 'Phone')]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['id'])

        # if os.path.exists('proxy.json'):
        #     with open('proxy.json', 'r+', encoding='utf-8') as f:
        #         json_data = json.load(f)
        #         self.__proxies = json_data

        # If header not yet written then write csv header first
        if self.__csv_header['id'] not in self.__url_cache:
            self.__write_data(self.__csv_header)
            self.__url_cache.append(self.__csv_header['id'])

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    if key not in opener.addheaders:
                        opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                cj = http.cookiejar.CookieJar()
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                # opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:

            url = 'http://www.immostreet.com/Agency/Search?currentPage=4500&placeId=4832011'
            self.__logger.info('Main URL: {}'.format(self.__start_url))

            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()

            response = opener.open(url)
            res_info = response.info()
            cookie = res_info['Set-Cookie']
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            print(data)

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            print(soup.prettify())

            del data


            # table = soup.find('table', id='ctl00_ContentPlaceHolder1_RepJobAnnouncement')
            # if not table:
            #     return
            #
            # tr_list = table.find_all('tr')
            # if not tr_list or len(tr_list) < 2:
            #     return
            #
            # for tr in tr_list[1:]:
            #     td_list = tr.find_all('td')
            #     if not td_list or len(td_list) < 6:
            #         continue
            #
            #     item = {'id': td_list[0].text.strip(),
            #             'date': td_list[1].text.strip(),
            #             'employer': td_list[2].text.strip(),
            #             'work_type': td_list[3].text.strip(),
            #             'port': td_list[4].text.strip(),
            #             'phone': td_list[5].text.strip()}
            #     print(item)


            event_args_table = soup.find('table', id='ctl00_ContentPlaceHolder1_cldStartDate')
            if not event_args_table:
                return

            event_args_a = event_args_table.find_all('a')
            if not event_args_a or len(event_args_a) == 0:
                return

            event_args_href = event_args_a[0].get('href')
            event_args = event_args_href.split(',')[-1]
            event_args = event_args.strip(')')
            event_args = event_args.strip('\'')
            print(event_args)
            # event_args = re.sub(r'[^0-9]', '', event_args)

            view_state_input = soup.find('input', id='__VIEWSTATE')
            if not view_state_input:
                return

            view_state = view_state_input.get('value')

            view_state_gen_input = soup.find('input', id='__VIEWSTATEGENERATOR')
            if not view_state_gen_input:
                return

            view_state_gen = view_state_gen_input.get('value')

            # __EVENTVALIDATION
            event_validation_input = soup.find('input', id='__VIEWSTATEGENERATOR')
            if not event_validation_input:
                return

            event_validation = event_validation_input.get('value')


            # next page url
            '''
            __EVENTTARGET: ctl00$ContentPlaceHolder1$cldStartDate
            __EVENTARGUMENT: 6695
            __VIEWSTATE: 
            __VIEWSTATEGENERATOR: 4E16B0A5
            __EVENTVALIDATION: '''

            post_data = {'__EVENTTARGET': 'ctl00$ContentPlaceHolder1$cldStartDate',
                         '__EVENTARGUMENT': event_args,
                         '__VIEWSTATE': view_state,
                         '__VIEWSTATEGENERATOR': view_state_gen,
                         '__EVENTVALIDATION': event_validation}

            self.__grab_by_date(opener, post_data, cookie.split(';')[0].strip())


            # After creating soup object simply dispose data to free memory
            del soup

        except Exception as x:
            self.__logger.error('Error when grab data. Error details: {}'.format(x))

    @tail_recursive
    def __grab_by_date(self, opener, post_data, cookie, retry=0):
        try:

            if 'Content-Type' not in [h[0] for h in opener.addheaders]:
                opener.addheaders.append(('Content-Type', 'application/x-www-form-urlencoded'))

            for h in opener.addheaders:
                if 'Cookie' in h[0]:
                    opener.addheaders.remove(h)
            opener.addheaders.append(('Cookie', cookie))

            print(opener.addheaders)

            self.__logger.info('Post URL: {}'.format(self.__start_url))
            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            response = opener.open(self.__start_url, data=post_req)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            print(soup.prettify())

            # After creating soup object simply dispose data to free memory
            del data
        except Exception as x:
            self.__logger.error('Error when get search results for ad. Error details: {}'.format(x))
            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_by_date(opener, post_data, cookie, retry + 1)

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = datetime.now().strftime('%y_%m_%d_%H_%M') + '_daywork123_job_output.csv'
    with DayworkJobSpider(output_file) as spider:
        spider.grab_data()
