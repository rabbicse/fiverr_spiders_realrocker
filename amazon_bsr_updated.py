import csv
import re
import socket
import urllib.request

import sys
import gzip
import datetime
from bs4 import BeautifulSoup
from anytree import Node

socket.setdefaulttimeout(60)
headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',
           'Host': 'www.amazon.co.uk',
           'Accept-Encoding': 'gzip, deflate, br',
           'Connection': 'keep-alive'}

start_url = 'https://www.amazon.co.uk/Best-Sellers-Welcome/zgbs/ref=zg_bs_unv_0_boost_1'
base_url = 'https://www.amazon.co.uk'


def navigate_top_links(url):
    try:
        print('Top URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read()
        data = gzip.decompress(data).decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        navs = soup.find('ul', id='zg_browseRoot').find('ul').find_all('li')
        for nav in navs:
            link = nav.find('a')
            if link:
                yield link.get('href').strip(), link.text.strip()

    except Exception as x:
        print(x)


def navigate_links(url):
    try:
        print('URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read()
        data = gzip.decompress(data).decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')

        selected_cat = soup.find('span', 'zg_selected')
        if not selected_cat:
            return

        li = selected_cat.find_parent('li')
        if not li:
            return

        next_ul = li.find_next_sibling('ul')
        if not next_ul:
            return

        navs = next_ul.find_all('li')
        for nav in navs:
            link = nav.find('a')
            if link:
                yield link.get('href').strip(), link.text.strip()

    except Exception as x:
        print(x)


def generate_nav_tree(node):
    try:
        tr = navigate_links(node.name[0])
        if tr:
            for link, title in tr:
                child = Node((link, title), node)
                generate_nav_tree(child)
    except Exception as x:
        print(x)


seaial_no = 0


def grab_details(url, args, price):
    try:
        global search_no
        print('Product Details URL: ' + url)

        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read()
        data = gzip.decompress(data).decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        print(soup.prettify())
        # print(soup.prettify())

        # 0 datetime
        # 1 serial
        # 2-6 no. of category= 5
        # 7 price
        # 8 rank
        # 9 weight
        # 10 title
        # 11 ASIN
        # 12 Review count
        # 13 User
        date = datetime.datetime.now().strftime('%y-%m-%d')
        csv_row = [date, '', '', '', '', '', '', price, '', '', '', '', '', 'Realrocker']

        if args and len(args) > 0:
            index = 2
            for ar in args:
                csv_row[index] = ar
                index += 1

        title_h1 = soup.find('h1', id='title')
        if title_h1:
            title = title_h1.text.strip()
            csv_row[10] = title

        user_data = soup.find('a', id='brand')
        if user_data:
            user = user_data.text.strip()
            csv_row[13] = user

        bsr = -1
        bsr_tr = soup.find('tr', id='SalesRank')
        if bsr_tr:
            bsr_m = re.search(r'(?i)([\d\,]+)\s*?in\s*?.*?\(.*?See top', bsr_tr.text.strip())
            if bsr_m:
                bsr = int(bsr_m.group(1).replace(',', ''))

        bsr_tr1 = soup.find('li', id='SalesRank')
        if bsr_tr1 and bsr == -1:
            bsr_m = re.search(r'(?i)([\d\,]+)\s*?in\s*?.*?\(.*?See top', bsr_tr1.text.strip())
            if bsr_m:
                bsr = int(bsr_m.group(1).replace(',', ''))

        if not 1 <= bsr <= 4000:
            print('BSR: ' + str(bsr))
            print('BSR not in range of 1-4000', file=sys.stderr)
            return
        csv_row[8] = bsr

        weight = 0
        div = soup.find('div', class_='wrapper GBlocale')
        table = soup.find('table', id='productDetailsTable')
        if div:
            divs = div.find_all('div', class_='pdTab')
            if divs:
                for d in divs:
                    trs = d.find_all('tr')
                    for tr in trs:
                        tds = tr.find_all('td')
                        if 'ASIN' in tds[0].text:
                            asin = tds[1].text.strip()
                            csv_row[11] = asin
                        if 'Shipping weight' in tds[0].text.strip() and weight == 0:
                            weight_m = re.search(r'(?i)(\d+)', tds[1].text.strip())
                            if weight_m:
                                weight = float(weight_m.group(1))
                        if 'Item Weight' in tds[0].text.strip():
                            weight_m = re.search(r'(?i)(\d+)', tds[1].text.strip())
                            if weight_m:
                                weight = float(weight_m.group(1))
                        if 'Customer Reviews' in tds[0].text:
                            review_m = re.search(r'(?i)(\d+)\s*?customer\s*?review.*?', tds[1].text.strip(),
                                                 re.MULTILINE)
                            if review_m:
                                review = review_m.group(1)
                                csv_row[12] = review
        elif table:
            li_list = table.find_all('li')
            if li_list:
                for li in li_list:
                    if 'ASIN:' in li.text:
                        asin = li.text.replace('ASIN:', '').strip()
                        csv_row[11] = asin
                    if 'Boxed-product Weight:' in li.text:
                        weight_m = re.search(r'(?i)(\d+)', li.text.strip())
                        if weight_m:
                            weight = float(weight_m.group(1))
                    if 'Average Customer Review:' in li.text:
                        review_m = re.search(r'(?i)(\d+)\s*?customer\s*?review.*?', li.text.strip(),
                                             re.MULTILINE)
                        if review_m:
                            review = review_m.group(1)
                            csv_row[12] = review

        if weight >= 2000:
            print('Weight: ' + str(weight))
            print('Weight exceeds max.', file=sys.stderr)
            return

        search_no += 1
        csv_row[9] = weight
        csv_row[1] = search_no
        print(csv_row)
        write_data('amazon_bsr_output.csv', csv_row)

    except Exception as x:
        print(x)


def grab_link_data(link, args, price):
    global csv_file
    try:
        # 0-4 no. of category= 5
        # 5 price
        csv_row = ['', '', '', '', '', price, link]
        if args and len(args) > 0:
            index = 0
            for ar in args:
                csv_row[index] = ar
                index += 1
        print(csv_row)
        write_data(csv_file, csv_row)
    except Exception as x:
        print(x)


def grab_listing_urls(node, url, index=1):
    try:
        print('Product Listing URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read()
        data = gzip.decompress(data).decode('utf-8')
        if not data:
            return

        args = []
        cur_node = node
        while not cur_node.is_root:
            args.append(cur_node.name[1])
            cur_node = cur_node.parent
        args.reverse()

        soup = BeautifulSoup(data, 'lxml')
        # zg_itemWrapper
        link_divs = soup.find_all('div', class_='zg_itemImmersion')
        for link_div in link_divs:
            link, price, search_no = '', '', ''
            price_span = link_div.find('span', class_='p13n-sc-price')
            if price_span:
                prices = re.sub(r'£', '', price_span.text)
                prices_sp = prices.split('-')
                price_low = prices_sp[0].strip() if len(prices_sp) == 2 else prices.strip()
                price_high = prices_sp[1].strip() if len(prices_sp) == 2 else prices.strip()
                if 14.95 <= float(price_low) <= 50.00 and 14.95 <= float(price_high) <= 50.00:
                    price = '{0:2f}'.format(float(price_low)) + ' - ' + '{0:2f}'.format(float(price_high)) if len(
                        prices_sp) > 0 else '{0:2f}'.format(float(price_low))
                    link = base_url + link_div.find('a').get('href')
                else:
                    print('Price: ' + prices)
                    print('Price not in range 14.95-50', file=sys.stderr)
                    continue

            # search_no_span = link_div.find('span', class_='zg_rankNumber')
            # if search_no_span:
            #     search_no = search_no_span.text.strip().replace('.', '')
            if price == '':
                print('Price unavailable', file=sys.stderr)
                continue

            if link == '':
                print('Link unavailable', file=sys.stderr)
                continue

            # grab_details(link, args, price)
            grab_link_data(link, args, price)

        # if '&pg=' not in url:
        # zg_page
        links_ol = soup.find('ol', class_='zg_pagination')
        if not links_ol:
            return
        paginated_pages = links_ol.find_all('li', class_='zg_page')
        if index < len(paginated_pages):
            page_url = paginated_pages[index].find('a').get('href')
            grab_listing_urls(node, page_url, index + 1)

    except Exception as x:
        print(x)


def traverse_and_grab(node):
    try:
        if not node.children:
            grab_listing_urls(node, node.name[0])

        for child in node.children:
            traverse_and_grab(child)
    except Exception as x:
        print(x)


csv_file = ''


def process_amazon_bsr():
    global csv_file

    csv_link_header = ['Top Level Category', 'Subcategory1', 'Subcategory2', 'Subcategory3', 'Subcategory4', 'Price_£',
                       'Link']

    root = Node('Root')
    sports_url = 'https://www.amazon.co.uk/Best-Sellers-Sports-Outdoors/zgbs/sports/ref=zg_bs_nav_0'
    parent = Node((sports_url, 'Sports & Outdoors'), root)

    i = 0
    for link, title in navigate_links(sports_url):
        if i > 14:
            csv_file = 'amazon_links_{}.csv'.format(i)
            write_data(csv_file, csv_link_header, mode='w')
            print('======== {} ==========='.format(title))
            parent1 = Node((link, title), parent)
            generate_nav_tree(parent1)

            traverse_and_grab(parent1)
        i += 1


def write_data(file_name, row, mode='a'):
    try:
        with open(file_name, mode, newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(row)
    except Exception as x:
        print('Error printing csv output: ' + str(x))


if __name__ == '__main__':
    process_amazon_bsr()
    # grab_listing_urls(None,
    #     'https://www.amazon.co.uk/Best-Sellers-Amazon-Launchpad-Baby/zgbs/boost/7424492031/ref=zg_bs_nav_2_7424489031/257-7654953-4872257')
    # grab_details(
    #     'https://www.amazon.co.uk/Wilson-Junior-Hypergrip-Arrow-Football/dp/B00F5X4I9G/ref=zg_bs_5104995031_13/258-6185501-4146814?_encoding=UTF8&psc=1&refRID=HZYVA8XQRE60EZVJF1Z4',
    #     (), None)
