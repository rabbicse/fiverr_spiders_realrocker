import csv
import email
import gzip
import json
import logging
import os
import random
import smtplib
import socket
import time
import urllib.parse
import urllib.request
from datetime import datetime
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from bs4 import BeautifulSoup


class VixCentralSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url = 'http://vixcentral.com'
    __start_url = 'http://vixcentral.com/historical/?days=3000'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 20

    def __init__(self, output_csv):
        self.__output_csv = './vixcentral_output/' + output_csv + '.csv' if not str(output_csv).endswith(
            '.csv') else './vixcentral_output/' + output_csv
        self.__username = None
        self.__password = None
        self.__from_address = None
        self.__to_address = None
        self.__from_header = None
        self.__to_header = None
        self.__subject = ''

    def _setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('vixcentral.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__lock = Lock()
        self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(30)

        if not os.path.exists('./vixcentral_output/'):
            os.mkdir('./vixcentral_output/')

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

        if os.path.exists('credentials.json'):
            with open('credentials.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                if json_data:
                    self.__username = json_data['username']
                    self.__password = json_data['password']
                    self.__from_address = json_data['from_address']
                    self.__to_address = json_data['to_addresses']
                    self.__from_header = json_data['from_header']
                    self.__to_header = json_data['to_header']
                    self.__subject = json_data['subject']
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self, retry=0):
        try:
            self.__logger.info('=== URL: {} ==='.format(self.__start_url))
            opener = self.__create_opener()
            data = opener.open(self.__start_url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            tr_list = soup.find_all('tr')
            for tr in tr_list:
                row = []
                td_list = tr.find_all('td')
                for td in td_list:
                    row.append(td.text.strip())
                self.__write_data(row)
        except Exception as x:
            self.__logger.error('Error when grab data. {}'.format(x))
            if retry < self.MAX_RETRY:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_data(retry + 1)

    def __send_email(self, body):
        try:
            self.__logger.info('Sending email...')
            msg = MIMEMultipart()
            msg['Subject'] = Header(self.__subject, 'utf-8')
            msg['From'] = email.utils.formataddr((self.__from_header['name'], self.__from_header['email']))
            msg['To'] = COMMASPACE.join(['{} <{}>'.format(kv['name'], kv['email']) for kv in self.__to_header])
            msg['Date'] = formatdate(localtime=True)

            msg.attach(MIMEText(body, 'plain', 'utf-8'))
            msg_body = msg.as_string()

            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.set_debuglevel(True)
            server.ehlo()
            # If we can encrypt this session, do it
            if server.has_extn('STARTTLS'):
                server.starttls()
                server.login(self.__username, self.__password)

            # send email
            server.sendmail(self.__from_address, self.__to_address, msg_body)
            server.quit()
            self.__logger.info('Email sent successfully.')
        except Exception as x:
            self.__logger.error('Error when sending email: {}'.format(x))

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'vixcentral_output.csv'
    with VixCentralSpider(output_file) as spider:
        spider.grab_data()
