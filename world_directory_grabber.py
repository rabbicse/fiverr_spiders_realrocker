import csv
import gzip
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock

from bs4 import BeautifulSoup


class WorldDirectoryGrabber:
    __lock = Lock()
    MAX_PAGE = 55537
    __category_url = 'https://www.worlddirectory.com.au/all-categories.htm'
    __region_url = 'https://www.localsearch.com.au/Regions'
    __base_url = 'https://www.worlddirectory.com.au/'
    __main_url = 'https://www.localsearch.com.au/find/{}/{}'
    __paginated_param = '?pgIndex={}&currentPage={}&pagination=100000'
    # __paginated_param = '?pgIndex={}&currentPage={}&pagination=100'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'Host': 'www.worlddirectory.com.au',
        'Upgrade-Insecure-Requests': '1'}

    __proxies = None
    __url_cache = []
    __total = 0

    def __init__(self, output_csv):
        self.__output_csv = output_csv
        socket.setdefaulttimeout(5 * 60)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # urllib.request.install_opener(opener)

    def __setup_logger(self):
        """
        Script will create log file and write std output to terminal to track bug or status
        :return:
        """
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('world_directory.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        """
        Initialize script and load required modules or initial setup of script.
        :return:
        """
        # lock object will need when we'll apply multi-threaded operation
        self.__lock = Lock()

        # Initialize/setup logger
        self.__setup_logger()

        # self.__browser = mechanicalsoup.Browser(
        #     soup_config={'features': 'html5lib'}
        # )

        hdr = [('name', 'Name'),
               ('desc', 'Description'),
               ('email', 'Email'),
               ('website', 'Website'),
               ('contact', 'Contact Number'),
               ('address', 'Address'),
               ('city', 'City'),
               ('state', 'State'),
               ('zip_code', 'Zip Code'),
               ('business_category', 'Business Category'),
               ('fax', 'Fax'),
               ('social', 'Social URLS'),
               ('url', 'URL')]
        csv_header = OrderedDict(hdr)
        self.__field_names = list(csv_header.keys())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        # if os.path.exists('proxy.json'):
        #     with open('proxy.json', 'r+', encoding='utf-8') as f:
        #         json_data = json.load(f)
        #         self.__proxies = json_data

        # If header not yet written then write csv header first
        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)
            self.__url_cache.append(csv_header['url'])

        # Calculate previous records for debugging purpose
        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            if proxy['https'] == 'yes':
                return {'https': '{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': '{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                self.__logger.info('Proxy: {}'.format(proxy))
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def grab_data(self):
        try:
            # d_url = 'https://www.worlddirectory.com.au/4444642-channel-manager'#'https://www.worlddirectory.com.au/176415-systronic-it-services'
            # self.__grab_details(d_url)
            # return
            # categories = self.__read_category()
            # print(categories)
            # regions = self.__read_region()
            # print(regions)

            # for category in categories:
            #     for region in regions:
            #         url = self.__main_url.format(category, region)
            #         self.__grab_details(url)
            # grab category
            for link, title in self.__grab_categories():
                has_next = self.__grab_pages(link, title)
                page = 1
                while has_next:
                    page += 1
                    has_next = self.__grab_pages(link, title, page)

                # grab regions
                # self.__grab_regions()

                # grab details
                # url = 'https://www.localsearch.com.au/find/plumbers/nt/alice-springs-region'
                # self.__grab_details(url)


                # csv_header = ['Name', 'Email', 'URL']
                # self.__write_data(csv_header)
                # for i in range(1, self.MAX_PAGE, 8):
                #     # start 4 worker processes
                #     with Pool(processes=8) as pool:
                #         pool.map(self.pool_data, range(i, i + 8))
        except Exception as x:
            print(x)

    def __grab_categories(self, retry=0):
        try:
            self.__logger.info('=== Category URL: {} ==='.format(self.__category_url))
            # req = urllib.request.Request(self.__category_url, headers=self.__headers)
            # data = urllib.request.urlopen(req).read()
            # data = data.decode('utf-8')
            # if not data:
            #     return
            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()
            response = opener.open(self.__category_url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            ul_list = soup.find_all('ul', class_='categories clearfix')
            for ul in ul_list:
                li_list = ul.find_all('li')
                for li in li_list:
                    li_a = li.find('a')

                    link = li_a.get('href').split('/')[-1].strip()
                    title = li_a.text.strip()
                    link = self.__base_url + str(link)
                    yield link, title
        except Exception as x:
            self.__logger.error('Error grab categories: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_pages(retry + 1)

    def __grab_pages(self, link, title, page=1, retry=0):
        try:
            url = link + self.__paginated_param.format(page, page)
            self.__logger.info('Main URL: {}'.format(url))
            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()
            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            listings = soup.find('div', class_='middle_boxes clearfix')
            if not listings:
                return

            article_list = listings.find_all('div', class_='middle_boxes_1')
            if not article_list or len(article_list) == 0:
                return

            for article in article_list:
                try:
                    name, data_link, email, website, contact = '', '', '', '', ''
                    h1_tag = article.find('h1')
                    if h1_tag:
                        name = h1_tag.text.strip()
                        name = re.sub(r'^\d+\s*?\)', '', name)
                        name = name.strip()
                        link_tag = h1_tag.find('a')
                        if link_tag:
                            data_link = str(link_tag.get('href')).strip()
                            if data_link.startswith('//'):
                                data_link = 'https:' + data_link
                            else:
                                data_link = self.__base_url + data_link

                            if data_link in self.__url_cache:
                                self.__logger.warning('URL: {} already grabbed!'.format(data_link))
                                continue

                    email_div = article.find('div', class_='message_contact')
                    if email_div:
                        email_link = email_div.find('a')
                        if email_link:
                            email = email_link.get('href').strip()
                            email = re.sub(r'mailto\:', '', email)

                    website_div = article.find('div', class_='web_contact')
                    if website_div:
                        web_link = website_div.find('a')
                        if web_link:
                            website = web_link.get('href').strip()

                    contact_div = article.find('div', class_='phone_contact')
                    if contact_div:
                        contact = contact_div.text.strip()

                    item = {'url': data_link, 'name': name, 'email': email, 'website': website, 'contact': contact,
                            'business_category': title}

                    desc_address_div = article.find('div', class_='middle_boxes_inner_left')
                    if desc_address_div:
                        desc_address_p = desc_address_div.find('p')
                        if desc_address_p:
                            desc_address_parts = desc_address_p.find_all(text=True)

                            desc_text, address_text = '', ''
                            if desc_address_parts and len(desc_address_parts) > 1:
                                desc_text = desc_address_parts[0].strip()
                                item['desc'] = desc_text
                                address_text = desc_address_parts[1].strip()
                            elif desc_address_parts and len(desc_address_parts) > 0:
                                address_text = desc_address_parts[0].strip()

                            address = re.sub(r'\n', '\t', address_text)
                            address = re.sub(r'\t+', ' ', address)
                            address = re.sub(r'[ ]+', ' ', address)
                            item['address'] = address

                            csz = address_text.split('\n')[-1]
                            csz_parts = csz.split(',')
                            if csz_parts and len(csz_parts) > 1:
                                csz_text = csz_parts[-2]
                                # print(csz_text)
                                zip_m = re.search(r'(\d+)$', csz_text)
                                if zip_m:
                                    zip = zip_m.group(1).strip()
                                    item['zip_code'] = zip
                                    csz_text = csz_text.replace(zip, '').strip()

                                city_state_parts = csz_text.split()
                                if city_state_parts and len(city_state_parts) > 0:
                                    state = city_state_parts[-1]
                                    city = ' '.join(city_state_parts[:-1])
                                    item['state'] = state
                                    item['city'] = city

                    # Finally write item
                    self.__write_item(item)
                except Exception as ex:
                    self.__logger.error('Error when process data: {}'.format(ex))

            has_next = soup.find('a', class_='page-numbers next')

            # Dispose soup object
            del soup

            return has_next is not None
        except Exception as x:
            self.__logger.error('Error grab details page: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_pages(link, title, page, retry + 1)

    def __grab_details(self, url, retry=0):
        """
        It'll take title and website as args and search keywords
        :param url:
        :param retry:
        :return:
        """
        try:
            self.__logger.info('Details URL: {}'.format(url))
            # Create opener object
            # open url
            # we'll get redirected url from response
            opener = self.__create_opener()
            response = opener.open(url)
            redirected_url = response.geturl()
            self.__logger.info('Redirected URL: {}'.format(redirected_url))

            # Read data from response
            # We've specified gzip on request header for faster download
            # If server doesn't support gzip then it should return data without compression.
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # If blank data then return
            if not data:
                return

            # Dispose opener object to free memory
            del opener

            # Create BeautifulSoup object with html5lib parser
            # If script cannot create soup object then return
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            # After creating soup object simply dispose data to free memory
            del data

            # Init item
            item = {'url': url}

            # name
            desc_tag = soup.find('meta', {'name': 'description'})
            if desc_tag:
                item['desc'] = desc_tag.text.strip()

            # address
            address_tag = soup.find('div', class_='addrss_contact')
            if address_tag:
                address_parts = address_tag.find_all(text=True)
                if address_parts and len(address_parts) > 0:
                    csz = address_parts[-1]
                    csz_parts = csz.split(',')
                    if csz_parts and len(csz_parts) > 1:
                        csz_text = csz_parts[-2]
                        zip_m = re.search(r'(\d+)$', csz_text)
                        if zip_m:
                            zip = zip_m.group(1).strip()
                            item['zip_code'] = zip
                            csz_text = csz_text.replace(zip, '').strip()

                        city_state_parts = csz_text.split()
                        if city_state_parts and len(city_state_parts) > 0:
                            state = city_state_parts[-1]
                            city = ' '.join(city_state_parts[:-1])
                            item['state'] = state
                            item['city'] = city

                address = address_tag.text.strip()
                item['address'] = address


            # Dispose soup object
            del soup

            # Finally write item
            # self.__write_item(item)
        except Exception as x:
            self.__logger.error('Error when grab details for URL: [{}]. {}'.format(url, x))

            # If any error occur then it'll try after 1-5 seconds randomly with maximum retries.
            if retry < 5:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __write_item(self, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            self.__logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, row, mode='a+'):
        """
        Write data to csv file as dictionary object,
        quoting=csv.QUOTE_ALL, delimiter=';'
        :param row:
        :param mode:
        :return:
        """
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            pass
            self.__logger.error('Error printing csv output: {}'.format(x))

    @staticmethod
    def __write_category(category):
        try:
            with open('category.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __write_region(category):
        try:
            with open('region.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __read_category():
        try:
            categories = []
            with open('category.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    category = line.strip()
                    categories.append(category)
            return categories
        except Exception as x:
            print(x)

    @staticmethod
    def __read_region():
        try:
            regions = []
            with open('region.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    region = line.strip()
                    regions.append(region)
            return regions
        except Exception as x:
            print(x)


if __name__ == '__main__':
    with WorldDirectoryGrabber('world_directory_output_09_08.csv') as spider:
        spider.grab_data()
