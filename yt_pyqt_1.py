import time
from PyQt5.QtCore import QUrl


def render(url):
    """Fully render HTML, JavaScript and all."""

    import sys
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtWebEngineWidgets import QWebEngineView

    class Render(QWebEngineView):
        def __init__(self, html):
            self.html = None
            self.app = QApplication(sys.argv)
            QWebEngineView.__init__(self)
            self.loadFinished.connect(self._loadFinished)
            self.setUrl(QUrl(url))
            self.app.exec_()

        def _loadFinished(self, result):
            # what's going on here? how can I get the HTML from toHtml?
            # self.page().MediaVideoCapture()
            # time.sleep(10)
            #            # while(inbox_divs.length === 0){
            # console.log('ok....');
            # inbox_divs = document.getElementsByClassName('inbox');
            # alert("OK...");
            # var isReady = setInterval(function(){
            # var inbox_divs = document.getElementsByClassName('inbox');
            #     if(inbox_divs.length > 0){
            #         clearInterval(isReady);}
            #     }, 100);
            print('loaded........')

            # time.sleep(10)

            js = """
            function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}

var inbox_divs = document.getElementsByClassName('inbox');
while(inbox_divs.length > 0){
wait(5000);
inbox_divs = document.getElementsByClassName('inbox');}
            }"""

            js_1 = """document.getElementsByClassName("inbox").length"""
            self.page().runJavaScript(js_1, self.__js_callback)

        def __js_callback(self, data):
            if data == 0:
                print(data)
                js_1 = """document.getElementsByClassName("inbox").length"""
                self.page().runJavaScript(js_1, self.__js_callback)
            else:
                self.page().toHtml(self.callable)

        def callable(self, data):
            self.html = data
            self.app.quit()

    return Render(url).html


# https://www.facebook.com/%ED%81%AC%EB%A1%9C%EC%8A%A4%ED%95%8F-%EC%A3%BC%EC%95%88-963017323720786/
# print(render('https://www.youtube.com/watch?v=fWNaR-rxAic'))
# print(render('https://www.facebook.com/%ED%81%AC%EB%A1%9C%EC%8A%A4%ED%95%8F-%EC%A3%BC%EC%95%88-963017323720786/'))
print(render(
    'http://www.mydgolyan.com/%D7%A7%D7%98%D7%92%D7%95%D7%A8%D7%99%D7%95%D7%AA/%D7%99%D7%95%D7%93%D7%90%D7%99%D7%A7%D7%94/%D7%9B%D7%95%D7%A1%D7%95%D7%AA-%D7%A7%D7%99%D7%93%D7%95%D7%A9/'))
