import csv
import gzip
import logging
import os
import random
import re
import readline
import socket
import time
import urllib.parse
import urllib.request
import sys
from multiprocessing.pool import ThreadPool

# import socks
from bs4 import BeautifulSoup
from multiprocessing import Lock

readline.parse_and_bind("control-v: paste")


class YellowPagesEzGrabber:
    '''
    Get this details
Name
Address
Contact number
Website
Business category
Social media urls if given
https://www.yellowpages.com.eg/en/related-categories
    '''
    __start_url = 'https://www.yellowpages.com.eg/en/related-categories'
    __base_url = 'https://www.yellowpages.com.eg'
    __proxies = (
        # 'http': '83.149.70.159:13042',
        # 'https': '37.48.118.90:13042'
        # 'http': '37.48.118.90:13042',
        # 'https': '83.149.70.159:13042'
        '37.48.118.90:13042',
        '83.149.70.159:13042'
    )
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __random_proxy(self):
        return random.randint(0, len(self.__proxies) - 1)

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('yellow_pages_ez.log')
        socket.setdefaulttimeout(60)
        # socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
        # socket.socket = socks.socksocket

        # proxy_handler = urllib.request.ProxyHandler(self.__proxies)
        # cookie_handler = urllib.request.HTTPCookieProcessor()
        # opener = urllib.request.build_opener(cookie_handler)
        # urllib.request.install_opener(opener)
        # opener = urllib.request.build_opener(proxy_handler)
        # urllib.request.install_opener(opener)

        self.__field_names = ['name', 'address', 'phone', 'fax', 'website', 'business_category', 'social_urls', 'url']
        csv_header = {'name': 'Name',
                      'address': 'Address',
                      'phone': 'Phone',
                      'fax': 'Fax',
                      'website': 'Website',
                      'business_category': 'Business Category',
                      'social_urls': 'Social Media URLs',
                      'url': 'URL'
                      }

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self.__logger
        del self

    def grab_data(self):
        try:
            # self.__grab_details(
            #     'https://www.yellowpages.com.eg/en/profile/yellow-media,-an-egypt-yellow-pages-company/44735?b=2&k=advertising-agencies&l=category')
            # return
            next_page = self.__grab_all_categories(self.__start_url)
            while next_page:
                next_page = self.__grab_all_categories(next_page)

        except Exception as x:
            print(x)

    def __grab_all_categories(self, url, retry=0):
        try:
            self.__logger.info('=== All Category URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)

            proxy_index = self.__random_proxy()
            if proxy_index:
                proxy = self.__proxies[proxy_index]
                req.set_proxy(proxy, 'http')

            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            search_results_tag = soup.find('div', class_='wrapper search-results')
            if not search_results_tag:
                return

            link_tags = search_results_tag.find_all('a')
            for link_tag in link_tags:
                link = link_tag.get('href')
                if '/en/category/' not in link:
                    continue
                category_link = self.__base_url + link
                next_category_url = self.__grab_category(category_link)
                while next_category_url:
                    next_category_url = self.__grab_category(next_category_url)

            next_url_tag = soup.find('a', {'aria-label': 'Next'})
            if next_url_tag:
                return self.__base_url + next_url_tag.get('href')

        except Exception as x:
            self.__logger.error('Error grab all categories: {}'.format(x))
            if retry < 5:
                return self.__grab_all_categories(url, retry + 1)

    def __grab_category(self, url, retry=0):
        try:
            self.__logger.info('=== Category URL: {} ==='.format(url))

            req = urllib.request.Request(url, headers=self.__headers)
            proxy_index = self.__random_proxy()
            if proxy_index:
                proxy = self.__proxies[proxy_index]
                req.set_proxy(proxy, 'http')

            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            company_tags = soup.find_all('a', class_='companyName')
            links = []
            for company_tag in company_tags:
                com_link = company_tag.get('href')
                if 'https:' not in com_link:
                    com_link = 'https:' + com_link
                    # self.__grab_details(com_link)
                    if com_link not in self.__url_cache:
                        # item = {'url': link}
                        links.append(com_link)
                        # if not self.__grab_details(link):
                        #     self.__logger.warning('Failed to grab details...')
                        #     name = link_tag.text.strip()
                        #     self.__parse_details(data_tag, link, name)
                    else:
                        self.__logger.warning('Already grabbed URL: {}.'.format(com_link))

            with ThreadPool(16) as p:
                p.map(self.__grab_details, links)

            next_url_tag = soup.find('a', {'aria-label': 'Next'})
            if next_url_tag:
                return self.__base_url + next_url_tag.get('href')
        except Exception as x:
            self.__logger.error('Error grab main page: {}'.format(x))
            if retry < 5:
                return self.__grab_category(url, retry + 1)

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            proxy_index = self.__random_proxy()
            if proxy_index:
                proxy = self.__proxies[proxy_index]
                req.set_proxy(proxy, 'http')

            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            # print(data)

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # print(data)

            item = {'url': url}
            name_ul = soup.find('ul', class_='breadcrumb')
            if name_ul:
                name_lis = name_ul.find_all('li')
                if name_lis:
                    item['name'] = name_lis[-1].text.strip()
                    item['business_category'] = name_lis[-2].text.strip()

            address_tags = soup.find_all('div', class_='col-xs-12 address')
            if address_tags:
                addresses = []
                for address_tag in address_tags:
                    address = address_tag.text.replace('Address', '').strip().strip(':')
                    address = re.sub(r'\r', ' ', address)
                    address = re.sub(r'\n', ' ', address)
                    address = re.sub(r'[ ]+', ' ', address)
                    addresses.append(address)
                item['address'] = ' '.join(addresses).strip()

            website_tag = soup.find('a', class_='openWebsite')
            if website_tag:
                item['website'] = website_tag.text.strip()

            social_link_tags = soup.find('span', class_='socialLinks')
            if social_link_tags:
                social_links = []
                link_tags = social_link_tags.find_all('a')
                if link_tags:
                    for link in link_tags:
                        social_links.append(link.get('href'))
                item['social_urls'] = ', '.join(social_links)

            phone_tag = soup.find('a', {'data-load-target': '.phonesPlaceHolder'})
            if phone_tag and phone_tag.has_attr('data-link'):
                phone_link = phone_tag.get('data-link').replace('true', '')
                phone_url = self.__base_url + phone_link
                item['phone'] = self.__retrieve_phone(phone_url)

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

            return True
        except Exception as x:
            self.__logger.error('Error grab details: {}'.format(x))
            if retry < 5:
                return self.__grab_details(url, retry + 1)

    def __retrieve_phone(self, url, retry=0):
        try:
            self.__logger.info('=== Phone URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            proxy_index = self.__random_proxy()
            if proxy_index:
                proxy = self.__proxies[proxy_index]
                req.set_proxy(proxy, 'http')

            data = urllib.request.urlopen(req).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return ''

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return ''

            all_phone_tag = soup.find('span', class_='listPhones')
            if all_phone_tag:
                phones = []
                phone_tags = all_phone_tag.find_all('span')
                for phone_tag in phone_tags:
                    phones.append(phone_tag.text.strip())
                return ', '.join(phones)
        except Exception as x:
            self.__logger.error('Error grab details: {}'.format(x))
            if retry < 3:
                self.__retrieve_phone(url, retry + 1)
        return ''

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'yellow_pages_ez_output.csv'
    with YellowPagesEzGrabber(output_file) as spider:
        spider.grab_data()
    print('Script finished!')
