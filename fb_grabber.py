import csv
import gzip
import html
import random
import time
import urllib.parse
import urllib.request
from multiprocessing import Process, Lock
from multiprocessing.pool import Pool
from tkinter import *
from tkinter import filedialog, messagebox

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Host': 'www.facebook.com',
    'Referer': 'https://www.facebook.com/',
    'Connection': 'keep-alive',
    'Accept-Encoding': 'gzip',
    'Upgrade-Insecure-Requests': '1'}

lock = Lock()
proxies = []
count = 0

redirect_handler = urllib.request.HTTPRedirectHandler()
opener = urllib.request.build_opener(redirect_handler)
urllib.request.install_opener(opener)


def print_count():
    global count
    count += 1
    print('Processed {}'.format(count))


def grab_fb_data(url, proxy=None):
    try:
        # print(url)
        # grab_about_page(url, proxy)
        about_url = url.strip('/')
        about_url = about_url.replace('#!/', '')
        about_url = re.sub(r'\?[^\$]*?$', '', about_url)
        about_url = about_url.strip('/')

        about_url = about_url + '/about/?ref=page_internal'
        print('=== URL: ' + about_url + ' ===')
        if proxy:
            print('Proxy: ' + proxy)

        # 1-5 sec random sleep
        time.sleep(random.randint(1, 5))

        req = urllib.request.Request(about_url, headers=headers)
        if proxy:
            req.set_proxy(proxy, 'https')

        response = urllib.request.urlopen(req).read()
        try:
            data = gzip.decompress(response).decode('utf-8', 'ignore')
        except Exception as e:
            print(e)
            data = response.decode('utf-8', 'ignore')

        return data
    except Exception as x:
        print('Error when grab data.' + str(x))


def grab_about_page(url, proxy=None):
    try:
        req = urllib.request.Request(url, headers=headers)
        if proxy:
            req.set_proxy(proxy, 'https')

        response = urllib.request.urlopen(req)
        response_data = response.read()

        print(response.geturl())
        print(response.info())
        try:
            data = gzip.decompress(response_data).decode('utf-8', 'ignore')
        except Exception as e:
            print(e)
            data = response_data.decode('utf-8', 'ignore')

            # print(data)
    except Exception as x:
        print(x)


def parse_email(data):
    try:
        pattern = r'<a href="mailto\:([^\?]*?)\?'
        m = re.search(pattern, data, re.MULTILINE)
        if m:
            email = m.group(1)
            email = urllib.parse.unquote_plus(email, encoding='utf-8')
            email = html.unescape(email)
            return email.strip()
    except Exception as x:
        print('Error when parse email from FB: ' + str(x))


def fb_operation(input_csv, output_csv, proxy_file=None):
    try:
        if proxy_file:
            with open(proxy_file, 'r', encoding='utf-8') as f:
                for line in f:
                    proxy = line.strip()
                    print(proxy)
                    proxies.append(proxy)

        with open(output_csv, 'w', newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(['Facebook URL', 'Email'])

        # pool = Pool()
        # pool.starmap(process_url,
        #              [(url, output_csv, proxy) for (url, proxy) in read_input_csv(input_csv) if url is not None])
        #
        # print('Finish!!!')

        for row in read_input_csv(input_csv):
            # grab data with proxy
            proxy = None
            if len(proxies) > 0:
                proxy = proxies[random.randint(0, len(proxies) - 1)]

            url = str(row[0]).strip()

            process_url(url, output_csv, proxy)
            # Process(target=process_url, args=(url, output_csv, proxy)).start()

            time.sleep(random.randint(1, 5))

        print('Finish!!')
    except Exception as x:
        print('Error when processing urls: ' + str(x))


def read_input_csv(input_csv):
    try:
        with open(input_csv, 'r', encoding='utf-8') as f:
            reader = csv.reader(f)
            for row in reader:
                if not row or len(row) == 0:
                    print('Invalid row...')
                    continue

                if 'https' not in row[0]:
                    print('wrong data...')
                    continue
                url = str(row[0]).strip()
                proxy = proxies[random.randint(0, len(proxies) - 1)] if len(proxies) > 0 else None
                yield url, proxy
    except Exception as x:
        print('Error when read csv file: ' + str(x))


def process_url(url, output_csv, proxy):
    try:
        data = grab_fb_data(url, proxy=proxy)
        if not data:
            print('Failed to grab facebook data.')
            write_data(output_csv, [url, ''])
            return

        email = parse_email(data)
        if email:
            print('Writing email address to csv: ' + email)
            write_data(output_csv, [url, email])
            return True
        else:
            print('Failed to parse email address from url: ' + url)
            write_data(output_csv, [url, ''])
    except Exception as x:
        print('Error when processing data: ' + str(x))


def write_data(file_name, row):
    try:
        lock.acquire()
        with open(file_name, 'a', newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(row)
            # print_count()
    except Exception as x:
        print('Error printing csv output: ' + str(x))
    finally:
        lock.release()


class Application(Frame):
    __input_csv = None
    __output_csv = None
    __proxy_file = None
    __process = None

    def __init__(self, master=None):
        super().__init__(master)
        self._master = master
        self.create_widgets()
        self._master.protocol("WM_DELETE_WINDOW", self.on_closing)

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            print('closing...')
            if self.__process:
                self.__process.terminate()
            self._master.destroy()

    def create_widgets(self):
        width = 350
        height = 200
        screen_width = self._master.winfo_screenwidth()
        screen_height = self._master.winfo_screenheight()

        # calculate position x and y coordinates
        x = (screen_width / 2) - (width / 2)
        y = (screen_height / 2) - (height / 2)
        self._master.geometry('%dx%d+%d+%d' % (width, height, x, y))

        self._lbl_in_csv = Label(self._master, text='Input CSV: ')
        self._lbl_in_csv.grid(row=0, column=0)
        self._btn_in_choose = Button(self._master, text='Choose', command=self.choose_in_csv_action)
        self._btn_in_choose.grid(row=0, column=1)

        self._lbl_out_csv = Label(self._master, text='Output CSV: ')
        self._lbl_out_csv.grid(row=1, column=0)
        self._btn_out_choose = Button(self._master, text='Choose', command=self.choose_out_csv_action)
        self._btn_out_choose.grid(row=1, column=1)

        self._lbl_proxy_list = Label(self._master, text='Proxy List: ')
        self._lbl_proxy_list.grid(row=2, column=0)
        self._btn_proxy_choose = Button(self._master, text='Choose', command=self.choose_proxy_list_action)
        self._btn_proxy_choose.grid(row=2, column=1)

        self._btn_grab = Button(self._master, text='Start Grab', command=self.start_grab_fb)
        self._btn_grab.grid(row=3, column=1)

    def choose_in_csv_action(self):
        file_name = filedialog.askopenfilename(initialdir="~/", title="Select file",
                                               filetypes=(("csv files", "*.csv"), ("All Files", "*.*")))
        self.__input_csv = file_name
        print(self.__input_csv)

    def choose_out_csv_action(self):
        file_name = filedialog.asksaveasfilename(initialdir="~/", title="Select file",
                                                 filetypes=(("csv files", "*.csv"), ("All Files", "*.*")))
        self.__output_csv = file_name if file_name and str(file_name).strip().endswith('.csv') else None
        print(self.__output_csv)

    def choose_proxy_list_action(self):
        file_name = filedialog.askopenfilename(initialdir="~/", title="Select file",
                                               filetypes=(("text files", "*.txt"), ("All Files", "*.*")))
        self.__proxy_file = file_name
        print(self.__proxy_file)

    def start_grab_fb(self):
        # self.__input_csv = './fb.csv'
        # self.__output_csv = './fb_out.csv'
        if not self.__input_csv:
            messagebox.showwarning('Warning', 'Please specify input csv!')
            return
        if not self.__output_csv:
            messagebox.showwarning('Warning', 'Please specify output csv!')
            return

        self.__process = Process(target=self.fb_operation_async)
        self.__process.start()

    def fb_operation_async(self):
        fb_operation(self.__input_csv, self.__output_csv, self.__proxy_file)


def gui():
    root = Tk()
    root.resizable(0, 0)
    app = Application(master=root)
    app.mainloop()


if __name__ == '__main__':
    gui()
    # pass
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-i', '--input', help='Please specify input csv file!')
    # parser.add_argument('-o', '--output', help='Please specify output csv file!')
    # args = parser.parse_args()
    #
    # if not args.input:
    #     print('Please specify input csv!')
    #     sys.exit(0)
    #
    # if not args.output:
    #     print('Please specify output csv!')
    #     sys.exit(0)
    #
    # fb_operation(args.input, args.output)
