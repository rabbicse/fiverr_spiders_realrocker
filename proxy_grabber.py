import csv
import gzip
import json
import socket
import urllib.request
import urllib.parse

from bs4 import BeautifulSoup

socket.setdefaulttimeout(2)

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    # 'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive'}


def check_proxy(proxy):
    try:
        return True
        print('Check proxy: {}'.format(proxy))
        proxy_handler = urllib.request.ProxyHandler(proxy)
        opener = urllib.request.build_opener(proxy_handler,
                                             urllib.request.HTTPCookieProcessor(),
                                             urllib.request.UnknownHandler(),
                                             urllib.request.HTTPHandler(),
                                             urllib.request.HTTPSHandler(),
                                             urllib.request.HTTPRedirectHandler(),
                                             urllib.request.HTTPDefaultErrorHandler(),
                                             urllib.request.HTTPErrorProcessor())

        url = 'https://github.com'
        data = opener.open(url).read()
        try:
            data = gzip.decompress(data).decode('utf-8', 'ignore')
        except:
            data = data.decode('utf-8', 'ignore')

        if not data:
            return

        print('Status: OK')
        return True
    except Exception as x:
        print(x)


def grab_proxy():
    try:
        url = 'https://www.us-proxy.org/'  # 'https://free-proxy-list.net/'
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read()
        data = data.decode('utf-8', 'ignore')
        soup = BeautifulSoup(data, 'html5lib')

        proxy_table = soup.find('table', id='proxylisttable').find_all('tr')
        if proxy_table:
            data = []
            for proxy in proxy_table[1:]:
                columns = proxy.find_all('td')

                if len(columns) < 8:
                    continue

                ip = columns[0].text.strip()
                port = columns[1].text.strip()
                https = columns[-2].text.strip()

                proxy = ''
                if https == 'yes':
                    proxy = {'https': 'https://{}:{}'.format(ip, port)}
                else:
                    proxy = {'http': 'http://{}:{}'.format(ip, port)}

                if check_proxy(proxy):
                    data.append({'ip': ip, 'port': port, 'https': https})

            with open('proxy.json', 'w+') as outfile:
                json.dump(data, outfile)

            with open('proxylist.csv', 'w+') as cf:
                cw = csv.writer(cf)
                for d in data:
                    if d['https'] == 'no':
                        cw.writerow([d['ip'] + ':' + d['port']])

                # with open('proxy.txt', 'w+') as f:
                #     for proxy in proxy_table[1:]:
                #         columns = proxy.find_all('td')
                #
                #         if len(columns) < 8:
                #             continue
                #
                #         http = 'http'
                #         if columns[-2].text == 'yes':
                #             http = 'https'
                #
                #         f.write(columns[0].text.strip() + ' ' + columns[1].text.strip() + ' ' + http + '\n')
    except Exception as x:
        print(x)


if __name__ == '__main__':
    grab_proxy()
