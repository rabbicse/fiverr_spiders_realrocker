import sys
from OpenGL import GL
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEnginePage
from PyQt5.QtWidgets import QApplication


class MyBrowser(QWebEnginePage):
    ''' Settings for the browser.'''

    def userAgentForUrl(self, url):
        ''' Returns a User Agent that will be seen by the website. '''
        return "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"


class Browser(QWebEngineView):
    def __init__(self):
        # QWebView
        self.view = QWebEngineView.__init__(self)
        # self.view.setPage(MyBrowser())
        self.setWindowTitle('Youtube')
        self.titleChanged.connect(self.adjustTitle)
        self.loadFinished.connect(self._load_finished)
        # super(Browser).connect(self.ui.webView,QtCore.SIGNAL("titleChanged (const QString&)"), self.adjustTitle)

    def load(self, url):
        self.setUrl(QUrl(url))

    def adjustTitle(self):
        self.setWindowTitle(self.title())

    def _load_finished(self):
        self.__loaded = True
        print('Loaded...')
        try:
            print(self.page().url())

            self.page().toHtml(self.processHtml)
        except Exception as x:
            print(x)
            # print(self.page().toHtml())
            # self.page().view()

    def processHtml(self, html):
        print(html)
        # self.page().runJavaScript("""document.getElementById('whatwho').value = 'plumber'""")
        # self.page().runJavaScript("""document.getElementById('where').value = 'toronto'""")
        # self.page().runJavaScript("""document.getElementsByName('search_button')[0].click()""")



app = QApplication(sys.argv)
view = Browser()
view.show()
# view.load("https://www.yellowpages.ca/")
view.load("https://www.facebook.com/#!/OxfordCrossFit")
app.exec_()
