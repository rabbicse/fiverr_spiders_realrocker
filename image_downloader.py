# -*- coding: utf-8 -*-

import csv
import gzip
import http.cookiejar
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup


class Recurse(Exception):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def recurse(*args, **kwargs):
    raise Recurse(*args, **kwargs)


def tail_recursive(f):
    def decorated(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recurse as r:
                args = r.args
                kwargs = r.kwargs
                continue

    return decorated


class ImageDownloader:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
    __base_url = 'http://www.immostreet.com'
    # __start_url = 'http://www.immostreet.com/Agency/Search?currentPage={}&placeId=4832011'
    # __start_url = 'http://www.immostreet.com/Agency/Search?currentPage={}&placeId=4828617'
    __start_url = 'http://www.immostreet.com/Agency/Search?currentPage={}&placeId={}'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Accept-Language': 'en-US,en;q=0.9',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'Host': 'www.webscrapingexpert.com',
        'Pragma': 'no-cache'
    }

    __proxies = None
    __url_cache = []
    __total = 0
    MAX_RETRY = 5
    MAX_THREAD = 32

    def __init__(self, input_csv):
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv

    def _setup_logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('image_downloader.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)
        return logger

    def __enter__(self):
        self.__lock = Lock()
        self.__logger = self._setup_logger()
        socket.setdefaulttimeout(100)

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        if os.path.exists(self.__input_csv):
            with open(self.__input_csv, 'r+', encoding='utf-8') as f:
                reader = csv.reader(f)
                for index, row in enumerate(reader):
                    if index == 0:
                        continue
                    self.__url_cache.append(row[-1].strip())
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    if key not in opener.addheaders:
                        opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                cj = http.cookiejar.CookieJar()
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_data(self):
        try:
            for index, url in enumerate(self.__url_cache):
                try:
                    self.__logger.info('Downloading image: {}'.format(url))

                    # Create opener object
                    # open url
                    # we'll get redirected url from response
                    opener = self.__create_opener()

                    response = opener.open(url)
                    redirected_url = response.geturl()
                    self.__logger.info('Redirected URL: {}'.format(redirected_url))

                    res_info = response.info()
                    print(res_info)

                    # Read data from response
                    # We've specified gzip on request header for faster download
                    # If server doesn't support gzip then it should return data without compression.
                    data = response.read()
                    if not data:
                        continue

                    ext = 'jpg'
                    if 'Content-Type' in res_info:
                        content_type = res_info['Content-Type']
                        if content_type == 'image/png':
                            ext = 'png'

                    with open('./data_images/image_{}.{}'.format(index, ext), 'wb') as f:
                        f.write(data)
                except Exception as ex:
                    self.__logger.error('Error when download image from {}. Details: {}'.format(url, ex))
        except Exception as x:
            self.__logger.error('Error when grab data. Error details: {}'.format(x))


if __name__ == '__main__':
    input_file = 'data.csv'
    with ImageDownloader(input_file) as spider:
        spider.grab_data()
