import csv
import platform
import gzip
import html
import json
import logging
import os
import random
import re
import readline
import socket
import urllib.parse
import urllib.request
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool
from OpenGL import GL
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

import sys
from PyQt5.QtCore import QUrl, QTimer
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings, QWebEngineProfile
from PyQt5.QtWidgets import QApplication

import time
from bs4 import BeautifulSoup

readline.parse_and_bind("control-v: paste")


class Browser(QWebEngineView):
    __redirected_url = None
    __html = None
    __clicked = False
    __success_count = 0

    def __init__(self, app):
        QWebEngineView.__init__(self)
        self.__app = app
        self.settings().setAttribute(QWebEngineSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.loadFinished.connect(self._load_finished)
        self.__loaded = False

    def fetch(self, login_url, details_url):
        try:
            self.__details_url = details_url
            self.__login_page = True
            self.timeout_timer = QTimer()
            self.timeout_timer.timeout.connect(self._request_timed_out)
            #
            self.timeout_timer.start(120 * 1000)

            self.setUrl(QUrl(login_url))
            self.resize(800, 600)
            # self.show()
            self.__app.exec_()
            return self.__redirected_url, self.__html
        except Exception as x:
            print(x)
        return self.__redirected_url, self.__html

    def _request_timed_out(self):
        try:
            print('Custom request timeout value exceeded.')
            self.timeout_timer.stop()
            # self.stop()
            # self.loadFinished.emit(False)
            if not self.__loaded:
                print('Not loaded...')
            else:
                print('loaded...')

            self.__app.quit()
        except Exception as x:
            print(x)

    def _load_finished(self):
        try:
            if self.__login_page:
                self.__run_js_login()
                return

            current_url = str(self.page().url().toString())
            print('Redirected URL: {}'.format(current_url))
            if current_url == 'https://www.quantopian.com/posts':
                self.setUrl(QUrl(self.__details_url))
            else:
                time.sleep(5)
                self.__run_js_position_count()
        except Exception as x:
            print(x)
            self.__app.quit()

    def __run_js_login(self):
        fill_user = """document.getElementById("user_email").value='{}'""".format('arthchrisjr@yahoo.com')
        fill_pass = """document.getElementById("user_password").value='{}'""".format('Loopcap1')
        submit = """document.getElementById("login-button").click()"""
        self.page().runJavaScript(fill_user)
        self.page().runJavaScript(fill_pass)

        self.page().runJavaScript(submit)
        self.__login_page = False

    def __run_js_position_count(self):
        algo_js = """document.querySelector('div[id="positions_count"]').outerHTML"""
        self.page().runJavaScript(algo_js, self.__js_click_callback)

    def __js_click_callback(self, data):
        print('click callback found...')
        print(data)
        if data:
            soup = BeautifulSoup(data, 'lxml')
            cnt = soup.find('div').text.strip()
            cnt = re.sub(r'[^0-9]', '', cnt).strip()
            if cnt == '' or int(cnt) == 0:
                time.sleep(1)
                self.__run_js_position_count()
            else:
                print(cnt)
                self.page().toHtml(self.processHtml)

                # page = soup.find('li').get('data-wiz-grid-hash')
                # if 'page={}'.format(self.__page_index + 1) in page:
                #     self.page().toHtml(self.processHtml)
                # else:
                #     # print('loading click...')
                #     js1 = """document.querySelector('li[data-wiz-next-page]').outerHTML"""
                #     self.page().runJavaScript(js1, self.__js_click_callback)
                # else:
                #     self.page().toHtml(self.processHtml)

    def processHtml(self, html):
        self.__html = html
        self.__app.quit()


class QuantopianGrabber:
    __login_form_url = 'https://www.quantopian.com/signin'
    __login_url = 'https://www.quantopian.com/users/sign_in'
    __algorithm_url = 'https://www.quantopian.com/live_algorithms/5a8ed9a04867d05e979e7ef4'

    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__app = QApplication(sys.argv)
        self.__browser = Browser(self.__app)
        self.__lock = Lock()
        self.__setup_logger('houzz.log')
        socket.setdefaulttimeout(30)

        self.__field_names = ['product_name', 'category', 'style', 'price', 'size', 'weight', 'color', 'material',
                              'large_img_link', 'embed_code_large_image', 'embed_code_small_image', 'product_desc',
                              'manufacturer', 'url']
        csv_header = {'product_name': 'Product name',
                      'category': 'Category(cat. /sub cat / sub cat//)',
                      'style': 'Style',
                      'price': 'Price',
                      'size': 'Size',
                      'weight': 'Weight',
                      'color': 'Color',
                      'material': 'Material',
                      'large_img_link': 'Large Image link',
                      'embed_code_large_image': 'Embed code (large image)',
                      'embed_code_small_image': 'Embed code (small image)',
                      'product_desc': 'Product Description',
                      'manufacturer': 'Manufacturer',
                      'url': 'URL'
                      }

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __create_opener(self):
        try:
            opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                 urllib.request.UnknownHandler(),
                                                 urllib.request.HTTPHandler(),
                                                 urllib.request.HTTPSHandler(),
                                                 urllib.request.HTTPRedirectHandler(),
                                                 urllib.request.HTTPDefaultErrorHandler(),
                                                 urllib.request.HTTPErrorProcessor())

            opener.addheaders.clear()
            for key in self.__headers:
                opener.addheaders.append((key, self.__headers[key]))

            return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__browser.close()
        self.__app.exit()
        del self.__logger
        del self

    def __build_browser(self):
        try:
            cap = webdriver.DesiredCapabilities.PHANTOMJS
            cap["phantomjs.page.settings.javascriptEnabled"] = True
            cap[
                'phantomjs.page.settings.userAgent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
            driver_path = './driver/phantomjs'
            if str(platform.system()).startswith('Windows'):
                driver_path = './driver/phantomjs.exe'

            browser = webdriver.PhantomJS(driver_path, desired_capabilities=cap,
                                          service_args=['--ignore-ssl-errors=true'])

            # chrome browser
            # chrome_options = Options()
            # chrome_options.add_argument("--headless")
            # chrome_options.add_argument("--window-size=800x600")
            # browser = webdriver.Chrome('/home/racecoder/webapps/htdocs/spiders/fiverr/trip_advisor/driver/chromedriver', desired_capabilities=DesiredCapabilities.CHROME,
            #                            chrome_options=chrome_options)
            # browser = webdriver.Chrome('./driver/chromedriver', desired_capabilities=DesiredCapabilities.CHROME,
            #                            chrome_options=chrome_options)
            return browser
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __operation(self):
        browser = None
        try:
            self.__lock.acquire()
            self.__logger.info('Trying to get web site address. URL: {}'.format(self.__login_form_url))
            browser = self.__build_browser()
            browser.get(self.__login_form_url)
            wait = WebDriverWait(browser, 60)
            wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

            # login
            elemu = browser.find_element_by_id('user_email')
            elemu.send_keys('arthchrisjr@yahoo.com')
            elemp = browser.find_element_by_id('user_password')
            elemp.send_keys('Loopcap1')
            elemp.send_keys(Keys.RETURN)

            wait = WebDriverWait(browser, 60)
            wait.until(EC.url_changes)

            print(browser.current_url)

            browser.get(self.__algorithm_url)
            wait = WebDriverWait(browser, 60)
            # wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
            wait.until(EC.presence_of_element_located((By.XPATH, '//div[@class="no-positions hidden"]')))

            # time.sleep(5)

            print(browser.current_url)
            element = browser.find_element_by_xpath('//div[@id="position-table"]')
            data = element.get_attribute('innerHTML')

            items = self.__parse_data(data)
            if items:
                pass
        except:
            print('error')

    def __parse_data(self, data):
        try:
            data = """<html><body><div hidefocus="" style="position:fixed;width:0;height:0;top:0;left:0;outline:0;" tabindex="0"></div><div class="slick-header ui-state-default" style="overflow:hidden;position:relative;"><div class="slick-header-columns" style="left: -1000px; width: 1332px;" unselectable="on"><div class="ui-state-default slick-header-column" id="slickgrid_921949sid" style="width: 45px;" title=""><span class="slick-column-name">Security</span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_921949shares" style="width: 45px;" title=""><span class="slick-column-name">Shares</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_921949price" style="width: 45px;" title=""><span class="slick-column-name">Price</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_921949cost_basis" style="width: 45px;" title=""><span class="slick-column-name">Avg Cost</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_921949value" style="width: 47px;" title=""><span class="slick-column-name">Value</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_921949unrealized" style="width: 47px;" title=""><span class="slick-column-name">Unrealized</span><span class="slick-sort-indicator"></span></div></div></div><div class="slick-headerrow ui-state-default hidden" style="overflow:hidden;position:relative;"><div class="slick-headerrow-columns" style="width: 316px;"></div><div style="display: block; height: 1px; position: absolute; top: 0px; left: 0px; width: 332px;"></div></div><div class="slick-top-panel-scroller ui-state-default hidden" style="overflow:hidden;position:relative;"><div class="slick-top-panel" style="width:10000px"></div></div><div class="slick-viewport" style="width: 100%; overflow: auto; outline: 0px; position: relative; height: 187px;"><div class="grid-canvas" style="width: 316px; height: 200px;"><div class="ui-widget-content slick-row even" style="top:0px"><div class="slick-cell l0 r0"><span class="quanto-sid-39214">TQQQ</span></div><div class="slick-cell l1 r1">16</div><div class="slick-cell l2 r2">$173.84</div><div class="slick-cell l3 r3">$161.66</div><div class="slick-cell l4 r4">$2,781.44</div><div class="slick-cell l5 r5"><span class="money-green">$194.85</span></div></div><div class="ui-widget-content slick-row odd" style="top:25px"><div class="slick-cell l0 r0"><span class="quanto-sid-37514">SPXL</span></div><div class="slick-cell l1 r1">35</div><div class="slick-cell l2 r2">$48.08</div><div class="slick-cell l3 r3">$45.27</div><div class="slick-cell l4 r4">$1,682.63</div><div class="slick-cell l5 r5"><span class="money-green">$98.05</span></div></div><div class="ui-widget-content slick-row even" style="top:50px"><div class="slick-cell l0 r0"><span class="quanto-sid-19659">XLP</span></div><div class="slick-cell l1 r1">30</div><div class="slick-cell l2 r2">$54.52</div><div class="slick-cell l3 r3">$53.89</div><div class="slick-cell l4 r4">$1,635.60</div><div class="slick-cell l5 r5"><span class="money-green">$18.96</span></div></div><div class="ui-widget-content slick-row odd" style="top:75px"><div class="slick-cell l0 r0"><span class="quanto-sid-19662">XLY</span></div><div class="slick-cell l1 r1">11</div><div class="slick-cell l2 r2">$106.55</div><div class="slick-cell l3 r3">$104.81</div><div class="slick-cell l4 r4">$1,172.05</div><div class="slick-cell l5 r5"><span class="money-green">$19.10</span></div></div><div class="ui-widget-content slick-row even" style="top:100px"><div class="slick-cell l0 r0"><span class="quanto-sid-26807">GLD</span></div><div class="slick-cell l1 r1">9</div><div class="slick-cell l2 r2">$126.28</div><div class="slick-cell l3 r3">$126.07</div><div class="slick-cell l4 r4">$1,136.53</div><div class="slick-cell l5 r5"><span class="money-green">$1.86</span></div></div><div class="ui-widget-content slick-row odd" style="top:125px"><div class="slick-cell l0 r0"><span class="quanto-sid-19661">XLV</span></div><div class="slick-cell l1 r1">13</div><div class="slick-cell l2 r2">$86.14</div><div class="slick-cell l3 r3">$84.51</div><div class="slick-cell l4 r4">$1,119.82</div><div class="slick-cell l5 r5"><span class="money-green">$21.15</span></div></div><div class="ui-widget-content slick-row even" style="top:150px"><div class="slick-cell l0 r0"><span class="quanto-sid-23870">IEF</span></div><div class="slick-cell l1 r1">3</div><div class="slick-cell l2 r2">$102.24</div><div class="slick-cell l3 r3">$101.84</div><div class="slick-cell l4 r4">$306.72</div><div class="slick-cell l5 r5"><span class="money-green">$1.20</span></div></div><div class="ui-widget-content slick-row odd" style="top:175px"><div class="slick-cell l0 r0"><span class="quanto-sid-23921">TLT</span></div><div class="slick-cell l1 r1">2</div><div class="slick-cell l2 r2">$118.29</div><div class="slick-cell l3 r3">$117.38</div><div class="slick-cell l4 r4">$236.58</div><div class="slick-cell l5 r5"><span class="money-green">$1.82</span></div></div></div></div><div hidefocus="" style="position:fixed;width:0;height:0;top:0;left:0;outline:0;" tabindex="0"></div></body></html>"""
            soup = BeautifulSoup(data, 'lxml')
            print(soup.prettify())
            tabs = soup.find_all('div', class_=re.compile(r'^ui-widget-content.*'))

            items = []
            for tab in tabs:
                # print(tab)
                item = {}
                columns = tab.find_all('div', class_=re.compile(r'^slick-cell.*?$'))
                item['security'] = columns[0].text.strip()
                item['shares'] = columns[1].text.strip()
                item['price'] = columns[2].text.strip()
                item['avg_cost'] = columns[3].text.strip()
                item['value'] = columns[4].text.strip()
                item['unrealized'] = columns[5].text.strip()
                items.append(item)

            return items
        except Exception as x:
            print(x)

    def grab_data(self):
        try:
            # self.__operation()
            self.__parse_data()

            # r, d = self.__browser.fetch(self.__login_form_url, self.__algorithm_url)
            # print(d)
            # self.__login()
            return
            # self.__grab_details('https://www.houzz.com/product/93990561')
            # return
            for category in self.__categories:
                category_url = self.__category_url.format(category)
                self.__grab_data_by_category(category_url)
                # break
        except Exception as x:
            print(x)

    def __login(self):
        try:
            # Get login form data
            self.__logger.info('Login form URL: {}'.format(self.__login_form_url))
            self.__opener = self.__create_opener()
            data = self.__opener.open(self.__login_form_url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            auth_token_tag = soup.find('input', {'name': 'authenticity_token'})
            if not auth_token_tag or not auth_token_tag.has_attr('value'):
                return

            # get auth token from form page
            auth_token = auth_token_tag.get('value')

            # post request to login with post data
            self.__logger.info('Trying to login via: {}'.format(self.__login_url))
            post_data = {'utf8': '✓',
                         'authenticity_token': auth_token,
                         'user[email]': 'arthchrisjr@yahoo.com',
                         'user[password]': 'Loopcap1',
                         'user[remember_me]': '0'}
            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            response = self.__opener.open(self.__login_url, data=post_req)
            print(response.info())
            print(response.url)

            response = self.__opener.open(self.__algorithm_url)
            time.sleep(10)
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return
            print(html.unescape(data))
            # req = urllib.request.Request(self.__login_page, headers=self.__headers, data=post_req)
            # response = urllib.request.urlopen(req)
            # redirected_url = response.geturl()
            # return redirected_url == self.__success_redirected_url
        except Exception as x:
            self.__logger.error('Error when login page: {}'.format(x))

    def grab_data_by_sub_cat_file(self, url_file):
        try:
            with open(url_file, 'r+') as rf:
                for url_line in rf.readlines():
                    url = url_line.strip()
                    print('==== URL from file: {} ==='.format(url))
                    opener = self.__grab_data_by_sub_category(url)
                    page = 100
                    while opener and page < 300:
                        opener = self.__grab_data_by_sub_category(url, page=page, opener=opener)
                        page += 100
                    del opener
        except Exception as x:
            self.__logger.error('Error when grab data by sub cat url from file: {}'.format(x))

    def __grab_data_by_category(self, url, retry=0):
        try:
            self.__logger.info('=== Category URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            sub_cat_tags = soup.find_all('div', class_='topic-group-column')
            if not sub_cat_tags:
                return

            for sub_cat_tag in sub_cat_tags:
                sub_cat_url_tag = sub_cat_tag.find('a', class_='topic-title header-5 text-unbold no-margin')
                if not sub_cat_url_tag:
                    continue

                sub_cat_url = sub_cat_url_tag.get('href').strip('/')
                return_val = self.__grab_data_by_sub_category(sub_cat_url)
                if not return_val:
                    continue

                if type(return_val) is list:
                    for sub_cat_inner_url in return_val:
                        opener = self.__grab_data_by_sub_category(sub_cat_inner_url)
                        page = 100
                        while opener and page < 300:
                            opener = self.__grab_data_by_sub_category(sub_cat_inner_url, page=page, opener=opener)
                            page += 100
                        del opener
                        # break
                else:
                    page = 100
                    opener = self.__grab_data_by_sub_category(sub_cat_url, page=page, opener=return_val)
                    page += 100
                    while opener and page < 300:
                        opener = self.__grab_data_by_sub_category(sub_cat_url, page=page, opener=opener)
                        page += 100
                    del opener
                    # break
        except Exception as x:
            self.__logger.error('Error grab category: {}'.format(x))
            if retry < 5:
                return self.__grab_data_by_category(url, retry + 1)

    def __grab_data_by_sub_category(self, url, page=0, opener=None, retry=0):
        try:
            url = url.strip('/') + '/ls=2' + ('' if page == 0 else '/p/{}'.format(page))  # /p/{}'.format(page)
            self.__logger.info('=== Sub-Category URL: {} ==='.format(url))

            if not opener:
                opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # Check if it has also sub category or not.
            sub_cat_tags = soup.find_all('div', class_='topic-group-column')
            if sub_cat_tags:
                sub_cat_url_list = []
                for sub_cat_tag in sub_cat_tags:
                    sub_cat_url_tag = sub_cat_tag.find('a', class_='topic-title header-5 text-unbold no-margin')
                    if not sub_cat_url_tag:
                        continue

                    sub_cat_url = sub_cat_url_tag.get('href')
                    sub_cat_url_list.append(sub_cat_url)
                return sub_cat_url_list

            # print(soup)
            # Grab all product links
            product_tags = soup.find_all('div', {'lb-entity-id': re.compile(r'^\s*?\d+\s*?$')})
            self.__logger.info('Total products Found: {}'.format(len(product_tags)))
            if product_tags and len(product_tags) > 0:
                url_list = []
                for product_tag in product_tags:
                    if not product_tag.has_attr('lb-entity-id'):
                        continue
                    prod_id = product_tag.get('lb-entity-id').strip()
                    prod_url = self.__product_url.format(prod_id)
                    # self.__logger.info(prod_url)
                    # self.__grab_details(prod_url)

                    url_list.append(prod_url)

                    # if prod_url not in self.__url_cache:
                    #     url_list.append(prod_url)
                    # else:
                    #     self.__logger.warning('Already grabbed URL: {}.'.format(prod_url))

                with ThreadPool(16) as p:
                    p.map(self.__grab_details, url_list)

                return opener
        except Exception as x:
            self.__logger.error('Error grab main page: {}'.format(x))
            if retry < 5:
                return self.__grab_data_by_sub_category(url, page, opener, retry + 1)

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            opener = self.__create_opener()
            # print(opener.addheaders)
            response = opener.open(url)
            # print(response.info())

            rid, csrf = '', ''
            response_info = response.info()
            if 'X-Request-Id' in response_info:
                rid = response_info['X-Request-Id']

            self.__logger.info('Redirected URL: {}'.format(response.url))
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            csrf_m = re.search(r'"csrfToken":"([^\"]*?)"', data)
            if csrf_m:
                csrf = csrf_m.group(1)
            elif retry < 15:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # print(data)

            item = {'url': url}

            product_tag = soup.find('h1', class_='header-1')
            if product_tag:
                item['product_name'] = product_tag.text.strip()

            product_desc_tag = soup.find('div', {'itemprop': 'description'})
            if product_desc_tag:
                prod_desc = product_desc_tag.text.strip()
                prod_desc = prod_desc.replace('Product Description', '').strip()
                item['product_desc'] = prod_desc

            price_tag = soup.find('span', {'itemprop': 'price'})
            if price_tag:
                item['price'] = price_tag.text.strip()

            category_list_tags = soup.find_all('a', class_='hz-breadcrumb__link text-primary')
            if category_list_tags:
                cat_list = []
                for category_list_tag in category_list_tags[1:]:
                    if not category_list_tag.has_attr('title'):
                        continue
                    cat_list.append(category_list_tag.get('title'))
                item['category'] = ' / '.join(cat_list)

            # specs_tag = soup.find('div', {'scopeid': 'productSpec'})
            # if specs_tag:
            dt_list = soup.find_all('dt')
            for dt in dt_list:
                dd = dt.find_next_sibling('dd')
                if not dd:
                    continue

                dt_text = dt.text.strip()
                dd_text = dd.text.strip()

                if 'Manufactured By' in dt_text:
                    item['manufacturer'] = dd_text

                if 'category' not in item and 'Category' in dt_text:
                    item['category'] = dd_text

                if 'Style' in dt_text:
                    item['style'] = dd_text

                if 'Materials' in dt_text:
                    item['material'] = dd_text

                if 'Size/Weight' in dt_text:
                    size_weight = dd_text.split('/')
                    size = ' / '.join(size_weight[:-1]).strip()
                    weight = size_weight[-1].strip('\.').strip()
                    item['size'] = size
                    item['weight'] = weight

                if 'Color' in dt_text:
                    item['color'] = dd_text

            image_tag = soup.find('img', class_='view-product-image-print visible-print-block')
            if image_tag:
                item['large_img_link'] = image_tag.get('src')

            if 'large_img_link' not in item:
                json_data_list = soup.find_all('script', {'type': 'application/ld+json'})
                for j_data in json_data_list:
                    jd = json.loads(j_data.text)
                    if 'image' in jd:
                        item['large_img_link'] = jd['image']

            if csrf != '' and rid != '':
                json_data = self.__get_embedded_data(url.split('/')[-1], rid, csrf, opener)
                if 'largeImageUrl' in json_data:
                    item['large_img_link'] = json_data['largeImageUrl']
                if 'smallImageEmbedCode' in json_data:
                    item['embed_code_small_image'] = json_data['smallImageEmbedCode']
                if 'largeImageEmbedCode' in json_data:
                    item['embed_code_large_image'] = json_data['largeImageEmbedCode']

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

            return True
        except Exception as x:
            self.__logger.error('Error grab details: {}'.format(x))
            if retry < 15:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __get_embedded_data(self, space_id, req_id, csrf, opener):
        try:
            post_data = {'spaceId': space_id,
                         'ajaxRequestId': '1',
                         'pageRequestId': req_id,
                         'isResiv': 'false'}
            self.__logger.info('Trying to get embedded link. Post data: {}'.format(post_data))

            opener.addheaders.append(('content-type', 'application/x-www-form-urlencoded; charset=UTF-8'))
            opener.addheaders.append(('x-hz-request', 'true'))
            opener.addheaders.append(('x-requested-with', 'XMLHttpRequest'))
            opener.addheaders.append(('rrid', req_id))
            opener.addheaders.append(('x-csrf-token', csrf))
            opener.addheaders.append(('origin', 'https://www.houzz.com'))

            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            response = opener.open(self.__embedded_url, data=post_req)
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # print(data)
            if not data:
                return

            json_data = json.loads(data)
            if json_data:
                return json_data

        except Exception as x:
            print(x)

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'quantopian_output_file.csv'
    with QuantopianGrabber(output_file) as spider:
        spider.grab_data()
    print('Script finished!')
