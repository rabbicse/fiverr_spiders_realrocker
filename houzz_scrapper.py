import csv
import gzip
import json
import logging
import os
import random
import re
import readline
import socket
import urllib.parse
import urllib.request
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool

import time
from bs4 import BeautifulSoup

readline.parse_and_bind("control-v: paste")


class HouzzGrabber:
    __base_url = 'https://www.houzz.com'
    __category_url = 'https://www.houzz.com/photos/{}'
    __product_url = 'https://www.houzz.com/product/{}'
    __embedded_url = 'https://www.houzz.com/j/pxy/getEmbedSpaceCode'
    __proxies = (
        '108.59.14.208:13040',
        '108.59.14.203:13040'
    )
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}
    __total = 0
    __url_cache = []
    __categories = ['living-products', 'home-decor', 'furniture', 'lighting', 'bedroom-products']

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('houzz.log')
        socket.setdefaulttimeout(30)

        self.__field_names = ['product_name', 'category', 'style', 'price', 'size', 'weight', 'color', 'material',
                              'large_img_link', 'embed_code_large_image', 'embed_code_small_image', 'product_desc',
                              'manufacturer', 'url']
        csv_header = {'product_name': 'Product name',
                      'category': 'Category(cat. /sub cat / sub cat//)',
                      'style': 'Style',
                      'price': 'Price',
                      'size': 'Size',
                      'weight': 'Weight',
                      'color': 'Color',
                      'material': 'Material',
                      'large_img_link': 'Large Image link',
                      'embed_code_large_image': 'Embed code (large image)',
                      'embed_code_small_image': 'Embed code (small image)',
                      'product_desc': 'Product Description',
                      'manufacturer': 'Manufacturer',
                      'url': 'URL'
                      }

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))

        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __get_random_proxy(self):
        try:
            # return None
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy_url = str(random.choice(self.__proxies))

            if not proxy_url.startswith('http://'):
                proxy_url = 'http://' + proxy_url

            return {
                "http": proxy_url,
                "https": proxy_url
            }
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self.__logger
        del self

    def grab_data(self):
        try:
            # self.__grab_details('https://www.houzz.com/product/93990561')
            # return
            for category in self.__categories:
                category_url = self.__category_url.format(category)
                self.__grab_data_by_category(category_url)
                # break
        except Exception as x:
            print(x)

    def grab_data_by_sub_cat_file(self, url_file):
        try:
            with open(url_file, 'r+') as rf:
                for url_line in rf.readlines():
                    url = url_line.strip()
                    print('==== URL from file: {} ==='.format(url))
                    opener = self.__grab_data_by_sub_category(url)
                    page = 100
                    while opener and page < 300:
                        opener = self.__grab_data_by_sub_category(url, page=page, opener=opener)
                        page += 100
                    del opener
        except Exception as x:
            self.__logger.error('Error when grab data by sub cat url from file: {}'.format(x))

    def __grab_data_by_category(self, url, retry=0):
        try:
            self.__logger.info('=== Category URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            sub_cat_tags = soup.find_all('div', class_='topic-group-column')
            if not sub_cat_tags:
                return

            for sub_cat_tag in sub_cat_tags:
                sub_cat_url_tag = sub_cat_tag.find('a', class_='topic-title header-5 text-unbold no-margin')
                if not sub_cat_url_tag:
                    continue

                sub_cat_url = sub_cat_url_tag.get('href').strip('/')
                return_val = self.__grab_data_by_sub_category(sub_cat_url)
                if not return_val:
                    continue

                if type(return_val) is list:
                    for sub_cat_inner_url in return_val:
                        opener = self.__grab_data_by_sub_category(sub_cat_inner_url)
                        page = 100
                        while opener and page < 300:
                            opener = self.__grab_data_by_sub_category(sub_cat_inner_url, page=page, opener=opener)
                            page += 100
                        del opener
                        # break
                else:
                    page = 100
                    opener = self.__grab_data_by_sub_category(sub_cat_url, page=page, opener=return_val)
                    page += 100
                    while opener and page < 300:
                        opener = self.__grab_data_by_sub_category(sub_cat_url, page=page, opener=opener)
                        page += 100
                    del opener
                    # break
        except Exception as x:
            self.__logger.error('Error grab category: {}'.format(x))
            if retry < 5:
                return self.__grab_data_by_category(url, retry + 1)

    def __grab_data_by_sub_category(self, url, page=0, opener=None, retry=0):
        try:
            url = url.strip('/') + '/ls=2' + ('' if page == 0 else '/p/{}'.format(page))  # /p/{}'.format(page)
            self.__logger.info('=== Sub-Category URL: {} ==='.format(url))

            if not opener:
                opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # Check if it has also sub category or not.
            sub_cat_tags = soup.find_all('div', class_='topic-group-column')
            if sub_cat_tags:
                sub_cat_url_list = []
                for sub_cat_tag in sub_cat_tags:
                    sub_cat_url_tag = sub_cat_tag.find('a', class_='topic-title header-5 text-unbold no-margin')
                    if not sub_cat_url_tag:
                        continue

                    sub_cat_url = sub_cat_url_tag.get('href')
                    sub_cat_url_list.append(sub_cat_url)
                return sub_cat_url_list

            # print(soup)
            # Grab all product links
            product_tags = soup.find_all('div', {'lb-entity-id': re.compile(r'^\s*?\d+\s*?$')})
            self.__logger.info('Total products Found: {}'.format(len(product_tags)))
            if product_tags and len(product_tags) > 0:
                url_list = []
                for product_tag in product_tags:
                    if not product_tag.has_attr('lb-entity-id'):
                        continue
                    prod_id = product_tag.get('lb-entity-id').strip()
                    prod_url = self.__product_url.format(prod_id)
                    # self.__logger.info(prod_url)
                    # self.__grab_details(prod_url)

                    url_list.append(prod_url)

                    # if prod_url not in self.__url_cache:
                    #     url_list.append(prod_url)
                    # else:
                    #     self.__logger.warning('Already grabbed URL: {}.'.format(prod_url))

                with ThreadPool(16) as p:
                    p.map(self.__grab_details, url_list)

                return opener
        except Exception as x:
            self.__logger.error('Error grab main page: {}'.format(x))
            if retry < 5:
                return self.__grab_data_by_sub_category(url, page, opener, retry + 1)

    def __grab_details(self, url, retry=0):
        try:
            self.__logger.info('=== Details URL: {} ==='.format(url))
            opener = self.__create_opener()
            # print(opener.addheaders)
            response = opener.open(url)
            # print(response.info())

            rid, csrf = '', ''
            response_info = response.info()
            if 'X-Request-Id' in response_info:
                rid = response_info['X-Request-Id']

            self.__logger.info('Redirected URL: {}'.format(response.url))
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')
            if not data:
                return

            csrf_m = re.search(r'"csrfToken":"([^\"]*?)"', data)
            if csrf_m:
                csrf = csrf_m.group(1)
            elif retry < 15:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            # print(data)

            item = {'url': url}

            product_tag = soup.find('h1', class_='header-1')
            if product_tag:
                item['product_name'] = product_tag.text.strip()

            product_desc_tag = soup.find('div', {'itemprop': 'description'})
            if product_desc_tag:
                prod_desc = product_desc_tag.text.strip()
                prod_desc = prod_desc.replace('Product Description', '').strip()
                item['product_desc'] = prod_desc

            price_tag = soup.find('span', {'itemprop': 'price'})
            if price_tag:
                item['price'] = price_tag.text.strip()

            category_list_tags = soup.find_all('a', class_='hz-breadcrumb__link text-primary')
            if category_list_tags:
                cat_list = []
                for category_list_tag in category_list_tags[1:]:
                    if not category_list_tag.has_attr('title'):
                        continue
                    cat_list.append(category_list_tag.get('title'))
                item['category'] = ' / '.join(cat_list)

            # specs_tag = soup.find('div', {'scopeid': 'productSpec'})
            # if specs_tag:
            dt_list = soup.find_all('dt')
            for dt in dt_list:
                dd = dt.find_next_sibling('dd')
                if not dd:
                    continue

                dt_text = dt.text.strip()
                dd_text = dd.text.strip()

                if 'Manufactured By' in dt_text:
                    item['manufacturer'] = dd_text

                if 'category' not in item and 'Category' in dt_text:
                    item['category'] = dd_text

                if 'Style' in dt_text:
                    item['style'] = dd_text

                if 'Materials' in dt_text:
                    item['material'] = dd_text

                if 'Size/Weight' in dt_text:
                    size_weight = dd_text.split('/')
                    size = ' / '.join(size_weight[:-1]).strip()
                    weight = size_weight[-1].strip('\.').strip()
                    item['size'] = size
                    item['weight'] = weight

                if 'Color' in dt_text:
                    item['color'] = dd_text

            image_tag = soup.find('img', class_='view-product-image-print visible-print-block')
            if image_tag:
                item['large_img_link'] = image_tag.get('src')

            if 'large_img_link' not in item:
                json_data_list = soup.find_all('script', {'type': 'application/ld+json'})
                for j_data in json_data_list:
                    jd = json.loads(j_data.text)
                    if 'image' in jd:
                        item['large_img_link'] = jd['image']

            if csrf != '' and rid != '':
                json_data = self.__get_embedded_data(url.split('/')[-1], rid, csrf, opener)
                if 'largeImageUrl' in json_data:
                    item['large_img_link'] = json_data['largeImageUrl']
                if 'smallImageEmbedCode' in json_data:
                    item['embed_code_small_image'] = json_data['smallImageEmbedCode']
                if 'largeImageEmbedCode' in json_data:
                    item['embed_code_large_image'] = json_data['largeImageEmbedCode']

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

            return True
        except Exception as x:
            self.__logger.error('Error grab details: {}'.format(x))
            if retry < 15:
                sleep_time = random.randint(5, 15)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(url, retry + 1)

    def __get_embedded_data(self, space_id, req_id, csrf, opener):
        try:
            post_data = {'spaceId': space_id,
                         'ajaxRequestId': '1',
                         'pageRequestId': req_id,
                         'isResiv': 'false'}
            self.__logger.info('Trying to get embedded link. Post data: {}'.format(post_data))

            opener.addheaders.append(('content-type', 'application/x-www-form-urlencoded; charset=UTF-8'))
            opener.addheaders.append(('x-hz-request', 'true'))
            opener.addheaders.append(('x-requested-with', 'XMLHttpRequest'))
            opener.addheaders.append(('rrid', req_id))
            opener.addheaders.append(('x-csrf-token', csrf))
            opener.addheaders.append(('origin', 'https://www.houzz.com'))

            post_req = urllib.parse.urlencode(post_data).encode('utf-8')
            response = opener.open(self.__embedded_url, data=post_req)
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            # print(data)
            if not data:
                return

            json_data = json.loads(data)
            if json_data:
                return json_data

        except Exception as x:
            print(x)

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'houzz_output_file.csv'
    with HouzzGrabber(output_file) as spider:
        spider.grab_data_by_sub_cat_file('houzz_input.txt')
        # spider.grab_data()
    print('Script finished!')
