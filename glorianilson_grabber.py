import csv
import re
import string
import urllib.request
from bs4 import BeautifulSoup
import socket

socket.setdefaulttimeout(20)

base_url = 'http://www.glorianilson.com'
url = 'http://www.glorianilson.com/real-estate-agents/index.cfm?letter='

headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}


def grab_data(url):
    req = urllib.request.Request(url, headers=headers)
    data = urllib.request.urlopen(req).read().decode('utf-8')
    soup = BeautifulSoup(data, 'lxml')

    table = soup.find('table', id='agent-dir')
    if table:
        tr_list = table.find_all('tr')
        for tr in tr_list:
            td_list = tr.find_all('td')
            # print(td_list[0])
            name_company = td_list[0].find('strong', class_='agentname').text
            name, company = name_company.split(',')
            job_title = td_list[0].find_next('br').next_element.strip()
            print(name.strip())
            print(company.strip())
            print(job_title.strip())

            address_url = td_list[0].find_all('a')[-1].get('href')
            print(address_url)

            email = td_list[1].find('a').get('href')
            email = re.sub(r'mailto\:', '', email)

            contact_br = td_list[1].find_all('a')[-1].find_next('br')
            cell = contact_br.next_element
            cell = re.sub(r'\(Cell\)', '', cell).strip()
            office = contact_br.find_next('br').next_element.strip()
            office = re.sub(r'\(Office\)', '', office).strip()
            print(email.strip())
            print(cell.strip())
            print(office.strip())

            break


def write_data(file_name, row):
    try:
        with open(file_name, 'a', newline='', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow(row)
    except Exception as x:
        print('Error printing csv output: ' + str(x))


def grab_detail_link(url, c):
    try:
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read().decode('utf-8')
        soup = BeautifulSoup(data, 'lxml')

        table = soup.find('table', id='agent-dir')
        if table:
            tr_list = table.find_all('tr')

            with open(c + '.txt', 'w') as f:
                for tr in tr_list:
                    td_list = tr.find_all('td')
                    link_tag = td_list[1].find_all('a')[1].get('onclick')
                    link_m = re.search(r'emailForm=window.open\(\'([^\']*)\'', link_tag)
                    if link_m:
                        link = base_url + link_m.group(1)
                        f.write(link + '\n')
                        # grab_detail(link)
                        # break
    except Exception as x:
        print(x)


def grab_detail(url):
    try:
        print('URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read().decode('utf-8')
        soup = BeautifulSoup(data, 'lxml')

        company_name = ''
        company_name_tag = soup.find('div', id='container-page')
        if company_name_tag:
            com_str_tag = company_name_tag.find('strong')
            if com_str_tag:
                company_name = com_str_tag.text.strip()

        name, job_title, address = '', '', ''
        div_1 = soup.find('div', class_='col-xs-6')
        if div_1:
            name_tags = div_1.find_all('strong')
            if name_tags:
                if len(name_tags) > 0:
                    name = name_tags[0].text.strip()
                if len(name_tags) > 1:
                    job_title = name_tags[1].text.strip()

            # address_tag = #re.search(r'</strong><br/>\s*?(.*?)</div>', str(div_1))
            address_tag = re.sub(r'<strong>.*?</strong>', '', str(div_1))
            address_tag = re.sub(r'<div class="col-xs-6">', '', address_tag)
            address_tag = re.sub(r'</div>', '', address_tag)
            address_tag = re.sub(r'<br/>', '', address_tag)
            address_tag = re.sub(r'\s+', ' ', address_tag)
            address = address_tag.strip()

        email, phone, fax = '', '', ''
        div_2 = soup.find('div', class_='col-xs-6 text-right')
        if div_2:
            email = div_2.next_element.strip()
            phone_m = re.search(r'Ph\:\s*?([^<]*?)<br/>', str(div_2))
            if phone_m:
                phone = phone_m.group(1).strip()
            fax_m = re.search(r'Fx\:\s*?([^<]*?)<br/>', str(div_2))
            if fax_m:
                fax = fax_m.group(1).strip()

        row = [name, company_name, address, job_title, email, phone, fax, url]
        print(row)
        write_data('output.csv', row)

    except Exception as x:
        print('Error for url: ' + url)
        print(x)


def grab_link(text_file):
    with open(text_file, 'r') as f:
        for line in f:
            link = line.strip()
            grab_detail(link)


def process_grabber():
    with open('output.csv', 'w', newline='', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['Name', 'Company Name', 'Address', 'Job Title', 'Email', 'Phone', 'Fax', 'URL'])

    for c in string.ascii_uppercase:
        print('==== Processing {} ===='.format(c))
        grab_link(c + '.txt')
        # print(url + c)
        # grab_detail_link(url + c, c)


if __name__ == '__main__':
    process_grabber()
    # grab_data(url + 'A')
