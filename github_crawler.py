import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import time
import urllib.parse
import urllib.request
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from multiprocessing import Lock
from multiprocessing.pool import ThreadPool

from bs4 import BeautifulSoup


class GithubSpider:
    __user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    __base_url = 'https://github.com'
    __start_url = 'https://github.com/search?o=desc&q=deep+learning&s=updated&type=Repositories&p={}'
    __headers = {
        'User-Agent': __user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    __proxies = None
    __url_cache = []
    __total = 0

    def __init__(self, output_csv):
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def _setup_logger(self):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('github.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __enter__(self):
        self.__lock = Lock()
        self._setup_logger()
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(30)

        hdr = [('repo_name', 'Repository Name'),
               ('acc_name', 'Account Name'),
               ('repo_url', 'Repository URL'),
               ('desc', 'Description'),
               ('name', 'Name'),
               ('email', 'Email'),
               ('commit_id', 'Commit ID'),
               ('url', 'URL')]

        csv_header = OrderedDict(hdr)

        self.__field_names = list(csv_header.keys())

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                proxies = json.load(f)
                self.__proxies = proxies

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        self.__logger.info('Total Previous Records Found: {}'.format(self.__total))
        return self

    def __get_random_proxy(self):
        try:
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': 'https://{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': 'http://{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(  # urllib.request.HTTPCookieProcessor(),
                    urllib.request.UnknownHandler(),
                    urllib.request.HTTPHandler(),
                    urllib.request.HTTPSHandler(),
                    urllib.request.HTTPRedirectHandler(),
                    urllib.request.HTTPDefaultErrorHandler(),
                    urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    def grab_all_data(self):
        # page = 1
        # has_next = self.grab_data(self.__start_url.format(page))
        # while has_next:
        #     page += 1
        #     has_next = self.grab_data(self.__start_url.format(page))
        with ThreadPool(8) as p:
            p.map(self.grab_data, [self.__start_url.format(page) for page in range(1, 101)])

    def grab_data(self, url, retry=0):
        try:
            self.__logger.info('=== Main URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            contents = []
            div_list = soup.find_all('div', class_='repo-list-item d-flex flex-justify-start py-4 public source')
            for div in div_list:
                a_tag = div.find('a', class_='v-align-middle')
                if not a_tag:
                    continue

                repo_acc_text = a_tag.text.strip()
                repo_acc = repo_acc_text.split('/')
                acc_name = repo_acc[0]
                repo_name = repo_acc[1]

                desc = ''
                desc_tag = div.find('p', class_='col-9 d-inline-block text-gray mb-2 pr-4')
                if desc_tag:
                    desc = desc_tag.text.strip()

                repo_url = self.__base_url + a_tag.get('href')
                commit_url = repo_url + '/commits/master'
                contents.append((commit_url, repo_url, repo_name, acc_name, desc))

                # self.grab_commits((commit_url, repo_url, repo_name, acc_name, desc))
                # break

            with ThreadPool(32) as p:
                p.map(self.grab_commits, contents)

            has_next = soup.find('a', class_='next_page')
            if has_next:
                return has_next
        except Exception as x:
            self.__logger.error('Error when grab main page.{}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_data(url, retry + 1)

    def grab_commits(self, args, retry=0):
        try:
            url = args[0]
            content = {'repo_name': args[2],
                       'acc_name': args[3],
                       'repo_url': args[1],
                       'desc': args[4]}
            self.__logger.info('=== Commits URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            commit_tags = soup.find_all('a', class_='message')
            contents = []
            for commit_tag in commit_tags:
                com_u = commit_tag.get('href').strip()
                commit_id = com_u.split('/')[-1]
                commit_url = self.__base_url + com_u + '.patch'
                if commit_url in self.__url_cache:
                    self.__logger.warning('Data already grabbed for commit ID: {}'.format(commit_id))
                    continue
                # name, email = self.grab_commit(commit_url)

                item = content.copy()
                item['commit_id'] = commit_id
                item['url'] = commit_url

                contents.append((commit_url, item))

            with ThreadPool(32) as p:
                p.map(self.grab_commit, contents)
                # item['name'] = name
                # item['email'] = email
                #
                # try:
                #     self.__logger.info('Data: {}'.format(item))
                #     self.__lock.acquire()
                #     self.__write_data(item)
                #     self.__url_cache.append(commit_id)
                # except Exception as ex:
                #     self.__logger.error('Error grab details page 2: {}'.format(ex))
                # finally:
                #     self.__lock.release()
        except Exception as x:
            self.__logger.error('Error when grab commits page.{}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_commits(args, retry + 1)

    def grab_commit(self, args, retry=0):
        try:
            url = args[0]
            item = args[1]

            self.__logger.info('=== Commit URL: {} ==='.format(url))
            opener = self.__create_opener()
            data = opener.open(url).read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return '', ''

            name = ''
            email = ''
            email_m = re.search(r'From\:([^\<]*)\<([^\>]*)\>', data)
            if email_m:
                name = email_m.group(1)
                name = name.strip()
                email = email_m.group(2)
                email = email.strip()
            # return name, email

            item['name'] = name
            item['email'] = email

            try:
                self.__logger.info('Data: {}'.format(item))
                self.__lock.acquire()
                self.__write_data(item)
                self.__url_cache.append(item['url'])
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

        except Exception as x:
            self.__logger.error('Error when grab commit page!{}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info('Script is going to sleep for {} seconds.'.format(sleep_time))
                time.sleep(sleep_time)
                return self.grab_commit(args, retry + 1)

        return '', ''

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = 'github.csv'
    with GithubSpider(output_file) as spider:
        spider.grab_all_data()
