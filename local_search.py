import csv
import gzip
import re
import socket
import string
import urllib.request
import urllib.parse
from multiprocessing import Lock, Pool
import time
from bs4 import BeautifulSoup


class LocalSearchGrabber:
    __lock = Lock()
    MAX_PAGE = 55537
    __category_url = 'https://www.localsearch.com.au/Categories/List_{}'
    __region_url = 'https://www.localsearch.com.au/Regions'
    __base_url = 'https://www.localsearch.com.au'
    __main_url = 'https://www.localsearch.com.au/find/{}/{}'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1'}

    def __init__(self, output_csv):
        self.__output_csv = output_csv
        socket.setdefaulttimeout(15)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            # csv_header = ['Name', 'Email', 'URL']
            # self.__write_data(csv_header)
            # categories = self.__read_category()
            # print(categories)
            # regions = self.__read_region()
            # print(regions)
            #
            # for category in categories:
            #     for region in regions:
            #         url = self.__main_url.format(urllib.parse.quote_plus(category), urllib.parse.quote_plus(region))
            #         self.__grab_details(url)
            # grab category
            # self.__grab_category_list()

            # grab regions
            self.__grab_regions()

            # grab details
            # url = 'https://www.localsearch.com.au/find/plumbers/nt/alice-springs-region'
            # self.__grab_details(url)


            # csv_header = ['Name', 'Email', 'URL']
            # self.__write_data(csv_header)
            # for i in range(1, self.MAX_PAGE, 8):
            #     # start 4 worker processes
            #     with Pool(processes=8) as pool:
            #         pool.map(self.pool_data, range(i, i + 8))
        except Exception as x:
            print(x)

    def __grab_regions(self):
        try:
            print('=== Region URL: {} ==='.format(self.__region_url))
            req = urllib.request.Request(self.__region_url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            ul_list = soup.find_all('ul', class_='list-unstyled col-md-3 col-sm-3')
            for ul in ul_list:
                li_list = ul.find_all('li')
                for li in li_list:
                    region = li.find('a').get('href').lower()
                    self.__write_region(region)

        except Exception as x:
            print(x)

    def __grab_category_list(self):
        try:
            for url in self.__generate_category_links():
                categories = self.__grab_category_by_link(url)
                if categories:
                    for category in categories:
                        self.__write_category(category)
        except Exception as x:
            print(x)

    def __grab_category_by_link(self, url):
        try:
            print('=== Category URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            ul_list = soup.find_all('ul', class_='list-unstyled col-md-3 col-sm-3')
            for ul in ul_list:
                li_list = ul.find_all('li')
                for li in li_list:
                    link = li.find('a').get('href').split('/')[-1].strip()
                    link = str(link).lower()
                    yield link
        except Exception as x:
            print(x)

    def __generate_category_links(self):
        for c in string.ascii_uppercase:
            url = self.__category_url.format(c)
            yield url

    def __grab_details(self, url):
        try:
            print('=== Main URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            listings = soup.find('div', id='listings')
            if not listings:
                return

            article_list = listings.find_all('article')
            if not article_list or len(article_list) == 0:
                return

            for article in article_list:
                try:
                    name, link, email = '', '', ''
                    h3_tag = article.find('h3', {'itemprop': 'name'})
                    if h3_tag:
                        name = h3_tag.text.strip()
                        link_tag = h3_tag.find_parent('a')
                        if link_tag:
                            link = self.__base_url + str(link_tag.get('href')).strip()
                    email_link = article.find('link', {'itemprop': 'email'})
                    if email_link:
                        email = email_link.get('href').strip()
                        email = re.sub(r'mailto\:', '', email)

                    csv_data = [name, email, link]
                    print(csv_data)
                    self.__write_data(csv_data)
                except Exception as ex:
                    print(ex)
        except Exception as x:
            print(x)

    def pool_data(self, page):
        try:
            url = self.__base_url.format(page)
            print('=== URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            result_div = soup.find('div', class_='search-results clearfix column')
            if not result_div:
                return

            results = result_div.find_all('div', class_='results-block')
            for result in results:
                try:
                    name, email, url = '', '', ''
                    title_tag = result.find('h3', class_='panel-title')
                    if title_tag:
                        tag = title_tag.find_all('a')[-1]
                        name = tag.text.strip()
                        url = tag.get('href')
                    email_m = re.search(r'<a href="mailto\:([^"]*?)"', str(result), re.MULTILINE)
                    if email_m:
                        email = email_m.group(1)

                    csv_data = [name, email, url]
                    print(csv_data)
                    self.__write_data(csv_data)
                except Exception as ex:
                    print(ex)
        except Exception as x:
            print(x)

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)
        except Exception as x:
            print('Error printing csv output: ' + str(x))
        finally:
            self.__lock.release()

    @staticmethod
    def __write_category(category):
        try:
            with open('category.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __write_region(category):
        try:
            with open('region.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __read_category():
        try:
            categories = []
            with open('category.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    category = line.strip()
                    categories.append(category)
            return categories
        except Exception as x:
            print(x)

    @staticmethod
    def __read_region():
        try:
            regions = []
            with open('region.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    region = line.strip()
                    regions.append(region)
            return regions
        except Exception as x:
            print(x)


if __name__ == '__main__':
    with LocalSearchGrabber('local_search_output_11_06_2019.csv') as spider:
        spider.grab_data()
