import csv
import gzip
import json
import random
import time
import logging
import os
import re
import socket
import readline
import urllib.parse
import urllib.request
from bs4 import BeautifulSoup

readline.parse_and_bind("control-v: paste")


class CmbaGrabber:
    __start_url = 'http://www.cmba-achc.ca/search/search.php'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'X-Requested-With': 'XMLHttpRequest',
        'Referer': 'http://www.cmba-achc.ca/search/index.htm',
        'Origin': 'http://www.cmba-achc.ca',
        'Host': 'www.cmba-achc.ca',
        'Cookie': '_icl_current_language=en',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__setup_logger('cmba.log')
        socket.setdefaulttimeout(60)
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)

        self.__field_names = ['name', 'company_name', 'city', 'phone', 'email']
        csv_header = {'name': 'Name',
                      'company_name': 'Company Name',
                      'city': 'City',
                      'phone': 'Phone',
                      'email': 'Email'}

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['email'])

        if csv_header['email'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            search_in_list = ['city', 'company']
            with open('cities.txt', 'r+', encoding='utf-8') as rf:
                for kw_line in rf.readlines():
                    keyword = kw_line.strip()
                    self.__logger.info('Current keyword: {}'.format(keyword))

                    for search_in in search_in_list:
                        post_data = {'searchtext': keyword,
                                     'searchtype': 'anywhere',
                                     'searchin': search_in,
                                     'sortby': 'city'}
                        post_req = urllib.parse.urlencode(post_data).encode('utf-8')
                        self.__logger.info('=== Search URL: {} ==='.format(self.__start_url))
                        req = urllib.request.Request(self.__start_url, headers=self.__headers, data=post_req)
                        data = urllib.request.urlopen(req).read()
                        try:
                            data = gzip.decompress(data).decode('utf-8', 'ignore')
                        except Exception as e:
                            data = data.decode('utf-8', 'ignore')

                        json_data = json.loads(data)
                        if json_data:
                            for record in json_data:

                                if record['email'] not in self.__url_cache:
                                    item = {}
                                    item['name'] = record['first_name'] + ' ' + record['last_name']
                                    item['company_name'] = record['company']
                                    item['city'] = record['city']
                                    item['phone'] = record['phone']
                                    item['email'] = record['email']

                                    self.__logger.info('Data: {}'.format(item))
                                    self.__write_data(item)

                                    self.__url_cache.append(item['email'])
                                else:
                                    self.__logger.warning('Already grabbed Email: {}.'.format(record['email']))
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    def __cleanup(self, data):
        try:
            result = urllib.parse.unquote_plus(data.encode('utf-8').decode('unicode-escape', 'ignore'))
            result = re.sub(r'&nbsp;', ' ', result)
            result = result.strip()
            return result
        except Exception as x:
            self.__logger.error('Error cleanup: {}'.format(x))
        return data

    def __sleep_script(self, min_sleep=5, max_sleep=15):
        sleep_time = random.randint(min_sleep, max_sleep)
        self.__logger.info(
            'Script is going to sleep for {} seconds to get rid from block.'.format(sleep_time))
        time.sleep(sleep_time)

    def __write_data(self, row, mode='a+'):
        try:
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    output_file = input('Please specify output csv: ')

    # input_file = 'input.csv'
    # output_file = 'cmba_output_final.csv'
    with CmbaGrabber(output_file) as spider:
        spider.grab_data()
