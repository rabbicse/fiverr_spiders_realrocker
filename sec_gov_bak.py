import csv
import datetime
import json
import logging
import os
import re
import urllib.request
import urllib.parse
from collections import OrderedDict
from multiprocessing import Semaphore, Value
from multiprocessing.pool import Pool, ThreadPool

import requests
from bs4 import BeautifulSoup, NavigableString
from easy_spider import Spider

logger = logging.getLogger(__name__)

from sqlalchemy import create_engine, Column, Integer, UnicodeText, Unicode, func, inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DB_HOST = '127.0.0.1'
DB_PORT = '3306'
DB_NAME = 'sec_gov'
DB_USERNAME = 'admin'
DB_PASS = 'password'

logger = logging.getLogger(__name__)

DeclarativeBase = declarative_base()


def db_connect():
    """Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance.
    """
    connection_str = 'mysql+mysqldb://{}:{}@{}:{}/{}?charset=utf8&use_unicode=1'.format(DB_USERNAME, DB_PASS, DB_HOST,
                                                                                        DB_PORT, DB_NAME)
    return create_engine(connection_str)


def create_tables(engine):
    """"""
    DeclarativeBase.metadata.create_all(engine)


def to_dict(obj, with_relationships=True):
    d = {}
    for column in obj.__table__.columns:
        if with_relationships and len(column.foreign_keys) > 0:
            # Skip foreign keys
            continue
        d[column.name] = getattr(obj, column.name)

    if with_relationships:
        for relationship in inspect(type(obj)).relationships:
            val = getattr(obj, relationship.key)
            d[relationship.key] = to_dict(val) if val else None
    return d


class Model(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "sec_gov"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    issuerCik = Column(Unicode(255))
    issuerTradingSymbol = Column(Unicode(255))
    issuerName = Column(Unicode(255))
    issuerRank = Column(Unicode(255))
    filings = Column(UnicodeText)
    filingDocumentsUrl = Column(Unicode(255))
    descriptionLine1 = Column(Unicode(255))
    descriptionLine2 = Column(Unicode(255))
    filingDate = Column(Unicode(255))
    fileNumber = Column(Unicode(255))
    filmNumber = Column(Unicode(255))
    Issuer_IrsNo = Column(UnicodeText)
    Issuer_StateOfIncrop = Column(Unicode(255))
    Issuer_BusinessAddress = Column(Unicode(255))
    Issuer_Phone = Column(Unicode(255))
    Issuer_MailingAddress = Column(Unicode(255))
    ReportingOwner_Name = Column(Unicode(255))
    ReportingOwner_Cik = Column(Unicode(255))
    ReportingOwner_IrsNo = Column(Unicode(255))
    ReportingOwner_StateOfIncorp = Column(Unicode(255))
    ReportingOwner_FiscalYearEnd = Column(Unicode(255))
    ReportingOwner_BusinessAddress = Column(Unicode(255))
    ReportingOwner_Phone = Column(Unicode(255))
    ReportingOwner_MailingAddress = Column(Unicode(255))
    DocumentUrl_HtmlFile = Column(Unicode(255))
    DocumentUrl_Xml = Column(Unicode(255))
    DocumentName_Xml = Column(Unicode(255))
    DocumentUrl_Txt = Column(Unicode(255))


class IssuerModel(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "issuer"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    issuerCik = Column(Unicode(100))
    issuerName = Column(Unicode(255))
    issuerTradingSymbol = Column(Unicode(255))
    issuerRank = Column(Unicode(100))
    Issuer_IrsNo = Column(Unicode(100))
    Issuer_StateOfIncrop = Column(Unicode(100))
    Issuer_BusinessAddress = Column(UnicodeText)
    Issuer_Phone = Column(Unicode(100))
    Issuer_MailingAddress = Column(UnicodeText)

    issuerCik_xml = Column(Unicode(100))
    issuerName_xml = Column(UnicodeText)
    issuerTradingSymbol_xml = Column(Unicode(255))


class ReportingOwnerModel(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "reporting_owner"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    ReportingOwner_Name = Column(Unicode(255))
    ReportingOwner_Cik = Column(Unicode(100))
    ReportingOwner_IrsNo = Column(Unicode(100))
    ReportingOwner_StateOfIncorp = Column(Unicode(100))
    ReportingOwner_FiscalYearEnd = Column(Unicode(100))
    ReportingOwner_BusinessAddress = Column(UnicodeText)
    ReportingOwner_Phone = Column(Unicode(100))
    ReportingOwner_MailingAddress = Column(UnicodeText)

    # from xml
    reportingOwnerId_rptOwnerCik = Column(UnicodeText)
    reportingOwnerId_rptOwnerCcc = Column(UnicodeText)
    reportingOwnerId_rptOwnerName = Column(UnicodeText)

    reportingOwnerAddress_rptOwnerStreet1 = Column(UnicodeText)
    reportingOwnerAddress_rptOwnerStreet2 = Column(UnicodeText)
    reportingOwnerAddress_rptOwnerCity = Column(UnicodeText)
    reportingOwnerAddress_rptOwnerState = Column(UnicodeText)
    reportingOwnerAddress_rptOwnerZipCode = Column(UnicodeText)
    reportingOwnerAddress_rptOwnerStateDescription = Column(UnicodeText)
    reportingOwnerAddress_rptOwnerGoodAddress = Column(UnicodeText)

    reportingOwnerRelationship_isDirector = Column(UnicodeText)
    reportingOwnerRelationship_isOfficer = Column(UnicodeText)
    reportingOwnerRelationship_isTenPercentOwner = Column(UnicodeText)
    reportingOwnerRelationship_isOther = Column(UnicodeText)
    reportingOwnerRelationship_officerTitle = Column(UnicodeText)
    reportingOwnerRelationship_otherText = Column(UnicodeText)


class OwnershipDocument(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "ownershipDocument"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    schemaVersion = Column(UnicodeText)
    periodOfReport = Column(Unicode(255))
    dateOfOriginalSubmission = Column(Unicode(50))
    notSubjectToSection16 = Column(UnicodeText)
    form3HoldingsReported = Column(UnicodeText)
    form4TransactionsReported = Column(UnicodeText)
    noSecuritiesOwned = Column(UnicodeText)
    documentType = Column(Unicode(255))
    footnotes = Column(UnicodeText)
    remarks = Column(UnicodeText)
    ownerSignature_signatureName = Column(UnicodeText)
    ownerSignature_signatureDate = Column(Unicode(50))

    issuerCik = Column(Integer)
    ReportingOwner_Cik = Column(Integer)


class DerivativeTable(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "derivativeTable"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    xml_id = Column(Integer)
    issuerCik = Column(Unicode(100))
    ReportingOwner_Cik = Column(Unicode(100))


class DerivativeTransaction(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "derivativeTransaction"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    dt_id = Column(Integer)

    securityTitle_footnoteId = Column(UnicodeText)
    securityTitle_value = Column(UnicodeText)
    conversionOrExercisePrice_footnoteId = Column(UnicodeText)
    conversionOrExercisePrice_value = Column(UnicodeText)
    transactionDate_footnoteId = Column(UnicodeText)
    transactionDate_value = Column(UnicodeText)
    deemedExecutionDate_footnoteId = Column(UnicodeText)
    deemedExecutionDate_value = Column(UnicodeText)
    transactionCoding_footnoteId = Column(UnicodeText)
    transactionCoding_transactionFormType = Column(UnicodeText)
    transactionCoding_transactionCode = Column(UnicodeText)
    transactionCoding_equitySwapInvolved = Column(UnicodeText)
    transactionTimeliness_footnoteId = Column(UnicodeText)
    transactionTimeliness_value = Column(UnicodeText)
    ta_transactionShares_footnoteId = Column(UnicodeText)
    ta_transactionShares_value = Column(UnicodeText)
    ta_transactionTotalValue_footnoteId = Column(UnicodeText)
    ta_transactionTotalValue_value = Column(UnicodeText)
    ta_transactionPricePerShare_footnoteId = Column(UnicodeText)
    ta_transactionPricePerShare_value = Column(UnicodeText)
    ta_transactionAcquiredDisposedCode_footnoteId = Column(UnicodeText)
    ta_transactionAcquiredDisposedCode_value = Column(UnicodeText)
    exerciseDate_footnoteId = Column(UnicodeText)
    exerciseDate_value = Column(UnicodeText)
    expirationDate_footnoteId = Column(UnicodeText)
    expirationDate_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_value = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_value = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_value = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_value = Column(UnicodeText)
    ownershipNature_natureOfOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_natureOfOwnership_value = Column(UnicodeText)


class DerivativeHolding(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "derivativeHolding"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    dt_id = Column(Integer)

    securityTitle_footnoteId = Column(UnicodeText)
    securityTitle_value = Column(UnicodeText)
    conversionOrExercisePrice_footnoteId = Column(UnicodeText)
    conversionOrExercisePrice_value = Column(UnicodeText)
    exerciseDate_footnoteId = Column(UnicodeText)
    exerciseDate_value = Column(UnicodeText)
    expirationDate_footnoteId = Column(UnicodeText)
    expirationDate_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_value = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_value = Column(UnicodeText)
    ownershipNature_natureOfOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_natureOfOwnership_value = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_value = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_value = Column(UnicodeText)


class NonDerivativeTable(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "nonDerivativeTable"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    xml_id = Column(Integer)
    issuerCik = Column(Unicode(100))
    ReportingOwner_Cik = Column(Unicode(100))


class NonDerivativeTransaction(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "nonDerivativeTransaction"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    ndt_id = Column(Integer)

    securityTitle_footnoteId = Column(UnicodeText)
    securityTitle_value = Column(UnicodeText)
    conversionOrExercisePrice_footnoteId = Column(UnicodeText)
    conversionOrExercisePrice_value = Column(UnicodeText)
    transactionDate_footnoteId = Column(UnicodeText)
    transactionDate_value = Column(UnicodeText)
    deemedExecutionDate_footnoteId = Column(UnicodeText)
    deemedExecutionDate_value = Column(UnicodeText)
    transactionCoding_footnoteId = Column(UnicodeText)
    transactionCoding_transactionFormType = Column(UnicodeText)
    transactionCoding_transactionCode = Column(UnicodeText)
    transactionCoding_equitySwapInvolved = Column(UnicodeText)
    transactionTimeliness_footnoteId = Column(UnicodeText)
    transactionTimeliness_value = Column(UnicodeText)
    transactionAmounts_transactionShares_footnoteId = Column(UnicodeText)
    transactionAmounts_transactionShares_value = Column(UnicodeText)
    transactionAmounts_transactionTotalValue_footnoteId = Column(UnicodeText)
    transactionAmounts_transactionTotalValue_value = Column(UnicodeText)
    transactionAmounts_transactionPricePerShare_footnoteId = Column(UnicodeText)
    transactionAmounts_transactionPricePerShare_value = Column(UnicodeText)
    transactionAmounts_transactionAcquiredDisposedCode_footnoteId = Column(UnicodeText)
    transactionAmounts_transactionAcquiredDisposedCode_value = Column(UnicodeText)
    exerciseDate_footnoteId = Column(UnicodeText)
    exerciseDate_value = Column(UnicodeText)
    expirationDate_footnoteId = Column(UnicodeText)
    expirationDate_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_value = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_value = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_value = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_value = Column(UnicodeText)
    ownershipNature_natureOfOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_natureOfOwnership_value = Column(UnicodeText)


class NonDerivativeHolding(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "nonDerivativeHolding"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    ndt_id = Column(Integer)

    securityTitle_footnoteId = Column(UnicodeText)
    securityTitle_value = Column(UnicodeText)
    conversionOrExercisePrice_footnoteId = Column(UnicodeText)
    conversionOrExercisePrice_value = Column(UnicodeText)
    exerciseDate_footnoteId = Column(UnicodeText)
    exerciseDate_value = Column(UnicodeText)
    expirationDate_footnoteId = Column(UnicodeText)
    expirationDate_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityTitle_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityShares_value = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_footnoteId = Column(UnicodeText)
    underlyingSecurity_underlyingSecurityValue_value = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_directOrIndirectOwnership_value = Column(UnicodeText)
    ownershipNature_natureOfOwnership_footnoteId = Column(UnicodeText)
    ownershipNature_natureOfOwnership_value = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_sharesOwnedFollowingTransaction_value = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_footnoteId = Column(UnicodeText)
    pta_valueOwnedFollowingTransaction_value = Column(UnicodeText)


# class Footnotes(DeclarativeBase):
#     """Sqlalchemy deals model"""
#     __tablename__ = "footnotes"
#     __table_args__ = {
#         'mysql_charset': 'utf8'
#     }
#
#     def __init__(self, **kwargs):
#         cls_ = type(self)
#         for k in kwargs:
#             if hasattr(cls_, k):
#                 setattr(self, k, kwargs[k])
#
#     id = Column(Integer, primary_key=True)
#     footnoteId = Column(Unicode(50))
#     footnote_key = Column(Unicode(100))
#     footnote_value = Column(UnicodeText)

class Footnotes(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "footnotes"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    footnoteId = Column(Unicode(50))
    footnote_key = Column(Unicode(100))
    footnote_value = Column(UnicodeText)


class OwnerSignature(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "ownerSignature"
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if hasattr(cls_, k):
                setattr(self, k, kwargs[k])

    id = Column(Integer, primary_key=True)
    signatureName = Column(UnicodeText)
    signatureDate = Column(Unicode(50))


# http://tsdr.uspto.gov/statusview/rn5754716
class SecGovCrawler(Spider):
    __start_url = 'https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=0000789019&type=&dateb=&owner=only&start=0&count=100'
    __base_uri = 'https://www.sec.gov'
    __api_url = 'https://t.justdial.com/api/india_api_write/01jan2018/searchziva.php?'
    __url_cache = []
    __page_cache = []
    __total = Value('i', 0)
    __lock = Semaphore()
    __MIN_COUNT = 500

    def __init__(self, input_csv, output_csv):
        Spider.__init__(self, log_file='sec_gov.log')
        self.__input_csv = input_csv + '.csv' if not str(input_csv).endswith('.csv') else input_csv
        self.__output_csv = output_csv + '.csv' if not str(output_csv).endswith('.csv') else output_csv

    def __enter__(self):
        self.__init_full()
        engine = db_connect()
        engine.execution_options(stream_results=True)
        create_tables(engine)
        self.Session = sessionmaker(bind=engine)
        return self

    def __init_full(self):
        hdr = [('url', 'Url'),
               ('issuerCik', 'issuerCik'),
               ('issuerTradingSymbol', 'issuerTradingSymbol'),
               ('issuerName', 'issuerName'),
               ('issuerRank', 'issuerRank'),
               ('filings', 'filings'),
               ('filingDocumentsUrl', 'filingDocumentsUrl'),
               ('descriptionLine1', 'descriptionLine1'),
               ('descriptionLine2', 'descriptionLine2'),
               ('filingDate', 'filingDate'),
               ('fileNumber', 'fileNumber'),
               ('filmNumber', 'filmNumber'),
               ('Issuer_IrsNo', 'Issuer_IrsNo'),
               ('Issuer_StateOfIncrop', 'Issuer_StateOfIncrop'),
               ('Issuer_FiscalYearEnd', 'Issuer_FiscalYearEnd'),
               ('Issuer_SicCode', 'Issuer_SicCode'),
               ('Issuer_SicDescription', 'Issuer_SicDescription'),
               ('Issuer_BusinessAddress', 'Issuer_BusinessAddress'),
               ('Issuer_Phone', 'Issuer_Phone'),
               ('Issuer_MailingAddress', 'Issuer_MailingAddress'),
               ('ReportingOwner_Name', 'ReportingOwner_Name'),
               ('ReportingOwner_Cik', 'ReportingOwner_Cik'),
               ('ReportingOwner_IrsNo', 'ReportingOwner_IrsNo'),
               ('ReportingOwner_StateOfIncorp', 'ReportingOwner_StateOfIncorp'),
               ('ReportingOwner_FiscalYearEnd', 'ReportingOwner_FiscalYearEnd'),
               ('ReportingOwner_BusinessAddress', 'ReportingOwner_BusinessAddress'),
               ('ReportingOwner_Phone', 'ReportingOwner_Phone'),
               ('ReportingOwner_MailingAddress', 'ReportingOwner_MailingAddress'),
               ('DocumentUrl_HtmlFile', 'DocumentUrl_HtmlFile'),
               ('DocumentUrl_Xml', 'DocumentUrl_Xml'),
               ('DocumentName_Xml', 'DocumentName_Xml'),
               ('DocumentUrl_Txt', 'DocumentUrl_Txt'), ]

        self.__csv_header = OrderedDict(hdr)

        self.__field_names = list(self.__csv_header.keys())
        self.__field_values = list(self.__csv_header.values())

        # If output csv file already exists, then cache old website lists, so script will skip hitting existing records
        # if os.path.exists(self.__output_csv):
        #     with open(self.__output_csv, 'r+', encoding='utf-8') as f:
        #         reader = csv.DictReader(f, self.__field_names)
        #         for row in reader:
        #             self.__url_cache.append(row['url'])
        #
        # # If header not yet written then write csv header first
        # if self.__csv_header['url'] not in self.__url_cache:
        #     self.__write_data(self.__csv_header)
        #     self.__url_cache.append(self.__csv_header['url'])

        # self.__prepare_proxies()

        with self.__total.get_lock():
            self.__total.value = len(self.__url_cache)
        logger.info('Total Previous Records Found: {}'.format(self.__total.value))

    def __prepare_proxies(self):
        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.proxies = json_data

        if os.path.exists('proxylist.csv'):
            with open('proxylist.csv', 'r+', encoding='utf-8') as f:
                self.proxies = []
                reader = csv.reader(f)
                for row in reader:
                    if not row:
                        continue

                    if len(row) <= 0:
                        continue

                    p = row[0]
                    https = 'no'
                    if 'https://' in p:
                        https = 'yes'

                    p = p.replace('https://', '')
                    p = p.replace('http://', '')
                    p_list = p.split(':')
                    if len(p_list) < 2:
                        continue

                    ip = p_list[0]  # + ':' + p_list[1]
                    # proxy = {"ip": ''.join(p_list[0:-1]), "port": p_list[-1], "https": "no"}
                    proxy = {"ip": ip, "port": p_list[-1], "https": https}
                    self.proxies.append(proxy)

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('Total records: {}'.format(self.__total.value))
        logger.info('Finish!!!')
        if self.Session:
            del self.Session
        del self

    def process_data(self):
        try:

            # self.download_data('https://www.sec.gov/Archives/edgar/data/1750/000112760219029270/xslF345X03/form4.xml')
            # return
            # kw = {
            #     'issuerCik': '123456',
            #     'url': 'https://www.sec.gov/Archives/edgar/data/1750/000112760219029270/0001127602-19-029270-index.htm'}
            #
            # if not os.path.exists(kw['issuerCik']):
            #     os.mkdir(kw['issuerCik'])
            # self._grab_details(kw)
            # return
            if os.path.exists(self.__input_csv):
                with open(self.__input_csv, 'r', errors='ignore') as f:
                    reader = csv.reader(f, quoting=csv.QUOTE_ALL)

                    items = []
                    for row in reader:
                        if not row or len(row) <= 0 or 'http' not in row[-1]:
                            continue

                        kw = {'issuerCik': row[0],
                              'issuerTradingSymbol': row[1],
                              'issuerName': row[2],
                              'issuerRank': row[3],
                              'url': row[4]}

                        # self.do_process(**kw)
                        # break
                        items.append(kw)
                        break

                    with ThreadPool(32) as pool:
                        pool.map(self.do_process, items)

            # self.do_process(self.__start_url.format(0))
        except Exception as x:
            logger.error('Error when process data: {}'.format(x))

    def do_process(self, kwargs):
        try:
            kw = kwargs.copy()
            if not os.path.exists(kw['issuerCik']):
                os.mkdir(kw['issuerCik'])

            output_csv = kw['issuerCik'] + '/' + kw['issuerCik'] + '.csv'
            if os.path.exists(output_csv):
                with open(output_csv, 'r+', encoding='utf-8') as f:
                    reader = csv.DictReader(f, self.__field_names)
                    for row in reader:
                        self.__url_cache.append(row['url'])

            # If header not yet written then write csv header first
            if self.__csv_header['url'] not in self.__url_cache:
                self.__write_data(output_csv, self.__csv_header)
                self.__url_cache.append(self.__csv_header['url'])

            next_page = self.__grab_page(**kw)
            # while next_page:
            #     kw = kwargs.copy()
            #     kw['url'] = next_page
            #     next_page = self.__grab_page(**kw)

        except Exception as x:
            logger.error('Error when get pages: {}'.format(x))

    def __grab_page(self, **kwargs):
        try:
            response = self.fetch_data(kwargs['url'])
            if not response:
                return

            data, redirected_url, opener = response

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            table = soup.find('table', class_='tableFile2')
            if not table:
                return

            rows = table.find_all('tr')
            for row in rows:
                cols = row.find_all('td')
                if not cols or len(cols) < 2:
                    continue

                link_a = cols[1].find('a')
                if not link_a:
                    continue

                link = '{}/{}'.format(self.__base_uri, link_a.get('href'))
                kw = kwargs.copy()
                kw['filings'] = cols[0].text.strip()
                kw['filingDocumentsUrl'] = link

                descs = cols[2].contents
                if descs and len(descs) > 0:
                    try:
                        kw['descriptionLine1'] = cols[2].contents[0].strip()
                        kw['descriptionLine2'] = cols[2].contents[-1].strip()
                    except Exception as ex:
                        print(ex)
                kw['filingDate'] = cols[3].text.strip()
                kw['url'] = link

                self._grab_details(kw)

            # pagination
            next_page = soup.find('input', {'value': 'Next 100'})
            if next_page:
                next_uri = next_page.get('onclick')
                next_uri = next_uri.replace('parent.location=', '')
                next_uri = next_uri.strip('\'')
                # next_page.get('onclick')
                return self.__base_uri + next_uri

        except Exception as x:
            logger.error('Error when parse next page: {}'.format(x))

    def _grab_details(self, kw):
        try:
            kwargs = kw.copy()
            kwargs['ReportingOwner_Cik'] = ''
            logger.info('Details page: {}'.format(kwargs['url']))
            response = self.fetch_data(kwargs['url'])
            if not response:
                return

            data, redirected_url, opener = response
            data = re.sub(r'\<br\s*?\/?\>', '|', data)
            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            divs = soup.find_all('div', id='filerDiv')
            if not divs:
                return

            for div in divs:

                com = div.find('span', class_='companyName')
                if not com:
                    continue

                mailer_type = 'i'
                if 'Issuer' in com.text:
                    mailer_type = 'i'
                else:
                    mailer_type = 'r'

                ident_infos = div.find_all('p', class_='identInfo')
                if ident_infos:
                    for ident_info in ident_infos:
                        print(ident_info.text)
                        texts = ident_info.text.split('|')
                        if not texts:
                            continue
                        for txt in texts:
                            if 'File No.' in txt:
                                kwargs['fileNumber'] = self.parse_text(txt, 'File No.')
                            if 'Film No.' in txt:
                                kwargs['filmNumber'] = self.parse_text(txt, 'Film No.')

                            if 'IRS No.' in txt:
                                if mailer_type == 'i':
                                    kwargs['Issuer_IrsNo'] = self.parse_text(txt, 'IRS No.')
                                else:
                                    kwargs['ReportingOwner_IrsNo'] = self.parse_text(txt, 'IRS No.')
                            if 'State of Incorp.' in txt:
                                if mailer_type == 'i':
                                    kwargs['Issuer_StateOfIncrop'] = self.parse_text(txt, 'State of Incorp.')
                                else:
                                    kwargs['ReportingOwner_StateOfIncorp'] = self.parse_text(txt, 'State of Incorp.')
                            if 'Fiscal Year End' in txt:
                                if mailer_type == 'i':
                                    kwargs['Issuer_FiscalYearEnd'] = self.parse_text(txt, 'Fiscal Year End')
                                else:
                                    kwargs['ReportingOwner_FiscalYearEnd'] = self.parse_text(txt, 'Fiscal Year End')
                            if 'SIC' in txt:
                                sic = self.parse_text(txt, 'SIC')
                                m = re.search(r'(\d+)([^$]*)', sic)
                                if m:
                                    if mailer_type == 'i':
                                        kwargs['Issuer_SicCode'] = m.group(1)
                                        kwargs['Issuer_SicDescription'] = m.group(2).strip()
                                else:
                                    if mailer_type == 'i':
                                        kwargs['Issuer_SicCode'] = sic

                mail_address_list = div.find_all('div', class_='mailer')
                if mail_address_list:
                    for mail_address in mail_address_list:
                        if 'Business Address' in mail_address.text:
                            m_addresses = mail_address.find_all('span', class_='mailerAddress')
                            if m_addresses:
                                if re.match(r'^\s+$', m_addresses[-1].text):
                                    if mailer_type == 'i':
                                        kwargs['Issuer_BusinessAddress'] = ' '.join(
                                            [ma.text for ma in m_addresses[:-1]])
                                        kwargs['Issuer_Phone'] = m_addresses[-1].text.strip()
                                    else:
                                        kwargs['ReportingOwner_BusinessAddress'] = ' '.join(
                                            [ma.text for ma in m_addresses[:-1]])
                                        kwargs['ReportingOwner_Phone'] = m_addresses[-1].text.strip()

                                else:
                                    if mailer_type == 'i':
                                        kwargs['Issuer_BusinessAddress'] = ' '.join([ma.text for ma in m_addresses])
                                    else:
                                        kwargs['ReportingOwner_BusinessAddress'] = ' '.join(
                                            [ma.text for ma in m_addresses])

                        if 'Mailing Address' in mail_address.text:
                            if mailer_type == 'i':
                                kwargs['Issuer_MailingAddress'] = mail_address.text.strip()
                            else:
                                kwargs['ReportingOwner_MailingAddress'] = mail_address.text.strip()

                com_name_tags = div.find_all('span', class_='companyName')
                if com_name_tags:
                    com_name_r = com_name_tags[-1].text.split('\n')[0].replace('(Reporting)', '').strip()
                    if mailer_type == 'i':
                        pass
                    else:
                        kwargs['ReportingOwner_Name'] = com_name_r

                    cik_text = com_name_tags[-1].text.split('\n')[-1]
                    cik = re.sub(r'[^\d]', '', cik_text)
                    if mailer_type == 'i':
                        pass
                    else:
                        kwargs['ReportingOwner_Cik'] = cik

            # download forms
            # kw['issuerCik']
            # dir: issuercjk/url_last_part/
            url_dir = kwargs['url'].split('/')[-1]
            url_dir = re.sub(r'[^\d\-]', '', url_dir).strip('-').strip()
            file_dir = os.path.join(kw['issuerCik'], url_dir)
            if not os.path.exists(file_dir):
                os.mkdir(file_dir)
            form_table = soup.find('table', class_='tableFile')
            if form_table:
                tr_list = form_table.find_all('tr')
                for tr in tr_list:
                    td_list = tr.find_all('td')
                    if not td_list or len(td_list) < 3:
                        continue

                    file_a = td_list[2].find('a')
                    if not file_a:
                        continue

                    filling_date = ''
                    if 'filingDate' in kwargs:
                        filling_date = kwargs['filingDate'].replace('-', '')
                    else:
                        filling_date = datetime.datetime.now().strftime('%Y%m%d')

                    base_file_name = file_dir + '/' + '{}_{}_{}_'.format(filling_date,
                                                                         kwargs['ReportingOwner_Cik'],
                                                                         kwargs['issuerCik'])

                    if file_a.text.strip().endswith(('.htm', '.html')):
                        kwargs['DocumentUrl_HtmlFile'] = self.__base_uri + file_a.get('href')
                        urllib.request.urlretrieve(kwargs['DocumentUrl_HtmlFile'], base_file_name + file_a.text.strip())
                        # html, rh = self.download_data(kwargs['DocumentUrl_HtmlFile'])
                        # with open(base_file_name + file_a.text.strip(), 'wb') as f:
                        #     f.write(html)
                    elif file_a.text.strip().endswith('.xml'):
                        kwargs['DocumentUrl_Xml'] = self.__base_uri + file_a.get('href')
                        urllib.request.urlretrieve(kwargs['DocumentUrl_Xml'], base_file_name + file_a.text.strip())
                        kwargs['DocumentName_Xml'] = base_file_name + file_a.text.strip()
                        # xml, rx = self.download_data(kwargs['DocumentUrl_Xml'])
                        # with open(base_file_name + file_a.text.strip(), 'w+') as f:
                        #     f.write(xml.decode('utf-8', 'ignore'))
                        #     # kwargs['DocumentName_Xml'] = self.__base_uri + file_a.get('href')
                        #     kwargs['DocumentName_Xml'] = base_file_name + file_a.text.strip()
                    elif file_a.text.strip().endswith('.txt'):
                        kwargs['DocumentUrl_Txt'] = self.__base_uri + file_a.get('href')
                        urllib.request.urlretrieve(kwargs['DocumentUrl_Txt'], base_file_name + file_a.text.strip())
                        # txt, rx = self.download_data(kwargs['DocumentUrl_Txt'])
                        # with open(base_file_name + file_a.text.strip(), 'wb') as f:
                        #     f.write(txt)

            output_csv = kw['issuerCik'] + '/' + kw['issuerCik'] + '.csv'
            self.__write_item(output_csv, kwargs)

            with open(kwargs['DocumentName_Xml'], 'r+') as f:
                xml = f.read()
                self.parse_xml(xml, kwargs)
        except Exception as x:
            logger.error('Error when parse details page: {}'.format(x))

    def xml_test(self, file_name):
        with open(file_name, 'r+') as f:
            xml = f.read()
            kwargs = {}
            self.parse_xml(xml, kwargs)

    def parse_text(self, txt, k):
        return txt.replace(k, '').replace(':', '').strip()

    def parse_xml(self, xml_data, kwargs):
        try:
            # session = self.Session()
            soup = BeautifulSoup(xml_data, 'lxml-xml')
            root = soup.find('ownershipDocument')
            if not root:
                return

            # sec_gov = Model(**kwargs)
            # session.add(sec_gov)
            # session.commit()

            # xml_tbl = {}
            # schema_version = root.find('schemaVersion')
            # if schema_version:
            #     xml_tbl['schemaVersion'] = schema_version.text.strip()
            # document_type = root.find('documentType')
            # if document_type:
            #     xml_tbl['documentType'] = document_type.text.strip()
            # period_of_report = root.find('periodOfReport')
            # if period_of_report:
            #     xml_tbl['periodOfReport'] = period_of_report.text.strip()

            # parse derivative table
            derivative_table = root.find('derivativeTable')
            self.parse_derivative_table(derivative_table)

            # parse non derivative table
            non_derivative_table = root.find('nonDerivativeTable')
            self.parse_non_derivative_table(non_derivative_table)

            # transaction_table = root.find('nonDerivativeTable')
            # if transaction_table:
            #     transactions = transaction_table.find_all('nonDerivativeTransaction')
            #     for transaction in transactions:
            #         transaction_model = {'issuerCik': kwargs['issuerCik'],
            #                              'ReportingOwner_Cik': kwargs['ReportingOwner_Cik']}
            #
            #         for child in transaction.children:
            #             if not child or not child.name:
            #                 continue
            #
            #             if 'securityTitle' in child.name:
            #                 transaction_model['securityTitle'] = child.text.strip()
            #
            #             if 'transactionDate' in child.name:
            #                 transaction_model['transactionDate'] = child.text.strip()
            #
            #             if 'ownershipNature' in child.name:
            #                 transaction_model['ownershipNature'] = child.text.strip()
            #
            #             if 'transactionTimeliness' in child.name:
            #                 transaction_model['transactionTimeliness'] = child.text.strip()
            #
            #             if 'transactionCoding' in child.name:
            #                 lst = []
            #                 for tr in child.children:
            #                     if not tr or not tr.name:
            #                         continue
            #                     lst.append('{}: {}'.format(tr.name, tr.text.strip()))
            #                 transaction_model['transactionCoding'] = ', '.join(lst)
            #
            #             if 'transactionAmounts' in child.name:
            #                 for tr in child.children:
            #                     if not tr or not tr.name:
            #                         continue
            #
            #                     if 'transactionShares' in tr.name:
            #                         transaction_model['transactionShares'] = tr.text.strip()
            #
            #                     if 'transactionPricePerShare' in tr.name:
            #                         transaction_model['transactionPricePerShare'] = tr.text.strip()
            #
            #                     if 'transactionAcquiredDisposedCode' in tr.name:
            #                         transaction_model['transactionAcquiredDisposedCode'] = tr.text.strip()
            #                     # lst.append('{}: {}'.format(tr.name, tr.text.strip()))
            #                     # transaction_model['transactionAmounts'] = ', '.join(lst)
            #
            #             if 'postTransactionAmounts' in child.name:
            #                 lst = []
            #                 for tr in child.children:
            #                     if not tr or not tr.name:
            #                         continue
            #                     lst.append('{}: {}'.format(tr.name, tr.text.strip()))
            #                 transaction_model['postTransactionAmounts'] = ', '.join(lst)
            #
            #             if 'ownershipNature' in child.name:
            #                 lst = []
            #                 for tr in child.children:
            #                     if not tr or not tr.name:
            #                         continue
            #                     lst.append('{}: {}'.format(tr.name, tr.text.strip()))
            #                 transaction_model['ownershipNature'] = ', '.join(lst)
            #
            #             if 'underlyingSecurity' in child.name:
            #                 lst = []
            #                 for tr in child.children:
            #                     if not tr or not tr.name:
            #                         continue
            #                     lst.append('{}: {}'.format(tr.name, tr.text.strip()))
            #                 transaction_model['underlyingSecurity'] = ', '.join(lst)
            #
            #         # tr_model = TransactionModel(**transaction_model)
            #         # session.add(tr_model)
            #         # session.commit()

            # issuer = root.find('issuer')
            # issuer_dict = {
            #     'issuerCik': kwargs['issuerCik'] if 'issuerCik' in kwargs else '',
            #     'issuerTradingSymbol': kwargs['issuerTradingSymbol'] if 'issuerTradingSymbol' in kwargs else '',
            #     'issuerName': kwargs['issuerName'] if 'issuerName' in kwargs else '',
            #     'issuerRank': kwargs['issuerRank'] if 'issuerRank' in kwargs else '',
            #     'Issuer_IrsNo': kwargs['Issuer_IrsNo'] if 'Issuer_IrsNo' in kwargs else '',
            #     'Issuer_StateOfIncrop': kwargs['Issuer_StateOfIncrop'] if 'Issuer_StateOfIncrop' in kwargs else '',
            #     'Issuer_BusinessAddress': kwargs['Issuer_BusinessAddress'] if 'Issuer_BusinessAddress' else '',
            #     'Issuer_Phone': kwargs['Issuer_Phone'] if 'Issuer_Phone' in kwargs else '',
            #     'Issuer_MailingAddress': kwargs['Issuer_MailingAddress'] if 'Issuer_MailingAddress' in kwargs else '',
            #     'issuerCik_xml': self.parse_xml_by_tag(issuer, 'issuerCik'),
            #     'issuerName_xml': self.parse_xml_by_tag(issuer, 'issuerName'),
            #     'issuerTradingSymbol_xml': self.parse_xml_by_tag(issuer, 'issuerTradingSymbol')}
            # issuer_mdl = IssuerModel(**issuer_dict)
            # session.add(issuer_mdl)
            # session.commit()

            # reporting_owner = root.find('reportingOwner')
            # reporting_owner_dict = {
            #     'ReportingOwner_Name': kwargs['ReportingOwner_Name'] if 'ReportingOwner_Name' in kwargs else '',
            #     'ReportingOwner_Cik': kwargs['ReportingOwner_Cik'] if 'ReportingOwner_Cik' in kwargs else '',
            #     'ReportingOwner_IrsNo': kwargs['ReportingOwner_IrsNo'] if 'ReportingOwner_IrsNo' in kwargs else '',
            #     'ReportingOwner_StateOfIncorp': kwargs[
            #         'ReportingOwner_StateOfIncorp'] if 'ReportingOwner_StateOfIncorp' in kwargs else '',
            #     'ReportingOwner_FiscalYearEnd': kwargs[
            #         'ReportingOwner_FiscalYearEnd'] if 'ReportingOwner_FiscalYearEnd' in kwargs else '',
            #     'ReportingOwner_BusinessAddress': kwargs[
            #         'ReportingOwner_BusinessAddress'] if 'ReportingOwner_BusinessAddress' in kwargs else '',
            #     'ReportingOwner_Phone': kwargs['ReportingOwner_Phone'] if 'ReportingOwner_Phone' in kwargs else '',
            #     'ReportingOwner_MailingAddress': kwargs[
            #         'ReportingOwner_MailingAddress'] if 'ReportingOwner_MailingAddress' in kwargs else '',
            # }
            # reporting_owner_mdl = ReportingOwnerModel(**reporting_owner_dict)
            # session.add(reporting_owner_mdl)
            # session.commit()
        except Exception as x:
            logger.error('Error when process data: {}'.format(x))
            # if session:
            #     session.rollback()
        # finally:
        #     if session:
        #         # session.commit()
        #         session.close()

    def parse_derivative_table(self, root):
        # parse all nonDerivativeTransaction
        ndt_list = root.find_all('derivativeTransaction')
        for ndt in ndt_list:
            ndt_dict = {}
            self.parse_childrens(ndt_dict, ndt, '')
            jdt = json.dumps(ndt_dict, indent=4, sort_keys=True)
            print(jdt)

        # parse all nonDerivativeTransaction
        ndh_list = root.find_all('derivativeHolding')
        for ndh in ndh_list:
            ndh_dict = {}
            self.parse_childrens(ndh_dict, ndh, '')
            jdh = json.dumps(ndh_dict, indent=4, sort_keys=True)
            print(jdh)

    def parse_non_derivative_table(self, root):
        # parse all nonDerivativeTransaction
        ndt_list = root.find_all('nonDerivativeTransaction')
        for ndt in ndt_list:
            ndt_dict = {}
            self.parse_childrens(ndt_dict, ndt, '')
            jdt = json.dumps(ndt_dict, indent=4, sort_keys=True)
            print(jdt)

        # parse all nonDerivativeTransaction
        ndh_list = root.find_all('nonDerivativeHolding')
        for ndh in ndh_list:
            ndh_dict = {}
            self.parse_childrens(ndh_dict, ndh, '')
            jdh = json.dumps(ndh_dict, indent=4, sort_keys=True)
            print(jdh)

    def parse_childrens(self, di, root, prefix=''):
        if isinstance(root, NavigableString):
            # print(prefix)
            if root.strip() != '':
                prefix = prefix.replace('postTransactionAmounts_', 'pta_')
                di[prefix] = root.strip()
            return

        ch = None
        try:
            ch = root.children
        except Exception as x:
            print(x)
        if not ch:
            return
        for c in ch:
            self.parse_childrens(di, c,
                                 (prefix + '_' if prefix != '' else prefix) + '{}'.format(c.name) if c.name else prefix)

    # def parse_xml(self, xml_data, kwargs):
    #     try:
    #         session = self.Session()
    #         soup = BeautifulSoup(xml_data, 'lxml-xml')
    #         root = soup.find('ownershipDocument')
    #         if not root:
    #             return
    #
    #         sec_gov = Model(**kwargs)
    #         session.add(sec_gov)
    #         session.commit()
    #
    #         xml_tbl = {}
    #         schema_version = root.find('schemaVersion')
    #         if schema_version:
    #             xml_tbl['schemaVersion'] = schema_version.text.strip()
    #         document_type = root.find('documentType')
    #         if document_type:
    #             xml_tbl['documentType'] = document_type.text.strip()
    #         period_of_report = root.find('periodOfReport')
    #         if period_of_report:
    #             xml_tbl['periodOfReport'] = period_of_report.text.strip()
    #
    #         transaction_table = root.find('nonDerivativeTable')
    #         if transaction_table:
    #             transactions = transaction_table.find_all('nonDerivativeTransaction')
    #             for transaction in transactions:
    #                 transaction_model = {'issuerCik': kwargs['issuerCik'],
    #                                      'ReportingOwner_Cik': kwargs['ReportingOwner_Cik']}
    #
    #                 for child in transaction.children:
    #                     if not child or not child.name:
    #                         continue
    #
    #                     if 'securityTitle' in child.name:
    #                         transaction_model['securityTitle'] = child.text.strip()
    #
    #                     if 'transactionDate' in child.name:
    #                         transaction_model['transactionDate'] = child.text.strip()
    #
    #                     if 'ownershipNature' in child.name:
    #                         transaction_model['ownershipNature'] = child.text.strip()
    #
    #                     if 'transactionTimeliness' in child.name:
    #                         transaction_model['transactionTimeliness'] = child.text.strip()
    #
    #                     if 'transactionCoding' in child.name:
    #                         lst = []
    #                         for tr in child.children:
    #                             if not tr or not tr.name:
    #                                 continue
    #                             lst.append('{}: {}'.format(tr.name, tr.text.strip()))
    #                         transaction_model['transactionCoding'] = ', '.join(lst)
    #
    #                     if 'transactionAmounts' in child.name:
    #                         for tr in child.children:
    #                             if not tr or not tr.name:
    #                                 continue
    #
    #                             if 'transactionShares' in tr.name:
    #                                 transaction_model['transactionShares'] = tr.text.strip()
    #
    #                             if 'transactionPricePerShare' in tr.name:
    #                                 transaction_model['transactionPricePerShare'] = tr.text.strip()
    #
    #                             if 'transactionAcquiredDisposedCode' in tr.name:
    #                                 transaction_model['transactionAcquiredDisposedCode'] = tr.text.strip()
    #                             # lst.append('{}: {}'.format(tr.name, tr.text.strip()))
    #                             # transaction_model['transactionAmounts'] = ', '.join(lst)
    #
    #                     if 'postTransactionAmounts' in child.name:
    #                         lst = []
    #                         for tr in child.children:
    #                             if not tr or not tr.name:
    #                                 continue
    #                             lst.append('{}: {}'.format(tr.name, tr.text.strip()))
    #                         transaction_model['postTransactionAmounts'] = ', '.join(lst)
    #
    #                     if 'ownershipNature' in child.name:
    #                         lst = []
    #                         for tr in child.children:
    #                             if not tr or not tr.name:
    #                                 continue
    #                             lst.append('{}: {}'.format(tr.name, tr.text.strip()))
    #                         transaction_model['ownershipNature'] = ', '.join(lst)
    #
    #                     if 'underlyingSecurity' in child.name:
    #                         lst = []
    #                         for tr in child.children:
    #                             if not tr or not tr.name:
    #                                 continue
    #                             lst.append('{}: {}'.format(tr.name, tr.text.strip()))
    #                         transaction_model['underlyingSecurity'] = ', '.join(lst)
    #
    #                 # tr_model = TransactionModel(**transaction_model)
    #                 # session.add(tr_model)
    #                 # session.commit()
    #
    #         issuer = root.find('issuer')
    #         issuer_dict = {
    #             'issuerCik': kwargs['issuerCik'] if 'issuerCik' in kwargs else '',
    #             'issuerTradingSymbol': kwargs['issuerTradingSymbol'] if 'issuerTradingSymbol' in kwargs else '',
    #             'issuerName': kwargs['issuerName'] if 'issuerName' in kwargs else '',
    #             'issuerRank': kwargs['issuerRank'] if 'issuerRank' in kwargs else '',
    #             'Issuer_IrsNo': kwargs['Issuer_IrsNo'] if 'Issuer_IrsNo' in kwargs else '',
    #             'Issuer_StateOfIncrop': kwargs['Issuer_StateOfIncrop'] if 'Issuer_StateOfIncrop' in kwargs else '',
    #             'Issuer_BusinessAddress': kwargs['Issuer_BusinessAddress'] if 'Issuer_BusinessAddress' else '',
    #             'Issuer_Phone': kwargs['Issuer_Phone'] if 'Issuer_Phone' in kwargs else '',
    #             'Issuer_MailingAddress': kwargs['Issuer_MailingAddress'] if 'Issuer_MailingAddress' in kwargs else '',
    #             'issuerCik_xml': self.parse_xml_by_tag(issuer, 'issuerCik'),
    #             'issuerName_xml': self.parse_xml_by_tag(issuer, 'issuerName'),
    #             'issuerTradingSymbol_xml': self.parse_xml_by_tag(issuer, 'issuerTradingSymbol')}
    #         issuer_mdl = IssuerModel(**issuer_dict)
    #         session.add(issuer_mdl)
    #         session.commit()
    #
    #         reporting_owner = root.find('reportingOwner')
    #         reporting_owner_dict = {
    #             'ReportingOwner_Name': kwargs['ReportingOwner_Name'] if 'ReportingOwner_Name' in kwargs else '',
    #             'ReportingOwner_Cik': kwargs['ReportingOwner_Cik'] if 'ReportingOwner_Cik' in kwargs else '',
    #             'ReportingOwner_IrsNo': kwargs['ReportingOwner_IrsNo'] if 'ReportingOwner_IrsNo' in kwargs else '',
    #             'ReportingOwner_StateOfIncorp': kwargs[
    #                 'ReportingOwner_StateOfIncorp'] if 'ReportingOwner_StateOfIncorp' in kwargs else '',
    #             'ReportingOwner_FiscalYearEnd': kwargs[
    #                 'ReportingOwner_FiscalYearEnd'] if 'ReportingOwner_FiscalYearEnd' in kwargs else '',
    #             'ReportingOwner_BusinessAddress': kwargs[
    #                 'ReportingOwner_BusinessAddress'] if 'ReportingOwner_BusinessAddress' in kwargs else '',
    #             'ReportingOwner_Phone': kwargs['ReportingOwner_Phone'] if 'ReportingOwner_Phone' in kwargs else '',
    #             'ReportingOwner_MailingAddress': kwargs[
    #                 'ReportingOwner_MailingAddress'] if 'ReportingOwner_MailingAddress' in kwargs else '',
    #         }
    #         reporting_owner_mdl = ReportingOwnerModel(**reporting_owner_dict)
    #         session.add(reporting_owner_mdl)
    #         session.commit()
    #     except Exception as x:
    #         logger.error('Error when process data: {}'.format(x))
    #         if session:
    #             session.rollback()
    #     finally:
    #         if session:
    #             # session.commit()
    #             session.close()

    def parse_xml_by_tag(self, soup, tag):
        try:
            tag_data = soup.find(tag)
            if tag_data:
                return tag_data.text.strip()
        except Exception as x:
            print(x)
        return ''

    def __write_item(self, output_csv, item):
        """
        Write item to csv file and write logs, lock writing to csv file as we've used multi-thread
        :param item:
        :return:
        """
        try:
            self.__lock.acquire()
            logger.info('Data: {}'.format(item))
            self.__write_data(output_csv, item)
            self.__url_cache.append(item['url'])
        except Exception as ex:
            logger.error('Error write csv: {}'.format(ex))
        finally:
            self.__lock.release()

    def __write_data(self, output_csv, row, mode='a+'):
        """
        :param row:
        :param mode:
        :return:
        """
        try:
            with open(output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                with self.__total.get_lock():
                    self.__total.value += 1
                    logger.info('Total: {}'.format(self.__total.value))
        except Exception as x:
            logger.error('Error printing csv output: {}'.format(x))

    # def __write_data(self, row, mode='a+'):
    #     """
    #     :param row:
    #     :param mode:
    #     :return:
    #     """
    #     try:
    #         with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
    #             writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
    #             writer.writerow(row)
    #
    #             with self.__total.get_lock():
    #                 self.__total.value += 1
    #                 logger.info('Total: {}'.format(self.__total.value))
    #     except Exception as x:
    #         logger.error('Error printing csv output: {}'.format(x))


if __name__ == '__main__':
    # mode = 1
    input_csv = 'input_sec.csv'  # input('Please specify Input csv: ')
    output_csv = 'sec_gov.csv'  # input('Please specify output csv: ')

    # mode = 0
    # input_csv = 'all_city_jd_p.csv'  # input('Please specify Input csv: ')
    # output_csv = 'all_city_jd_output.csv'  # input('Please specify output csv: ')
    # with JustDialCrawler('jd.csv', 'justdial_output_updated.csv') as crawler:

    # mode = input('Please specify Mode (1 for save all urls; 0 for parse details from previously saved pages): ')
    # input_csv = input('Please specify Input csv: ')
    # output_csv = input('Please specify output csv: ')
    with SecGovCrawler(input_csv, output_csv) as crawler:
        crawler.xml_test('test.xml')
        # crawler.process_data()
