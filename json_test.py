# write to a file
import json

d = {'id': 1, 'name': 'rabbi'}
# with open("4forces.json","a+") as f:
#   json.dump([d], f)
#
# # reads it back
# with open("4forces.json","r+") as f:
#   d1 = json.load(f)
#   d1.update(d)
#   json.dump(d1, f)
#   print(d)

# import json
# def append_to_json(_dict,path):
#   with open(path, 'a+') as f:
#     f.seek(0,2)              #Go to the end of file
#     if f.tell() == 0 :       #Check if file is empty
#       json.dump([_dict], f)  #If empty, write an array
#     else :
#       f.seek(-1,2)
#       f.truncate()           #Remove the last character, open the array
#       f.write(' , ')         #Write the separator
#       json.dump(_dict,f)     #Dump the dictionary
#       f.write(']')           #Close the array
#
# append_to_json(d, 'test.json')
# append_to_json(d, 'test.json')
# append_to_json(d, 'test.json')


def write_to_json():
    with open("info_.json", "a") as data:
        information = {'surname': 'surname', 'age': 10}
        data.write(json.dumps(information))
        data.close()

write_to_json()
write_to_json()
write_to_json()