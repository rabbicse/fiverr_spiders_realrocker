import multiprocessing, multiprocessing_logging
import logging
import os
import sys
import threading
import traceback
from logging.handlers import RotatingFileHandler
from multiprocessing.pool import Pool
from multiprocessing import Lock
from io import StringIO

from multiprocessing_logging import install_mp_handler, MultiProcessingHandler





class MultiProcessingLog(logging.Handler):
    def __init__(self, name, mode, maxsize, rotate):
        logging.Handler.__init__(self)

        self._handler = RotatingFileHandler(name, mode, maxsize, rotate)
        self.queue = multiprocessing.Queue(-1)

        t = threading.Thread(target=self.receive)
        t.daemon = True
        t.start()

    def setFormatter(self, fmt):
        logging.Handler.setFormatter(self, fmt)
        self._handler.setFormatter(fmt)

    def receive(self):
        while True:
            try:
                record = self.queue.get()
                self._handler.emit(record)
                print('received on pid {}'.format(os.getpid()))
            except (KeyboardInterrupt, SystemExit):
                raise
            except EOFError:
                break
            except:
                traceback.print_exc(file=sys.stderr)

    def send(self, s):
        self.queue.put_nowait(s)

    def _format_record(self, record):
        # ensure that exc_info and args have been stringified. Removes any
        # chance of unpickleable things inside and possibly reduces message size
        # sent over the pipe
        if record.args:
            record.msg = record.msg % record.args
            record.args = None
        if record.exc_info:
            dummy = self.format(record)
            record.exc_info = None

        return record

    def emit(self, record):
        try:
            s = self._format_record(record)
            self.send(s)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def close(self):
        self._handler.close()
        logging.Handler.close(self)


# class MulFileHandler(RotatingFileHandler):
#     "Multiprocess-safe logging.FileHandler replacement (abstract class)."
#
#     def createLock(self):  # abstract method
#         "Create a lock for serializing access to the underlying I/O."
#         raise NotImplementedError
#
#
# class MultiprocessFileHandler(MulFileHandler):
#     "Multiprocess-safe logging.FileHandler replacement (abstract class)."
#
#     def __init__(self, lock, filename, mode='a', maxBytes=0, backupCount=0, encoding=None, delay=False):
#         self.mlock = None
#         self.createLock = lambda: None
#         MulFileHandler.__init__(self, filename, mode=mode, maxBytes=maxBytes, backupCount=backupCount,
#                                 encoding=encoding, delay=delay)
#         del self.createLock  # now unmask...
#         self.createLock()  # ...and call it
#
#     def createLock(self):  # abstract method
#         "Create a lock for serializing access to the underlying I/O."
#         self.lock = self.mlock


def create_logger():
    # logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger('root')
    logger.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    # fh = MultiProcessingHandler('mp-file', RotatingFileHandler('log.log', maxBytes=2 * 1024 * 1024, backupCount=5))
    # fh.setLevel(logging.INFO)
    # fh.setFormatter(formatter)

    # create console handler with a higher log level
    stream = StringIO()
    ch = logging.StreamHandler(stream=stream)
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)

    # add the handlers to the logger
    # logger.addHandler(fh)
    logger.addHandler(ch)

    return logger

class PoolTest:
    __lock = Lock()

    # logger = None

    def __init__(self):
        pass

    def __enter__(self):
        self.logger = self.setup_logger()
        # self.logger = create_logger() #self.setup_logger()
        # install_mp_handler(self.logger)

        # multiprocessing.log_to_stderr(logging.DEBUG)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self

    # def setup_logger(self):
    #     # logging.basicConfig(level=logging.DEBUG)
    #     logger = logging.Logger('root')
    #     # logger.setLevel(logging.DEBUG)
    #
    #     # create formatter and add it to the handlers
    #     formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    #
    #     # create file handler which logs even debug messages
    #     # fh = MultiProcessingHandler('mp-file', RotatingFileHandler('log.log', maxBytes=2 * 1024 * 1024, backupCount=5))
    #     # fh.setLevel(logging.INFO)
    #     # fh.setFormatter(formatter)
    #
    #     # create console handler with a higher log level
    #     stream = StringIO()
    #     ch = MultiProcessingHandler('mp-handler', logging.StreamHandler(stream=stream))
    #     # ch = MultiProcessingLog('log.log', 'a', 2 * 1024 * 1024, 5)
    #     ch.setLevel(logging.INFO)
    #     ch.setFormatter(formatter)
    #
    #     # add the handlers to the logger
    #     # logger.addHandler(fh)
    #     # logger.addHandler(ch)
    #
    #     return logger


    def setup_logger(self):
        logger = logging.getLogger('root')


        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = RotatingFileHandler('log.log', maxBytes=2 * 1024 * 1024, backupCount=5)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)

        return logger

    def do_process(self):
        with Pool(32) as pool:
            pool.map(self.do_multiprocess, range(500))

    def do_multiprocess(self, data):
        try:
            self.__lock.acquire()
            print('ok...')
            # sys.stdout.flush()
            self.logger.info('Data: {}'.format(data))
        except Exception as x:
            print(x)
        finally:
            self.__lock.release()


if __name__ == '__main__':
    with PoolTest() as p:
        p.do_process()
