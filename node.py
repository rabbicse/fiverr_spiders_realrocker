# Copyright (C) by Brett Kromkamp 2011-2014 (brett@perfectlearn.com)
# You Programming (http://www.youprogramming.com)
# May 03, 2014


class Node:
    def __init__(self, identifier, parent=None):
        self.__parent = parent
        self.__identifier = identifier
        self.__children = []

    @property
    def parent(self):
        return self.__parent

    @property
    def identifier(self):
        return self.__identifier

    @property
    def children(self):
        return self.__children

    @property
    def child_count(self):
        return len(self.__children)

    def add_child(self, identifier):
        self.__children.append(identifier)
