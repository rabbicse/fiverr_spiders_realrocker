import socket
import urllib.request
from bs4 import BeautifulSoup
from anytree import Node, RenderTree

socket.setdefaulttimeout(20)
headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0',
           'Host': 'www.amazon.co.uk',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}

start_url = 'https://www.amazon.co.uk/Best-Sellers-Welcome/zgbs/ref=zg_bs_unv_0_boost_1'
base_url = 'https://www.amazon.co.uk'


def navigate_top_links(url):
    try:
        print('URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read().decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        navs = soup.find('ul', id='zg_browseRoot').find('ul').find_all('li')
        for nav in navs:
            link = nav.find('a')
            if link:
                yield link.get('href').strip(), link.text.strip()

    except Exception as x:
        print(x)


def navigate_links(url):
    try:
        print('URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read().decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')

        selected_cat = soup.find('span', 'zg_selected')
        if not selected_cat:
            return

        li = selected_cat.find_parent('li')
        if not li:
            return

        next_ul = li.find_next_sibling('ul')
        if not next_ul:
            return

        navs = next_ul.find_all('li')
        for nav in navs:
            link = nav.find('a')
            if link:
                yield link.get('href').strip(), link.text.strip()

    except Exception as x:
        print(x)


# (_ROOT, _DEPTH, _BREADTH) = range(3)
#
# tree = Tree()


def generate_nav_tree(node):
    try:
        tr = navigate_links(node.name[0])
        if tr:
            for link, title in tr:
                child = Node((link, title), node)
                generate_nav_tree(child)
                break
    except Exception as x:
        print(x)


def grab_details(url):
    try:
        print('Product Details URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read().decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        # zg_itemWrapper
        link_divs = soup.find_all('div', class_='zg_itemWrapper')
        for link_div in link_divs:
            link = base_url + link_div.find('a').get('href')
            print(link)

    except Exception as x:
        print(x)


def grab_listing_urls(url):
    try:
        print('Product Listing URL: ' + url)
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req).read().decode('utf-8')
        if not data:
            return

        soup = BeautifulSoup(data, 'lxml')
        # zg_itemWrapper
        link_divs = soup.find_all('div', class_='zg_itemWrapper')
        for link_div in link_divs:
            link = base_url + link_div.find('a').get('href')
            print(link)

        if '&pg=' not in url:
            # zg_page
            links_ol = soup.find('ol', class_='zg_pagination')
            if not links_ol:
                return
            paginated_pages = links_ol.find_all('li', class_='zg_page')
            for page in paginated_pages[1:]:
                page_url = page.find('a').get('href')
                # grab_listing_urls(page_url)

    except Exception as x:
        print(x)


def traverse_and_grab(node):
    try:
        if not node.children:
            # todo more work
            print(node.name)
            grab_listing_urls(node.name[0])
            print(node.depth)

        for child in node.children:
            traverse_and_grab(child)
    except Exception as x:
        print(x)


def process_amazon_bsr():
    # root = Tree('Root')
    # global tree

    # grab_listing_urls('https://www.amazon.co.uk/Best-Sellers-Amazon-Launchpad-Baby/zgbs/boost/7424492031/ref=zg_bs_nav_2_7424489031/259-4367490-9323446')
    # return

    root = Node('Root')
    for link, title in navigate_top_links(start_url):
        parent = Node((link, title), root)
        generate_nav_tree(parent)
        break

    traverse_and_grab(root)

    # parent = Node(('https://www.amazon.co.uk/Best-Sellers-Welcome/zgbs/ref=zg_bs_unv_0_boost_1', 'Title 1'), root)
    # child = Node(('https://www.amazon.co.uk/Best-Sellers-Amazon-Launchpad/zgbs/boost/ref=zg_bs_nav_0/259-2619436-6076948', 'Title 1'), parent)
    # child_1 = Node(('URL: https://www.amazon.co.uk/Best-Sellers-Amazon-Launchpad-Body/zgbs/boost/7424489031/ref=zg_bs_nav_1_boost/259-2619436-6076948', 'Title 2'), child)
    # child2 = Node(('https://www.amazon.co.uk/Best-Sellers-Amazon-Launchpad-Baby/zgbs/boost/7424492031/ref=zg_bs_nav_2_7424489031/259-2619436-6076948', 'Title 3'), child_1)


    # current_node = root
    # while current_node.children:
    #     current_node = current_node.children[0]
    #
    # print(current_node.name)
    # print(RenderTree(root))

    # print(tree.traverse_br(root))
    # for node in tree.traverse_br(root):
    #     print(node.children)

    # tree.display(root)
    # print("***** DEPTH-FIRST ITERATION *****")
    # for node in tree.traverse(root):
    #     print(node.identifier)

    # print("***** BREADTH-FIRST ITERATION *****")
    # for node in tree.traverse(root, mode=_BREADTH):
    #     print(node.identifier)

    # print("***** BREADTH-FIRST ITERATION *****")
    # for node in tree.traverse(root, mode=_BREADTH):
    #     print(node.children)
    #     if node.children is None or node.child_count() == 0:
    #         print(node.identifier)

    # ch_list = tree.traverse_normal(root)
    # for ch in ch_list.children:
    #     traverse_and_grab(ch)

    # if ch.children[0] == 0:
    #     print(ch)
    # else:
    #     print('ok...1')
    # if node.child_count == 0:
    #     print('OK...')

    # for node in tree.traverse_normal(root):
    #     print(node)


if __name__ == '__main__':
    process_amazon_bsr()
