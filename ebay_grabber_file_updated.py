import csv
import gzip
import json
import logging
import os
import random
import re
import socket
import urllib.parse
import urllib.request
from collections import OrderedDict
from multiprocessing.pool import ThreadPool
import time
from bs4 import BeautifulSoup
from multiprocessing import Lock


class EbayGrabber:
    __start_url = 'http://stores.ebay.com/OfficialBestBuy/_i.html?rt=nc&_sasi={}&_pgn={}'
    __search_url = 'https://www.ebay.com/sch/i.html?_from=R40&_sacat=0&_nkw={}&_sop=15'
    # __proxies = [
    #     # '83.149.70.159:13012'
    #     {"ip": "83.149.70.159", "port": "13012", "https": "no"}
    # ]
    __proxies = None
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'}
    __total = 0
    __url_cache = []

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        self.__lock = Lock()
        self.__setup_logger('ebay.log')
        socket.setdefaulttimeout(15)
        hdr = [
            ('url', 'Product Page Url'),
            ('product_id', 'Product ID'),
            ('item_title', 'Title'),
            ('desc', 'Description'),
            ('img_src', 'Image source url'),
            ('sku', 'SKU'),
            ('mpn', 'MPN'),
            ('model', 'Model'),
            ('upc', 'UPC'),
            ('brand', 'Brand'),
            ('category_string', 'Category string'),
            ('sale_price', 'Current Price'),
            ('orig_price', 'Original price'),
            ('shipping_cost', 'Shipping cost'),
            ('bnew_price', 'Brand New Price'),
            ('ref_price', 'Refurbished Price'),
            ('openbox_price', 'Open Box Price'),
            ('pre_owned_price', 'Pre-owned Price'),
            ('condition', 'Condition'),
            ('returns', 'Returns'),
            ('store_name', 'Store Name'),
            ('full_desc', 'Full Description')]
        csv_header = OrderedDict(hdr)
        self.__field_names = list(csv_header.keys())

        if os.path.exists(self.__output_csv):
            with open(self.__output_csv, 'r+', encoding='utf-8') as f:
                reader = csv.DictReader(f, self.__field_names)
                for row in reader:
                    self.__url_cache.append(row['url'])

        if os.path.exists('proxy.json'):
            with open('proxy.json', 'r+', encoding='utf-8') as f:
                json_data = json.load(f)
                self.__proxies = json_data

        if csv_header['url'] not in self.__url_cache:
            self.__write_data(csv_header)

        self.__total = len(self.__url_cache)
        return self

    def __setup_logger(self, log_file_name):
        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create file handler which logs even debug messages
        fh = logging.FileHandler(log_file_name)
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)

        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.__logger.addHandler(fh)
        self.__logger.addHandler(ch)

    def __get_random_proxy(self):
        try:
            return
            if not self.__proxies or len(self.__proxies) == 0:
                return

            proxy = random.choice(self.__proxies)
            self.__logger.info('Proxy: {}'.format(proxy))

            if proxy['https'] == 'yes':
                return {'https': '{}:{}'.format(proxy['ip'], proxy['port'])}
            else:
                return {'http': '{}:{}'.format(proxy['ip'], proxy['port'])}
        except Exception as x:
            self.__logger.error('Error when get random proxies. {}'.format(x))

    def __create_opener(self):
        try:
            proxy = self.__get_random_proxy()
            if proxy:
                proxy_handler = urllib.request.ProxyHandler(proxy)
                opener = urllib.request.build_opener(proxy_handler,
                                                     urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
            else:
                opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(),
                                                     urllib.request.UnknownHandler(),
                                                     urllib.request.HTTPHandler(),
                                                     urllib.request.HTTPSHandler(),
                                                     urllib.request.HTTPRedirectHandler(),
                                                     urllib.request.HTTPDefaultErrorHandler(),
                                                     urllib.request.HTTPErrorProcessor())

                opener.addheaders.clear()
                for key in self.__headers:
                    opener.addheaders.append((key, self.__headers[key]))

                return opener
        except Exception as x:
            self.__logger.error('Error when create opener.{}'.format(x))

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_details_from_file(self, filename):
        try:
            # self.__grab_details([
            #     'https://www.ebay.com/p/Apple-Watch-Series-3-38mm-Space-Gray-Aluminium-Case-with-Black-Sport-Band-GPS-MQKV2LL-A/239054700?thm=2000'])
            # print(self.__url_cache)
            # return
            if os.path.exists(filename):
                with open(filename, 'r+', encoding='utf-8') as f:
                    reader = csv.reader(f, quoting=csv.QUOTE_ALL)
                    index = 0
                    product_urls = []
                    for row in reader:
                        if row[index] != '' and row[index].strip() not in self.__url_cache:
                            if row[index].strip() not in product_urls:
                                product_urls.append([row[index].strip()])
                                # product_urls.append(row)
                        else:
                            self.__logger.info('url: {} already exists!')

                        if len(product_urls) == 512:
                            with ThreadPool(32) as p:
                                p.map(self.__grab_details, product_urls)
                            product_urls = []

                    with ThreadPool(32) as p:
                        p.map(self.__grab_details, product_urls)

                self.__logger.info('==== Finish ====')
        except Exception as x:
            self.__logger.error('Error: {}'.format(x))

    # def __grab_details(self, args, retry=0):
    #     try:
    #         url = args[3]
    #         self.__logger.info('=== Details URL: {} ==='.format(url))
    #
    #         opener = self.__create_opener()
    #         response = opener.open(url)
    #         data = response.read()
    #         try:
    #             data = gzip.decompress(data).decode('utf-8', 'ignore')
    #         except Exception as e:
    #             data = data.decode('utf-8', 'ignore')
    #
    #         if not data:
    #             return
    #
    #         soup = BeautifulSoup(data, 'html5lib')
    #         if not soup:
    #             return
    #
    #         item = {'keyword': args[0], 'position': args[1], 'search_volume': args[2], 'url': url, 'item_title': args[4], 'product_price': args[5]}
    #         # title_tag = soup.find('h1', id='itemTitle')
    #         # if title_tag:
    #         #     item['item_title'] = title_tag.text.strip()
    #
    #         images = re.search(r'\"imgArr\"\s*?\:\s*?(\[[^\]]*?\])', data, re.MULTILINE)
    #         if images:
    #             img_array = images.group(1)
    #             try:
    #                 img_array = urllib.parse.unquote_plus(img_array.encode('utf-8').decode('unicode-escape', 'ignore'))
    #                 json_data = json.loads(img_array)
    #                 if json_data:
    #                     for json_img in json_data:
    #                         if 'maxImageUrl' in json_img:
    #                             item['img_src'] = json_img['maxImageUrl']
    #                             break
    #             except Exception as ex:
    #                 self.__logger.error('Error parsing large image: {}'.format(ex))
    #         else:
    #             img_src_tag = soup.find('img', id='icImg')
    #             if img_src_tag:
    #                 item['img_src'] = img_src_tag.get('src').strip()
    #
    #         desc_tag = soup.find('meta', {'name': 'description'})
    #         if desc_tag and desc_tag.has_attr('content'):
    #             item['desc'] = desc_tag.get('content').strip()
    #
    #         upc_tag = soup.find('h2', {'itemprop': 'gtin13'})
    #         if upc_tag:
    #             item['upc'] = upc_tag.text.strip()
    #
    #         mpn_tag = soup.find('h2', {'itemprop': 'mpn'})
    #         if mpn_tag:
    #             item['mpn'] = mpn_tag.text.strip()
    #
    #         brand_tag = soup.find('h2', {'itemprop': 'brand'})
    #         if brand_tag:
    #             item['brand'] = brand_tag.text.strip()
    #
    #         model_tag = soup.find('h2', {'itemprop': 'model'})
    #         if model_tag:
    #             item['model'] = model_tag.text.strip()
    #
    #         orig_price_tag = soup.find('span', id='orgPrc')
    #         if orig_price_tag:
    #             price = orig_price_tag.text.strip()
    #             price = re.sub('[^\d\.]', '', price)
    #             item['orig_price'] = price.strip()
    #
    #         sale_price_tag = soup.find('span', id='prcIsum')
    #         if sale_price_tag:
    #             price = sale_price_tag.text.strip()
    #             price = re.sub('[^\d\.]', '', price)
    #             item['sale_price'] = price.strip()
    #
    #         if 'orig_price' not in item and 'sale_price' in item:
    #             item['orig_price'] = item['sale_price']
    #             item['sale_price'] = ''
    #
    #         item_condition_tag = soup.find('div', {'itemprop': 'itemCondition'})
    #         if item_condition_tag:
    #             item['condition'] = item_condition_tag.text.replace('(see details)', '').strip()
    #
    #         shipping_cost_tag = soup.find('span', id='fshippingCost')
    #         if shipping_cost_tag:
    #             item['shipping_cost'] = shipping_cost_tag.text.strip()
    #
    #         category_string_tag = soup.find('a', class_='scnd')
    #         if category_string_tag:
    #             item['category_string'] = category_string_tag.text.strip()
    #
    #         # item['keyword'] = self.__current_keyword
    #         self.__logger.info('Data: {}'.format(item))
    #         self.__write_data(item)
    #
    #         try:
    #             self.__lock.acquire()
    #             self.__url_cache.append(url)
    #         except Exception as ex:
    #             self.__logger.error('Error grab details page 2: {}'.format(ex))
    #         finally:
    #             self.__lock.release()
    #
    #     except Exception as x:
    #         self.__logger.error('Error grab details page: {}'.format(x))
    #         if retry < 3:
    #             sleep_time = random.randint(1, 5)
    #             self.__logger.info(
    #                 'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
    #             time.sleep(sleep_time)
    #             return self.__grab_details(args, retry + 1)

    def __grab_details(self, args, retry=0):
        try:
            url = args[0]
            self.__logger.info('=== Details URL: {} ==='.format(url))

            opener = self.__create_opener()
            response = opener.open(url)
            redirected_url = response.url
            data = response.read()
            try:
                data = gzip.decompress(data).decode('utf-8', 'ignore')
            except Exception as e:
                data = data.decode('utf-8', 'ignore')

            if not data:
                return

            soup = BeautifulSoup(data, 'html5lib')
            if not soup:
                return

            item = {'url': redirected_url}

            pid = urllib.parse.urlparse(url).path.split('/')[-1]
            item['product_id'] = pid

            if not pid or len(pid) == '':
                pid_tag = soup.find('a', {'data-destination': 'email'})
                if pid_tag and pid_tag.has_attr('data-itemid'):
                    item['product_id'] = pid_tag.get('data-itemid')

            title_tag = soup.find('h1', id='itemTitle')
            if title_tag:
                item['item_title'] = title_tag.text.strip()

            if 'item_title' not in item:
                title_tag = soup.find('h1', class_='product-title')
                if title_tag:
                    item['item_title'] = title_tag.text.strip()

            desc_tag = soup.find('meta', {'name': 'description'})
            if desc_tag and desc_tag.has_attr('content'):
                item['desc'] = desc_tag.get('content').strip()

            images = re.search(r'\"imgArr\"\s*?\:\s*?(\[[^\]]*?\])', data, re.MULTILINE)
            if images:
                img_array = images.group(1)
                try:
                    img_array = urllib.parse.unquote_plus(img_array.encode('utf-8').decode('unicode-escape', 'ignore'))
                    json_data = json.loads(img_array)
                    if json_data:
                        for json_img in json_data:
                            if 'maxImageUrl' in json_img:
                                item['img_src'] = json_img['maxImageUrl']
                                break
                except Exception as ex:
                    self.__logger.error('Error parsing large image: {}'.format(ex))

            if 'img_src' not in item:
                img_spans = soup.find_all('span', class_='cc-image-component')
                if img_spans and len(img_spans) > 0:
                    imgs = []
                    for img_span in img_spans:
                        img_tag = img_span.find('img')
                        if img_tag and img_tag.has_attr('data-originalimg'):
                            img_src = img_tag.get('data-originalimg').strip()
                            if str(img_src).endswith('.jpg'):
                                imgs.append(img_src.strip())
                    item['img_src'] = '; '.join(imgs)

            if 'img_src' not in item:
                img_src_tag = soup.find('img', id='icImg')
                if img_src_tag:
                    item['img_src'] = img_src_tag.get('src').strip()

            mpn_tag = soup.find('h2', {'itemprop': 'mpn'})
            if mpn_tag:
                item['mpn'] = mpn_tag.text.strip()

            model_tag = soup.find('h2', {'itemprop': 'model'})
            if model_tag:
                item['model'] = model_tag.text.strip()

            upc_tag = soup.find('h2', {'itemprop': 'gtin13'})
            if upc_tag:
                item['upc'] = upc_tag.text.strip()

            brand_tag = soup.find('h2', {'itemprop': 'brand'})
            if brand_tag:
                item['brand'] = brand_tag.text.strip()

            brand_tags = soup.find_all('td', class_='attrLabels')
            if brand_tags and len(brand_tags) > 0:
                for b_tag in brand_tags:
                    if 'manufacturer part number' in b_tag.text.strip().lower():
                        text_tag = b_tag.find_next_sibling('td')
                        if text_tag:
                            item['mpn'] = text_tag.text.strip()

                    if 'brand' not in item and 'brand' in b_tag.text.strip().lower():
                        text_tag = b_tag.find_next_sibling('td')
                        if text_tag:
                            item['brand'] = text_tag.text.strip()

                    if 'upc' in b_tag.text.strip().lower():
                        text_tag = b_tag.find_next_sibling('td')
                        if text_tag:
                            item['upc'] = text_tag.text.strip()

            spec_rows = soup.find_all('div', class_='spec-row')
            if spec_rows and len(spec_rows) > 0:
                for spec_row in spec_rows:
                    li_list = spec_row.find_all('li')
                    if li_list and len(li_list) > 0:
                        for li in li_list:
                            s_name_tag = li.find('div', class_='s-name')
                            if not s_name_tag:
                                continue

                            s_val_tag = li.find('div', class_='s-value')
                            if not s_val_tag:
                                continue

                            s_name = s_name_tag.text.strip().lower()
                            s_val = s_val_tag.text.strip()

                            if 'brand' in s_name:
                                item['brand'] = s_val

                            if 'mpn' in s_name:
                                item['mpn'] = s_val

                            if 'model' in s_name:
                                item['model'] = s_val

                            if 'upc' in s_name:
                                item['upc'] = s_val

            category_string_tag = soup.find('a', class_='scnd')
            if category_string_tag:
                item['category_string'] = category_string_tag.text.strip()

            if not 'category_string' in item:
                nav_bar = soup.find('div', class_='navigation-bar clearfix')
                if nav_bar:
                    li_list = nav_bar.find_all('li')
                    if li_list:
                        item['category_string'] = ''.join([li.text.strip() for li in li_list])

            sale_price_tag = soup.find('span', id='prcIsum')
            if sale_price_tag:
                price = sale_price_tag.text.strip()
                price = re.sub('[^\d\.]', '', price)
                item['sale_price'] = price.strip()

            orig_price_tag = soup.find('span', id='orgPrc')
            if orig_price_tag:
                price = orig_price_tag.text.strip()
                price = re.sub('[^\d\.]', '', price)
                item['orig_price'] = price.strip()

            if 'orig_price' not in item and 'sale_price' in item:
                item['orig_price'] = item['sale_price']

            if 'sale_price' not in item:
                p_display = soup.find('h2', class_='display-price')
                if p_display:
                    price = p_display.text.strip()
                    price = re.sub('[^\d\.]', '', price)
                    item['sale_price'] = price.strip()

            shipping_cost_tag = soup.find('span', id='fshippingCost')
            if shipping_cost_tag:
                item['shipping_cost'] = shipping_cost_tag.text.strip()

            if 'shipping_cost' not in item:
                p_shipping = soup.find('span', class_='logistics-cost')
                if p_shipping:
                    price = p_shipping.text.strip()
                    price = re.sub('[^\d\.]', '', price)
                    item['shipping_cost'] = price.strip()

            item_condition_tag = soup.find('div', {'itemprop': 'itemCondition'})
            if item_condition_tag:
                item['condition'] = item_condition_tag.text.replace('(see details)', '').strip()

            rp_columns = soup.find_all('td', class_='rpTblColm')
            if rp_columns and len(rp_columns) > 0:
                item['returns'] = rp_columns[0].text.strip()

            store_name_tag = soup.find('span', class_='mbg-nw')
            if store_name_tag:
                item['store_name'] = store_name_tag.text.strip()

            item_highlights = soup.find_all('li', class_='item-highlight')
            if item_highlights:
                for item_highlight in item_highlights:
                    if 'condition' not in item and ' condition' in item_highlight.text:
                        item['condition'] = item_highlight.text.replace(' condition', '').strip()

                    if 'returns' not in item and 'returns' in item_highlight.text.strip():
                        item['returns'] = item_highlight.text.strip()

            if 'store_name' not in item:
                store_div = soup.find('div', class_='seller-persona')
                if store_div:
                    spans = store_div.find_all('span')
                    if spans and len(spans) > 0:
                        for span in spans:
                            if 'Sold by' in span.text:
                                next_span = span.find_next_sibling('span')
                                if next_span:
                                    item['store_name'] = next_span.text.strip()
                                break

            carosuls = soup.find_all('div', class_='theme-header')
            if carosuls and len(carosuls) > 0:
                for carosul in carosuls:
                    if not carosul.has_attr('title'):
                        continue

                    title = carosul.get('title')
                    if 'Brand new' in title:
                        item['bnew_price'] = self.extract_price(title)
                    if 'Refurbished' in title:
                        item['ref_price'] = self.extract_price(title)
                    if 'Open box' in title:
                        item['openbox_price'] = self.extract_price(title)
                    if 'Pre-owned' in title:
                        item['pre_owned_price'] = self.extract_price(title)

            long_desc_section = soup.find('section', class_='product-spectification')
            if long_desc_section:
                spec_rows = long_desc_section.find_all('div', class_='spec-row')
                if spec_rows and len(spec_rows) > 0:
                    descs = []
                    for spec_row in spec_rows:
                        h2 = spec_row.find('h2')
                        if h2 and 'Product Information' in h2.text:
                            ul = spec_row.find('ul')
                            if not ul:
                                continue

                            descs.append(ul.text.strip())
                    item['full_desc'] = '\n'.join(descs)

            self.__logger.info('Data: {}'.format(item))
            self.__write_data(item)

            try:
                self.__lock.acquire()
                self.__url_cache.append(url)
            except Exception as ex:
                self.__logger.error('Error grab details page 2: {}'.format(ex))
            finally:
                self.__lock.release()

        except Exception as x:
            self.__logger.error('Error grab details page: {}'.format(x))
            if retry < 3:
                sleep_time = random.randint(1, 5)
                self.__logger.info(
                    'Script is going to sleep for {} seconds.Retrying...'.format(sleep_time))
                time.sleep(sleep_time)
                return self.__grab_details(args, retry + 1)

    def extract_price(self, price_text):
        try:
            price = price_text.strip()
            price = re.sub(r'[^\d\.]', '', price)
            return price.strip()
        except Exception as x:
            self.__logger.error('Error parse price: {}'.format(x))
        return ''

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.DictWriter(f, fieldnames=self.__field_names, quoting=csv.QUOTE_ALL)
                writer.writerow(row)

                self.__logger.info('Total: {}'.format(self.__total))
                self.__total += 1
        except Exception as x:
            self.__logger.error('Error printing csv output: {}'.format(x))
        finally:
            self.__lock.release()


if __name__ == '__main__':
    # input_file = input('Please specify output file name: ')
    # output_file = input('Please specify output file name: ')

    input_file = 'eBay.EPID2.csv'  # 'Alpesh_ebay.csv'
    output_file = 'eBay.EPID2.output.csv'
    with EbayGrabber(output_file) as spider:
        spider.grab_details_from_file(input_file)
