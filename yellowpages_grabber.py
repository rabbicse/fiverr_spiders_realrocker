import csv
import gzip
import re
import socket
import string
import urllib.request
import urllib.parse
from multiprocessing import Lock, Pool
import time
from bs4 import BeautifulSoup
import mechanicalsoup


class YellowPagesGrabber:
    __lock = Lock()
    __sitemap_url = 'http://www.tours.com/sitemapindex.xml'
    __headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'cookie': 'JSESSIONID=A950858D52F4CB7D9D1295A7A295A822; yellow-guid=32ddeb54-b857-497b-b7cd-c7ac5c3218b8; locationClue=; clue=Doctors; _qst_s=2; _qsst_s=1506350878862; s_cc=true; _hjDonePolls=167768; s_fid=306DCD659A1AD9D5-130840BBADF8AC68; s_sq=telstrassyellowpagesprd%3D%2526c.%2526a.%2526activitymap.%2526page%253DSD%25253ADir%25253AYP%25253ASearch%25253AType%25253ABusiness%252520Listings%2526link%253D2%2526region%253Dsearch-results-page%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253DSD%25253ADir%25253AYP%25253ASearch%25253AType%25253ABusiness%252520Listings%2526pidt%253D1%2526oid%253Dhttps%25253A%25252F%25252Fwww.yellowpages.com.au%25252Fsearch%25252Flistings%25253Fclue%25253DDoctors%252526pageNumber%25253D2%252526referredBy%25253Dwww.yellowpages.%2526ot%253DA'}

    def __init__(self, output_csv):
        self.__output_csv = output_csv

    def __enter__(self):
        cookie_handler = urllib.request.HTTPCookieProcessor()
        opener = urllib.request.build_opener(cookie_handler)
        urllib.request.install_opener(opener)
        socket.setdefaulttimeout(60)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def grab_data(self):
        try:
            url = 'https://www.yellowpages.com.au/search/listings?clue=Doctors&pageNumber=2&referredBy=www.yellowpages.com.au&&eventType=pagination'
            print('=== Main URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            print(soup.prettify())
            # csv_header = ['Name', 'Country', 'Email', 'Website', 'URL']
            # self.__write_data(csv_header)
        except Exception as x:
            print(x)

    def __grab_sitemap(self):
        try:
            print('=== Sitemap URL: {} ==='.format(self.__sitemap_url))
            req = urllib.request.Request(self.__sitemap_url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            sitemaps = soup.find_all('loc')
            for sitemap in sitemaps:
                yield sitemap.text.strip()

        except Exception as x:
            print(x)

    def __grab_sitemap_details(self, url):
        try:
            print('=== Sitemap Details URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            sitemaps = soup.find_all('loc')
            for sitemap in sitemaps:
                tmp_url = sitemap.text.strip()
                if 'tour_operators' not in tmp_url:
                    continue
                # print(tmp_url)
                yield tmp_url

        except Exception as x:
            print(x)

    def __grab_details(self, url):
        try:
            print('=== Main URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = data.decode('utf-8', errors='ignore')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            main_content = soup.find('div', class_='ltbdr')
            if not main_content:
                return

            info_list = main_content.find_all('div')

            name, country, email, web = '', '', '', ''
            for info in info_list:
                # print(info)
                if 'Tour Operator:' in str(info):
                    name = info.text.replace('Tour Operator:', '')
                    name = name.strip()
                if 'COMPANY HQ:' in str(info):
                    country = info.text.replace('COMPANY HQ:', '')
                    country = country.strip()
                if 'Email:' in str(info):
                    email = info.text.replace('Email:', '')
                    email = email.replace('mailto:', '')
                    email = email.strip()
                if 'Website:' in str(info):
                    web = info.text.replace('Website:', '')
                    web = web.strip()

            csv_data = [name, country, email, web, url]
            print(csv_data)
            self.__write_data(csv_data)

            # for article in article_list:
            #     try:
            #         name, link, email = '', '', ''
            #         h3_tag = article.find('h3', {'itemprop': 'name'})
            #         if h3_tag:
            #             name = h3_tag.text.strip()
            #             link_tag = h3_tag.find_parent('a')
            #             if link_tag:
            #                 link = self.__base_url + str(link_tag.get('href')).strip()
            #         email_link = article.find('link', {'itemprop': 'email'})
            #         if email_link:
            #             email = email_link.get('href').strip()
            #             email = re.sub(r'mailto\:', '', email)
            #
            #         csv_data = [name, email, link]
            #         print(csv_data)
            #         self.__write_data(csv_data)
            #     except Exception as ex:
            #         print(ex)
        except Exception as x:
            print(x)

    def pool_data(self, page):
        try:
            url = self.__base_url.format(page)
            print('=== URL: {} ==='.format(url))
            req = urllib.request.Request(url, headers=self.__headers)
            data = urllib.request.urlopen(req).read()
            data = gzip.decompress(data).decode('utf-8')
            if not data:
                return

            soup = BeautifulSoup(data, 'lxml')
            if not soup:
                return

            result_div = soup.find('div', class_='search-results clearfix column')
            if not result_div:
                return

            results = result_div.find_all('div', class_='results-block')
            for result in results:
                try:
                    name, email, url = '', '', ''
                    title_tag = result.find('h3', class_='panel-title')
                    if title_tag:
                        tag = title_tag.find_all('a')[-1]
                        name = tag.text.strip()
                        url = tag.get('href')
                    email_m = re.search(r'<a href="mailto\:([^"]*?)"', str(result), re.MULTILINE)
                    if email_m:
                        email = email_m.group(1)

                    csv_data = [name, email, url]
                    print(csv_data)
                    self.__write_data(csv_data)
                except Exception as ex:
                    print(ex)
        except Exception as x:
            print(x)

    def __write_data(self, row, mode='a+'):
        try:
            self.__lock.acquire()
            with open(self.__output_csv, mode, newline='', encoding='utf-8') as f:
                writer = csv.writer(f)
                writer.writerow(row)
        except Exception as x:
            print('Error printing csv output: ' + str(x))
        finally:
            self.__lock.release()

    @staticmethod
    def __write_category(category):
        try:
            with open('category.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __write_region(category):
        try:
            with open('region.txt', 'a+', encoding='utf-8') as f:
                f.write(category + '\n')
        except Exception as x:
            print('Error write data for {}'.format(category))
            print(x)

    @staticmethod
    def __read_category():
        try:
            categories = []
            with open('category.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    category = line.strip()
                    categories.append(category)
            return categories
        except Exception as x:
            print(x)

    @staticmethod
    def __read_region():
        try:
            regions = []
            with open('region.txt', 'r', encoding='utf-8') as f:
                for line in f:
                    region = line.strip()
                    regions.append(region)
            return regions
        except Exception as x:
            print(x)


if __name__ == '__main__':
    with YellowPagesGrabber('yellow_pages_output.csv') as spider:
        spider.grab_data()
